import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { AppLoading } from 'expo';
import { View, ScrollView, Image, UIManager, LayoutAnimation } from 'react-native';
import { DoubleBounce } from 'react-native-loader';
import { scaleVertical, size, scale } from '../../utils/scale';
import { getHistoryQuiniela } from '../../Store/Services/Socket';
import c1b from '../../assets/cards/1B.png';
import c2b from '../../assets/cards/2B.png';
import c3b from '../../assets/cards/3B.png';
import cc from '../../assets/cards/C.png';
import ccf from '../../assets/cards/CF.png';
import clf from '../../assets/cards/LF.png';
import crf from '../../assets/cards/RF.png';
import cp from '../../assets/cards/P.png';
import css from '../../assets/cards/SS.png';


class MyQuinielas extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      quinielas: false
    };
    const options = {
      title: 'QUINIELAS',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() {
    console.log('his.props.currentActiveGame', this.props.currentActiveGame);
    getHistoryQuiniela(this.props.currentActiveGame)
      .then((quinielas) => {
        this.setState({ quinielas });
      })
      .catch((err) => {
        console.log('error en quinielas', err);
      });
  }

  componentWillUpdate(nextProps, nextState) {
    let animate = false;

    if (this.state.isReady !== nextState.isReady || this.state.quinielas !== nextState.quinielas) {
      animate = true;
    }

    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }

      LayoutAnimation.easeInEaseOut();
    }
  }
  async _cacheResourcesAsync() {
    return Promise.all();
  }

  _getContent() {
    if (this.props.currentActiveGame && this.props.currentActiveGame.finished !== 1 && this.state.quinielas.length !== 0) {
      return this._haveGame();
    }
    if (this.props.currentActiveGame && this.props.currentActiveGame.finished !== 1 && this.state.quinielas.length === 0) {
      return this._notInGame();
    }
    return this._notActiveGame();
  }

  _notActiveGame() {
    return (
      <View style={{ paddingTop: scaleVertical(15) }}>
        <View>
          <View style={{ paddingHorizontal: scaleVertical(20), paddingTop: scaleVertical(30) }}>
            <RkText style={{ textAlign: 'center', fontSize: scaleVertical(25), fontFamily: 'TTPollsBoldItalic' }}> No hay juegos disponibles. </RkText>
          </View>
          <View style={{ paddingHorizontal: scaleVertical(20), paddingVertical: scaleVertical(10) }}>
            <RkText style={{ textAlign: 'center', fontSize: scaleVertical(15), fontFamily: 'Roboto-Regular' }}> Muy pronto publicaremos el próximo juego. </RkText>
          </View>
        </View>
      </View>
    );
  }

  _getPrefig(num) {
    switch (num) {
      case 1:
        return 'r';
      case 2:
        return 'da';
      case 3:
        return 'ra';
      default:
        return 'ta';
    }
  }
  _haveGame() {
    const content = this.state.quinielas.map((quiniela, index) => {
      const bgColor = quiniela.complete ? '#e1e1e1' : '#e9f2fb';
      return (
        <View style={{ paddingVertical: scaleVertical(5) }}>
          <View style={{ backgroundColor: bgColor, paddingVertical: scaleVertical(15) }}>
            {this._headerOfQuiniela(quiniela)}
            {this._getContentOfQuiniela(quiniela)}
            {this._getHeaderOfQuiniela(quiniela)}
          </View>
        </View>
      );
    });
    return content;
  }


  _headerOfQuiniela(quiniela) {
    const prefig = this._getPrefig(quiniela.inning);
    const buttonText = quiniela.complete ? 'FINALIZO' : 'EN JUEGO';
    const buttonStyle = quiniela.complete ? { backgroundColor: '#363636' } : { backgroundColor: '#transparent', borderColor: '#3ab44a', borderWidth: scaleVertical(2) };
    const buttonStyleBase = { width: size.widthMargin / 4, height: size.widthMargin / 9 };
    const buttonTextStyleBase = { fontSize: scaleVertical(9) };
    const buttonTextStyle = quiniela.complete ? { color: '#fff' } : { color: '#3ab44a' };
    const header = (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={{ flex: 1.75, paddingHorizontal: scaleVertical(10), justifyContent: 'center' }}>
          <RkText style={{ fontSize: scaleVertical(20), color: 'black', fontFamily: 'TTPollsBold' }}> {quiniela.inning}{prefig} Quiniela </RkText>
          <RkText style={{ fontSize: scaleVertical(15), color: 'black', fontFamily: 'Roboto-Light' }}> TARJETAS JUGANDO </RkText>
        </View>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <RkButton disabled={true} style={[buttonStyleBase, buttonStyle]}>
            <RkText style={[buttonTextStyleBase, buttonTextStyle]}>
              {buttonText}
            </RkText>
          </RkButton>
        </View>
      </View>
    );
    return header;
  }

  _getContentOfQuiniela(quiniela) {


    const contentCards2 = quiniela.cards.map((cardsArray, colorID) => {
      let bgColor;
      let pointsToWin;

      switch (colorID) {
        case 0:
          bgColor = '#f05a28';
          pointsToWin = 160;
          break;
        case 1:
          bgColor = '#009344';
          pointsToWin = 400;
          break;
        default:
          bgColor = '#26aae0';
          pointsToWin = 800;
          break;
      }
      const Text = cardsArray.length !== 0 ? (<RkText style={{ color: bgColor, paddingTop: scale(15), fontFamily: 'TTPollsBoldItalic', fontSize: scale(15), textAlign: 'center' }} >Podrás ganar  {pointsToWin} puntos por cada carta ganadora.</RkText>) : (<View />);

      const contentCards = cardsArray.map((card, i) => {
        const prefig = card.quantity >= 2 ? 'tarjetas' : 'tarjeta';
        const src = this._getRequire(card.position);

        const cardsInfo = (
          <View key={i} style={{ width: size.widthMargin / 3.2, padding: scaleVertical(4), justifyContent: 'center' }}>
            <View style={{ flex: 1 }}>
              <View style={{ height: size.widthMargin / 2.5, justifyContent: 'center' }}>
                <Image style={{ alignSelf: 'center', resizeMode: 'contain' }} source={src} />
              </View>
              <View style={{ flex: 1 }}>
                <RkText style={{ fontSize: scaleVertical(12), textAlign: 'center' }}> {card.quantity} {prefig} </RkText>
                <RkText style={{ fontSize: scaleVertical(12), textAlign: 'center', color: bgColor }}> {card.name} </RkText>
              </View>
            </View>
          </View>
        );
        return cardsInfo;
      });
      const contentColor = (
        <View style={{ flex: 1 }}>
          {Text}
          <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
            {contentCards}
          </View>
        </View>
      );
      return contentColor;
    });
    const content = (
      <View style={{ flex: 1, flexWrap: 'wrap' }}>
        {contentCards2}
      </View>
    );
    return content;
  }


  _getHeaderOfQuiniela(quiniela) {
    const footer = quiniela.complete ?
      (<View style={{ paddingVertical: scaleVertical(15) }}>
        <RkText style={{ paddingVertical: scaleVertical(5), textAlign: 'center', fontSize: scaleVertical(25), fontFamily: 'TTPollsBoldItalic' }}>{quiniela.winnerCards} tarjetas ganadoras</RkText>
        <RkButton style={{ width: size.widthMargin * 0.8, backgroundColor: '#2e3192', alignSelf: 'center' }}> COMPARTIR </RkButton>
      </View>)
      :
      (<View style={{ paddingVertical: scaleVertical(10), justifyContent: 'space-around' }}>


        <QuinielaModals navigation={this.props.navigation} stilo={3} />


        <View style={{ paddingVertical: scaleVertical(15) }}>
          <RkButton onPress={() => { this.props.navigation.navigate('CalendarMenu'); }} style={{ borderRadius: scaleVertical(5), alignSelf: 'center', backgroundColor: 'transparent', width: size.widthMargin * 0.8, borderWidth: scaleVertical(2), borderColor: '#eb0029' }}>
            <RkText style={{ color: '#000' }}>VER JUEGO EN VIVO</RkText>
          </RkButton>
        </View>
      </View>);
    return footer;
  }

  _getRequire(name) {
    switch (name) {
      case '1B':
        return c1b;
      case '2B':
        return c2b;
      case '3B':
        return c3b;
      case 'C':
        return cc;
      case 'CF':
        return ccf;
      case 'LF':
        return clf;
      case 'RF':
        return crf;
      case 'P':
        return cp;
      default:
        return css;
    }
  }

  _notInGame() {
    return (
      <View style={{ paddingTop: scaleVertical(15) }}>

          <View style={{ paddingHorizontal: scaleVertical(20), paddingVertical: scaleVertical(30) }}>
            <RkText style={{ textAlign: 'center', fontSize: scaleVertical(23), fontFamily: 'TTPollsBoldItalic' }}> Aún no estas jugando en la quiniela. </RkText>
          </View>

          <QuinielaModals navigation={this.props.navigation} stilo={3} />

          <View style={{ paddingVertical: scaleVertical(10) }}>


            <RkButton onPress={() => { this.props.navigation.navigate('CalendarMenu'); }} style={{ borderRadius: scaleVertical(5), alignSelf: 'center', backgroundColor: 'transparent', width: size.widthMargin * 0.8, borderWidth: scaleVertical(2), borderColor: '#eb0029' }}>
              <RkText style={{ fontSize: scaleVertical(18), }}>VER JUEGO EN VIVO</RkText>
            </RkButton>
          </View>

      </View>
    );
  }
  render() {
    if (!this.state.quinielas) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color="#eb0029" />
        </View>
      );
    }

    return (<View style={styles.root} >
      <ScrollView style={styles.root} >
        <View style={{ paddingVertical: scaleVertical(15) }} >
          {this._getContent()}
        </View>
      </ScrollView>
    </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff',
  },
}));




MyQuinielas.propTypes = {
  currentActiveGame: PropTypes.object,
  navigation: PropTypes.object,
};

const mapStateToProps = state => ({
  usuario: state.reducerSesion,
  currentActiveGame: state.reducerGame,
});


export default connect(mapStateToProps)(MyQuinielas);
