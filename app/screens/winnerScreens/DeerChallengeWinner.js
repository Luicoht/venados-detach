import React from 'react';
import PropTypes from 'prop-types';
import { View, ImageBackground, SafeAreaView } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { RkStyleSheet, RkText } from 'react-native-ui-kitten';
import { Asset } from 'expo';
import { DoubleBounce } from 'react-native-loader';
import { connect } from 'react-redux';
import { size, } from '../../utils/scale';
import shutterstock from '../../assets/JPEG/shutterstock_684586789.jpg';
import { formatNumber } from '../../Store/Services/Socket';
import NumbersView from './numbersView';


class DeerChallengeWinner extends React.Component {
  constructor(props) {
    super(props);
    const notification = this.props.navigation.state.params.notification;
    this.state = {
      isReady: false,
      notification: notification
    };
  }

  componentDidMount() {
    this._cacheResourcesAsync();
  }

  getWin() {
    return (
      <Col>
        <RkText style={styles.text}> Ganaste {formatNumber(this.state.notification.reward)} puntos</RkText>
        <RkText style={styles.title}>en Reto Venados</RkText>
      </Col>
    );
  }

  getNumbersCeros() {
    const n = this.state.notification.reward;
    if (n >= 1 && n <= 9) return `00000${n}`;
    if (n >= 10 && n <= 99) return `0000${n}`;
    if (n >= 100 && n <= 999) return `000${n}`;
    if (n >= 1000 && n <= 9999) return `00${n}`;
    if (n >= 10000 && n <= 99999) return `0${n}`;
    if (n >= 100000 && n <= 999999) return `${n}`;
  }

  async _cacheResourcesAsync() {
    const images = [
      shutterstock
    ];
    const cacheImages = images.map((image) => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color="#eb0029" />
        </View>
      );
    }

    const numbers = this.getNumbersCeros();
    const number1 = parseInt(numbers[0], 32) || 0;
    const number2 = parseInt(numbers[1], 32) || 0;
    const number3 = parseInt(numbers[2], 32) || 0;
    const number4 = parseInt(numbers[3], 32) || 0;
    const number5 = parseInt(numbers[4], 32) || 0;
    const number6 = parseInt(numbers[4], 32) || 0;

    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground resizeMode="cover"
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            justifyContent: 'center'
          }} source={shutterstock}
        >
          <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.7)' }}>
            <View style={{ flex: 1 }}>
              <View style={{ flex: 1 }} />
              <View style={{ flex: 2, justifyContent: 'center' }}>
                <Grid style={{}}>
                  <Col>
                    <Row style={{}}>
                      <Col style={{}}>
                        <RkText style={styles.title}>¡Bien jugado Venado!</RkText>
                        <RkText style={styles.text}> {this.state.notification.player} marco {this.state.notification.bet} como lo prognosticate.</RkText>
                      </Col>
                    </Row>
                    <Row style={{ textAlign: 'center' }}>
                      <Col />
                      <Col />
                      <Col>
                        <NumbersView numberSelected={number1} />
                      </Col>
                      <Col>
                        <NumbersView numberSelected={number2} />
                      </Col>
                      <Col>
                        <NumbersView numberSelected={number3} />
                      </Col>
                      <Col>
                        <NumbersView numberSelected={number4} />
                      </Col>
                      <Col>
                        <NumbersView numberSelected={number5} />
                      </Col>
                      <Col>
                        <NumbersView numberSelected={number6} />
                      </Col>
                      <Col />
                      <Col />
                    </Row>
                    <Row style={{}}>
                      {this.getWin()}
                    </Row>
                  </Col>
                </Grid>
              </View>
              <View style={{ flex: 1 }} />
            </View>
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,1)',
    paddingLeft: 5,
    paddingRight: 5
  },
  title: {
    color: 'white',
    fontFamily: 'TTPollsBoldItalic',
    textAlign: 'center',
    fontSize: 28,
    marginBottom: 10
  },
  text: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    textAlign: 'center',
    fontSize: 18,
  },
}));


DeerChallengeWinner.navigationOptions = {
  title: 'INICIO',
  hidden: true
};


DeerChallengeWinner.propTypes = {
  navigation: PropTypes.object,
};

const mapStateToProps = state => ({
  usuario: state.reducerSesion,
  currentActiveGame: state.reducerGame,
});


export default connect(mapStateToProps)(DeerChallengeWinner);
