import React from 'react';
import PropTypes from 'prop-types';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { View, ScrollView, UIManager, LayoutAnimation } from 'react-native';
import { DoubleBounce } from 'react-native-loader';
import { Asset } from 'expo';
import { gamesIPlayed } from '../../Store/Services/Socket';
import logoVenados from '../../assets/images/venados.png';
import { scaleVertical, size } from '../../utils/scale';
import { AsyncImage } from '../../components/AsyncImage';
import { teams } from './teams';

export class MyPreviousGames extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      games: false
    };
    const options = {
      title: 'JUEGOS ANTERIORES',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() {
    this._cacheResourcesAsync();
    gamesIPlayed()
      .then((dates) => {
        this.setState({ games: dates });
      })
      .catch((err) => { console.log('error', err); });
  }

  componentWillUpdate(nextProps, nextState) {
    let animate = false;

    if (this.state.games !== nextState.games) {
      animate = true;
    }

    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }

      LayoutAnimation.easeInEaseOut();
    }
  }
  async _cacheResourcesAsync() {
    const images = [
      logoVenados
    ];
    const cacheImages = images.map((image) => Asset.fromModule(image).downloadAsync());
    return Promise.all(cacheImages);
  }

  _getTeamByName(team) {
    // console.log('_getTeamByName', team);
    for (let i = 0; i < teams.length; i++) {
      const element = teams[i];
      if (element.name === team) return element;
    }
  }

  _getContent() {
    const content = this.state.games.map((game) => {
      const goToGame = (
        <View style={styles.justifyContent} >
          <RkButton style={styles.button} onPress={() => { this.props.navigation.navigate('detailsOfPreviousGamesMenu', { game: game}); }}>
            <RkText style={styles.buttonText}>VER</RkText>
          </RkButton>
        </View>
      );
      const v = this._getTeamByName(game.nickname);
      return (
        <View style={styles.rowContent} >
          <View style={{ backgroundColor: '#f2f2f2', flexDirection: 'row', flex: 1 }} >
            <View style={{ flex: 1.2, justifyContent: 'center', alignItems: 'center' }} >
              <RkText
                style={styles.date}
              > {game.date}</RkText>
              <RkText
                style={styles.place}
              > {game.place}</RkText>
            </View>
            <View style={{ flex: 2, flexDirection: 'row' }} >
              <View style={styles.justifyContent} >
                <AsyncImage
                  style={styles.logo}
                  source={logoVenados}
                  placeholderColor='#f2f2f2' />
              </View>
              <View style={styles.justifyContent} >
                <RkText style={styles.vsText} >VS</RkText>
              </View>
              <View style={styles.justifyContent} >
                <AsyncImage
                  style={styles.logo}
                  source={{ uri: v.logo }}
                  placeholderColor='#f2f2f2' />
              </View>
            </View>
            {goToGame}
          </View>
        </View>
      );
    });
    return content;
  }

  _getNoGames() {
    return (
      <View style={{ paddingTop: scaleVertical(15) }}>
        <View>
          <View style={{ paddingHorizontal: scaleVertical(20), paddingTop: scaleVertical(30) }}>
            <RkText style={{ textAlign: 'center', fontSize: scaleVertical(25), fontFamily: 'TTPollsBoldItalic' }}> No hay juegos disponibles. </RkText>
          </View>
          <View style={{ paddingHorizontal: scaleVertical(20), paddingVertical: scaleVertical(10) }}>
            <RkText style={{ textAlign: 'center', fontSize: scaleVertical(15), fontFamily: 'Roboto-Regular' }}> Cuando participes en Quiniela o Reto Venados aquí aparecera. </RkText>
          </View>
        </View>
      </View>
    );
  }
  render() {
    if (!this.state.games) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color="#eb0029" />
        </View>
      );
    }
    if (this.state.games.length === 0) return this._getNoGames();
    return (
      <View style={styles.root} >
        <ScrollView style={styles.root} >
          {this._getContent()}
        </ScrollView>
      </View>
    );
  }
}

const styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: scaleVertical(10),
    paddingBottom: scaleVertical(20)
  },
  date: {
    color: '#002157',
    fontSize: scaleVertical(12),
    textAlign: 'center',
    textVerticalAlign: 'center'
  },
  place: {
    fontSize: scaleVertical(13),
    textAlign: 'center',
    textVerticalAlign: 'center'
  },
  logo: {
    height: size.width / 7,
    width: size.width / 7,
  },
  button: {
    height: size.width / 10,
    width: size.width / 5.5,
    backgroundColor: '#eb0029',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#fff',
    fontSize: scaleVertical(18),
    textAlign: 'center',
    textVerticalAlign: 'center'
  },
  vsText: {
    fontFamily: 'Roboto-Bold',
    color: '#002157',
    fontSize: scaleVertical(20)
  },
  justifyContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  rowContent: {
    height: size.width / 5,
    width: size.width - scaleVertical(30),
    paddingVertical: scaleVertical(5)
  }
}));


MyPreviousGames.propTypes = {
  navigation: PropTypes.object,
};
