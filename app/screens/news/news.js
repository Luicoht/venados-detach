import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { View, StyleSheet, ScrollView, Image, TouchableOpacity, UIManager, LayoutAnimation, Linking } from 'react-native';

import { RkText, RkCard } from 'react-native-ui-kitten';
import { DoubleBounce } from 'react-native-loader';
import { connect } from 'react-redux';
import { scaleVertical, size, } from '../../utils/scale';

const styles = StyleSheet.create({
  screen: {
    backgroundColor: '#f0f1f5',
    padding: 12,
  },
  buttonIcon: {
    marginRight: 7,
    fontSize: 19.7,
  },
  footer: {
    marginHorizontal: 16,
  },
  avatar: {
    width: 42,
    height: 42,
    borderRadius: 21,
    marginRight: 17,
  },
  dot: {
    fontSize: 6.5,
    color: '#0000008e',
    marginLeft: 2.5,
    marginVertical: 10,
  },
  floating: {
    width: 56,
    height: 56,
    position: 'absolute',
    zIndex: 200,
    right: 16,
    top: 173,
  },
  footerButtons: {
    flexDirection: 'row',
  },
  overlay: {
    justifyContent: 'flex-end',
    paddingVertical: 23,
    paddingHorizontal: 16,
  },
});


class News extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      news: false,
      isReady: false,
      content: ''
    };
    const options = {
      title: 'NOTICIAS',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() {
    const config = {
      method: 'get',
      url: 'https://api.lmp.mx/medios-v1.2/news/maz',
    };

    axios(config).then((response) => {
      const serverResponse = response.data.response;
      this.setState({ news: serverResponse });
      this.setState({ isReady: true });
    }).catch((error) => {
      this.setState({ news: [] });
      this.setState({ isReady: true });
    });
  }


  componentWillUpdate(nextProps, nextState) {
    let animate = false;

    if (this.state.isReady !== nextState.isReady) {
      animate = true;
    }

    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }

      LayoutAnimation.easeInEaseOut();
    }
  }

  async _cacheResourcesAsync() {
    return Promise.all();
  }

  renderContent() {
    return this.state.news.map((route) => ((
      <TouchableOpacity
        key={route.id}
        style={styles.panelMenu}
        activeOpacity={1}
        onPress={() => {
          Linking.openURL(`https://www.lmp.mx/noticia/${route.id}`);
        }}
      >
        <View style={{ paddingBottom: scaleVertical(15) }}>
          <RkCard rkType='shadowed' >
            <View>
              <Image rkCardImg source={{ uri: route.image }} />
            </View>

            <View rkCardHeader>
              <View>
                <RkText rkType='header' style={{ color: '#000', fontFamily: 'Roboto-Black' }}>{route.title}</RkText>
                <RkText rkType='subtitle' style={{ color: '#363636', fontFamily: 'Roboto-Regular' }}>{route.date}</RkText>
              </View>
            </View>


          </RkCard>
        </View>
      </TouchableOpacity>
    )));
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color="#eb0029" />
        </View>
      );
    }

    return (
      <View style={{ flex: 1 }}>
        <ScrollView automaticallyAdjustContentInsets style={styles.screen}>
          {this.renderContent()}
        </ScrollView>
      </View>
    );
  }
}



News.propTypes = {
  navigation: PropTypes.object,
};


const mapStateToProps = state => ({ prop: state.prop });


export default connect(mapStateToProps)(News);
