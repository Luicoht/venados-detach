import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { View, ImageBackground, UIManager, LayoutAnimation } from 'react-native';
import { RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { Asset } from 'expo';
import { DoubleBounce } from 'react-native-loader';
import { scale, scaleVertical, size } from '../utils/scale';
import { AsyncImage } from '../components/AsyncImage';
import shutterstock from '../assets/JPEG/shutterstock_476504581.jpg';
import aguilas from '../assets/images/aguilas.png';
import cañeros from '../assets/images/caneros.png';
import charros from '../assets/images/charros.png';
import mayos from '../assets/images/mayos.png';
import naranjeros from '../assets/images/naranjeros.png';
import tomateros from '../assets/images/tomateros.png';
import yaquis from '../assets/images/yaquis.png';
import venados from '../assets/images/venados.png';


export class Scoreboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      day: this.props.dateArray[2],
      month: this.props.dateArray[1],
      year: this.props.dateArray[0],
      gameDetails: false,
      boxscore: false,
      detailsToImage: false
    };
  }
  componentDidMount() {
    this._cacheResourcesAsync();
    const config = {
      method: 'get',
      url: `https://api.lmp.mx/medios-v1.2/scoreboard/${this.props.dateArray[0]}/${this.props.dateArray[1]}/${this.props.dateArray[2]}`
    };
    axios(config).then((response) => {
      const serverResponse = response.data.response;
      for (let i = 0; i < serverResponse.length; i++) {
        const game = serverResponse[i];
        if (game.team_home === 'MAZ' || game.team_away === 'MAZ') {
          this.setState({ gameDetails: game });
          this.setDetailsToImage(game);
          this.getBoxScore();
        }
      }
    }).catch((error) => {
      // console.log('error scoreboard', error);
      this.setDetailsToImage(null);
      // this.setState({ isReady: true });
    });
  }

  componentWillUpdate(nextProps, nextState) {
    let animate = false;
    if (this.state.isReady !== nextState.isReady || this.state.gameDetails !== nextState.gameDetails || this.state.boxscore !== nextState.boxscore || this.state.detailsToImage !== nextState.detailsToImage) {
      animate = true;
    }
    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      LayoutAnimation.easeInEaseOut();
    }
  }

  setDetailsToImage(game) {
    // console.log('setDetailsToImage', game);
    if (game) {
      // console.log('HAY JUEGO');
      const homeLogo = this.getLogo(game.team_home);
      const awayLogo = this.getLogo(game.team_away);
      const data = {
        home: {
          name: game.team_home,
          points: game.homeRuns,
          logo: homeLogo
        },
        away: {
          name: game.team_away,
          points: game.awayRuns,
          logo: awayLogo
        }
      };
      this.setState({ detailsToImage: data });
    } else {
      console.log('NO HAY JUEGO', this.props.game);
    }
  }


  getLogo(team) {
    switch (team) {
      case 'MXC':
        return aguilas;
      case 'NAV':
        return mayos;
      case 'OBR':
        return yaquis;
      case 'CUL':
        return tomateros;
      case 'MOC':
        return cañeros;
      case 'HER':
        return naranjeros;
      case 'JAL':
        return charros;
      default:
        return venados;
    }
  }
  getBoxScore() {
    const config = {
      method: 'get',
      url: `https://api.lmp.mx/medios-v1.2/boxscore/${this.state.gameDetails.id}`
    };
    axios(config).then((response) => {
      // console.log('boxscore response.data', response.data);
      let serverResponse;
      if (response.data.length !== 0) { serverResponse = response.data.response[0]; } else { serverResponse = 'NoGame'; }
      // console.log('boxscore sera serverResponse', serverResponse);
      this.setState({ boxscore: serverResponse });
      this.setState({ isReady: true });
    }).catch((error) => {
      console.log('error boxscore', error);
      this.setState({ isReady: true });
    });
  }

  async _cacheResourcesAsync() {
    const images = [
      shutterstock,
      aguilas,
      cañeros,
      charros,
      mayos,
      naranjeros,
      tomateros,
      yaquis,
      venados
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  _getHeader() {
    const header = ['', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'R', 'H', 'E'];
    const textBase = {
      textAling: 'center',
      fontFamily: 'Roboto-Black',
      fontSize: scaleVertical(15),
      color: '#000'
    };
    const content = header.map((info) => {
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={textBase}>{info}</RkText>
        </View>
      );
    });
    return (
      <View style={{ flex: 1, flexDirection: 'row', padding: scaleVertical(5), borderColor: '#9fa0a3', borderBottomWidth: scaleVertical(2) }}>
        {content}
      </View>
    );
  }


  _getCleanRow(team) {
    let header = [];
    if (team === 'home') header.push(this.state.gameDetails.team_home);
    if (team === 'away') header.push(this.state.gameDetails.team_away);
    header.push('-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');
    const content = header.map((info) => {
      const textBase = (info === this.state.gameDetails.team_away || info === this.state.gameDetails.team_home) ? { fontFamily: 'Roboto-Bold', color: '#eb0029', fontSize: scaleVertical(9) } : {
        textAling: 'center',
        fontFamily: 'Roboto-Black',
        fontSize: scaleVertical(15),
        color: '#000'
      };
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={textBase}>{info}</RkText>
        </View>
      );
    });
    return (
      <View style={{ flex: 1, flexDirection: 'row', padding: scaleVertical(5), borderColor: '#9fa0a3', borderBottomWidth: scaleVertical(2) }}>
        {content}
      </View>
    );
  }

  _getHome() {
    let header = [];
    header.push(this.state.boxscore.home);
    for (let i = 0; i < this.state.boxscore.scores.length; i++) {
      header.push(this.state.boxscore.scores[i].home);
    }
    header.push(this.state.boxscore.homeR);
    header.push(this.state.boxscore.homeH);
    header.push(this.state.boxscore.homeE);

    const content = header.map((info) => {
      const textBase = (info === this.state.boxscore.home) ?
        {
          fontFamily: 'Roboto-Bold',
          color: '#eb0029',
          fontSize: scaleVertical(9)
        } : {
          textAling: 'center',
          fontFamily: 'Roboto-Black',
          fontSize: scaleVertical(15),
          color: '#000'
        };
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={textBase}>{info}</RkText>
        </View>
      );
    });
    return (
      <View style={{ flex: 1, flexDirection: 'row', padding: scaleVertical(5), borderColor: '#9fa0a3', borderBottomWidth: scaleVertical(2) }}>
        {content}
      </View>
    );
  }

  _getAway() {
    let header = [];
    header.push(this.state.boxscore.away);
    for (let i = 0; i < this.state.boxscore.scores.length; i++) {
      header.push(this.state.boxscore.scores[i].away);
    }
    header.push(this.state.boxscore.awayR);
    header.push(this.state.boxscore.awayH);
    header.push(this.state.boxscore.awayE);


    const content = header.map((info) => {
      const textBase = (info === this.state.boxscore.away) ? { fontFamily: 'Roboto-Bold', color: '#eb0029', fontSize: scaleVertical(9) } : {
        textAling: 'center',
        fontFamily: 'Roboto-Black',
        fontSize: scaleVertical(15),
        color: '#000'
      };
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={textBase}>{info}</RkText>
        </View>
      );
    });
    return (
      <View style={{ flex: 1, flexDirection: 'row', padding: scaleVertical(5), borderColor: '#9fa0a3', borderBottomWidth: scaleVertical(2) }}>
        {content}
      </View>
    );
  }


  _getImage() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground style={{ flex: 1 }} source={shutterstock} >
          <View style={{ flex: 1, padding: scaleVertical(10) }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <AsyncImage
                  style={{
                    height: size.width / 5.5,
                    width: size.width / 5.5,
                    alignSelf: 'center'
                  }}
                  source={this.state.detailsToImage.home.logo}
                  placeholderColor='transparent'
                />
                <RkText style={{ color: '#fff', fontSize: scaleVertical(25), textAlign: 'center' }} >{this.state.detailsToImage.home.name}</RkText>
              </View>
              <View style={{ flex: 1, flexDirection: 'row', paddingVertical: scaleVertical(30) }}>
                <View style={{ flex: 1.5 }}>
                  <RkText style={{ color: '#fff', fontSize: scaleVertical(35), textAlign: 'center' }} >{this.state.detailsToImage.home.points}</RkText>
                </View>
                <View style={{ flex: 1.5 }}>
                  <RkText style={{ color: '#fff', fontSize: scaleVertical(35), textAlign: 'center' }} >{this.state.detailsToImage.away.points}</RkText>
                </View>
              </View>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <AsyncImage
                  style={{
                    height: size.width / 5.5,
                    width: size.width / 5.5,
                    alignSelf: 'center'
                  }}
                  source={this.state.detailsToImage.away.logo}
                  placeholderColor='transparent'
                />
                <RkText style={{ color: '#fff', fontSize: scaleVertical(25), textAlign: 'center' }} >{this.state.detailsToImage.away.name}</RkText>
              </View>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }


  _getTableResults() {
    const result = this.state.boxscore === undefined ? (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          {this._getHeader()}
          {this._getCleanRow('home')}
          {this._getCleanRow('away')}
        </View>
      </View>
    ) :
      (
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            {this._getHeader()}
            {this._getHome()}
            {this._getAway()}
          </View>
        </View>
      );
    return result;
  }


  render() {
    if (!this.state.isReady || !this.state.gameDetails || this.state.boxscore === false || !this.state.detailsToImage) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    return (
      <View style={styles.root}>
        <View style={{ flex: 1, padding: scaleVertical(15) }}>
          {this._getImage()}
          <View style={{ flex: 1, top: -scaleVertical(10), paddingHorizontal: scale(5) }}>
            <View style={{ flex: 1, backgroundColor: '#f2f2f2', padding: scaleVertical(5) }}>
              {this._getTableResults()}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
  title: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(20),
    padding: scaleVertical(15),
  },
  date: {
    fontFamily: 'Roboto-Light',
    fontSize: scaleVertical(12),
    textAlign: 'right',
    padding: scaleVertical(10)
  },
  content: {
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(15),
    padding: scaleVertical(15)
  },

  boton: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45),
  },
}));


Scoreboard.navigationOptions = {
  title: 'Detalles'.toUpperCase(),
  hidden: false
};

Scoreboard.propTypes = {
  game: PropTypes.object,
};
