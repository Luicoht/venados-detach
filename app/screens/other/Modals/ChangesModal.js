import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, TouchableOpacity, View, TextInput, Text, DatePickerAndroid, TouchableWithoutFeedback } from 'react-native';
import { Col, Grid } from 'react-native-easy-grid';
import { Icon } from 'native-base';
import { RkButton, RkText } from 'react-native-ui-kitten';
import { isEqual } from 'lodash';
import formatter from '../../../utils/helpers';

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingLeft: 5,
    paddingRight: 5
  },
  title: {
    color: 'black',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10
  },
  text: {
    color: 'black',
    textAlign: 'center'
  },
  textError: {
    color: 'red',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  input: {
    width: 250,
    fontSize: 14,
    letterSpacing: 0.28,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    paddingLeft: 10,
    height: 50,
    zIndex: 2,
    alignSelf: 'center'
  },
  focusedBorder: {
    borderBottomColor: '#0091ea',
    borderBottomWidth: 2,
  },
  errorBorder: {
    borderBottomColor: 'red',
    borderBottomWidth: 2,
  },
  acceptButton: {
    marginTop: 15,
    marginBottom: 5,
    marginLeft: 5,
    backgroundColor: '#007C89',
    borderColor: 'white',
    borderWidth: 1
  },

  outterContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
    paddingLeft: 20,
    paddingRight: 20,
  },
  mainView: {
    backgroundColor: 'white',
    elevation: 5,
  },
};


class ChangesModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.defaultValue || '',
      valueConfirm: '',
      inputType: '',
      message: '',
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps) {
    if (prevProps.visible && !this.props.visible) {
      this.resetInput();
    }
  }


  // ----------------------
  // ------ methods -------
  // ----------------------
  onSave() {
    const { value, message } = this.state;

    if (!message) {
      const setValue = typeof value === 'object' ? value.toString() : value;
      this.props.onClose();
      this.props.onSave(setValue);
    }
  }

  onChangeText(value) {
    let regex = /[^A-ZÑÜ a-zñüáéíóúÁÉÍÓÚ0-9]/g;

    if (this.state.inputType === 'number') {
      regex = /[^0-9]/g;
    }

    if (this.state.inputType === 'email') {
      regex = /[^A-ZÑÜ a-zñüáéíóúÁÉÍÓÚ0-9]@._/g;
    }

    if (!regex.test(value)) {
      this.setState({ value });
    }

    setTimeout(() => { this.validate(); }, 2);
  }

  onChangeTextConfirm(valueConfirm) {
    const regex = /[^A-ZÑÜ a-zñüáéíóúÁÉÍÓÚ0-9]/g;

    if (!regex.test(valueConfirm)) {
      this.setState({ valueConfirm });
    }

    setTimeout(() => { this.validate(); }, 2);
  }

  getDate() {
    const { value } = this.state;
    return `${value.getDate()}/${value.getMonth() + 1}/${value.getFullYear()}`;
  }

  resetInput() {
    this.setState({
      value: '',
      valueConfirm: '',
      message: '',
    });
  }

  async datePicker() {
    try {
      const { action, year, month, day } = await DatePickerAndroid.open({
        // Use `new Date()` for current date.
        // May 25 2020. Month 0 is January.
        date: new Date()
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        // Selected year, month (0-11),
        this.setState({ value: new Date(year, month, day) });
      }
    } catch ({ code, message }) {
      console.warn('Cannot open date picker', message);
    }
  }

  validate() {
    const { value, valueConfirm } = this.state;
    let message = '';
    const menorDe6 = /( *?[0-9a-zA-Z] *?){7,}/g;
    const mayorDe18 = /( *?[0-9a-zA-Z] *?){18,}/g;

    switch (this.props.title) {
      case 'Foto del perfil':
        break;

      case 'Celular':
        if (value.length < 10) {
          message = 'El numero celular no es correcto';
        } else {
          message = '';
        }
        break;

      case 'Correo electrónico':
        if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
          message = 'El correo electrónico es invalido';
        } else {
          message = '';
        }
        break;

      case 'Fecha de nacimiento':
        break;

      case 'Contraseña':
        if (!menorDe6.test(value)) {
          message = 'La contraseña deben ser al menos 7 caracteres';
        } else if (mayorDe18.test(value)) {
          message = 'La contraseña debe ser menor de 18 caracteres';
        } else if (value !== valueConfirm) {
          message = 'Las contraseñas no son iguales';
        } else {
          message = '';
        }
        break;

      default:

        break;
    }

    this.setState({ message });
  }


  // -----------------------------
  // ------ render methods -------
  // -----------------------------
  renderContent() {
    const { placeholder, datepicker } = this.props;
    let dataDetectorTypes;
    let textContentType;
    let maxLength;
    let inputType;

    switch (this.props.title) {
      case 'Foto del perfil':
        inputType = 'file';
        dataDetectorTypes = 'file';
        textContentType = 'URL';
        break;

      case 'Celular':
        inputType = 'number';
        dataDetectorTypes = 'phoneNumber';
        textContentType = 'telephoneNumber';
        maxLength = 10;
        break;

      case 'Correo electrónico':
        inputType = 'email';
        dataDetectorTypes = 'all';
        textContentType = 'emailAddress';
        maxLength = 50;
        break;

      case 'Fecha de nacimiento':
        inputType = 'date';
        dataDetectorTypes = 'calendarEvent';
        textContentType = 'none';
        break;

      case 'Contraseña':
        inputType = 'text';
        dataDetectorTypes = 'all';
        textContentType = 'password';
        maxLength = 18;
        break;

      default:
        inputType = 'file';
        dataDetectorTypes = 'all';
        textContentType = 'none';
        maxLength = 50;
        break;
    }

    this.setState({ inputType });

    if (textContentType === 'password') {
      return (
        <View style={{ marginTop: 40, marginBottom: 40 }}>
          <View>
            <TextInput
              secureTextEntry
              style={styles.input}
              onChangeText={this.onChangeText.bind(this)}
              value={this.state.value}
              placeholder={placeholder}
              dataDetectorTypes={dataDetectorTypes}
              textContentType={textContentType}
              maxLength={maxLength}
            />
          </View>
          <View style={{ marginTop: 40 }}>
            <TextInput
              secureTextEntry
              style={styles.input}
              onChangeText={this.onChangeTextConfirm.bind(this)}
              value={this.state.valueConfirm}
              placeholder='Ingresa de nuevo la contraseña'
              dataDetectorTypes={dataDetectorTypes}
              textContentType={textContentType}
              maxLength={maxLength}
            />
          </View>
        </View>
      );
    }

    if (datepicker) {
      return (
        <View style={{ marginTop: 40, marginBottom: 40 }}>
          <TouchableOpacity style={styles.input} onPress={this.datePicker.bind(this)}>
            <Text>
              { this.state.value ? this.getDate() : 'SELECIONAR FECHA' }
            </Text>
          </TouchableOpacity>
        </View>
      );
    }

    return (
      <View style={{ marginTop: 40, marginBottom: 40 }}>
        <TextInput
          style={styles.input}
          onChangeText={this.onChangeText.bind(this)}
          value={this.state.value}
          placeholder={placeholder}
          dataDetectorTypes={dataDetectorTypes}
          textContentType={textContentType}
          keyboardType={this.props.cellphone ? 'phone-pad' : 'default'}
          maxLength={maxLength}
        />
      </View>
    );
  }

  renderButton() {
    if (!this.state.message) {
      return (
        <View style={{ marginBottom: 15 }}>
          <RkButton rkType="buttonGoogleLogin" onPress={this.onSave.bind(this)}>
            <RkText rkType="h4  DecimaMonoBold" style={{ color: '#fff' }}>GUARDAR</RkText>
          </RkButton>
        </View>
      );
    }

    return null;
  }

  renderMessage() {
    const { message } = this.state;

    if (message) {
      return (
        <View style={{ marginTop: 15, marginBottom: 15 }}>
          <RkText style={styles.textError}>{ message }</RkText>
        </View>
      );
    }
  }

  render() {
    const { visible, title, onClose } = this.props;

    return (
      <Modal
        animationType="fade"
        visible={visible}
        transparent
        onRequestClose={onClose}
        supportedOrientations={['portrait', 'landscape']}
      >
        <TouchableWithoutFeedback onPress={onClose}>
          <Grid style={styles.outterContainer}>
            <Col style={styles.mainView}>
              <TouchableWithoutFeedback>
                <View>
                  <TouchableOpacity onPress={onClose} style={{ padding: 20 }}>
                    <Icon type='FontAwesome' name='times' color='white' style={{ width: 30, height: 30, color: 'red' }} />
                  </TouchableOpacity>

                  <RkText style={styles.title}>Cambiar { title }</RkText>

                  { this.renderContent() }
                  { this.renderMessage() }
                  { this.renderButton() }
                </View>
              </TouchableWithoutFeedback>
            </Col>
          </Grid>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}


ChangesModal.propTypes = {
  visible: PropTypes.bool,
  cellphone: PropTypes.bool,
  title: PropTypes.string,
  placeholder: PropTypes.string,
  defaultValue: PropTypes.any,
  onClose: PropTypes.func,
  onSave: PropTypes.func,
  datepicker: PropTypes.bool,
};


export default ChangesModal;
