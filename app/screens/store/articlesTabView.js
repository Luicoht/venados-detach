import React from 'react';
import PropTypes from 'prop-types';
import { DoubleBounce } from 'react-native-loader';
import { View, ScrollView, TouchableOpacity } from 'react-native';
import { RkStyleSheet, RkText, RkTabView } from 'react-native-ui-kitten';
import { scale, scaleVertical, size } from '../../utils/scale';
import { AsyncImage } from '../../components/AsyncImage';
import { getCategories, getArticlesByCategory } from '../../Store/Services/Socket';


export class ArticlesTabView extends React.Component {
  static navigationOptions = { // eslint-disable-line
    title: 'Tienda'.toUpperCase(),
    hidden: false
  };

  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      categories: false
    };
  }

  componentWillMount() { }
  componentDidMount() {
    getCategories()
      .then((categories) => {
        this.setState({ isReady: true });
        this.setState({ categories: categories });
      })
      .catch((err) => { console.log('error', err); });
  }

  _getArticlesView() {
    getArticlesByCategory({ id: 1 })
      .then((articles) => {
        const solve = articles.map((article) => {
          const item = (
            <View style={{ width: size.widthMargin / 2, apdding: scaleVertical(5) }} >
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('ArticleDetailsMenu', { article: article }); }} style={{ backgroundColor: '#fff' }} >
                <View style={{ flex: 1 }}>
                  <AsyncImage
                    style={{
                      height: size.width / 2,
                      width: size.width / 2,
                      alignSelf: 'center'
                    }}
                    source={{ uri: article.photoURL }}
                    placeholderColor='transparent' />
                </View>
                <View style={{ flex: 1, padding: scaleVertical(5) }}>
                  <RkText style={{ fontSize: scaleVertical(15), fontFamily: 'Roboto-Bold', textAlign: 'auto' }}> {article.name} </RkText>
                  <RkText style={{ fontSize: scaleVertical(18), fontFamily: 'Roboto-Bold', color: '#eb0029', textAlign: 'left', textDecorationLine: 'underline' }}> {article.points} puntos </RkText>
                  <RkText style={{ fontSize: scaleVertical(18), fontFamily: 'Roboto-Bold', color: '#002157', textAlign: 'left', textDecorationLine: 'underline' }}> ${article.price} </RkText>
                </View>
              </TouchableOpacity>
            </View>
          );
          return item;
        });
        const articlesScroll = (
          <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-around' }}>
            {solve}
          </View>
        );
        return articlesScroll;
      })
      .catch((err) => { console.log('error', err); });
  }

  _getTabsViews() {
    const articles = this._getArticlesView();
    const tabs = this.state.categories.map((category) => {
      const tab = (
        <RkTabView.Tab style={{ flex: 1, fontFamily: 'Roboto-Bold', fontSize: scale(12) }} title={category.name}>
          <View style={{ flex: 1 }}>
            <ScrollView style={{ flex: 1 }} >
              {articles}
            </ScrollView>
          </View>
        </RkTabView.Tab>
      );
      return tab;
    });
    return tabs;
  }


  _getCategoriesTabs() {
    const content = (
      <View style={{ flex: 1 }}>
        <RkTabView rkType='standing' maxVisibleTabs={1.7}>
          {this._getTabsViews()}
        </RkTabView>
      </View>
    );
    return content;
  }

  render() {
    if (!this.state.isReady || !this.state.categories) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color="#eb0029" />
        </View>
      );
    }

    return (
      <View style={styles.root}>
        {this._getCategoriesTabs()}
      </View>
    );
  }
}


ArticlesTabView.navigationOptions = {
  title: 'Tienda'.toUpperCase(),
  hidden: false
};


ArticlesTabView.propTypes = {
  navigation: PropTypes.object,
};

const styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: scaleVertical(15)
  },
}));
