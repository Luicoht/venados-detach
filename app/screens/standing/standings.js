import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { Asset } from 'expo';
import { StyleSheet, View, ScrollView, UIManager, LayoutAnimation } from 'react-native';
import { RkTabView, RkText } from 'react-native-ui-kitten';
import { DoubleBounce } from 'react-native-loader';
import { scale, scaleVertical, size } from '../../utils/scale';
import { AsyncImage } from '../../components/AsyncImage';
import aguilas from '../../assets/images/aguilas.png';
import caneros from '../../assets/images/caneros.png';
import charros from '../../assets/images/charros.png';
import mayos from '../../assets/images/mayos.png';
import naranjeros from '../../assets/images/naranjeros.png';
import tomateros from '../../assets/images/tomateros.png';
import venados from '../../assets/images/venados.png';
import yaquis from '../../assets/images/yaquis.png';

const headerVuelta = ['EQUIPO', 'W', 'L', 'PCT'];
const headerGeneral = ['EQUIPO', 'W', 'L', 'PCT', 'STRIKE'];
const headerTotal = ['EQUIPO', '1A', '2A', 'PCT', 'TOTAL'];

class Standings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      data: false,
    };
  }

  componentDidMount() {
    this._cacheResourcesAsync();
    const config = {
      method: 'get',
      url: 'https://api.lmp.mx/medios-v1.2/standings',
    };

    axios(config).then((response) => {
      const serverResponse = response.data.response;
      this.setState({ data: serverResponse });
      this.setState({ isReady: true });
    }).catch((error) => {
      this.setState({ isReady: true });
    });
  }


  componentWillUpdate(nextProps, nextState) {
    let animate = false;
    if (this.state.isReady !== nextState.isReady) {
      animate = true;
    }
    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      LayoutAnimation.easeInEaseOut();
    }
  }


  _getRequire(teamName) {
    switch (teamName) {
      case 'Venados':
        return venados;
      case 'Aguilas':
        return aguilas;
      case 'Naranjeros':
        return naranjeros;
      case 'Yaquis':
        return yaquis;
      case 'Mayos':
        return mayos;
      case 'Cañeros':
        return caneros;
      case 'Tomateros':
        return tomateros;
      default:
        return charros;
    }
  }

  async _cacheResourcesAsync() {
    const images = [
      aguilas,
      caneros,
      charros,
      mayos,
      naranjeros,
      tomateros,
      venados,
      yaquis,
    ];
    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    });
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  _getHeaderVuelta(header) {
    const items = header.map((item, index) => {
      let flexSize = 1;
      if (index === 0) flexSize = 4;
      if (index === 3) flexSize = 2;
      const partial = (
        <View style={{ flex: flexSize, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', textAlign: 'center', textVerticalAlign: 'center', fontSize: scaleVertical(15) }}>{item}</RkText>
        </View>
      );
      return partial;
    });
    const solve = (
      <View style={{ backgroundColor: '#000', height: scaleVertical(35), flexDirection: 'row', justifyContent: 'center' }}>
        {items}
      </View>
    );
    return solve;
  }


  _getHeaderGeneral(header) {
    const items = header.map((item, index) => {
      let flexSize = 1;
      let fontSize = scaleVertical(15);
      if (index === 0) flexSize = 4.5;
      if (index === 3) flexSize = 2;
      if (index === 5) { flexSize = 2; fontSize = scaleVertical(10); }
      const partial = (
        <View style={{ flex: flexSize, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', textAlign: 'center', textVerticalAlign: 'center', fontSize: fontSize }}>{item}</RkText>
        </View>
      );
      return partial;
    });
    const solve = (
      <View style={{ backgroundColor: '#000', height: scaleVertical(35), flexDirection: 'row', justifyContent: 'center' }}>
        {items}
      </View>
    );
    return solve;
  }

  _getHeaderTotal(header) {
    const items = header.map((item, index) => {
      let flexSize = 2;
      if (index === 0) flexSize = 4;
      const partial = (
        <View style={{ flex: flexSize, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', textAlign: 'center', textVerticalAlign: 'center', fontSize: scaleVertical(15) }}>{item}</RkText>
        </View>
      );
      return partial;
    });
    const solve = (
      <View style={{ backgroundColor: '#000', height: scaleVertical(35), flexDirection: 'row', justifyContent: 'center' }}>
        {items}
      </View>
    );
    return solve;
  }


  _getContentVuelta(vuelta) {
    const data = vuelta === 1 ? this.state.data.first : this.state.data.second;
    const items = data.map((item) => {
      const partial = (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', paddingVertical: scaleVertical(10) }}>
            <View style={{ flex: 4, justifyContent: 'center', flexDirection: 'row' }}>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <AsyncImage
                  style={{
                    height: scaleVertical(20),
                    width: scaleVertical(20),
                    alignSelf: 'center'
                  }}
                  source={{ uri: item.image }}
                  placeholderColor='transparent'
                />
              </View>
              <View style={{ flex: 2, justifyContent: 'center' }}>
                <RkText style={{ fontFamily: 'Roboto-Bold', fontSize: scaleVertical(15) }}>{item.name}</RkText>
              </View>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', }}>
              <RkText style={{ fontFamily: 'Roboto-Light' }}>{item.wins}</RkText>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', }}>
              <RkText style={{ fontFamily: 'Roboto-Light' }}>{item.losses}</RkText>
            </View>
            <View style={{ flex: 2, justifyContent: 'center', flexDirection: 'row', }}>
              <RkText style={{ fontFamily: 'Roboto-Light' }}>{item.percent}</RkText>
            </View>
          </View>
          <View style={{ backgroundColor: '#dbdbdb', height: scaleVertical(2) }} />
        </View>
      );
      return partial;
    });
    const solve = (
      <View style={{ justifyContent: 'center' }}>
        {items}
      </View>
    );
    return solve;
  }

  _getContenGeneral() {
    const items = this.state.data.general.map((item) => {
      const partial = (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', paddingVertical: scaleVertical(10) }}>
            <View style={{ flex: 4.5, justifyContent: 'center', flexDirection: 'row' }}>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <AsyncImage
                  style={{
                    height: scaleVertical(20),
                    width: scaleVertical(20),
                    alignSelf: 'center'
                  }}
                  source={{ uri: item.image }}
                  placeholderColor='transparent'
                />
              </View>
              <View style={{ flex: 2, justifyContent: 'center' }}>
                <RkText style={{ fontFamily: 'Roboto-Bold', fontSize: scaleVertical(15) }}>{item.name}</RkText>
              </View>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', }}>
              <RkText style={{ fontFamily: 'Roboto-Light' }}>{item.wins}</RkText>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', }}>
              <RkText style={{ fontFamily: 'Roboto-Light' }}>{item.losses}</RkText>
            </View>
            <View style={{ flex: 2, justifyContent: 'center', flexDirection: 'row', }}>
              <RkText style={{ fontFamily: 'Roboto-Light' }}>{item.percent}</RkText>
            </View>
            <View style={{ flex: 2, justifyContent: 'center', flexDirection: 'row', }}>
              <RkText style={{ fontFamily: 'Roboto-Light' }}>{item.strk}</RkText>
            </View>
          </View>
          <View style={{ backgroundColor: '#dbdbdb', height: scaleVertical(2) }} />
        </View>
      );
      return partial;
    });
    const solve = (
      <View style={{ justifyContent: 'center' }}>
        {items}
      </View>
    );
    return solve;
  }

  _getContenTotal() {
    const items = this.state.data.points.map((item) => {
      const partial = (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', paddingVertical: scaleVertical(10) }}>
            <View style={{ flex: 4, justifyContent: 'center', flexDirection: 'row' }}>

              <View style={{ flex: 1, justifyContent: 'center' }}>
                <RkText style={{ fontFamily: 'Roboto-Bold', fontSize: scaleVertical(15) }}>{item.team_name}</RkText>
              </View>
            </View>
            <View style={{ flex: 2, justifyContent: 'center', flexDirection: 'row', }}>
              <RkText style={{ fontFamily: 'Roboto-Light' }}>{item.first}</RkText>
            </View>
            <View style={{ flex: 2, justifyContent: 'center', flexDirection: 'row', }}>
              <RkText style={{ fontFamily: 'Roboto-Light' }}>{item.second}</RkText>
            </View>
            <View style={{ flex: 2, justifyContent: 'center', flexDirection: 'row', }}>
              <RkText style={{ fontFamily: 'Roboto-Light' }}>{item.percent}</RkText>
            </View>
            <View style={{ flex: 2, justifyContent: 'center', flexDirection: 'row', }}>
              <RkText style={{ fontFamily: 'Roboto-Light' }}>{item.total}</RkText>
            </View>
          </View>
          <View style={{ backgroundColor: '#dbdbdb', height: scaleVertical(2) }} />
        </View>
      );
      return partial;
    });
    const solve = (
      <View style={{ justifyContent: 'center' }}>
        {items}
      </View>
    );
    return solve;
  }

  render() {
    if (!this.state.isReady || !this.state.data) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    return (
      <ScrollView style={styles.root}>
        <RkTabView onTabChanged={(index) => { this.setState({ root: index }); }} maxVisibleTabs={1.5} rkType='standing' >
          <RkTabView.Tab style={{ flex: 1, fontFamily: 'Roboto-Bold', fontSize: scale(12) }} title={'Primer vuelta'} >
            <View style={{ backgroundColor: '#fff' }}>
              {this._getHeaderVuelta(headerVuelta)}
              {this._getContentVuelta(1)}
            </View>
          </RkTabView.Tab>
          <RkTabView.Tab style={{ flex: 1, fontFamily: 'Roboto-Bold', fontSize: scale(12) }} title={'Segunda vuelta'} >
            <View style={{ backgroundColor: '#fff' }}>
              {this._getHeaderVuelta(headerVuelta)}
              {this._getContentVuelta(2)}
            </View>
          </RkTabView.Tab>
          <RkTabView.Tab style={{ flex: 1, fontFamily: 'Roboto-Bold', fontSize: scale(12) }} title={'Standing general'} >
            <View style={{ backgroundColor: '#fff' }}>
              {this._getHeaderGeneral(headerGeneral)}
              {this._getContenGeneral()}
            </View>
          </RkTabView.Tab>
          <RkTabView.Tab style={{ flex: 1, fontFamily: 'Roboto-Bold', fontSize: scale(12) }} title={'Puntos'} >
            <View style={{ backgroundColor: '#fff' }}>
              {this._getHeaderTotal(headerTotal)}
              {this._getContenTotal()}
            </View>
          </RkTabView.Tab>
        </RkTabView>
      </ScrollView>
    );
  }
}

let styles = StyleSheet.create({
  root: {
    flex: 1,
    paddingTop: scaleVertical(15),
    paddingHorizontal: scaleVertical(10)
  }
});


const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Standings);
