import React from 'react';
import { View } from 'react-native';
import { RkStyleSheet, RkText } from 'react-native-ui-kitten';
import { DoubleBounce } from 'react-native-loader';
import { Asset } from 'expo';
import { scale, scaleVertical, size } from '../../utils/scale';
import { AsyncImage } from '../../components/AsyncImage';
import { formatNumber } from '../../Store/Services/Socket';
import { communityTopData } from './communityExample';

let superColor = 'white';

export class FeaturedPlayer extends React.Component {
  static navigationOptions = {
    title: 'Comunidad'.toUpperCase(),
    hidden: false
  };

  constructor(props) {
    super(props);
    superColor = this.props.backgroundColor;
    this.state = {
      isReady: false,
    };
  }

  componentDidMount() { }

  _getProfileImageView() {
    const profileImageView = (
      <View style={styles.userImage}>
        <View style={styles.imageCircle} >
          <View style={styles.redCircle} >
            <View style={styles.whiteCircle} >
              <AsyncImage
                style={styles.imageBackground}
                source={{ uri: communityTopData.image }}
                placeholderColor='transparent' />
            </View>
          </View>
        </View>
      </View>
    );
    return profileImageView;
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 6} color='#eb0029' />
        </View>
      );
    }
    return (
      <View style={[styles.root, { backgroundColor: superColor, padding: scaleVertical(15), justifyContent: 'center' }]}>
        <View style={{ borderRadius: scaleVertical(10), backgroundColor: '#002157', flex: 1, paddingVertical: scaleVertical(15), paddingHorizontal: scaleVertical(10) }}>
          <View style={{ flex: 1, paddingHorizontal: scaleVertical(15), paddingVertical: scaleVertical(10), justifyContent: 'space-around' }}>
            <RkText style={{ textAlign: 'center', color: '#fff', fontFamily: 'TTPollsBold', fontSize: scaleVertical(25) }} >Jugador destacado</RkText>
            <RkText style={{ textAlign: 'center', color: '#fff', fontFamily: 'TTPollsBold', fontSize: scaleVertical(20) }} >Temporada 2018-2019</RkText>
          </View>
          <View style={{ flex: 1, paddingHorizontal: scaleVertical(15) }}>
            {this._getProfileImageView()}
          </View>
          <View style={{ flex: 1, paddingHorizontal: scaleVertical(15), paddingVertical: scaleVertical(10), justifyContent: 'space-around' }}>
            <RkText style={{ textAlign: 'center', color: '#fff', fontFamily: 'TTPollsBold', fontSize: scaleVertical(20) }} >{communityTopData.name}</RkText>
            <RkText style={{ textAlign: 'center', color: '#fff', fontFamily: 'TTPollsRegular', fontSize: scaleVertical(15) }} >SCORE:  {formatNumber(communityTopData.score)}</RkText>
          </View>
        </View>
      </View>
    );
  }
}


let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: superColor,
    paddingVertical: scaleVertical(15)
  },
  cardHeaderText: {
    color: '#303191',
    fontSize: scale(30),
    marginTop: scale(20),
    marginLeft: scale(15),
    fontFamily: 'TTPollsBold',
  },
  flag: {
    width: size.width / 7,
    height: size.width / 7,
    alignSelf: 'center'
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: scaleVertical(10),
    borderBottomWidth: scaleVertical(2),
    borderColor: '#dbdbdb'
  },
  placeCircle: {
    width: size.width / 5,
    height: size.width / 5,
    borderRadius: size.width / 10,
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  whiteCirlce: {
    width: size.width / 5.4,
    height: size.width / 5.4,
    borderRadius: size.width / 10.8,
    backgroundColor: '#fff',
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  contentCircle: {
    width: size.width / 5.8,
    height: size.width / 5.8,
    borderRadius: size.width / 11.6,
    backgroundColor: '#fff',
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  userImage: {
    width: size.width / 5,
    height: size.width / 5,
    alignSelf: 'center',
    paddingVertical: scaleVertical(15),
  },
  textName: {
    textAlign: 'center',
    fontFamily: 'Roboto-Bold'
  },
  textScore: {
    textAlign: 'center',
    fontFamily: 'Roboto-Regular',
    color: '#002157',
    fontSize: scaleVertical(20)
  },
  redCircle: {
    width: size.width / 2.8,
    height: size.width / 2.8,
    borderRadius: size.width / 5.6,
    backgroundColor: '#002157',
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  imageCircle: {
    width: size.width / 2.6,
    height: size.width / 2.6,
    borderRadius: size.width / 5.2,
    backgroundColor: '#dbdbdb',
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  imageBackground: {
    width: size.width / 3,
    height: size.width / 3,
    alignSelf: 'center'
  },
  whiteCircle: {
    width: size.width / 3.1,
    height: size.width / 3.1,
    borderRadius: size.width / 6.2,
    alignSelf: 'center',
    overflow: 'hidden',
  },
}));
