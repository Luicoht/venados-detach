import React from 'react';
import { View, ScrollView, Image, ImageBackground, TouchableOpacity, Alert, UIManager, LayoutAnimation } from 'react-native';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { Asset, AppLoading } from 'expo';
import PropTypes from 'prop-types';
import { TextField } from 'react-native-material-textfield';
import Modal from 'react-native-modal';
import { scale, scaleVertical, size } from '../../utils/scale';
import dont from '../../assets/icons/dont.png';
import challengeImage from '../../assets/icons/reto-3.png';
import bonusImage from '../../assets/icons/bonus-3.png';
import { consultBonusBetDeerChallenge, playBonusBetDeerChallenge, formatNumber, getWalletPoints } from '../../Store/Services/Socket';


class PlayBonus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deer: this.props.deer,
      isModalExtraHitVisible: false,
      isModalExtraOutVisible: false,
      isModalInPlayVisible: false,
      pointsToWin: 0,
      pointsToBet: 0,
      bonusInfo: false,
      wallet: false
    };
  }
  componentDidMount() {
    this._getWallet();
  }


  componentWillUpdate(nextProps, nextState) {
    let animate = false;

    if (this.state.isReady !== nextState.isReady) {
      animate = true;
    }

    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }

      LayoutAnimation.easeInEaseOut();
    }
  }


  onChanged(text) {
    let newText = '';
    const numbers = '0123456789';

    for (let i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        newText += text[i];
      } else {
        // your call back function
        console.log('please enter numbers only');
      }
    }
    this.setState({ pointsToBet: newText });
  }

  _getWallet() {
    getWalletPoints()
      .then((wallet) => {
        // console.log('getWalletPoints', wallet);
        this.setState({ wallet });
      })
      .catch((err) => { console.log('error', err); });
  }
  async _cacheResourcesAsync() {
    const images = [
      dont,
      challengeImage,
      bonusImage
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    return Promise.all(cacheImages);
  }

  _consultBonusBetDeerChallenge(event) {
    console.log('consultBonusBetDeerChallenge ', this.state.pointsToBet);
    const sendData = {
      playerID: this.state.deer.playerID,
      points: this.state.pointsToBet,
      event: event
    };
    consultBonusBetDeerChallenge(sendData)
      .then((result) => {
        const points = parseInt(result.gain, 0);
        this.setState({ pointsToWin: points });
      })
      .catch(() => {
        this.setState({
          consultBonusBetDeerChallenge: []
        });
      });
  }

  _toggleModalOutExtra() {
    console.log('_toggleModalOutExtra');
    this.setState({ isModalExtraOutVisible: !this.state.isModalExtraOutVisible });
  }


  _toggleModalIsOnPlay() {
    console.log('_toggleModalIsOnPlay');
    this.setState({ isModalInPlayVisible: !this.state.isModalInPlayVisible });
  }

  _toggleModalHitExtra() {
    console.log('_toggleModalHitExtra');
    this.setState({ isModalExtraHitVisible: !this.state.isModalExtraHitVisible });
  }

  _playExtraDeerChallenge(event) {
    if (this.state.pointsToBet === 0) {
      Alert.alert(
        'Error',
        '¿Cuantos puntos quieres jugar?',
      );
      return;
    }

    if (!this.state.deer) {
      Alert.alert(
        'Error',
        'Ocurrio algo inesperado',
      );
      return;
    }
    const sendData = {
      points: this.state.pointsToBet,
      id: this.state.deer.id
    };
    playBonusBetDeerChallenge(sendData)
      .then((bonusInfo) => {
        const points = this.state.walletPoints - this.state.pointsToBet;
        this.setState({ walletPoints: points });
        console.log('estas jugando bonus', bonusInfo);
        if (event === 1) {
          this._toggleModalHitExtra();
          this._toggleModalIsOnPlay();
        } else {
          this._toggleModalOutExtra();
          this._toggleModalIsOnPlay();
        }
      })
      .catch(() => {
      });
  }


  _rederModalExtraHit() {
    return (
      <Modal isVisible={this.state.isModalExtraHitVisible}>
        <View style={styles.modalTrasparentContent}>
          <View style={{ flex: 1, backgroundColor: '#6dcff6' }}>
            <View style={{ flexDirection: 'row', height: size.width / 8 }}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleModalHitExtra(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground resizeMode="contain" style={styles.dontImageStyle} source={dont} />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5, backgroundColor: 'transparent' }} />
            </View>
            <ScrollView style={{ flex: 1 }}>

              <View style={{ flex: 1 }}>
                <View style={{ height: scaleVertical(80), alignItems: 'center' }}>

                  <Image style={{ flex: 1, resizeMode: 'center', alignSelf: 'center' }} source={bonusImage} />
                </View>
                <View style={{ flex: 1 }}>
                  <RkText style={styles.deerText}>Reto Venados</RkText>
                  <RkText style={styles.deerSubText}>¿Cuántos puntos a que tu jugador anota Homerun?</RkText>
                </View>
                <View style={{ flex: 0.75, flexDirection: 'row' }}>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <RkText style={styles.haveText}>TIENES</RkText>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#2e3192', height: scaleVertical(30) }}>
                      <RkText style={styles.myPointsWhite}>{formatNumber(this.state.wallet.points)}</RkText>
                    </View>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <RkText style={{
                      fontFamily: 'Roboto-Light',
                      fontSize: scaleVertical(14),
                      textAlign: 'center'
                    }}>PUNTOS</RkText>
                  </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }} />
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                      }}>PUNTOS A GANAR</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#2e3192', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{formatNumber(this.state.pointsToWin)}</RkText>
                      </View>
                    </View>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }} />
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, paddingHorizontal: scaleVertical(15) }}>
                    <TextField
                      textColor='black'
                      tintColor='black'
                      baseColor='black'
                      onBlur={() => console.log('onBlur Ingresa puntos')}
                      onFocus={() => console.log('onFocus Ingresa puntos')}
                      label='Ingresa puntos'
                      keyboardType='numeric'
                      onChangeText={(text) => this.onChanged(text)}
                      value={this.state.pointsToBet}
                      error=''
                    />
                  </View>
                  <View style={{ flex: 1, alignItems: 'center', alignSelf: 'center' }}>
                    <RkButton style={styles.botonCalcular} onPress={() => { this._consultBonusBetDeerChallenge(1); }}>
                      <RkText style={{ textAlign: 'center', color: 'black', fontFamily: 'Roboto-Regular', fontSize: scaleVertical(12) }}>CALCULAR</RkText>
                    </RkButton>
                  </View>
                </View>
                <View style={{ flex: 1, paddingVertical: scale(25) }}>
                  <RkButton onPress={() => { this._playExtraDeerChallenge(1); }} style={styles.boton}> JUGAR </RkButton>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }

  _renderModalExtraOut() {
    return (
      <Modal isVisible={this.state.isModalExtraOutVisible}>
        <View style={styles.modalTrasparentContent}>
          <View style={{ flex: 1, backgroundColor: '#002157' }}>
            <View style={{ flexDirection: 'row', height: size.width / 8 }}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleModalOutExtra(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground resizeMode="contain" style={styles.dontImageStyle} source={dont} />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5, backgroundColor: 'transparent' }} />
            </View>
            <ScrollView style={{ flex: 1 }}>
              <View style={{ flex: 1 }}>
                <View style={{ height: scaleVertical(80), alignItems: 'center' }}>
                  <Image style={{ flex: 1, resizeMode: 'center', alignSelf: 'center' }} source={bonusImage} />
                </View>
                <View style={{ flex: 1 }}>
                  <RkText style={{
                    fontFamily: 'TTPollsBoldItalic',
                    fontSize: scaleVertical(28),
                    textAlign: 'center',
                    color: '#fff'
                  }}>Reto Venados</RkText>
                  <RkText style={{
                    fontFamily: 'Roboto-Regular',
                    fontSize: scaleVertical(18),
                    textAlign: 'center',
                    color: '#fff'
                  }}>¿Cuántos puntos a que tu jugador será ponchado?</RkText>
                </View>
                <View style={{ flex: 0.75, flexDirection: 'row' }}>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <RkText style={{
                      fontFamily: 'Roboto-Light',
                      fontSize: scaleVertical(14),
                      textAlign: 'center',
                      color: '#fff'
                    }}>TIENES</RkText>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#6dcff6', height: scaleVertical(30) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Bold',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                        color: '#fff'
                      }}>{formatNumber(this.state.wallet.points)}</RkText>
                    </View>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <RkText style={{
                      fontFamily: 'Roboto-Light',
                      fontSize: scaleVertical(14),
                      textAlign: 'center',
                      color: '#fff'
                    }}>PUNTOS</RkText>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }} />
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                        color: '#fff'
                      }}>PUNTOS A GANAR</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#6dcff6', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{formatNumber(this.state.pointsToWin)}</RkText>
                      </View>
                    </View>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }} />
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, paddingHorizontal: scaleVertical(15) }}>
                    <TextField
                      textColor='#fff'
                      tintColor='#fff'
                      baseColor='#fff'
                      onBlur={() => console.log('onBlur Ingresa puntos')}
                      onFocus={() => console.log('onFocus Ingresa puntos')}
                      label='Ingresa puntos'
                      keyboardType='numeric'
                      onChangeText={(text) => this.onChanged(text)}
                      value={this.state.pointsToBet}
                      error=''
                    />
                  </View>
                  <View style={{ flex: 1, alignItems: 'center', alignSelf: 'center' }}>
                    <RkButton onPress={() => { this._consultBonusBetDeerChallenge(2); }} style={styles.botonCalcular}>
                      <RkText style={{ textAlign: 'center', color: '#fff', fontFamily: 'Roboto-Regular', fontSize: scaleVertical(12) }}>CALCULAR</RkText>
                    </RkButton>
                  </View>
                </View>
                <View style={{ flex: 1, paddingVertical: scale(25) }}>
                  <RkButton onPress={() => { this._playExtraDeerChallenge(2); }} style={styles.boton}> JUGAR </RkButton>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }

  _renderModalIsInPlay() {
    return (
      <Modal isVisible={this.state.isModalInPlayVisible}>
        <View style={styles.modalTrasparentContent}>
          <View style={{ flex: 1, backgroundColor: '#002157' }}>
            <View style={{ flexDirection: 'row', height: size.width / 8 }}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleModalIsOnPlay(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground resizeMode="contain" style={styles.dontImageStyle} source={dont} />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5, backgroundColor: 'transparent' }} />
            </View>
            <ScrollView style={{ flex: 1 }}>
              <View style={{ flex: 1 }}>
                <View style={{ height: scaleVertical(80), alignItems: 'center' }}>
                  <Image style={{ flex: 1, resizeMode: 'center', alignSelf: 'center' }} source={bonusImage} />
                </View>
                <View style={{ flex: 1 }}>
                  <RkText style={{
                    fontFamily: 'TTPollsBoldItalic',
                    fontSize: scaleVertical(28),
                    textAlign: 'center',
                    color: '#fff'
                  }}>Jugando Reto venados</RkText>

                </View>


                <View style={{ flex: 1, paddingVertical: scale(25) }}>
                  <RkButton onPress={() => { this._toggleModalIsOnPlay(); }} style={styles.boton}> ACEPTAR </RkButton>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }

  _press() {
    console.log(this.props.deer);
    this._getWallet();
    if (this.state.deer.event === 1) {
      this._toggleModalHitExtra();
      return;
    }
    this._toggleModalOutExtra();
    return;
  }

  render() {
    if (!this.state.isReady || !this.state.wallet || !this.state.deer) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }

    const buttonStyle = {
      width: size.width - scaleVertical(55),
      alignSelf: 'center',
      height: size.width / 7,
      backgroundColor: this.state.deer.isActive === 1 ? '#eb0029' : '#868686',
      justifyContent: 'center',
    };
    const buttonTextStyle = {
      fontSize: scaleVertical(18),
      color: '#fff',
      textVerticalAlign: 'center',
    };
    const active = this.state.deer.isActive !== 1;
    return (
      <View style={{ flex: 1, paddingVertical: scaleVertical(10), justifyContent: 'center' }}>
        {this._renderModalIsInPlay()}
        {this._renderModalExtraOut()}
        {this._rederModalExtraHit()}
        <RkButton disabled={active} style={buttonStyle} onPress={() => { this._press(); }}>
          <RkText style={buttonTextStyle}>JUGAR BONUS</RkText>
        </RkButton>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff'
  },
  header: {
    fontSize: scaleVertical(32),
    color: '#eb0029',
    fontFamily: 'TTPollsBold'
  },
  headerSection: {
    paddingVertical: scaleVertical(15),
    paddingHorizontal: scaleVertical(10)
  },
  subHeaderSection: {
    paddingVertical: scaleVertical(15),
    paddingHorizontal: scaleVertical(10)
  },
  subHeader: {
    fontSize: scaleVertical(18),
    fontFamily: 'TTPollsBold'
  },
  container: {
    paddingHorizontal: scaleVertical(10),
    flex: 1,
    flexDirection: 'row',
  },
  tableHeaderTextBlack: {
    height: scaleVertical(32),
    paddingVertical: scaleVertical(6),
    color: '#fff',
    fontFamily: 'Roboto-Bold',
    textAlign: 'center',
    fontSize: scaleVertical(14),
    backgroundColor: '#000',
  },
  tableHeaderTextRed: {
    height: scaleVertical(32),
    paddingVertical: scaleVertical(6),
    color: '#fff',
    fontFamily: 'Roboto-Bold',
    textAlign: 'center',
    fontSize: scaleVertical(14),
    backgroundColor: '#eb0029',
  },
  tableContentTextRed: {
    height: scaleVertical(32),
    paddingVertical: scaleVertical(6),
    color: '#fff',
    fontFamily: 'Roboto-Bold',
    textAlign: 'center',
    fontSize: scaleVertical(14),
  },
  tableContentTextBlack: {
    height: scaleVertical(32),
    paddingVertical: scaleVertical(6),
    color: '#fff',
    fontFamily: 'Roboto-Regular',
    textAlign: 'center',
    fontSize: scaleVertical(14),
  },
  tableBorder: {
    borderColor: '#9a9a9a',
    borderBottomWidth: scaleVertical(2),
    height: scaleVertical(40),
    flex: 1,
    flexDirection: 'row',
  },
  tableContentDeer: {
    height: scaleVertical(32),
    backgroundColor: '#eaeaea',
  },
  tableContentDetails: {
    height: scaleVertical(32),
    backgroundColor: '#fff',
  },
  boton: {
    backgroundColor: '#ed0a27',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45),
    paddingVertical: scaleVertical(25)
  },
  botonCalcular: {
    borderRadius: scale(5),
    alignSelf: 'center',
    backgroundColor: 'transparent',
    borderWidth: scaleVertical(2),
    borderColor: '#ed0a27',
    paddingHorizontal: scaleVertical(15)
  },
  headerText: {
    height: scaleVertical(40),
    paddingVertical: scaleVertical(6),
    color: '#eb0029',
    fontFamily: 'Roboto-Bold',
    paddingLeft: scaleVertical(4),
    fontSize: scaleVertical(14),
  },
  positionText: {
    height: scaleVertical(40),
    paddingVertical: scaleVertical(6),
    color: '#000',
    fontFamily: 'Roboto-Regular',
    textAlign: 'center',
    fontSize: scaleVertical(14)
  },
  whiteContainer: {
    flex: 5,
    height: scaleVertical(40),
    backgroundColor: '#e5e5e5'
  },
  grayContainer: {
    borderColor: '#9a9a9a',
    borderBottomWidth: scaleVertical(2),
    height: scaleVertical(40),
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: scaleVertical(5),
  },
  hitText: {
    color: '#fff',
    fontFamily: 'Roboto-Bold',
    fontSize: scaleVertical(12),
    textAlign: 'center',
  },
  outButton: {
    paddingVertical: scaleVertical(5),
    backgroundColor: '#002157',
    width: size.width / 5,
    height: scaleVertical(30)
  },
  buttonText: {
    color: '#fff',
    fontFamily: 'Roboto-Bold',
    fontSize: scaleVertical(12),
    textAlign: 'center',
  },
  goToPlayerLink: {
    flex: 1,
    justifyContent: 'center',
  },
  seeText: {
    fontFamily: 'Roboto-Bold',
    color: '#00aeef',
    fontSize: scaleVertical(12),
    textAlign: 'center',
    textVerticalAlign: 'center'
  },
  fullModalContainerLightBlue: {
    flex: 1,
    paddingHorizontal: scaleVertical(5),
    paddingVertical: scaleVertical(size.height / 12)
  },
  deerText: {
    fontFamily: 'TTPollsBoldItalic',
    fontSize: scaleVertical(28),
    textAlign: 'center'
  },
  deerSubText: {
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(18),
    textAlign: 'center'
  },
  haveText: {
    fontFamily: 'Roboto-Light',
    fontSize: scaleVertical(14),
    textAlign: 'center'
  },
  myPointsWhite: {
    fontFamily: 'Roboto-Bold',
    fontSize: scaleVertical(14),
    textAlign: 'center',
    color: '#fff'
  },


  modalTrasparentContent: {
    flex: 1,
    backgroundColor: 'transparent',
    paddingHorizontal: scaleVertical(5),
    paddingVertical: scaleVertical(20),
    alignContent: 'center'
  },
  dontImageStyle: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  }
}));


PlayBonus.propTypes = {
  player: PropTypes.object,
  deer: PropTypes.object,
};

export default connect()(PlayBonus);
