import React from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  View,
  ScrollView,
  Image,
  Platform,
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { RkStyleSheet, RkText, RkTheme } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { MainRoutes } from '../../config/navigation/routes';
import { authentication } from '../../Store/Services/Firebase';
import { scaleVertical, AppbarHeight, scale } from '../../utils/scale';
import logoPNG from '../../assets/images/logo.png';
import enVivo from '../../assets/icons/en_vivo.png';
// import QuinielaModal from './Modals/QuinielaModal';
// import BuyInStoreModal from './Modals/BuyInStore';


class SideMenu extends React.Component {
  constructor(props) {
    super(props);
    this._navigateAction = this._navigate.bind(this);
  }

  componentDidMount() { }

  _sigOut() {
    authentication.signOut();
  }
  _navigate(route) {
    this.props.navigation.navigate(route.id);
  }

  _renderIcon() {
    return (
      <TouchableOpacity style={{ flex: 1, justifyContent: 'center' }} onPress={() => this._goHome()} activeOpacity={1}>
        <Image
          style={{ height: AppbarHeight, resizeMode: 'contain' }}
          source={logoPNG}
        />
      </TouchableOpacity>
    );
  }

  _renderIconLive(route) {
    if (route.id === 'PlayMenu' && this.props.currentActiveGame && this.props.currentActiveGame.finished !== 1) {
      return (
        <Image
          resizeMode='contain'
          style={[styles.image]}
          source={enVivo}
        />
      );
    }
    return <View />;
  }


  _goHome() {
    // const resetAction = NavigationActions.reset({
    //   index: 0,
    //   actions: [
    //     NavigationActions.navigate({ routeName: 'DashboardMenu' })
    //   ]
    // });

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'DashboardMenu' })],
    });
    this.props.navigation.dispatch(resetAction);
    // this.props.navigation.dispatch(resetAction);
  }


  render() {
    const menu = MainRoutes.map((route) => {
      const row = route.title !== 'Cerrar sessión' ?
        (
          <TouchableOpacity
            style={styles.container}
            key={route.id}
            underlayColor={RkTheme.current.colors.button.underlay}
            activeOpacity={1}
            onPress={() => this._navigateAction(route)}
          >
            <View style={styles.content}>
              <View style={styles.content}>
                <RkText style={styles.textMenu}>
                  {route.title}
                </RkText>
              </View>
              {this._renderIconLive(route)}
            </View>
          </TouchableOpacity>
        ) :
        (
          <TouchableOpacity
            style={styles.container}
            key={route.id}
            underlayColor={RkTheme.current.colors.button.underlay}
            activeOpacity={1}
            onPress={() => this._sigOut()}
          >
            <View style={styles.content}>
              <View style={styles.content}>
                <RkText style={[styles.textMenu, { color: '#eb0029' }]}>
                  {route.title}
                </RkText>
              </View>
            </View>
          </TouchableOpacity>
        );
      return row;
    });
    return (
      <View style={styles.root}>
        {/* <QuinielaModal
        <BuyInStoreModal
          visible={this.state.quinielaModal}
          onClose={() => this.setState({ quinielaModal: false })}
          data={{ position: 'primera base', win: 1255 }}
        /> */}
        <View style={styles.imageContent}>{this._renderIcon()}</View>
        <ScrollView style={styles.divider} showsVerticalScrollIndicator={false}>
          {menu}
        </ScrollView>
        <View style={styles.textInfoContent}>
          <RkText style={[styles.textInfo, { color: '#fff' }]} onPress={() => { this.props.navigation.navigate('AboutMenu'); }}>
            Acerca de Venados
          </RkText>
        </View>
      </View>
    );
  }
}



let styles = RkStyleSheet.create(() => ({
  textMenu: {
    color: '#fff',
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(16)
  },
  textInfo: {
    color: '#fff',
    fontFamily: 'DecimaMono',
    fontSize: scaleVertical(18),
    textAlign: 'center',
  },
  container: {
    height: AppbarHeight,
    paddingHorizontal: 16,
    borderBottomColor: '#fff',
    borderBottomWidth: 0.8
  },
  root: {
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: '#000000',
    flex: 1
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  imageContent: {
    justifyContent: 'center',
    height: scale(80),
  },
  image: {
    alignSelf: 'center',
    resizeMode: 'contain',
    height: scale(50)
  },
  divider: {
    marginLeft: 20,
    marginRight: 20
  },
  textInfoContent: {
    marginTop: Platform.OS === 'ios' ? 30 : 10,
    marginBottom: Platform.OS === 'ios' ? 30 : 10,
  }
}));

SideMenu.propTypes = {
  currentActiveGame: PropTypes.object,
  navigation: PropTypes.object,
};

const mapStateToProps = state => ({
  currentActiveGame: state.reducerGame
});


const mapDispatchToProps = () => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SideMenu);
