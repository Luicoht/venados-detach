
import { authentication } from './Firebase';

export function GetUser() {
  return authentication.currentUser;
}
