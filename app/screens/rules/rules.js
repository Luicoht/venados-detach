import React from 'react';
import { ScrollView } from 'react-native';
import { RkStyleSheet, RkText } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { scaleVertical } from '../../utils/scale';

class Rules extends React.Component {
  constructor(props) {
    super(props);
    const options = {
      title: 'REGLAS',
      hidden: true
    };
    this.props.navigation.setParams({ options });
  }

  render() {
    return (
      <ScrollView style={styles.root}>
        <RkText style={[styles.title, { color: '#eb0029' }]}>Reglas</RkText>
        <RkText style={[styles.titleTwo, { color: 'black' }]}>Quiniela.</RkText>
        <RkText style={[styles.item]}>1.- Para poder iniciar a jugar es necesario que hagas check-in en el estadio y que la aplicación marque juego en vivo.</RkText>
        <RkText style={[styles.item]}>2.- Existen tres tipos de tarjetas, cada una tiene un valor diferente y te da puntos diferentes, puedes seleccionar el número que de tarjetas que quieras de acuerdo al número de puntos que tengas.</RkText>
        <RkText style={[styles.item]}>3.- En Quiniela no se pueden seleccionar jugadores, sino que se asignan aleatoriamente posiciones a cada tarjeta, por lo que es muy probable que si eliges más de una tarjeta puedan tocarte posiciones repetidas.</RkText>
        <RkText style={[styles.item]}>4.- Las posiciones que se asignan a las tarjetas son las posiciones del campo, aunque el juego sea al bat.</RkText>
        <RkText style={[styles.item]}>5.- Los resultados son por entradas, cada entrada hay un ganador y una nueva Quiniela.</RkText>
        <RkText style={[styles.item]}>6.- Gana la posición que anote la primer carrera en cada entrada. {'\n'} En caso de que no se anote ninguna carrera en alguna entrada, se dará como ganador la posición del último out.</RkText>
        <RkText style={[styles.title, { color: '#eb0029' }]}>Reglas</RkText>
        <RkText style={[styles.titleTwo, { color: 'black' }]}>Reto Venados.</RkText>
        <RkText style={[styles.item]}>1.- Para poder iniciar a jugar es necesario que hagas check-in en el estadio y que la aplicación marque juego en vivo.</RkText>
        <RkText style={[styles.item]}>2.- Cada reto podrá jugarse con un jugador a la vez, pero podrás jugar todos los retos que quieras durante el juego.</RkText>
        <RkText style={[styles.item]}>3.- Solo podrás seleccionar jugadores de Venados.</RkText>


        <RkText style={[styles.item]}>4.- Podrás elegir libremente el número de puntos que quieras jugar, y se descontarán inmediatamente de tu bolsa de puntos acumulados.</RkText>
        <RkText style={[styles.item]}>5.- Los puntos a ganar dependen del porcentaje de out o bateo de los jugadores, te recomendamos revisar sus estadísticas para que puedas elegir mejor.</RkText>
        <RkText style={[styles.item]}>6.- Jugar Bonus es adicional al juego normal de Reto Venados, se contemplan como ganancias o pérdidas separadas. Si elegiste previo al Bonus jugar a Hit, podrás jugar Bonus solo a Home Run, y si jugaste a Out podrás jugar Bonus solo a Ponche.</RkText>
      </ScrollView>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    backgroundColor: '#fff',
    flex: 1,
    paddingVertical: scaleVertical(15),
    paddingHorizontal: scaleVertical(15)
  },
  title: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(25),
    paddingVertical: scaleVertical(15),
  },
  titleTwo: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(20),
  },
  item: {
    fontFamily: 'Roboto-Regular',
    paddingVertical: scaleVertical(10),
    fontSize: scaleVertical(15),
    color: '#7a7a7a'
  },
}));


const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Rules);
