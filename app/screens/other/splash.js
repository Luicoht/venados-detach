import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, StatusBar } from 'react-native';
import { RkTheme } from 'react-native-ui-kitten';
import { connect } from 'react-redux';

import { StackActions, NavigationActions } from 'react-navigation';

import { ProgressBar } from '../../components/progressBar';
import { scale, size } from '../../utils/scale';
import { KittenTheme } from './../../config/theme';
import { AsyncImage } from './../../components/AsyncImage';
import venadosRojoImage from '../../assets/images/VenadosRojo.png';

const timeFrame = 500;


class SplashScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      progress: 0
    };
  }

  componentDidMount() {
    StatusBar.setHidden(true, 'none');
    RkTheme.setTheme(KittenTheme);

    this.timer = setInterval(() => {
      if (this.state.progress === 1) {
        clearInterval(this.timer);

        setTimeout(() => {
          StatusBar.setHidden(false, 'slide');
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Home' })],
          });
          this.props.navigation.dispatch(resetAction);
        }, timeFrame);
      } else {
        const random = 3000;
        let progress = this.state.progress + random;

        if (progress > 1) {
          progress = 1;
        }

        this.setState({ progress });
      }
    }, timeFrame);
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <AsyncImage
            style={styles.image}
            source={venadosRojoImage}
            placeholderColor='transparent'
          />
        </View>

        <ProgressBar
          color="#eb0029"
          style={styles.progress}
          progress={this.state.progress} width={scale(320)}
        />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    backgroundColor: '#363636',
    justifyContent: 'space-around',
    flex: 1
  },
  image: {
    resizeMode: 'contain',
    alignSelf: 'center',
    width: size.width - scale(40),
  },
  progress: {
    alignSelf: 'center',
    marginBottom: 35,
    backgroundColor: '#e5e5e5'
  }
});


SplashScreen.propTypes = {
  navigation: PropTypes.object,
};


const mapStateToProps = state => ({
  prop: state.prop
});


export default connect(mapStateToProps)(SplashScreen);
