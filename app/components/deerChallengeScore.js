import React from 'react';
import { View, ScrollView } from 'react-native';
import { RkText, RkStyleSheet, RkTabView } from 'react-native-ui-kitten';
import { DoubleBounce } from 'react-native-loader';
import { Asset } from 'expo';
import { scaleVertical, size } from '../utils/scale';
import { getHistoryDeerChallenge } from '../Store/Services/Socket';

export class DeerChallengeScore extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      game: props.game,
      deerChallenge: false
    };
  }

  componentDidMount() {
    this._cacheResourcesAsync();
    getHistoryDeerChallenge(this.state.game)
      .then((deerChallenge) => {
        this.setState({ deerChallenge });
        // console.log('deerChallenge', deerChallenge);
      })
      .catch((err) => {
        console.log('getHistoryDeerChallenge err', err);
      });
  }

  getDeerChallenge() {
    const content = (
      <View style={{ flex: 1 }}>
        <RkTabView rkType='standing'>
          <RkTabView.Tab title={'Reto Venados'}>
            <ScrollView horizontal>
              {this._getDeerContent()}
            </ScrollView>
          </RkTabView.Tab>
          <RkTabView.Tab title={'Bonus'}>
            <ScrollView horizontal>
              {this._getBonusContent()}
            </ScrollView>
          </RkTabView.Tab>
        </RkTabView>
      </View>
    );
    return content;
  }


  _getDeerContent() {
    if (!this._haveGames()) return this._noGame();
    const content = (
      this.state.deerChallenge.map((game) => {
        const bgColor = game.winSimple === 1 ? '#c8eeff' : '#e1e1e1';
        const event = game.event === 1 ? 'HIT' : 'OUT';
        const winPoints = game.winSimple ? `${game.simplePointsEarned} puntos` : 'Suerte la próxima';
        const solve = (
          <View style={{ flex: 1, backgroundColor: bgColor }}>
            <View style={{ width: size.widthMargin / 2.25, padding: scaleVertical(4), justifyContent: 'center' }}>
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                  <RkText style={{ fontSize: scaleVertical(12), textAlign: 'center' }}> {game.name} </RkText>
                  <RkText style={{ fontSize: scaleVertical(12), textAlign: 'center', color: '#4a4a4c' }}> {game.position} </RkText>
                  <RkText style={{ fontSize: scaleVertical(20), textAlign: 'center', color: '#2e3192' }}> {event} </RkText>

                  <RkText style={{ fontSize: scaleVertical(12), textAlign: 'center', color: '#4a4a4c' }}> {winPoints} </RkText>
                </View>
              </View>
            </View>
          </View>
        );
        return solve;
      })
    );
    return content;
  }
  _haveGames() {
    return this.state.deerChallenge.length !== 0;
  }


  _haveBonus() {
    let solve = false;
    this.state.deerChallenge.map((game) => {
      if (game.bonusBet !== 0) {
        solve = true;
      }
    });
    return solve;
  }

  _getBonusContent() {
    if (!this._haveBonus() || !this._haveGames()) {
      return this._noGame();
    }
    const content = (
      this.state.deerChallenge.map((game) => {
        const bgColor = game.winBonus ? '#c8eeff' : '#e1e1e1';
        const event = game.event === 1 ? 'HOME RUN' : 'PONCHE';
        const winPoints = game.winBonus ? `${game.pointsToWinInBonus} puntos` : 'Suerte la próxima';
        const solve = (
          <View style={{ flex: 1, backgroundColor: bgColor }}>
            <View style={{ width: size.widthMargin / 2.25, padding: scaleVertical(4), justifyContent: 'center' }}>
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                  <RkText style={{ fontSize: scaleVertical(12), textAlign: 'center' }}> {game.name}  </RkText>
                  <RkText style={{ fontSize: scaleVertical(12), textAlign: 'center', color: '#4a4a4c' }}> {game.position}  </RkText>
                  <RkText style={{ fontSize: scaleVertical(20), textAlign: 'center', color: '#2e3192' }}> {event} </RkText>

                  <RkText style={{ fontSize: scaleVertical(12), textAlign: 'center', color: '#4a4a4c' }}> {winPoints}</RkText>
                </View>
              </View>
            </View>
          </View>
        );
        return solve;
      })
    );

    return content;
  }


  _noGame() {
    const noGame = (
      <View style={{ flex: 1, backgroundColor: '#e1e1e1' }}>
        <View style={{ paddingVertical: scaleVertical(30), justifyContent: 'center' }}>
          <View style={{ flex: 1 }}>
            <RkText style={{ fontSize: scaleVertical(20), textAlign: 'center', color: '#2e3192' }}> NO PARTICIPASTE EN RETO VENADOS</RkText>
          </View>
        </View>
      </View>
    );
    return noGame;
  }
  async _cacheResourcesAsync() {
    const images = [
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  render() {
    if (!this.state.isReady || !this.state.deerChallenge) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    const content = this.getDeerChallenge();
    return (
      <View style={styles.root}>
        <View style={{ flex: 1, backgroundColor: '#e1e1e1' }}>
          {content}
        </View>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
}));
