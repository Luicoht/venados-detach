import React from 'react';
import { View, Platform, TouchableOpacity, ImageBackground, } from 'react-native';
import _ from 'lodash';
import { RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { Asset, AppLoading } from 'expo';
import { scale, scaleVertical, size, AppbarHeight } from '../utils/scale';
import games from '../assets/icons/mis-juegos-3.png';

export class MyGamesSubItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }

  _goTo(route) {
    if (this.props.navigation.state.routeName !== route) {
      if (route == 'DashboardMenu') {
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'DashboardMenu' })
          ]
        });
        this.props.navigation.dispatch(resetAction);
      } else {
        this.props.navigation.navigate(route);
      }
    }
  }


  async _cacheResourcesAsync() {
    const images = [
      games,
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    return Promise.all(cacheImages);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }
    return (
      <TouchableOpacity activeOpacity={1} style={styles.subMenuItem} onPress={() => { this._goTo('MyGamesMenu'); }}>
        <View style={styles.subMenuItem}>
          <View style={styles.test2}>
            <ImageBackground
              resizeMode='contain'
              style={styles.bgImage} source={games}
            />
          </View>
          <View >
            <RkText style={styles.subMenuText}>MIS JUEGOS</RkText>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  test2: {
    // height: Platform.OS === 'ios' ? scaleVertical(45) : scaleVertical(50),
    height: Platform.OS === 'ios' ? AppbarHeight - scaleVertical(10) : AppbarHeight - scaleVertical(15)
  },
  subMenuItem: {
    width: size.width / 5,
    height: AppbarHeight + scaleVertical(10),
    left: scale(1),
  },
  subMenuText: {
    color: '#fff',
    fontSize: scaleVertical(8),
    textAlign: 'center',
    textAlignVertical: 'bottom'
  },
  bgImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  }
}));
