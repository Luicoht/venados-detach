
import { authentication, baseDeDatos } from './Firebase';

export function createProfileFromUser(user) {
  const email = user.email ? user.email : user.providerData[0].email;

  const Profile = {
    displayName: user.displayName,
    token: user.token,
    email,
    photoURL: user.photoURL,
    partner: false,
    active: true,
    uidFacebook: user.providerData[0].providerId === 'facebook.com' ? user.providerData[0].uid : '',
    uidGoogle: user.providerData[0].providerId === 'google.com' ? user.providerData[0].uid : '',
    userEmailKey: '',
    FacebookLink: user.providerData[0].providerId === 'facebook.com',
    GoogleLink: user.providerData[0].providerId === 'google.com',
    EmailLink: false,
    facebookPhotoURL: user.providerData[0].providerId === 'facebook.com' ? user.photoURL : '',
    googlePhotoURL: user.providerData[0].providerId === 'google.com' ? user.photoURL : '',
  };

  // console.log('PROFILE SAVE', Profile);
  baseDeDatos.ref('/profiles/').push(Profile);

  baseDeDatos.ref('/profiles/').orderByChild('email').equalTo(email).once('value')
    .then(snapshot => {
      if (snapshot.val() !== undefined && snapshot.val() !== null) {
        const key = Object.keys(snapshot.val())[0];
        createWalletFromSocial(Profile, key);
      }
    });

  return;
}

export function createFacebookUser(user) {
  const email = user.email ? user.email : user.providerData[0].email;

  const User = {
    displayName: user.displayName,
    email,
    photoURL: user.photoURL,
    uid: user.providerData[0].uid,
    provider: 'Facebook'
  };
  baseDeDatos.ref('/facebook-users/').push(User);
  return;
}

export function createGoogleUser(user) {
  const email = user.email ? user.email : user.providerData[0].email;

  const User = {
    displayName: user.displayName,
    email,
    phone: '',
    photoURL: user.photoURL,
    uid: user.providerData[0].uid,
    provider: 'Google'
  };

  baseDeDatos.ref('/google-users/').push(User);
  return;
}

export function createWallet(Profile) {
  const Wallet = {
    displayName: Profile.displayName,
    email: Profile.email,
    uidFacebook: Profile.uidFacebook,
    uidGoogle: Profile.uidGoogle,
    FacebookLink: Profile.FacebookLink,
    GoogleLink: Profile.GoogleLink,
    score: 1000,
    EmailLink: false,
    userEmailKey: ''

  };
  baseDeDatos.ref('/wallet/').push(Wallet);
  return;
}

export function createWalletFromSocial(Profile, key) {
  const Wallet = {
    displayName: Profile.displayName,
    email: Profile.email,
    uidFacebook: Profile.uidFacebook,
    uidGoogle: Profile.uidGoogle,
    userEmailKey: '',
    FacebookLink: Profile.FacebookLink,
    GoogleLink: Profile.GoogleLink,
    score: 1300,
    profile: key
  };
  baseDeDatos.ref('/wallet/').push(Wallet);
  return;
}

export function loginEmail(user, registro) {
  // console.log('*********************+');
  // console.log('loginEmail');
  // console.log('*********************+');
  baseDeDatos.ref('/profiles/').orderByChild('email').equalTo(user.email).once('value')
    .then(snapshot => {
      if (snapshot.val() !== undefined && snapshot.val() !== null) {
        // console.log('YA EXISTE UN PERFIL CON ESTE EMAIL');
        const Profile = Object.values(snapshot.val())[0];
        const ProfileKey = Object.keys(snapshot.val())[0];

        if (!Profile.EmailLink) {
          updateEmailLink(user, Profile, ProfileKey, registro);
        }
      } else {
        // console.log('Es un nuevo usuario ');
        createUserByEmail(user, registro);
      }
    });
}


function updateEmailLink(user, Profile, ProfileKey, registro) {
  // console.log('*********************+');
  // console.log('updateEmailLink');
  // console.log('*********************+');
  if (registro.additionalUserInfo.isNewUser) {
    const User = {
      displayName: user.name,
      email: user.email,
      phone: user.phone,
      photoURL: '',
      provider: 'Email'
    };
    baseDeDatos.ref('/email-users/').push(User);
    // console.log('push 1');
    updateProfileEmailLink(Profile, ProfileKey);
  }
}

function updateProfileEmailLink(Profile, ProfileKey) {
  // console.log('*********************+');
  // console.log('updateProfileEmailLink');
  // console.log('*********************+');
  baseDeDatos.ref('/email-users/').orderByChild('email').equalTo(Profile.email).once('value')
    .then(userEmail => {
      const userEmailKey = Object.keys(userEmail.val())[0];

      Profile.EmailLink = true;
      Profile.userEmailKey = userEmailKey;
      baseDeDatos.ref().child(`/profiles/${ProfileKey}`).update(Profile);

      baseDeDatos.ref('/wallet/').orderByChild('profile').equalTo(ProfileKey).once('value')
        .then(wallet => {
          const walletKey = Object.keys(wallet.val())[0];
          const walletData = Object.values(wallet.val())[0];
          walletData.EmailLink = true;
          walletData.userEmailKey = userEmailKey;
          baseDeDatos.ref().child(`/wallet/${walletKey}`).update(walletData);
        });
    });
}

function createUserByEmail(userData, registro) {
  // console.log('createUserByEmail userData');
  const User = {
    displayName: userData.name,
    email: userData.email,
    phone: userData.phone,
    photoURL: '',
    provider: 'Email'
  };
  baseDeDatos.ref('/email-users/').push(User);
  // console.log('push 2');
  createProfileByEmail(User, registro);
  // createWalletFromEmail(User, registro);

  return;
}

function createProfileByEmail(user, registro) {
  // console.log('*********************+');
  // console.log('createProfileByEmail');
  // console.log('*********************+');
  baseDeDatos.ref('/email-users/').orderByChild('email').equalTo(user.email).once('value')
    .then(userEmail => {
      if (userEmail.val() !== undefined && userEmail.val() !== null) {
        const userEmailKey = Object.keys(userEmail.val())[0];
        const Profile = {
          displayName: user.displayName,
          email: user.email,
          photoURL: user.photoURL,
          partner: false,
          active: true,
          uidFacebook: '',
          uidGoogle: '',
          FacebookLink: false,
          GoogleLink: false,
          facebookPhotoURL: '',
          googlePhotoURL: '',
          userEmailKey,
        };

        baseDeDatos.ref('/profiles/').push(Profile);
      }
    });
}


export function createWalletFromEmail(Profile) {
  // console.log('createWalletFromEmail', Profile);


  baseDeDatos.ref('/profiles/').orderByChild('email').equalTo(Profile.email).once('value')
    .then(profileSnapshot => {
      const UserProfile = Object.values(profileSnapshot.val())[0];
      const profileKey = Object.keys(profileSnapshot.val())[0];


      const Wallet = {
        displayName: UserProfile.displayName,
        email: UserProfile.email,
        uidFacebook: UserProfile.uidFacebook,
        uidGoogle: UserProfile.uidGoogle,
        userEmailKey: UserProfile.userEmailKey,
        FacebookLink: UserProfile.FacebookLink,
        GoogleLink: UserProfile.GoogleLink,
        EmailLink: true,
        score: 1000,
        profile: profileKey
      };
      baseDeDatos.ref('/wallet/').push(Wallet);
    });

  return;
}

export function editDataUser(profile) {
  const user = authentication.currentUser;

  baseDeDatos.ref('/email-users/').orderByChild('email').equalTo(user.email).once('value')
    .then(userEmail => {
      if (userEmail.val() !== undefined && userEmail.val() !== null) {
        const userEmailKey = Object.keys(userEmail.val())[0];
        const Profile = {
          displayName: user.displayName,
          email: user.email,
          photoURL: profile.photoURL || user.photoURL,
          partner: false,
          active: true,
          uidFacebook: '',
          uidGoogle: '',
          FacebookLink: false,
          GoogleLink: false,
          facebookPhotoURL: '',
          googlePhotoURL: '',
          userEmailKey,
          ...profile
        };

        baseDeDatos.ref().child(`/profiles/${user.uid}`).update(Profile)
          .then((res) => console.log('Changed info', res))
          .catch(err => console.log('update profile', err));
      }
    });
}

export function getProfileData(data) {
  const user = authentication.currentUser;

  baseDeDatos.ref(`/profiles/${user.uid}`).once('value')
    .then(prf => {
      if (prf.val() !== undefined && prf.val() !== null) {
        data(prf);
      }
    });
}

export function findProfileByUser(user) {
  // console.log('findProfileByUser', user);
  const provider = user.providerData[0].providerId;
  const email = user.email ? user.email : user.providerData[0].email;

  baseDeDatos.ref('/profiles/').orderByChild('email').equalTo(email).once('value')
    .then(snapshot => {
      if (snapshot.val() === undefined || snapshot.val() == null) {
        if (provider === 'google.com') {
          createGoogleUser(user);
          createProfileFromUser(user);
          return;
        }
        createFacebookUser(user);
        createProfileFromUser(user);
        return;
      }
      const solve = Object.values(snapshot.val())[0];
      const key = Object.keys(snapshot.val())[0];
      // console.log('findProfileByUser solve', solve);

      // console.log('findProfileByUser actualiza el wallet ya existe el perfil o crea la referencia de la red social');
      if (provider === 'google.com') {
        if (!solve.GoogleLink) {
          // console.log('No se ha creado con google');
          createGoogleUser(user);
          updateSocialLinkAndProfileByUser(key, solve, user);
          return;
        }
      } else if (!solve.FacebookLink) {
        // console.log('No se ha creado con facebook', solve.FacebookLink);
        createFacebookUser(user);
        updateSocialLinkAndProfileByUser(key, solve, user);
        return;
      }
    });
}

export function updateSocialLinkAndProfileByUser(key, profile, user) {
  const provider = user.providerData[0].providerId;
  const dataProfile = profile;
  if (provider === 'google.com') {
    if (!profile.GoogleLink) {
      dataProfile.GoogleLink = true;
      dataProfile.displayName = user.displayName;
      dataProfile.googlePhotoURL = user.photoURL;
      dataProfile.photoURL = user.photoURL;
      dataProfile.uidGoogle = user.providerData[0].uid;
      baseDeDatos.ref().child(`/profiles/${key}`).update(profile);
      linkToSocial(profile, key);
      return;
    }
  } else if (!profile.FacebookLink) {
    // console.log('RESET DATA --------->', user);
    dataProfile.FacebookLink = true;
    dataProfile.token = user.token;
    dataProfile.facebookPhotoURL = user.photoURL;
    dataProfile.uidFacebook = user.providerData[0].uid;
    baseDeDatos.ref().child(`/profiles/${key}`).update(profile);
    linkToSocial(profile, key);
    return;
  }
}
export function linkToSocial(profile, key) {
  baseDeDatos.ref('/wallet/').orderByChild('email').equalTo(profile.email).once('value')
    .then(snapshot => {
      const wallet = Object.values(snapshot.val())[0];
      const key = Object.keys(snapshot.val())[0];
      wallet.score += 300;
      wallet.displayName = profile.displayName;
      wallet.uidFacebook = profile.uidFacebook;
      wallet.uidGoogle = profile.uidGoogle;
      wallet.FacebookLink = profile.FacebookLink;
      wallet.GoogleLink = profile.GoogleLink;
      wallet.profile = key;
      baseDeDatos.ref().child(`/wallet/${key}`).update(wallet);
    });
}

export function becomePartner() {
  const email = authentication.currentUser.email ? authentication.currentUser.email : authentication.currentUser.providerData[0].email;
  baseDeDatos.ref('/profiles/').orderByChild('email').equalTo(email).once('value')
    .then(snapshotProfile => {
      if (snapshotProfile.val() !== undefined) {
        const profileKey = Object.keys(snapshotProfile.val())[0];
        const profile = Object.values(snapshotProfile.val())[0];

        if (!profile.partner) {
          profile.partner = true;
          // console.log('profileKey', profileKey);
          // console.log('profile', profile);
          baseDeDatos.ref().child(`/profiles/${profileKey}`).update(profile);
          baseDeDatos.ref('/wallet/').orderByChild('email').equalTo(email).once('value')
            .then(snapshotWallet => {
              if (snapshotWallet.val() !== undefined) {
                const wallet = Object.values(snapshotWallet.val())[0];
                const walletKey = Object.keys(snapshotWallet.val())[0];
                wallet.score += 20000;
                baseDeDatos.ref().child(`/wallet/${walletKey}`).update(wallet);
                return;
              }
              // console.log('error inesperado walletController becomePartner');
            });
        } else {
          // console.log('Ya eres socio venado');
          return;
        }
      }
    });
}
