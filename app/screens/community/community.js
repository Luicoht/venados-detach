import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { TextField } from 'react-native-material-textfield';
import { Asset, AppLoading } from 'expo';
import Modal from 'react-native-modal';
import { scale, scaleVertical, size } from '../../utils/scale';
import { AsyncImage } from '../../components/AsyncImage';
import { getWalletPoints, getComunityPoints } from '../../Store/Services/Socket';
import dont from '../../assets/icons/dont.png';
import backgroundImage from '../../assets/JPEG/shutterstock_212667757.jpg';
import { CommunityCard } from './communityCard';
import { FeaturedPlayer } from './featuredPlayer';
import { ProfileImage } from './profileImage';
import { ScorePlayer } from './scorePlayer';
import InviteAFriend from './inviteAFriend';



class Community extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isReady: false,
      user: '',
      wallet: '',
      isModalInviteFriendsVisible: false,
    };

    const options = {
      title: 'COMUNIDAD',
      hidden: false
    };

    this.props.navigation.setParams({ options });
  }


  componentDidMount() {
    this._cacheResourcesAsync();
    getWalletPoints()
      .then((wallet) => {
        this.setState({ wallet });
      })
      .catch((err) => { console.log('error', err); });

    getComunityPoints({ page: 1 })
      .then((community) => {
        console.log('community', community);
      })
      .catch((err) => { console.log('error', err); });
  }


  async _cacheResourcesAsync() {
    const images = [
      dont,
      backgroundImage
    ];

    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    return Promise.all(cacheImages);
  }

  _toggleIsModalInviteFriendsVisible() { this.setState({ isModalInviteFriendsVisible: !this.state.isModalInviteFriendsVisible }); }

  _getButtonsView() {
    const buttonsView = (
      <View style={styles.btnRow}>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkButton
            rkType="clear"
            onPress={() => { this.props.navigation.navigate('FriendsMenu'); }}
            style={styles.btnFriends}
          >
            <RkText style={{ color: '#fff', textVerticalAlign: 'center' }}>AMIGOS</RkText>
          </RkButton>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkButton rkType='clear' style={styles.btnCommunity}>
            <RkText style={{ color: '#fff', textVerticalAlign: 'center' }}>COMUNIDAD</RkText>
          </RkButton>
        </View>
      </View>
    );

    return buttonsView;
  }

  _getProfileImageView() {
    const profileImageView = (
      <View style={styles.userImage}>
        <View style={styles.imageCircle} >
          <View style={styles.redCircle} >
            <View style={styles.whiteCircle} >
              <AsyncImage
                style={styles.imageBackground}
                source={{ uri: this.props.profile.data.avatarURL }}
                placeholderColor='transparent'
              />
            </View>
          </View>
        </View>
      </View>
    );
    return profileImageView;
  }

  _getProfileScoreView() {
    const score = (
      <View style={styles.infoContent}>
        <RkText style={styles.userName}> {this.state.user.displayName} </RkText>
        <View style={styles.divider} />
        <RkText style={styles.userScore}> SCORE:  {this.state.wallet.points} </RkText>
        <View style={styles.divider} />
      </View>
    );
    return score;
  }

  _getInviteFriendsView() {
    const score = (
      <View style={{ backgroundColor: 'black' }}>
        <View style={{ height: size.width }}>
          <ImageBackground
            resizeMode='cover'
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              justifyContent: 'center'
            }}
            source={backgroundImage}
          />
          <View style={{ height: size.width, backgroundColor: 'rgba(0, 0, 0, 0.7)', paddingHorizontal: scaleVertical(15) }}>
            <View style={{ paddingTop: size.width / 5, }}>
              <RkText style={{ alignSelf: 'center', color: '#fff', textAlign: 'center', fontFamily: 'TTPollsBoldItalic', fontSize: scaleVertical(25) }}>¡Invita a tus amigos a ser socios Venados y gana puntos!</RkText>
            </View>
            <View style={{ paddingVertical: scaleVertical(15) }}>
              <View style={{ paddingVertical: scaleVertical(10) }}>
                <RkButton style={{ width: size.widthMargin, height: scaleVertical(50), backgroundColor: '#eb0029', justifyContent: 'center', alignSelf: 'center' }} onPress={() => { this._toggleIsModalInviteFriendsVisible(); }}>
                  <RkText style={{ color: '#fff', textAlign: 'center', textVerticalAlign: 'center' }}>INGRESAR DATOS </RkText>
                </RkButton>
              </View>
              <View style={{ paddingVertical: scaleVertical(5) }}>
                <RkButton style={{ width: size.widthMargin, height: scaleVertical(50), backgroundColor: '#3b5998', justifyContent: 'center', alignSelf: 'center' }} onPress={() => { this._toggleIsModalInviteFriendsVisible(); }}>
                  <RkText style={{ color: '#fff', textAlign: 'center', textVerticalAlign: 'center' }}>INVITAR VÍA FACEBOOK</RkText>
                </RkButton>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
    return score;
  }

  _renderInviteFriendsModal() {
    const modal = (
      <Modal isVisible={this.state.isModalInviteFriendsVisible}>
        <View style={styles.trasnparentContent}>
          <View style={styles.modalContent}>
            <View style={{ flexDirection: 'row', height: size.width / 8 }}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleIsModalInviteFriendsVisible(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground resizeMode='contain' style={styles.closeImage} source={dont} />
                </TouchableOpacity>
              </View>
              <View style={styles.jumpHeaderModal} />
            </View>
            <ScrollView style={{ flex: 1 }}>
              <View style={styles.contentModal}>
                <View style={{ paddingVertical: scaleVertical(15) }}>
                  <TextField
                    style={styles.textField}
                    textColor='white'
                    tintColor='white'
                    baseColor='white'
                    label='Nombre de tu amigo'
                  />
                  <TextField
                    style={styles.textField}
                    textColor='white'
                    tintColor='white'
                    baseColor='white'
                    label='Número de célular de tu amigo'
                  />
                </View>
                <View style={styles.modalFooter}>
                  <RkButton style={styles.buttonSend}>
                    <RkText style={styles.textSend}> ENVIAR </RkText>
                  </RkButton>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>);
    return modal;
  }


  render() {
    //   <View>
    //   <ModalDropdown options={['option 1', 'option 2']} />
    // </View>

    // let data = [{
    //   value: 'Banana',
    // }, {
    //   value: 'Mango',
    // }, {
    //   value: 'Pear',
    // }];
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }

    return (
      <ScrollView style={styles.root}>
        {this._getButtonsView()}
        <ProfileImage />
        <ScorePlayer complete={false} color={'#ec0029'} />
        <CommunityCard />
        <InviteAFriend />
        <FeaturedPlayer backgroundColor={'#ec0029'} />
      </ScrollView>
    );
  }
}


let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#ec0029',
  },
  btnRow: {
    flexDirection: 'row',
    paddingTop: scaleVertical(15),
    paddingBottom: scaleVertical(20),
  },
  userImage: {
    paddingBottom: scaleVertical(30),
  },
  btnFriends: {
    backgroundColor: '#00a3e3',
    width: (size.width / 2) - scale(45),
    fontSize: scale(40),
    height: scaleVertical(40),
    alignSelf: 'center',
    marginLeft: scale(15),
    justifyContent: 'center'
  },
  btnCommunity: {
    backgroundColor: '#303191',
    width: (size.width / 2) - scale(45),
    fontSize: scale(40),
    height: scaleVertical(40),
    alignSelf: 'center',
    marginRight: scale(15),
    justifyContent: 'center'
  },
  imageBackground: {
    width: size.width / 2.3,
    height: size.width / 2.3,
    alignSelf: 'center'
  },
  whiteCircle: {
    width: size.width / 2.3,
    height: size.width / 2.3,
    borderRadius: size.width / 4.6,
    alignSelf: 'center',
    overflow: 'hidden',
  },
  redCircle: {
    width: size.width / 2.1,
    height: size.width / 2.1,
    borderRadius: size.width / 4.2,
    backgroundColor: '#ec0029',
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  imageCircle: {
    width: size.width / 2,
    height: size.width / 2,
    borderRadius: size.width / 4,
    backgroundColor: 'white',
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  infoContent: {
    marginLeft: scale(15),
    marginRight: scale(15),
  },
  divider: {
    backgroundColor: 'white',
    width: size.width - scale(30),
    height: scale(2),
  },
  dividerBlack: {
    backgroundColor: '#d9d9d9',
    marginLeft: scale(15),
    marginRight: scale(15),
    height: scale(2),
  },
  userName: {
    color: '#fff',
    alignSelf: 'center',
    fontFamily: 'TTPollsBold',
    fontSize: scale(30),
    textAlign: 'center'
  },
  userScore: {
    color: '#fff',
    alignSelf: 'center',
    fontFamily: 'Roboto-Bold',
    fontSize: scale(15),
    marginTop: scale(15),
    marginBottom: scale(15),
  },
  cardHeaderText: {
    color: '#303191',
    fontSize: scale(30),
    marginTop: scale(20),
    marginLeft: scale(15),
    fontFamily: 'TTPollsBold',
  },
  closeImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  },
  trasnparentContent: {
    flex: 1,
    backgroundColor: 'transparent',
    paddingHorizontal: scaleVertical(5),
    paddingVertical: scaleVertical(20),
    alignContent: 'center',
    justifyContent: 'center'
  },
  modalContent: {
    backgroundColor: '#394353',
    height: size.width * 1.25
  },
  jumpHeaderModal: {
    flex: 5,
    backgroundColor: 'transparent'
  },
  contentModal: {
    flex: 1,
    paddingHorizontal: scaleVertical(15)
  },
  textField: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: scale(20),
    fontAlign: 'center'
  },
  modalFooter: {
    flex: 1,
    paddingHorizontal: scaleVertical(15),
    paddingVertical: scaleVertical(20),
  },
  buttonSend: {
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#eb0029',
    width: size.widthMargin - scaleVertical(60)
  },
  textSend: {
    textAlign: 'center',
    textVerticalAlign: 'center',
    color: '#fff',
    fontFamily: 'DecimaMono-Bold'
  }
}));


Community.propTypes = {
  navigation: PropTypes.object,
  profile: PropTypes.object,
};


const mapStateToProps = state => ({
  prop: state.prop,
  usuario: state.reducerSesion,
  profile: state.profileData
});


export default connect(mapStateToProps)(Community);
