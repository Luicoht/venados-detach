import React from 'react';
import { View } from 'react-native';
import { RkStyleSheet } from 'react-native-ui-kitten';
import { scaleVertical, size } from '../../utils/scale';
import { authentication, baseDeDatos } from '../../Store/Services/Firebase';
import { AsyncImage } from '../../components/AsyncImage';
import { getWalletPoints } from '../../Store/Services/Socket';

let superColor;

export class ProfileImage extends React.Component {
  static navigationOptions = {
    title: 'Comunidad'.toUpperCase(),
    hidden: false
  };

  constructor(props) {
    super(props);
    superColor = this.props.color;
    this.state = {
      isReady: false,
      user: '',
    };
  }

  componentWillMount() {
    getWalletPoints()
      .then((wallet) => {
        this.setState({ wallet });
      })
      .catch((err) => { console.log('error', err); });
    baseDeDatos.ref('/profiles').orderByChild('email').equalTo(authentication.currentUser.providerData[0].email)
      .once('value')
      .then(snapshot => {
        this.setState({ user: Object.values(snapshot.val())[0] });
      });
  }

  _getProfileImageView() {
    console.log('PROPS ', this.props );
    const profileImageView = (
      <View style={styles.userImage}>
        <View style={styles.imageCircle} >
          <View style={styles.redCircle} >
            <View style={styles.whiteCircle} >
              <AsyncImage
                style={styles.imageBackground}
                source={{ uri: this.state.user.photoURL }}
                placeholderColor='transparent' />
            </View>
          </View>
        </View>
      </View>
    );
    return profileImageView;
  }

  render() {
    return (
      <View style={styles.root}>
        {this._getProfileImageView()}
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  userImage: {
    paddingBottom: scaleVertical(30),
  },
  imageBackground: {
    width: size.width / 2.3,
    height: size.width / 2.3,
    alignSelf: 'center'
  },
  whiteCircle: {
    width: size.width / 2.3,
    height: size.width / 2.3,
    borderRadius: size.width / 4.6,
    alignSelf: 'center',
    overflow: 'hidden',
  },
  redCircle: {
    width: size.width / 2.1,
    height: size.width / 2.1,
    borderRadius: size.width / 4.2,
    backgroundColor: superColor,
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  imageCircle: {
    width: size.width / 2,
    height: size.width / 2,
    borderRadius: size.width / 4,
    backgroundColor: 'white',
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
}));
