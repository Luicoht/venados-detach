import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Svg } from 'expo';

const arrayNumber = [
  require('../../assets/images/numeros/numeros_01.svg'),
  require('../../assets/images/numeros/numeros_02.svg'),
  require('../../assets/images/numeros/numeros_11.svg'),
  require('../../assets/images/numeros/numeros_12.svg'),
  require('../../assets/images/numeros/numeros_21.svg'),
  require('../../assets/images/numeros/numeros_22.svg'),
  require('../../assets/images/numeros/numeros_31.svg'),
  require('../../assets/images/numeros/numeros_32.svg'),
  require('../../assets/images/numeros/numeros_41.svg'),
  require('../../assets/images/numeros/numeros_42.svg'),
  require('../../assets/images/numeros/numeros_51.svg'),
  require('../../assets/images/numeros/numeros_52.svg'),
  require('../../assets/images/numeros/numeros_61.svg'),
  require('../../assets/images/numeros/numeros_62.svg'),
  require('../../assets/images/numeros/numeros_71.svg'),
  require('../../assets/images/numeros/numeros_72.svg'),
  require('../../assets/images/numeros/numeros_81.svg'),
  require('../../assets/images/numeros/numeros_82.svg'),
  require('../../assets/images/numeros/numeros_91.svg'),
  require('../../assets/images/numeros/numeros_92.svg'),
];


class NumbersView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      consecutive: props.numberSelected * 2,
      time: 0,
      timeLoop: 30,
      timeFinish: 5000,
    };
  }

  componentDidMount() {

  }

  getNumberRandom() {
    return (
      <Svg width='50' height='50' source={arrayNumber[this.state.consecutive]} />
    );
  }

  render() {
    return this.getNumberRandom();
  }
}


NumbersView.propTypes = {
  numberSelected: PropTypes.number,
};


export default NumbersView;
