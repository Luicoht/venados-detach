import { Toast } from 'native-base';
import { Permissions, Notifications } from 'expo';
import { AsyncStorage, Alert } from 'react-native';


export default {
  show(message) {
    Toast.show({
      supportedOrientations: ['portrait', 'landscape'],
      text: message,
      position: 'bottom',
      buttonText: 'Aceptar',
      duration: 5000,
    });
  },

  alert(title, message) {
    Alert.alert(
      title,
      message,
      [{ text: 'Aceptar', onPress: () => {} }],
      { cancelable: false }
    );
  },

  async getToken() {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;

    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }

    if (finalStatus !== 'granted') {
      return null;
    }

    const token = await Notifications.getExpoPushTokenAsync();
    this.startNotificationsListener();
    return token;
  },

  startNotificationsListener() {
    Notifications.addListener((notification) => {
      console.log(' ');
      console.log('-----------------');
      console.log('notification: ', notification);
      console.log('USERID SET : ', notification.data.userID)
      if (notification.data && notification.data.userID) AsyncStorage.setItem('userID', notification.data.userID);
    });
  }
};
