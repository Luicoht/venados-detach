import React, { Component } from 'react';
import { View, StyleSheet, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { createDrawerNavigator, createStackNavigator } from 'react-navigation';
import { authInDevice, getProfile, getCurrentActiveGame } from './Store/Services/Socket';
import { authentication } from './Store/Services/Firebase';
import { actionEstablishSession, actionCloseSession, actionEstablishCurrentGame, actionEstablishProfile } from './Store/ACTIONS';
import { AppRoutes } from './config/navigation/routesBuilder';
import SideMenu from './screens/navigation/sideMenu';
import track from './config/analytics';
import { NonAuthenticatedRoutes } from './screens/NotAuthenticated/NonAuthenticatedRoutes';


function getCurrentRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }

  const route = navigationState.routes[navigationState.index];

  if (route.routes) {
    return getCurrentRouteName(route);
  }

  return route.routeName;
}

const KittenApp = createStackNavigator(
  {
    First: {
      screen: createDrawerNavigator(
        {
          ...AppRoutes
        },
        {
          drawerOpenRoute: 'DrawerOpen',
          drawerCloseRoute: 'DrawerClose',
          drawerToggleRoute: 'DrawerToggle',
          contentComponent: props => <SideMenu {...props} />
        }
      )
    }
  },
  {
    headerMode: 'none'
  }
);


class Guard extends Component {
  componentDidMount() {
    this.props.authentication();
  }


  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />

        {this.props.usuario ?
          <KittenApp
            onNavigationStateChange={(prevState, currentState) => {
              const currentScreen = getCurrentRouteName(currentState);
              const prevScreen = getCurrentRouteName(prevState);
              if (prevScreen !== currentScreen) {
                track(currentScreen);
              }
            }}
          /> : <NonAuthenticatedRoutes />}
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

const mapStateToProps = state => ({
  usuario: state.reducerSesion,
  currentActiveGame: state.reducerGame
});

const mapDispatchToProps = dispatch => ({
  authentication: () => {
    authentication.onAuthStateChanged(usuario => {
      if (usuario) {
        authInDevice().then(() => {
          getProfile().then(profile => {
            dispatch(actionEstablishProfile(profile));
            dispatch(actionEstablishSession(usuario));
          });
        });
      } else {
        dispatch(actionCloseSession());
      }
    });
  },


  currentGame:
    getCurrentActiveGame().then((game) => {
      if (game) dispatch(actionEstablishCurrentGame(game));
      if (!game) dispatch(actionEstablishCurrentGame({ active: 0, finished: 1, inning: 0 }));
    })
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Guard);
