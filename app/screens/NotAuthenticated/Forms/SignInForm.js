import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { RkButton, RkText } from 'react-native-ui-kitten';
import { TextField } from 'react-native-material-textfield';

import { scale } from './../../../utils/scale';

const validate = values => {
  const errors = {};
  if (!values.correo) {
    errors.correo = 'Requerido';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.correo)) {
    errors.correo = 'Correo invalido';
  }

  if (!values.password) {
    errors.password = 'Requerido';
  } else if (values.password.length < 7) {
    errors.password = 'Deben ser al menos 7 caracteres';
  } else if (values.password.length > 18) {
    errors.password = 'Debe ser menor de 18 caracteres';
  }
  return errors;
};

const fieldNombre = props => (
  <View style={{ }}>
    <TextField
      textColor='#000'
      tintColor='#000'
      baseColor='#000'
      labelTextStyle={{ fontSize: scale(18), fontFamily: 'Roboto-Regular' }}
      titleTextStyle={{ fontSize: scale(18), fontFamily: 'Roboto-Regular' }}
      affixTextStyle={{ fontSize: scale(18), fontFamily: 'Roboto-Regular' }}
      errorColor='#eb0029'
      placeholder={props.ph}
      onChangeText={props.input.onChange}
      value={props.input.value}
      keyboardType={props.input.name === 'correo' ? 'email-address' : 'default'}
      autoCapitalize='none'
      secureTextEntry={!!(props.input.name === 'password' || props.input.name === 'confirmacion')}
      onBlur={props.input.onBlur}
      label={props.ph}
      error={props.meta.touched &&
        props.meta.error && props.meta.error ? props.meta.error : ''}
    />
  </View>
);

fieldNombre.propTypes = {
  ph: PropTypes.string,
  input: PropTypes.object,
  meta: PropTypes.object,
};

const SignInForm = props => (
  <View>
    <Field name='correo' component={fieldNombre} ph='Correo electrónico' />
    <Field secureTextEntry name='password' component={fieldNombre} ph='Contraseña' />
    <View style={{}}>
      <RkButton rkType='buttonEmailLogin' style={{ alignSelf: 'center' }} onPress={props.handleSubmit(props.login)} >
        <RkText rkType='h4  DecimaMonoBold' style={{ color: '#fff' }}> INICIAR SESIÓN</RkText>
      </RkButton>
    </View>
  </View>
);

SignInForm.propTypes = {
  handleSubmit: PropTypes.func,
  login: PropTypes.object,
};

export default reduxForm({ form: 'SignInForm', validate })(SignInForm);
