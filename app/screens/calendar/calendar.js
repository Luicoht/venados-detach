import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { Dropdown } from 'react-native-material-dropdown';
import { DoubleBounce } from 'react-native-loader';
import { connect } from 'react-redux';
import { View, ScrollView, UIManager, LayoutAnimation } from 'react-native';
import { RkStyleSheet, RkText } from 'react-native-ui-kitten';
import DatePicker from 'react-native-datepicker';
import { Calendar } from '../../components/calendar';
import { scale, scaleVertical, size } from '../../utils/scale';
import SpinnerModal from '../../components/SpinnerModal';


const dicTeams = [
  {
    name: 'Aguilas',
    abr: 'MXC'
  },
  {
    name: 'Mayos',
    abr: 'NAV'
  },
  {
    name: 'Yaquis',
    abr: 'OBR'
  },
  {
    name: 'Tomateros',
    abr: 'CUL'
  },
  {
    name: 'Cañeros',
    abr: 'MOC'
  },
  {
    name: 'Naranjeros',
    abr: 'HER'
  },
  {
    name: 'Charros',
    abr: 'JAL'
  }
];


class CalendarView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dates: false,
      isReady: false,
      date: false,
      modalVisible: true
    };
    const options = {
      title: 'CALENDARIO',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() {
    const today = new Date();
    const date = {
      year: today.getFullYear(),
      month: today.getMonth() + 1,
      day: today.getDate(),
    };
    this.setState({ date: `${date.year}-${date.month}-${date.day}` });
    this.getGamesByDate(date);
  }


  componentWillUpdate(nextProps, nextState) {
    let animate = false;
    if (this.state.isReady !== nextState.isReady || this.state.dates !== nextState.dates || this.state.date !== nextState.date) {
      animate = true;
    }
    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      LayoutAnimation.easeInEaseOut();
    }
  }


  componentWillUnmount() {
    this.setState({ dates: false });
    this.setState({ date: false });
    this.setState({ isReady: false });
  }

  getGamesByDate(date) {
    this.setState({ dates: [] });
    this.setState({ modalVisible: true });
    const config = {
      method: 'get',
      url: `https://api.lmp.mx/medios-v1.2/calendar/maz/${date.year}/${date.month}`
    };
    axios(config).then((response) => {
      const serverResponse = response.data.response;
      const keys = Object.keys(serverResponse);
      const dates = {};
      for (let i = 0; i < keys.length; i++) {
        const element = serverResponse[keys[i]];
        for (let k = 0; k < element.length; k++) {
          const game = element[k];
          const dateArray = game.start.split('-');
          dates[game.start] = {
            customData: {
              isLocal: game.home,
              place: game.home ? 'CASA' : 'VISITA',
              iWent: false,
              // iWent: i % 2 === 0,
              date: game.start,
              name: game.content,
              year: dateArray[0],
              month: dateArray[1],
              day: dateArray[2],
              abbreviation: this.getAbbreviation(game.content)
            }
          };
        }
      }
      this.setState({ dates });
      this.setState({ isReady: true });
      this.setState({ modalVisible: false });
    }).catch((error) => {
      console.log('Errror ', error);
      this.setState({ isReady: true });
      this.setState({ modalVisible: false });
    });
  }

  getAbbreviation(name) {
    for (let i = 0; i < dicTeams.length; i++) {
      if (name.includes(dicTeams[i].name)) {
        return dicTeams[i].abr;
      }
    }
  }

  render() {
    if (!this.state.isReady || !this.state.date) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }

    return (
      <View style={styles.root}>
        <SpinnerModal
          visible={this.state.modalVisible}
          label="Actualizando datos"
        />
        <ScrollView style={styles.root}>
          <View style={styles.root}>
            <View style={styles.content}>
              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <RkText rkType='h1  TTPollsBold '>Calendario</RkText>
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>


                  <DatePicker
                    androidMode='spinner'
                    style={{ width: scale(150) }}
                    date={this.state.date}
                    mode='date'
                    placeholder='Selecciona una fecha'
                    format='YYYY-MM-DD'
                    confirmBtnText='Confirmar'
                    cancelBtnText='Cancelar'
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0
                      },
                      dateInput: {
                        marginLeft: 36,
                        borderColor: '#eb0029',
                      },
                      dateText: {
                        color: '#eb0029',
                      }
                      // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => {
                      this.setState({ date });
                      const tokens = date.split('-');
                      const req = {
                        year: tokens[0],
                        month: tokens[1]
                      };
                      this.getGamesByDate(req);
                    }}
                  />
                </View>
              </View>
              <View style={{}}>
                <RkText rkType='h5  RobotoLight '>Haz click en el día para ver los resultados</RkText>
              </View>

            </View>

            <Calendar
              current={this.state.date}
              style={{
                borderWidth: 1,
                borderColor: '#dbdbdb',
                paddingTop: scale(15)
              }}
              onMonthChange={(month) => {
                this.setState({ date: month.dateString });
                this.getGamesByDate(month);
              }}
              navigation={this.props.navigation}
              markingType={'custom'}
              markedDates={this.state.dates}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    backgroundColor: '#fff',
    flex: 1,
  },
  content: {
    justifyContent: 'space-around',
    paddingHorizontal: scaleVertical(15),
    paddingVertical: scaleVertical(10),
  }
}));

CalendarView.propTypes = {
  navigation: PropTypes.object,
};



const mapStateToProps = state => ({ prop: state.prop });

export default connect(
  mapStateToProps
)(CalendarView);

