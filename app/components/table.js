// import liraries
import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import { RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { DoubleBounce } from 'react-native-loader';
import { scaleVertical, size } from '../utils/scale';
import { randomBG } from '../utils/colorUtils';

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      header: this.props.header,
      content: this.props.content,
      sizes: this.props.sizes,
      table: false
    };
    console.log('Table en constructor', this.state);
  }

  componentDidMount() {
    const table = this._getTable();
    this.setState({ table: table });
    this.setState({ isReady: true });
  }


  _getHeader() {
    const cols = this.state.header.map((text, count) => {
      return (<View style={{ width: scaleVertical(30) * this.state.sizes[count], height: scaleVertical(30), justifyContent: 'center', alignItems: 'center' }}>
        <RkText style={{ color: '#fff', fontSize: scaleVertical(16) }}> {text} </RkText>
      </View>);
    });
    return (
      <View style={{ flex: 1, flexDirection: 'row', backgroundColor: '#000' }} >
        {cols}
      </View>
    );
  }

  _getContent() {
    const rows = this.state.content.map((data, index) => {
      const cols = data.map((text, count) => {
        return (<View style={{ width: scaleVertical(30) * this.state.sizes[count], height: scaleVertical(30), justifyContent: 'center', alignItems: 'center' }}>
          <RkText style={{ color: '#000', fontSize: scaleVertical(14) }}> {text} </RkText>
        </View>);
      });
      const bg = index % 2 === 0 ? '#fefefe' : '#c8eeff';
      return (
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: bg, height: scaleVertical(30) }} >
          {cols}
        </View>
      );
    });
    const content = (
      <View style={{ flex: 1 }} >
        {rows}
      </View>
    );
    return (
      <View style={{ flex: 1 }} >
        {content}
      </View>
    );
  }

  _getTable() {
    const table = (
      <View style={{ flex: 1 }}>
        {this._getHeader()}
        {this._getContent()}
      </View>
    );
    return table;
  }

  render() {
    if (!this.state.isReady || !this.state.table) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <ScrollView horizontal={true} style={{ flex: 1 }}>
          <View style={{ flex: 1, paddingHorizontal: scaleVertical(20) }}>
            {this.state.table}
          </View>
        </ScrollView>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  screen: {
    flex: 1,
    backgroundColor: '#2e3192'
  },
}));


const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Table);
