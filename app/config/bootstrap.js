import { StatusBar } from 'react-native';
import { RkTheme } from 'react-native-ui-kitten';
import { scale, size } from '../utils/scale';
import { KittenTheme } from './theme';

export const bootstrap = () => {
  RkTheme.setTheme(KittenTheme, null);
  /*
  RkText types
   */

  RkTheme.setType('RkText', 'basic', {
    fontFamily: theme => theme.fonts.family.bold,
    backgroundColor: 'transparent'
  });

  //all font sizes
  for (let key in RkTheme.current.fonts.sizes) {
    RkTheme.setType('RkText', key, {
      fontSize: theme => theme.fonts.sizes[key]
    });
  }


  //all font family
  for (let key in RkTheme.current.fonts.family) {
    RkTheme.setType('RkText', key, {
      fontFamily: theme => theme.fonts.family[key]
    });
  }

  //all text colors
  for (let key in RkTheme.current.colors.text) {
    RkTheme.setType('RkText', `${key}Color`, {
      color: theme => theme.colors.text[key]
    });
  }

  //all text line heights
  for (let key in RkTheme.current.fonts.lineHeights) {
    RkTheme.setType('RkText', `${key}Line`, {
      text: {
        lineHeight: theme => theme.fonts.lineHeights[key]
      }
    });
  }


  /*
  RkButton types
   */

  RkTheme.setType('RkButton', 'basic', {
    container: {
      alignSelf: 'auto',
    }
  });


  RkTheme.setType('RkButton', 'buttonGoogleLogin', {
    alignSelf: 'center',
    backgroundColor: '#4385f5',
    width: size.width - scale(90),
    height: scale(45),
    container: {
      justifyContent: 'center',
    }
  });


  RkTheme.setType('RkButton', 'buttonPartnerLogin', {
    alignSelf: 'center',
    backgroundColor: 'transparent',
    width: size.width - scale(90),
    height: scale(45),
    borderWidth: scale(2),
    borderColor: '#eb0029',
    container: {
      justifyContent: 'center',
      color: '#000'
    }
  });


  RkTheme.setType('RkButton', 'buttonEmailLogin', {
    alignSelf: 'center',
    backgroundColor: '#eb0029',
    width: size.width - scale(90),
    height: scale(45),
    container: {
      justifyContent: 'center',
    }
  });
  RkTheme.setType('RkButton', 'buttonFacebookLogin', {
    alignSelf: 'center',
    backgroundColor: '#3c5999',
    width: size.width - scale(90),
    height: scale(45),
    container: {
      justifyContent: 'center',
    }
  });

  RkTheme.setType('RkButton', 'square', {
    borderRadius: 3,
    backgroundColor: theme => theme.colors.button.back,
    container: {
      flexDirection: 'column',
      margin: 8
    },
  });


  /*
  RkModalImg types
   */

  RkTheme.setType('RkModalImg', 'basic', {
    img: {
      margin: 1.5,
    },
    modal: {
      backgroundColor: theme => theme.colors.screen.base
    },
    footer: {
      backgroundColor: theme => theme.colors.screen.base,
      height: 50
    },
    header: {
      backgroundColor: theme => theme.colors.screen.base,
      paddingBottom: 6
    },
  });

  /*
  RkTextInput
   */

  RkTheme.setType('RkTextInput', 'basic', {
    input: {
      fontFamily: theme => theme.fonts.family.bold
    },
    color: theme => theme.colors.text.base,
    backgroundColor: theme => theme.colors.control.background,
    labelColor: theme => theme.colors.input.label,
    placeholderTextColor: theme => theme.colors.input.placeholder,
  });


  /*
  RkCard types
   */


  RkTheme.setType('RkTabView', 'standing', {
    backgroundColor: 'transparent',
    color: 'black',
    borderColor: '#e5e5e5',
    tabContainer: {
      padding: 2,
      overflow: 'hidden',
      borderWidth: 0,
      borderLeftWidth: 0,
      borderRightWidth: 0,
      borderBottomWidth: 5,
      borderColor: '#eb0029',
      backgroundColor: '#e5e5e5',
      height: size.width / 7,
      alignItems: 'center',
      justifyContent: 'center',
    },

  });

  RkTheme.setType('RkTabView', 'standingSelected', {
    backgroundColor: 'transparent',
    color: 'black',
    borderColor: '#e5e5e5',
    tabContainer: {
      padding: 2,
      overflow: 'hidden',
      borderWidth: 0,
      borderLeftWidth: 0,
      borderRightWidth: 0,
      borderBottomWidth: 10,
      borderColor: '#eb0029',
      height: size.width / 7,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

  StatusBar.setBarStyle('dark-content', true);
};
