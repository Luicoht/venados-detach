import React, { Component } from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import { RkButton, RkText } from 'react-native-ui-kitten';
import ImagePicker from '../../components/imagePicker';
import camera from '../../assets/icons/camera-1.png';

const WINDOWS_WIDTH = Dimensions.get('window').width;

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  inputSection: {
    backgroundColor: '#ecf0f1',
    position: 'absolute',
    width: '100%',
    bottom: 0,
    right: 0,
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
    elevation: 10,
    borderTopWidth: 1,
    borderTopColor: 'rgb(220,220,220)',
  },
  imagePickerContainer: {
    backgroundColor: '#ecf0f1',
    position: 'absolute',
    width: '100%',
    top: 0,
    right: 0,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(240,240,240)',
    elevation: 2,
  },
  imagePicker: {
    position: 'relative',
    width: '90%',
    alignSelf: 'center',
    marginVertical: 10
  },
  selectedImage: {
    position: 'relative',
    width: WINDOWS_WIDTH * 0.7,
    height: WINDOWS_WIDTH * 0.7
  }
};


class PhotosView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedImage: null,
    };
  }

  onImageUpload(image) {
    this.setState({ selectedImage: image });
  }

  renderImagePicker() {
    return (
      <View style={styles.imagePickerContainer}>
        <ImagePicker
          style={styles.imagePicker}
          onImageUpload={this.onImageUpload.bind(this)}
          aspectRatio={[4, 4]}
          name='picture'
        >
          <View style={{ flexDirection: 'row' }}>
            <Image style={{ height: 50, width: 50 }} source={camera} />

            <RkText style={{ color: 'black', fontSize: 20, top: 25, left: 15, paddingRight: 20 }} rkType='danger'>
              Seleccionar imagen
            </RkText>
          </View>
        </ImagePicker>
      </View>
    );
  }

  renderInputSection() {
    return (
      <View style={styles.inputSection}>
        <View style={{ flex: 3, alignContent: 'center' }}>
          <Text style={{ color: 'black', fontSize: 18 }}>
            Escribe un mensaje
          </Text>
        </View>

        <View style={{ flex: 2, alignContent: 'flex-end' }}>
          <RkButton
            style={{ backgroundColor: 'red', width: 100, top: -5 }}
            contentStyle={{ color: 'white' }}
          >
            Enviar
          </RkButton>
        </View>
      </View>
    );
  }

  renderSelectedImage() {
    const { selectedImage } = this.state;

    if (selectedImage) {
      return (
        <Image
          style={styles.selectedImage}
          source={{ uri: selectedImage.imageUri }}
        />
      );
    }

    return null;
  }

  render() {
    return (
      <View style={styles.container}>
        { this.renderImagePicker() }
        { this.renderSelectedImage() }
        { this.renderInputSection() }
      </View>
    );
  }
}


export default PhotosView;
