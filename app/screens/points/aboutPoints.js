import React from 'react';
import { View, ScrollView, TouchableOpacity } from 'react-native';
import { RkStyleSheet, RkText, } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { scale, scaleVertical } from '../../utils/scale';


class AboutPoints extends React.Component {
  constructor(props) {
    super(props);
    const Content = [
      {
        about: 'Por ser Socio Venados',
        score: '5,000'
      },
      {
        about: 'Descargar la aplicación',
        score: '1,000'
      },
      {
        about: 'Registrar tu boleto de entrada al estadio',
        score: '200'
      },
      {
        about: 'Hacer Check-in con foto publicada',
        score: '300'
      },
      {
        about: 'Entrar antes de la 1r entrada',
        score: '300'
      },
      {
        about: 'Permanecer en el estadio hasta la 9na entrada',
        score: '300'
      },
      {
        about: 'Permanecer en el estadio hasta el final del juego en extra innings',
        score: '500'
      },
      {
        about: 'Bono de asistencia a partir del 3er juego consecutivo',
        score: '500'
      },
    ];

    this.state = {
      content: Content
    };
    const options = {
      title: 'GANAR PUNTOS',
      hidden: true
    };
    this.props.navigation.setParams({ options });
  }

  componentWillMount() { }

  render() {
    const content = this.state.content.map((route) => {
      return (
        <TouchableOpacity activeOpacity={1} style={styles.tableContent}>
          <View style={styles.tableHeaderContent}>
            <View style={styles.item}>
              <View style={styles.leftContent}>
                <RkText style={styles.textLeftContent} > {route.about}</RkText>
              </View>
              <View style={styles.rightContent}>
                <RkText style={styles.textRightContent} > {route.score}</RkText>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      );
    });

    return (
      <ScrollView style={styles.root}>
        <View style={styles.textContainer}>
          <RkText style={styles.textBg}> Formas de ganar puntos </RkText>
        </View>
        <View style={styles.table}>
          <View style={styles.tableHeader}>
            <View style={styles.leftHeader}>
              <RkText style={[styles.textHeader, styles.textLeft]}> CONCEPTO</RkText>
            </View>
            <View style={styles.rightHeader}>
              <RkText style={[styles.textHeader, styles.textRight]}> PUNTOS</RkText>
            </View>
          </View>
          {content}
        </View>
      </ScrollView>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: 'white',
  },
  textContainer: {
    paddingTop: scaleVertical(20)
  },
  textBg: {
    color: 'black',
    fontFamily: 'TTPollsBold',
    fontSize: scale(27),
    textAlign: 'center'
  },
  table: {
    backgroundColor: 'white',
    marginLeft: scale(10),
    marginRight: scale(10),
  },
  tableHeader: {
    backgroundColor: 'black',
    marginLeft: scale(10),
    marginRight: scale(10),
    marginTop: scale(20),
    flexDirection: 'row',
  },
  tableContent: {
    marginLeft: scale(10),
    marginRight: scale(10),
    marginTop: scale(20),
  },
  leftHeader: {
    flex: 3,
    padding: scale(5),
  },
  rightHeader: {
    flex: 1,
    padding: scale(5),
  },
  textHeader: {
    color: 'white',
    fontSize: scale(15),
  },
  textLeft: {
    textAlign: 'left',
    marginLeft: scale(5),
  },
  textRight: {
    textAlign: 'right',
    marginRight: scale(5),
  },
  tableHeaderContent: {
    marginLeft: scale(10),
    marginRight: scale(10),
    flexDirection: 'row',
    alignSelf: 'center'
  },
  item: {
    flex: 1,
    borderBottomWidth: scale(1),
    flexDirection: 'row',
  },
  leftContent: {
    flex: 3,
  },
  rightContent: {
    flex: 1.5,
  },
  textLeftContent: {
    fontSize: scale(16),
    textAlign: 'left',
  },
  textRightContent: {
    fontSize: scale(18),
    textAlign: 'right',
    color: '#eb0029'
  },
}));


const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AboutPoints);
