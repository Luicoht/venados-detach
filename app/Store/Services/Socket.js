import openSocket from 'socket.io-client';
import * as firebase from 'firebase';
import { Notifications, Google, Facebook } from 'expo';
import { setItem, removeItem, getItem } from '../../utils/localdatabase';
import { FACEBOOKSDK, FACEBOOK_UI, API_URL } from '../../constants';

// export const socket = openSocket('http://192.168.1.67:3001');
// export const socket = openSocket('http://192.168.0.102:3001');
// const socket = openSocket('http://192.168.0.16:3001');
// const socket = openSocket('http://192.168.1.87:3001');
// const socket = openSocket('http://localhost:3001');
export const socket = openSocket(`${API_URL}/`);

const { PERMISSIONS } = FACEBOOKSDK;
const { APP_ID } = FACEBOOK_UI;


function tryLinkTogoogle() {
  console.log('tryLinkTogoogle Promise');
  return new Promise(((resolve, reject) => {
    Google.logInAsync({
      androidClientId: '383435945674-mulbiqr2qt2442buf7dpoevkqoql9evn.apps.googleusercontent.com',
      iosClientId: '383435945674-tancgilrgl5u6ieojcsgecf4onp2p3r5.apps.googleusercontent.com',
      // iosClientId: 'com.googleusercontent.apps.383435945674-tancgilrgl5u6ieojcsgecf4onp2p3r5',
      scopes: ['profile', 'email'],
      // androidStandaloneAppClientId: '383435945674-877to7mhvig9mc340ihejagmfidoc9hf.apps.googleusercontent.com'
    }).then((result) => {
      const prevUser = firebase.auth().currentUser;
      if (result.type === 'success') {
        const provider = firebase.auth.GoogleAuthProvider;
        const credential = provider.credential(null, result.accessToken);

        firebase.auth().signInAndRetrieveDataWithCredential(credential).then((googleUser) => {
          prevUser.linkAndRetrieveDataWithCredential(credential).then(user => {
            // authInDevice();
            resolve(user);
          })
            .catch(error => {
              reject({ error, prevUser, porno: googleUser });
            });
        }).catch((error) => {
          console.error('Error in login: ', error);
          reject({ error });
        });
      }
      console.log('cancelled', result.accessToken);
    });
  }));
}


function tryLinkToFacebook() {
  console.log('tryLinkToFacebook Promise');
  return new Promise(((resolve, reject) => {
    Facebook.logInWithReadPermissionsAsync(APP_ID, {
      permissions: PERMISSIONS,
    }).then((data) => {
      if (data.type === 'success') {
        const credential = firebase.auth.FacebookAuthProvider.credential(data.token);
        firebase.auth().signInAndRetrieveDataWithCredential(credential).then(user => {
          // authInDevice();
          resolve(user);
        }).catch((error) => {
          console.error('Error in login FacebookAuthProvider: ', error);
          reject(error);
        });
      } else {
        reject({ error: 'canceled' });
      }
    });
  }));
}





function updatePassword(newPassword) {
  const user = firebase.auth().currentUser;

  return new Promise(((resolve, reject) => {
    user.updatePassword(newPassword)
      .then(() => {
        resolve('Se ha guardado la contraseña de manera exitosa.');
      })
      .catch((error) => reject(error));
  }));
}

function getUserID() {
  return new Promise(((resolve, reject) => {
    getItem('userID').then((userID) => {
      resolve(userID);
    })
      .catch(err => reject(err));
  }));
}


function gamesIPlayed() {
  return new Promise(((resolve, reject) => {
    getItem('userID').then((userID) => {
      socket.emit('gamesIPlayed', { userID }, (err, solve) => {
        if (err) {
          reject(err);
        } else {
          resolve(solve);
        }
      });
    })
      .catch(err => reject(err));
  }));
}

function getAllPointsEarnedInAGame(data) {
  return new Promise(((resolve, reject) => {
    getItem('userID').then((userID) => {
      socket.emit('getAllPointsEarnedInAGame', { userID, gameID: data.id }, (err, solve) => {
        if (err) {
          reject(err);
        } else {
          resolve(solve);
        }
      });
    })
      .catch(err => reject(err));
  }));
}


function getProfile() {
  return new Promise(((resolve, reject) => {
    firebase.auth().currentUser.getIdToken(true)
      .then((idToken) => {
        const sendDate = {
          token: idToken
        };
        socket.emit('getProfile', sendDate, (err, solve) => {
          if (err) { reject(err); } else {
            removeItem('profile');
            setItem('profile', solve);
            resolve(solve);
          }
        });
      });
  }));
}

function updateProfile(params) {
  console.log('updateProfile', params);
  return new Promise(((resolve, reject) => {
    socket.emit('updateProfile', params, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function authInDevice() {
  return new Promise(((resolve, reject) => {
    Notifications.getExpoPushTokenAsync()
      .then((registrationToken) => {
        // console.log('getExpoPushTokenAsync registrationToken', registrationToken);
        firebase.auth().currentUser.getIdToken(true)
          .then((idToken) => {
            const data = {
              token: idToken,
              registrationToken
            };
            // console.log('authInDevice data', data);
            socket.emit('authInDevice', data, (err, solve) => {
              if (err) {
                console.log('error al emitir el auth device');
                reject({ error: 'error al emitir el auth device' });
              } else {
                // console.log('authInDevice solve', solve);
                if (solve !== undefined || solve !== null) {
                  removeItem('userID');
                  setItem('userID', solve);
                  resolve(solve);
                }
              }
            });
          });
      })
      .catch((error) => {
        console.log('getExpoPushTokenAsync error', error);
        reject({ error });
      });
  }));
}

function getPlayers() {
  return new Promise(((resolve, reject) => {
    socket.emit('getPlayers', {}, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getPlayersForAdmin() {
  return new Promise(((resolve, reject) => {
    socket.emit('getPlayersForAdmin', {}, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function checkIn(params) {
  return new Promise(((resolve, reject) => {
    getItem('userID').then((userID) => {
      socket.emit('checkIn', {...params, userID }, (err, solve) => {
        if (err) {
          reject(err);
        } else {
          resolve(solve);
        }
      });
    })
      .catch(err => reject(err));
  }));
}

function getGames() {
  return new Promise(((resolve, reject) => {
    socket.emit('getGames', {}, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getCurrentActiveGame() {
  return new Promise(((resolve, reject) => {
    socket.emit('getCurrentActiveGame', {}, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getCards(data) {
  return new Promise(((resolve, reject) => {
    socket.emit('getCards', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function buyCards(cards) {
  return new Promise(((resolve, reject) => {
    getItem('userID').then((userID) => {
      const data = {
        userID,
        cards
      };
      socket.emit('buyCards', data, (err, solve) => {
        if (err) {
          reject(err);
        } else {
          resolve(solve);
        }
      });
    }).catch(err => console.log(err));
  }));
}

function getWalletPoints() {
  return new Promise(((resolve, reject) => {
    getItem('userID').then((result) => {
      socket.emit('getWalletPoints', { userID: result }, (err, solve) => {
        if (err) {
          reject(err);
        } else {
          resolve(solve);
        }
      });
    }).catch(err => console.log(err));
  }));
}

function getHistoryQuiniela(game) {
  return new Promise(((resolve, reject) => {
    getItem('userID').then((userID) => {
      const sendData = {
        userID,
        gameID: game.id
      };
      socket.emit('getHistoryQuiniela', sendData, (err, solve) => {
        if (err) {
          reject(err);
        } else {
          resolve(solve);
        }
      });
    }).catch(err => console.log(err));
  }));
}

function consultSimpleBetDeerChallenge(data) {
  return new Promise(((resolve, reject) => {
    const sendData = {
      playerID: data.playerID,
      points: data.points,
      event: data.event
    };
    socket.emit('consultSimpleBetDeerChallenge', sendData, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function consultBonusBetDeerChallenge(data) {
  return new Promise(((resolve, reject) => {
    socket.emit('consultBonusBetDeerChallenge', data, (err, solve) => {
      if (err) {
        console.log('consultBonusBetDeerChallenge err', err);
        reject(err);
      } else {
        console.log('consultBonusBetDeerChallenge solve', solve);
        resolve(solve);
      }
    });
  }));
}

function getHistoryDeerChallenge(game) {
  return new Promise(((resolve, reject) => {
    getItem('userID').then((userID) => {
      const sendData = {
        userID,
        gameID: game.id
      };
      socket.emit('getHistoryDeerChallenge', sendData, (err, solve) => {
        if (err) {
          reject(err);
        } else {
          resolve(solve);
        }
      });
    }).catch(err => console.log(err));
  }));
}

function playSimpleBetDeerChallenge(data) {
  return new Promise(((resolve, reject) => {
    getItem('userID').then((userID) => {
      const sendData = {
        playerID: data.playerID,
        points: data.points,
        event: data.event,
        userID
      };
      socket.emit('playSimpleBetDeerChallenge', sendData, (err, solve) => {
        if (err) {
          reject(err);
        } else {
          console.log('playSimpleBetDeerChallenge');
          resolve(solve);
        }
      });
    }).catch(err => console.log(err));
  }));
}

function playBonusBetDeerChallenge(data) {
  return new Promise(((resolve, reject) => {
    getItem('userID').then((userID) => {
      const sendData = {
        points: data.points,
        id: data.id,
        userID,
      };
      console.log('playBonusBetDeerChallenge sendData', sendData);

      socket.emit('playBonusBetDeerChallenge', sendData, (err, solve) => {
        if (err) {
          reject(err);
        } else {
          resolve(solve);
        }
      });
    }).catch(err => console.log(err));
  }));
}

function verifyPurchaseCode(data) {
  console.log('verifyPurchaseCode');
  return new Promise(((resolve, reject) => {
    socket.emit('verifyPurchaseCode', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function completeArticlePurchase(data) {
  console.log('completeArticlePurchase');
  return new Promise(((resolve, reject) => {
    socket.emit('completeArticlePurchase', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getCategories() {
  console.log('getCategories');
  return new Promise(((resolve, reject) => {
    socket.emit('getCategories', {}, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getArticlesByCategory(data) {
  const sendData = { categoryID: data.id };
  console.log('getArticlesByCategory ', sendData);
  return new Promise(((resolve, reject) => {
    socket.emit('getArticlesByCategory', sendData, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getAllPointsEarned(data) {
  console.log('getAllPointsEarned');
  return new Promise(((resolve, reject) => {
    socket.emit('getAllPointsEarned', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}
function historyRecords() {
  console.log('historyRecords');
  return new Promise(((resolve, reject) => {
    getItem('userID').then((userID) => {
      socket.emit('historyRecords', { userID }, (err, solve) => {
        if (err) {
          reject(err);
        } else {
          resolve(solve);
        }
      });
    }).catch(err => console.log(err));
  }));
}


function signUpBySMS(data) {
  console.log('signUpBySMS');
  return new Promise(((resolve, reject) => {
    socket.emit('signUpBySMS', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function confirmSignUpBySMS(data) {
  console.log('confirmSignUpBySMS');
  return new Promise(((resolve, reject) => {
    socket.emit('confirmSignUpBySMS', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function authBySMS(data) {
  console.log('authBySMS');
  return new Promise(((resolve, reject) => {
    socket.emit('authBySMS', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function confirmSignInBySMS(data) {
  console.log('confirmSignInBySMS');
  return new Promise(((resolve, reject) => {
    socket.emit('confirmSignInBySMS', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getFriends(data) {
  console.log('getFriends');
  return new Promise(((resolve, reject) => {
    socket.emit('getFriends', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getFriendsPoints(data) {
  console.log('getFriendsPoints');
  return new Promise(((resolve, reject) => {
    socket.emit('getFriendsPoints', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getFriendsPointsBySeason(data) {
  console.log('getFriendsPointsBySeason');
  return new Promise(((resolve, reject) => {
    socket.emit('getFriendsPointsBySeason', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getFriendsPointsToday(data) {
  console.log('getFriendsPointsToday');
  return new Promise(((resolve, reject) => {
    socket.emit('getFriendsPointsToday', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getComunityPoints(data) {
  console.log('getComunityPoints');
  return new Promise(((resolve, reject) => {
    socket.emit('getComunityPoints', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getComunityPointsBySeason(data) {
  console.log('getComunityPointsBySeason');
  return new Promise(((resolve, reject) => {
    socket.emit('getComunityPointsBySeason', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function getComunityPointsToday(data) {
  console.log('getComunityPointsToday');
  return new Promise(((resolve, reject) => {
    socket.emit('getComunityPointsToday', data, (err, solve) => {
      if (err) {
        reject(err);
      } else {
        resolve(solve);
      }
    });
  }));
}

function formatNumber(number) {
  let number1 = number.toString();
  let result = '';
  let estado = true;
  if (parseInt(number1, 0) < 0) {
    estado = false;
    number1 = parseInt(number1, 0) * -1;
    number1 = number1.toString();
  }
  if (number1.indexOf(',') === -1) {
    while (number1.length > 3) {
      result = `${','}${number1.substr(number1.length - 3)}${result}`;
      number1 = number1.substring(0, number1.length - 3);
    }
    result = number1 + result;
    if (estado === false) {
      result = `-${result}`;
    }
  } else {
    const pos = number1.indexOf(',');
    let numberInt = number1.substring(0, pos);
    const numberDec = number1.substring(pos, number1.length);
    while (numberInt.length > 3) {
      result = `${','}${numberInt.substr(numberInt.length - 3)}${result}`;
      numberInt = numberInt.substring(0, numberInt.length - 3);
    }
    result = numberInt + result + numberDec;
    if (estado === false) {
      result = `-${result}`;
    }
  }
  return result;
}

function registerLikeDeerMember(data) {
  return new Promise(((resolve, reject) => {
    getItem('userID').then((userID) => {
      const sendData = {
        memberCode: data.memberCode,
        phone: data.phone,
        userID: userID
      };
      console.log('registerLikeDeerMember', sendData);
      socket.emit('registerLikeDeerMember', sendData, (err, solve) => {
        console.log('registerLikeDeerMember', err);
        console.log('registerLikeDeerMember', solve);
        if (err) { reject(err); } else { resolve(solve); }
      });
    });
  }));
}

function confirmRegisterLikeDeerMember(data) {
  console.log('chalalalalalla');
  return new Promise(((resolve, reject) => {
    socket.emit('confirmRegisterLikeDeerMember', data, (err, solve) => {
      if (err === 'ok' || solve === 'ok') resolve('ok');
      if (err) { reject(err); } else { resolve('ok'); }
    });
  }));
}


export {
  updatePassword,
  updateProfile,
  registerLikeDeerMember,
  confirmRegisterLikeDeerMember,
  getUserID,
  gamesIPlayed,
  getAllPointsEarnedInAGame,
  getProfile,
  getPlayersForAdmin,
  getPlayers,
  authInDevice,
  getGames,
  getCurrentActiveGame,
  getCards,
  buyCards,
  getWalletPoints,
  getHistoryQuiniela,
  consultSimpleBetDeerChallenge,
  consultBonusBetDeerChallenge,
  getHistoryDeerChallenge,
  playSimpleBetDeerChallenge,
  playBonusBetDeerChallenge,
  verifyPurchaseCode,
  completeArticlePurchase,
  getCategories,
  getArticlesByCategory,
  getAllPointsEarned,
  historyRecords,
  signUpBySMS,
  confirmSignUpBySMS,
  authBySMS,
  confirmSignInBySMS,
  getFriends,
  getFriendsPoints,
  getFriendsPointsBySeason,
  getFriendsPointsToday,
  getComunityPoints,
  getComunityPointsBySeason,
  getComunityPointsToday,
  formatNumber,
  checkIn,
  tryLinkTogoogle,
  tryLinkToFacebook
};
