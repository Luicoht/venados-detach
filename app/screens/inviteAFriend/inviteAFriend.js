import React from 'react';
import { ScrollView, View, Image, ImageBackground } from 'react-native';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { DoubleBounce } from 'react-native-loader';
import { Asset } from 'expo';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';
import { scale, scaleVertical, size } from '../../utils/scale';
import shutterstock from '../../assets/JPEG/shutterstock_212667757.jpg';
import venadosRojo from '../../assets/images/VenadosRojo.png';
import imgDont from '../../assets/icons/dont.png';

class InviteAFriend extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
    const options = {
      title: 'INVITAR',
      hidden: true
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() { this._cacheResourcesAsync(); }
  async _cacheResourcesAsync() {
    const images = [
      shutterstock,
      venadosRojo,
      imgDont,
    ];
    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    });
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  registro(values) {
    this.props.registro(values);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }

    return (
      <View style={styles.root}>
        <ImageBackground style={{ flex: 1 }} source={shutterstock} >
          <ScrollView style={{ flex: 1 }}>
            <View style={styles.table}>
              <Image
                style={styles.IconBG}
                source={venadosRojo}
              />
              <RkText style={styles.textBg} >¡Invita a tus amigos a ser socio Venados y gana puntos!</RkText>
              <RkText style={styles.textInfo} >Si tu amigo se convierte en Socio Venados por tu recomendación, en agradecimineto te daremos 5,000 puntos para que juegues o canjees por lo que tú quieras.</RkText>
              <RkText style={styles.textInfoDos} >Además tu amigo podrá tener un descuento del 30% de descuento en la compra de su Carnet Socio Venados</RkText>
              <RkText style={styles.textInfoRed} >¿CÓMO FUNCIONA?</RkText>
              <RkText style={styles.textInfoDos} >Solo tienes que dejarnos en este formulario los datos de tu amigo, y nosotros lo contactaremos para informarle sobre tu invitación y el beneficio de adquirir su membresía por recomendación tuya.</RkText>
              <RkText style={styles.textBgDos} >¡Solo incluye sus datos y envíale una invitación!</RkText>
              <View style={{ paddingHorizontal: scaleVertical(20) }} >
                <TextField
                  textColor='white'
                  tintColor='white'
                  baseColor='white'
                  label='Tu nombre'
                  error=''
                />
                <TextField
                  textColor='white'
                  tintColor='white'
                  baseColor='white'
                  error=''
                  label='Nombre de tu amigo'
                />
                <TextField
                  textColor='white'
                  tintColor='white'
                  baseColor='white'
                  error=''
                  label='Número de célular de tu amigo'
                />
              </View>
              <RkButton style={styles.boton} > ENVIAR </RkButton>
              <View style={{ height: scaleVertical(25) }} />
            </View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}


let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1
  },
  playContainer: {
    flex: 1
    // width: size.width,
    // height: size.height / 3
  },
  imageBg: {
    width: size.width,
    height: size.height / 3
  },
  label: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
  },
  textInfo: {
    color: 'white',
    fontFamily: 'DecimaMono',
    fontSize: scale(20),
    paddingVertical: scaleVertical(10),
    paddingHorizontal: scaleVertical(20),
  },
  textInfoDos: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: scale(20),
    paddingVertical: scaleVertical(10),
    paddingHorizontal: scaleVertical(20),
  },
  textInfoRed: {
    color: '#ed0a27',
    fontFamily: 'Roboto-Bold',
    fontSize: scale(20),
    paddingVertical: scaleVertical(10),
    paddingHorizontal: scaleVertical(20),
  },
  textBg: {
    color: 'white',
    fontFamily: 'TTPollsBold',
    fontSize: scale(35),
    paddingVertical: scaleVertical(10),
    paddingHorizontal: scaleVertical(20),
  },
  textBgDos: {
    color: 'white',
    fontFamily: 'TTPollsBold',
    fontSize: scale(25),
    paddingVertical: scaleVertical(10),
    paddingHorizontal: scaleVertical(20),
  },
  IconBGBack: {
    height: scaleVertical(32),
    resizeMode: 'contain',
    alignSelf: 'left'
  },
  IconBG: {
    // width: scaleVertical(64),
    height: scaleVertical(50),
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  subMenuItems: {
    width: (size.width / 3) - scaleVertical(10),
    height: (size.height / 6) - scaleVertical(10),
    flexDirection: 'column',
    fontSize: 8,
  },
  subMenuIcon: {
    marginTop: 1,
    marginBottom: 1,
  },
  subMenuText: {
    color: '#fff',
    fontSize: scaleVertical(14),
    alignSelf: 'center'
  },
  boton: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45),
  },
  table: {
    backgroundColor: 'rgba(0,0,0,0.85)',
    paddingTop: scaleVertical(25),
  },
  tableHeader: {
    marginLeft: scale(10),
    marginRight: scale(10),
    marginTop: scale(20),
    flexDirection: 'row',
  },
  tableContent: {
    marginLeft: scale(10),
    marginRight: scale(10),
    marginTop: scale(20),
  },
  leftHeader: {
    flex: 3,
    padding: scale(5),
  },
  rightHeader: {
    flex: 1,
    padding: scale(5),
  },
  textHeader: {
    color: '#eb0029',
    fontSize: scale(15),
  },
  textLeft: {
    textAlign: 'left',
    marginLeft: scale(5),
  },
  textRight: {
    textAlign: 'right',
    color: '#eb0029',
  },
  tableHeaderContent: {
    marginLeft: scale(10),
    marginRight: scale(10),
    flexDirection: 'row',
    alignSelf: 'center'
  },
  item: {
    flex: 1,
    borderBottomWidth: scale(1),
    flexDirection: 'row',
  },
  leftContent: {
    flex: 3,
  },
  rightContent: {
    flex: 1.5,
  },
  textLeftContent: {
    fontSize: scale(16),
    textAlign: 'left',
    color: 'white',
    fontFamily: 'Roboto-Regular',
  },
  textRightContent: {
    fontSize: scale(18),
    textAlign: 'right',
    color: '#eb0029',
    fontFamily: 'Roboto-Regular',
  },
}));

const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InviteAFriend);
