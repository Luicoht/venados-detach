import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { View, ScrollView, UIManager, LayoutAnimation } from 'react-native';
import { DoubleBounce } from 'react-native-loader';
import { getHistoryDeerChallenge, formatNumber } from '../../Store/Services/Socket';
import { scaleVertical, size } from '../../utils/scale';
import PlayBonus from './playBonus';

class MyDeerChallenge extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deerChallengeData: false,
      isReady: false
    };
    const options = {
      title: 'RETO VENADOS',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() {
    getHistoryDeerChallenge(this.props.currentActiveGame)
      .then((deerChallengeData) => {
        this.setState({ deerChallengeData: deerChallengeData });
        this.setState({ isReady: true });
      })
      .catch((err) => {
        // console.log('getHistoryDeerChallenge err', err);
      });
  }

  componentWillUpdate(nextProps, nextState) {
    let animate = false;

    if (this.state.isReady !== nextState.isReady || this.state.deerChallengeData !== nextState.deerChallengeData) {
      animate = true;
    }

    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }

      LayoutAnimation.easeInEaseOut();
    }
  }

  _getContent() {
    if (this.props.currentActiveGame && this.props.currentActiveGame.finished !== 1 && this.state.deerChallengeData.length !== 0) {
      return this._haveGame();
    }
    if (this.props.currentActiveGame && this.props.currentActiveGame.finished !== 1 && this.state.deerChallengeData.length === 0) {
      return this._notInGame();
    }
    return this._notActiveGame();
  }


  _notActiveGame() {
    return (
      <View style={{ paddingTop: scaleVertical(15) }}>
        <View>
          <View style={{ paddingHorizontal: scaleVertical(20), paddingTop: scaleVertical(30) }}>
            <RkText style={{ textAlign: 'center', fontSize: scaleVertical(25), fontFamily: 'TTPollsBoldItalic' }}> No hay juegos disponibles. </RkText>
          </View>
          <View style={{ paddingHorizontal: scaleVertical(20), paddingVertical: scaleVertical(10) }}>
            <RkText style={{ textAlign: 'center', fontSize: scaleVertical(15), fontFamily: 'Roboto-Regular' }}> Muy pronto publicaremos el próximo juego. </RkText>
          </View>
        </View>
      </View>
    );
  }

  _haveGame() {
    const content = this._getDeerContent();
    return (
      <View >
        {content}
      </View>
    );
  }
  _notInGame() {
    return (
      <View style={{ paddingTop: scaleVertical(15) }}>
        <View>
          <View style={{ paddingHorizontal: scaleVertical(20), paddingVertical: scaleVertical(30) }}>
            <RkText style={{ textAlign: 'center', fontSize: scaleVertical(25), fontFamily: 'TTPollsBoldItalic' }}> Aún no estas jugando el reto Venados. </RkText>
          </View>
          <View style={{ paddingVertical: scaleVertical(10) }}>
            <RkButton style={{ borderRadius: scaleVertical(5), alignSelf: 'center', backgroundColor: '#eb0029', height: size.width / 7, width: size.width - scaleVertical(60) }} onPress={() => { this.props.navigation.navigate('LineUpMenu'); }}>
              <RkText style={{ fontSize: scaleVertical(18), color: '#fff' }}>JUGAR RETO VENADOS</RkText>
            </RkButton>
          </View>
          <View style={{ paddingVertical: scaleVertical(10) }}>
            <RkButton onPress={() => { this.props.navigation.navigate('CalendarMenu'); }} style={{ borderRadius: scaleVertical(5), alignSelf: 'center', backgroundColor: 'transparent', height: size.width / 7, width: size.width - scaleVertical(60), borderWidth: scaleVertical(2), borderColor: '#eb0029' }}>
              <RkText style={{ fontSize: scaleVertical(18), }}>VER JUEGO EN VIVO</RkText>
            </RkButton>
          </View>
        </View>
      </View>
    );
  }


  _getDeerContent() {
    const solve = this.state.deerChallengeData.map((route) => {
      const bgColor = route.isActive ? '#e9f2fb' : '#e1e1e1';
      const header = this._getDeerHeaderContent(route);
      const footer = this._getFooter(route);
      return (
        <View style={{ paddingVertical: scaleVertical(10) }}>
          <View style={{ backgroundColor: bgColor, paddingTop: scaleVertical(10) }}>
            {header}
            {footer}
          </View>
        </View>
      );
    });
    return solve;
  }


  _activeAndBonus(route) {
    const event = route.event === 1 ? 'HOME RUN' : 'PONCHE';
    return (<View style={{ backgroundColor: '#0d014d', flex: 1 }}>
      <View style={{ paddingVertical: scaleVertical(10), flexDirection: 'row' }}>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(25), textAlign: 'center' }}>BONUS</RkText>
        </View>
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1, }}>
            <RkText style={{ color: '#fff', fontSize: scaleVertical(10), textAlign: 'center' }}>PUNTOS A GANAR</RkText>
          </View>
          <View style={{ flex: 1, }}>
            <RkText style={{ color: '#6ccff6', textAlign: 'center', fontSize: scaleVertical(18) }}>{formatNumber(route.willGainBonusBet)}</RkText>
          </View>
        </View>
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1, }}>
            <RkText style={{ color: '#fff', fontSize: scaleVertical(10), textAlign: 'center' }}>EVENTO</RkText>
          </View>
          <View style={{ flex: 1, }}>
            <RkText style={{ color: '#6ccff6', textAlign: 'center', fontSize: scaleVertical(18) }}>{event}</RkText>
          </View>
        </View>
      </View>
    </View>);
  }
  _activeAndNotBobus(route) {
    return (
      <View style={{ flex: 1, paddingVertical: scaleVertical(10), justifyContent: 'center' }}>
        <PlayBonus deer={route} />
      </View>
    );
  }


  _unactiveAndBonus(route) {
    // console.log('_unactiveAndBonus');
    const event = route.event === 1 ? 'HOME RUN' : 'PONCHE';
    const buttonStyle = {
      width: size.width - scaleVertical(55),
      alignSelf: 'center',
      height: size.width / 7,
      backgroundColor: '#2e3192',
      justifyContent: 'center',
    };
    const buttonTextStyle = {
      fontSize: scaleVertical(18),
      color: '#fff',
      textVerticalAlign: 'center',
    };
    const colorText = route.winSimple === 1 ? '#002157' : '#eb0029';
    const puntos = formatNumber(route.bonusPointsEarned + route.simplePointsEarned);
    const totalWinnerPoint = route.winSimple === 1 ? `${puntos} puntos ganados` : 'SIGUE PARTICIPANDO';
    const footer = route.isWinner ?
      (<View style={{ paddingVertical: scaleVertical(15) }} >
        <RkText style={{ color: colorText, paddingBottom: scaleVertical(15), fontFamily: 'TTPollsBoldItalic', fontSize: scaleVertical(25), textAlign: 'center' }}>{totalWinnerPoint} </RkText>
        <RkButton style={buttonStyle}>
          <RkText style={buttonTextStyle}>COMPARTIR</RkText>
        </RkButton>
      </View>)
      :
      (<View style={{ paddingVertical: scaleVertical(15) }} >
        <RkText style={{ color: colorText, paddingBottom: scaleVertical(15), fontFamily: 'TTPollsBoldItalic', fontSize: scaleVertical(25), textAlign: 'center' }}>{totalWinnerPoint}</RkText>

        <RkButton style={buttonStyle}>
          <RkText style={buttonTextStyle}>COMPARTIR</RkText>
        </RkButton>
      </View>);
    return (
      <View style={{ paddingHorizontal: scaleVertical(10), paddingVertical: scaleVertical(10) }} >
        <View style={{ backgroundColor: '#b7b7b7' }} >
          <View style={{ paddingVertical: scaleVertical(10), flexDirection: 'row' }}>

            <View style={{ flex: 1, justifyContent: 'center' }}>
              <RkText style={{ color: '#868686', fontSize: scaleVertical(25), textAlign: 'center' }}>BONUS</RkText>
            </View>
            <View style={{ flex: 1 }}>
              <View style={{ flex: 1, }}>
                <RkText style={{ color: '#868686', fontSize: scaleVertical(10), textAlign: 'center' }}>PUNTOS A GANAR</RkText>
              </View>
              <View style={{ flex: 1, }}>
                <RkText style={{ color: '#7b869c', textAlign: 'center', fontSize: scaleVertical(18) }}>{formatNumber(route.willGainBonusBet)}</RkText>
              </View>
            </View>
            <View style={{ flex: 1 }}>
              <View style={{ flex: 1, }}>
                <RkText style={{ color: '#868686', fontSize: scaleVertical(10), textAlign: 'center' }}>EVENTO</RkText>
              </View>
              <View style={{ flex: 1, }}>
                <RkText style={{ color: '#7b869c', textAlign: 'center', fontSize: scaleVertical(18) }}>{event}</RkText>
              </View>
            </View>
          </View>
        </View>
        {footer}
      </View>);
  }

  _unactiveAndNotBonus(route) {
    // console.log('_unactiveAndNotBonus');
    const buttonStyle = {
      width: size.width - scaleVertical(55),
      alignSelf: 'center',
      height: size.width / 7,
      backgroundColor: '#2e3192',
      justifyContent: 'center',
    };
    const buttonTextStyle = {
      fontSize: scaleVertical(18),
      color: '#fff',
      textVerticalAlign: 'center',
    };

    const footer = route.winSimple === 1 ?
      (<View style={{ paddingVertical: scaleVertical(15) }} >
        <RkText style={{ color: '#002157', paddingBottom: scaleVertical(15), fontFamily: 'TTPollsBoldItalic', fontSize: scaleVertical(25), textAlign: 'center' }}>{formatNumber(route.simplePointsEarned)} puntos ganados </RkText>
        <PlayBonus deer={route} />
        <RkButton style={buttonStyle}>
          <RkText style={buttonTextStyle}>COMPARTIR</RkText>
        </RkButton>
      </View>)
      :
      (<View style={{ paddingVertical: scaleVertical(15) }} >
        <PlayBonus deer={route} />
        <RkText style={{ color: '#eb0029', paddingVertical: scaleVertical(15), fontFamily: 'TTPollsBoldItalic', fontSize: scaleVertical(25), textAlign: 'center' }}> SIGUE PARTICIPANDO </RkText>

        <RkButton style={buttonStyle}>
          <RkText style={buttonTextStyle}>COMPARTIR</RkText>
        </RkButton>
      </View>);
    return footer;
  }

  _getFooter(route) {
    if (route.isActive && route.bonusBet !== 0) {
      return this._activeAndBonus(route);
    }
    if (route.isActive && route.bonusBet === 0) {
      return this._activeAndNotBobus(route);
    }
    if (!route.isActive && route.bonusBet !== 0) {
      return this._unactiveAndBonus(route);
    }
    if (!route.isActive && route.bonusBet === 0) {
      return this._unactiveAndNotBonus(route);
    }
  }

  _getDeerHeaderContent(route) {
    const buttonStyleBase = {
      height: size.width / 10,
      width: size.width / 4,
      alignSelf: 'center'
    };
    const buttonStyle = route.isActive ?
      { backgroundColor: 'transparent', borderWidth: scaleVertical(2), borderColor: '#eb0029' } :
      { backgroundColor: '#252525' };

    const buttonText = route.isActive ? 'EN JUEGO' : 'FINALIZO';
    const buttonTextStyleBase = { fontFamily: 'Roboto-Bold', fontSize: scaleVertical(11) };
    const buttonTextStyle = route.isActive ? { color: '#eb0029' } : { color: '#fff' };
    const textStatus = route.event === 1 ? 'HIT' : 'OUT';

    const solve = (
      <View style={{ flexDirection: 'row' }}>
        <View style={{ flex: 1, }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 2 }}>
              <RkText style={{ textAlign: 'center', fontFamily: 'Roboto-Bold', fontSize: scaleVertical(18) }} > {route.name} </RkText>
            </View>
            <View style={{ flex: 1.4, justifyContent: 'center' }}>
              <RkButton style={[buttonStyleBase, buttonStyle]}>
                <RkText style={[buttonTextStyleBase, buttonTextStyle]}>{buttonText}</RkText>
              </RkButton>
            </View>
          </View>
          <View style={{ flex: 2, flexDirection: 'row', paddingVertical: scaleVertical(5) }}>
            <View style={{ flex: 1 }}>
              <RkText style={{ textAlign: 'center', textAlign: 'center', fontFamily: 'Roboto-Light', fontSize: scaleVertical(10) }}>EVENTO</RkText>
            </View>
            <View style={{ flex: 1 }}>
              <RkText style={{ textAlign: 'center', fontFamily: 'TTPollsBold', fontSize: scaleVertical(18), color: '#002157' }}>{textStatus}</RkText>
            </View>
            <View style={{ flex: 1 }}>
              <RkText style={{ textAlign: 'center', fontFamily: 'Roboto-Light', fontSize: scaleVertical(10) }}>PUNTOS A GANAR</RkText>
            </View>
            <View style={{ flex: 1.8 }}>
              <RkText style={{ textAlign: 'center', fontFamily: 'TTPollsBold', fontSize: scaleVertical(18), color: '#002157' }}>{formatNumber(route.willGainSimpleBet)}</RkText>
            </View>
          </View>
        </View>
      </View>);
    return solve;
  }

  render() {
    if (!this.state.isReady || !this.state.deerChallengeData) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color="#eb0029" />
        </View>
      );
    }

    return (
      <View style={styles.root} >
        <ScrollView style={styles.root} >
          <View style={styles.root} >
            {this._getContent()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(theme => ({
  root: {
    flex: 1,
    backgroundColor: '#fff',
  },
}));


MyDeerChallenge.propTypes = {
  currentActiveGame: PropTypes.object
};

const mapStateToProps = state => ({
  usuario: state.reducerSesion,
  currentActiveGame: state.reducerGame,
});


export default connect(mapStateToProps)(MyDeerChallenge);
