import React, { Component } from 'react';
import { View, YellowBox } from 'react-native';
import { Font } from 'expo';
import { Provider } from 'react-redux';
import { DoubleBounce } from 'react-native-loader';
import { MenuProvider } from 'react-native-popup-menu';
import _ from 'lodash';
import { bootstrap } from './app/config/bootstrap';
import Store from './app/Store/Store';
import Guard from './app/Guard';
import { size } from './app/utils/scale';
import fontawesome from './app/assets/fonts/fontawesome.ttf';
import icomoon from './app/assets/fonts/icomoon.ttf';
import RighteousRegular from './app/assets/fonts/Righteous-Regular.ttf';
import RobotoBold from './app/assets/fonts/Roboto-Bold.ttf';
import RobotoMedium from './app/assets/fonts/Roboto-Medium.ttf';
import RobotoRegular from './app/assets/fonts/Roboto-Regular.ttf';
import RobotoLight from './app/assets/fonts/Roboto-Light.ttf';
import DecimaMono from './app/assets/fonts/DecimaMono/DecimaMono.otf';
import DecimaMonoBold from './app/assets/fonts/DecimaMono/DecimaMono-Bold.otf';
import DecimaMonoBoldItalic from './app/assets/fonts/DecimaMono/DecimaMono-BoldItalic.otf';
import RobotoBlack from './app/assets/fonts/roboto/Roboto-Black.ttf';
import RobotoBlackItalic from './app/assets/fonts/roboto/Roboto-BlackItalic.ttf';
import RobotoBoldItalic from './app/assets/fonts/roboto/Roboto-BoldItalic.ttf';
import TTPollsBlack from './app/assets/fonts/TTPolls/TTPollsBlack.otf';
import TTPollsBlackItalic from './app/assets/fonts/TTPolls/TTPollsBlackItalic.otf';
import TTPollsBold from './app/assets/fonts/TTPolls/TTPollsBold.otf';
import TTPollsBoldItalic from './app/assets/fonts/TTPolls/TTPollsBoldItalic.otf';
import TTPollsRegular from './app/assets/fonts/TTPolls/TTPollsRegular.otf';

bootstrap();
console.disableYellowBox = true;


class App extends Component {
  constructor(props) {
    super(props);

    YellowBox.ignoreWarnings(['Setting a timer']);
    const _console = _.clone(console);

    console.warn = (message) => {
      if (message.indexOf('Setting a timer') <= -1) {
        _console.warn(message);
      }
    };

    this.state = {
      loaded: false,
      isImagesReady: false
    };
  }

  componentWillMount() {
    this.loadAssets();
  }

  async loadAssets() {
    await Font.loadAsync({
      fontawesome,
      icomoon,
      DecimaMono,
      TTPollsBlack,
      TTPollsBlackItalic,
      TTPollsBold,
      TTPollsBoldItalic,
      TTPollsRegular,
      'Righteous-Regular': RighteousRegular,
      'Roboto-Bold': RobotoBold,
      'Roboto-Medium': RobotoMedium,
      'Roboto-Regular': RobotoRegular,
      'Roboto-Light': RobotoLight,
      'DecimaMono-Bold': DecimaMonoBold,
      'DecimaMono-BoldItalic': DecimaMonoBoldItalic,
      'Roboto-Black': RobotoBlack,
      'Roboto-BlackItalic': RobotoBlackItalic,
      'Roboto-BoldItalic': RobotoBoldItalic,
    });

    this.setState({ loaded: true });
  }

  render() {
    if (!this.state.loaded) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color="#eb0029" />
        </View>
      );
    }

    return (
      <Provider store={Store} style={{ flex: 1 }}>
        <MenuProvider>
          <Guard />
        </MenuProvider>
      </Provider>
    );
  }
}


export default App;
