import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as NativeBase from 'native-base';


class Button extends Component {
  render() {
    let buttonContent;
    const { children, style, block, onPress, loading, transparent, small, rounded, spinnerColor } = this.props;

    if (loading) {
      buttonContent = <NativeBase.Spinner color={spinnerColor || 'black'} />;
    } else {
      buttonContent = children;
    }

    return (
      <NativeBase.Button
        style={style}
        block={block}
        transparent={transparent}
        small={small}
        rounded={rounded}
        onPress={onPress}
      >
        { buttonContent }
      </NativeBase.Button>
    );
  }
}


Button.propTypes = {
  children: PropTypes.any,
  style: PropTypes.object,
  block: PropTypes.bool,
  onPress: PropTypes.func,
  loading: PropTypes.bool,
  transparent: PropTypes.bool,
  small: PropTypes.bool,
  rounded: PropTypes.bool,
  spinnerColor: PropTypes.string
};


export { Button };
