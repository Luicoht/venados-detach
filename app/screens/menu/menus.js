import React from 'react';
import * as Routes from '../../config/navigation/routesBuilder';
import { CategoryMenu } from './categoryMenu';

export class NavigationMenu extends React.Component {
  static navigationOptions = {
    title: 'Navigation'.toUpperCase(),
    hidden: false
  };
  render() {
    return (
      <CategoryMenu
        navigation={this.props.navigation}
        items={Routes.NavigationRoutes}
      />
    );
  }
}

export class OtherMenu extends React.Component {
  static navigationOptions = {
    title: 'Other'.toUpperCase(),
    hidden: false
  };
  render() {
    return (
      <CategoryMenu
        navigation={this.props.navigation}
        items={Routes.OtherRoutes}
      />
    );
  }
}
export class IconsMenu extends React.Component {
  static navigationOptions = {
    title: 'Other'.toUpperCase(),
    hidden: false
  };
  render() {
    return (
      <CategoryMenu
        navigation={this.props.navigation}
        items={Routes.IconsRoutes}
      />
    );
  }
}
export class PlayMenu extends React.Component {
  static navigationOptions = {
    title: 'Play'.toUpperCase(),
    hidden: false
  };
  render() {
    return (
      <CategoryMenu
        navigation={this.props.navigation}
        items={Routes.IconsRoutes}
      />
    );
  }
}
export class PointsMenu extends React.Component {
  static navigationOptions = {
    title: 'Points'.toUpperCase(),
    hidden: false
  };
  render() {
    return (
      <CategoryMenu
        navigation={this.props.navigation}
        items={Routes.IconsRoutes}
      />
    );
  }
}


export class CalendarMenu extends React.Component {
  static navigationOptions = {
    title: 'Calendar'.toUpperCase(),
    hidden: false
  };
  render() {
    return (
      <CategoryMenu
        navigation={this.props.navigation}
        items={Routes.IconsRoutes}
      />
    );
  }
}

export class StandingMenu extends React.Component {
  static navigationOptions = {
    title: 'Standing'.toUpperCase(),
    hidden: false
  };
  render() {
    return (
      <CategoryMenu
        navigation={this.props.navigation}
        items={Routes.IconsRoutes}
      />
    );
  }
}

export class CommunityMenu extends React.Component {
  static navigationOptions = {
    title: 'Community'.toUpperCase(),
    hidden: false
  };
  render() {
    return (
      <CategoryMenu
        navigation={this.props.navigation}
        items={Routes.IconsRoutes}
      />
    );
  }
}

export class NewsMenu extends React.Component {
  static navigationOptions = {
    title: 'News'.toUpperCase(),
    hidden: false
  };
  render() {
    return (
      <CategoryMenu
        navigation={this.props.navigation}
        items={Routes.IconsRoutes}
      />
    );
  }
}

export class CheckInMenu extends React.Component {
  static navigationOptions = {
    title: 'CheckIn'.toUpperCase(),
    hidden: false
  };
  render() {
    return (
      <CategoryMenu
        navigation={this.props.navigation}
        items={Routes.IconsRoutes}
      />
    );
  }
}
export class ProfileMenu extends React.Component {
  static navigationOptions = {
    title: 'Profile'.toUpperCase(),
    hidden: false
  };
  render() {
    return (
      <CategoryMenu
        navigation={this.props.navigation}
        items={Routes.IconsRoutes}
      />
    );
  }
}
export class DashboardMenu extends React.Component {
  static navigationOptions = {
    title: 'Dashboard'.toUpperCase(),
    hidden: false
  };
  render() {
    return (
      <CategoryMenu
        navigation={this.props.navigation}
        items={Routes.IconsRoutes}
      />
    );
  }
}
