
import _ from 'lodash';
import 'moment/locale/es';


const formatter = {
  clean(object) {
    const result = {};
    const objectAttributes = Object.keys(object);

    objectAttributes.forEach((attribute) => {
      const attributeValue = object[attribute];

      if (_.isPlainObject(attributeValue)) {
        result[attribute] = formatter.clean(attributeValue);
      } else if (!_.isFunction(attributeValue)) {
        result[attribute] = attributeValue;
      }
    });

    return result;
  },
};


export default formatter;
