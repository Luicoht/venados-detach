import React from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView } from 'react-native';
import { RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { Asset } from 'expo';
import { DoubleBounce } from 'react-native-loader';
import { connect } from 'react-redux';
import { scale, scaleVertical, size } from '../../utils/scale';
import { Scoreboard } from '../../components/scoreboard';
import { QuinielaScore } from '../../components/quinielaScore';
import { DeerChallengeScore } from '../../components/deerChallengeScore';
import shutterstock from '../../assets/JPEG/shutterstock_476504581.jpg';
import { getAllPointsEarnedInAGame } from '../../Store/Services/Socket';

const months = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre'
];

class DetailsOfPreviousGames extends React.Component {
  constructor(props) {
    super(props);

    const game = this.props.navigation.state.params.game;
    const items = game.date.split('-');
    this.state = {
      isReady: false,
      pointsEarned: false,
      playQuiniela: false,
      playDeerChallenge: false,
      quinielaRecords: false,
      deerChallengeRecords: false,
      day: items[2],
      month: items[1],
      year: items[0],
      dateArray: items,
      game
    };
    const options = {
      title: 'DETALLES',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() {
    this._cacheResourcesAsync();
    getAllPointsEarnedInAGame(this.state.game)
      .then((pointsEarned) => {
        if (pointsEarned.pointsDeerChallenge) this.setState({ playDeerChallenge: true });
        if (pointsEarned.pointsQuiniela) this.setState({ playQuiniela: true });
        this.setState({ pointsEarned });
      })
      .catch((err) => {
        console.log('error en quinielas', err);
      });
  }
  getSTRDate() {
    return `${this.state.day} de ${months[this.state.month - 1]} del ${this.state.year}`;
  }
  async _cacheResourcesAsync() {
    const images = [
      shutterstock
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }

    return (
      <View style={styles.root}>
        <ScrollView style={{ flex: 1 }}>
          <RkText style={styles.textResults} >Resultados</RkText>
          <RkText style={styles.textDate} >{this.getSTRDate()}</RkText>
          <View style={styles.root}>
            <Scoreboard game={this.state.game} dateArray={this.state.dateArray} />
          </View>
          <View style={{ flex: 1 }}>
            <QuinielaScore game={this.state.game} dateArray={this.state.dateArray} />
          </View>
          <View style={{ flex: 1 }}>
            <DeerChallengeScore game={this.state.game} dateArray={this.state.dateArray} />
          </View>

        </ScrollView>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
  title: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(20),
    padding: scaleVertical(15),
  },
  content: {
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(15),
    padding: scaleVertical(15)
  },
  boton: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45),
  },
  textResults: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(25),
    paddingTop: scaleVertical(15),
    paddingHorizontal: scaleVertical(15),
  },
  textDate: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(25),
    paddingTop: scaleVertical(5),
    paddingBotton: scaleVertical(10),
    paddingHorizontal: scaleVertical(15),
  }
}));


DetailsOfPreviousGames.propTypes = {
  navigation: PropTypes.object,
};


const mapStateToProps = state => ({ prop: state.prop });

export default connect(
  mapStateToProps
)(DetailsOfPreviousGames);
