import React from 'react';
import { RkStyleSheet, RkTabView, RkText } from 'react-native-ui-kitten';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { DoubleBounce } from 'react-native-loader';
import { scaleVertical, size } from '../../utils/scale';
import MyDeerChallenge from './deerChallenge';
import { MyPreviousGames } from './previous';
import MyQuinielas from './quiniela';


class MyGames extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      roots: ['Quiniela', 'Reto venados', 'Anteriores'],
      root: 0,
      status: '',
      isReady: true,
      quality: '',
      game: false
    };
    const options = {
      title: 'MIS JUEGOS',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color="#eb0029" />
        </View>
      );
    }

    return (
      <View style={styles.root} >
        <View style={styles.header} >
          <RkText style={{ fontSize: scaleVertical(15) }}>{this.state.currentGame}</RkText>
          <RkText style={styles.headerTxt}>Mis juegos</RkText>
        </View>
        <View style={{ paddingHorizontal: scaleVertical(15), flex: 1 }} >
          <RkTabView
            maxVisibleTabs={1.7}
            rkType='standing'>


            <RkTabView.Tab title={'Quiniela'}>
              <MyQuinielas game={this.props.currentActiveGame} navigation={this.props.navigation} />
            </RkTabView.Tab>

            <RkTabView.Tab title={'Reto venados'} >
              <MyDeerChallenge game={this.props.currentActiveGame} navigation={this.props.navigation} />
            </RkTabView.Tab>

            <RkTabView.Tab title={'Anteriores'} >
              <MyPreviousGames game={this.props.currentActiveGame} navigation={this.props.navigation} />
            </RkTabView.Tab>
          </RkTabView>
        </View>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    height: scaleVertical(60),
    justifyContent: 'center',
    paddingHorizontal: scaleVertical(15),
  },
  headerTxt: {
    fontSize: scaleVertical(30),
    fontFamily: 'TTPollsBold',
    textAlignVertical: 'center'
  },
}));

const mapStateToProps = state => ({
  currentActiveGame: state.reducerGame
});


export default connect(
  mapStateToProps
)(MyGames);

