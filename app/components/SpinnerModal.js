import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, View, ActivityIndicator } from 'react-native';
import { Text } from './Text';


class SpinnerModal extends Component {
  render() {
    return (
      <Modal
        animationType="fade"
        visible={this.props.visible}
        transparent
        onRequestClose={() => {}}
        supportedOrientations={['portrait', 'landscape']}
      >
        <View style={styles.container}>
          <ActivityIndicator animating style={styles.activityIndicator} size="large" />
          <Text style={styles.text}>{ this.props.label }</Text>
        </View>
      </Modal>
    );
  }
}


const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    opacity: 0.8
  },
  text: {
    color: 'white'
  },
  activityIndicator: {
    height: 80
  }
};


SpinnerModal.propTypes = {
  visible: PropTypes.bool,
  label: PropTypes.string
};


export default SpinnerModal;
