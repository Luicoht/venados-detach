import { takeSnapshotAsync } from 'expo';
import { uploadImage } from '../Store/Services/Firebase';
import { FACEBOOKSDK } from '../constants';

const { API_URL, API_VERSION, FIELDS } = FACEBOOKSDK;
const image = 'https://image.ibb.co/nAFDr9/36443430-1048267648659021-8108797108368179200-n.jpg';

const facebookApi = (token, callback) => {
  fetch(`${API_URL}/${API_VERSION}/me?fields=${FIELDS}&access_token=${token}`) // eslint-disable-line
    .then((response) => response.json())
    .then((json) => {
      callback(null, json);
    })
    .catch(() => {
      callback('ERROR GETTING DATA FROM FACEBOOK', null);
    });
};

const friendsRequest = (token, callback) => {
  fetch(`${API_URL}/${API_VERSION}/me/friends?fields=picture,email,name,id&access_token=${token}`) // eslint-disable-line
    .then((response) => response.json())
    .then((json) => {
      callback(null, json);
    })
    .catch(() => {
      callback('ERROR GETTING DATA FROM FACEBOOK', null);
    });
};

const facebookApi2 = (token, callback) => {
  fetch(`${API_URL}/${API_VERSION}/me?photos`, { // eslint-disable-line
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      access_token: token,
      source: image,
      message: 'POST DE PRUEBA :D'
    })
  }) // eslint-disable-line
    .then((response) => response.json())
    .then((json) => {
      callback(null, json);
    })
    .catch(() => {
      callback('ERROR GETTING DATA FROM FACEBOOK', null);
    });
};


export const uploadPhoto = (token) => {
  const friends = new Promise((resolve, reject) => {
    facebookApi2(token, (error, userData) => {
      if (error) {
        reject(error);
      } else {
        resolve(userData);
      }
    });
  });

  return friends;
};

export const getFriends = (token) => {
  const friends = new Promise((resolve, reject) => {
    friendsRequest(token, (error, friendList) => {
      if (error) {
        reject(error);
      } else {
        resolve(friendList);
      }
    });
  });

  return friends;
};

export const getData = (token) => {
  const friends = new Promise((resolve, reject) => {
    facebookApi(token, (error, userData) => {
      if (error) {
        reject(error);
      } else {
        resolve(userData);
      }
    });
  });

  return friends;
};

export const logOutFacebook = (token, idUser) => {
  const LogOut = new Promise((resolve, reject) => {
      fetch(`${API_URL}/${API_VERSION}/${idUser}/permissions`, { // eslint-disable-line
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        access_token: token,
      }),
    })
      .then((response) => response.json())
      .then((json) => {
        resolve(json);
      })
      .catch((error) => {
        reject(`ERROR GETTING DATA FROM FACEBOOK: ${error}`);
      });
  });

  return LogOut;
};

export const encodeHTMLURL = (uri) => {
  const enodedUri = encodeURIComponent(uri);
  return enodedUri;
};

const getUserPosts = (token, callback) => {
  fetch(`${API_URL}/${API_VERSION}/me?fields=posts{place, created_time}&access_token=${token}`) // eslint-disable-line
    .then((response) => response.json())
    .then((json) => {
      callback(null, json.posts.data);
    })
    .catch((err) => {
      console.log('err :', err);
      callback('ERROR GETTING DATA FROM FACEBOOK', null);
    });
};


export const findCheckinPost = (params, callback) => {
  const { facebookToken, openingDate } = params;

        const checkCreateTime = (ps) => {
        const { created_time, place } = ps;
        const sharedTime = new Date(created_time.split('+')[0]);

        return place && compare_dates(openingDate, sharedTime);
      };
  getUserPosts(facebookToken, (error, posts) => {
    if (!error) {
      const checkInPosts = posts.filter(checkCreateTime);

      console.log('checkInPosts :', checkInPosts);

      if (checkInPosts.length) {
        const postToVerify = checkInPosts[0];
        callback(null, postToVerify);
      } else {
        callback('NO SE ENCONTRO POST');
      }
    } else {
      callback(error);
    }
  });
};

export const takeSnapshot = async (reactDom) => {
  const result = await takeSnapshotAsync(reactDom, {
    result: 'tmpfile',
    height: 600,
    width: 300,
    quality: 1,
    format: 'jpg',
  });

  const promise = new Promise((resolve) => {
    takeImageUri(result, res => {
      resolve(res);
    });
  });

  return promise;
};


const takeImageUri = async (result, cb) => {
  try {
    const blob = await urlToBlob(result); // eslint-disable-line
    const imgName = Math.random().toString(36).slice(-8);
    uploadImage(blob, imgName, (img) => {
      const resImg = img || 'Not defined';
      cb(resImg);
    });
  } catch (err) { console.log('err :', err); }
};

const compare_dates = (date1, date2) => {
  if (date1.getFullYear() === date2.getFullYear()) {
    if (date1.getMonth() === date2.getMonth()) {
      if (date1.getDate() >= date2.getDate()) {
        if (date1.getMinutes() === date2.getMinutes()) {
          console.log('date1.getSeconds(), date2.ge :', date1.getMinutes(), date2.getMinutes());
          return true;
        }
      }
    }
  }

  return false;
};

const urlToBlob = (url) => {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest(); // eslint-disable-line
    xhr.onerror = reject;

    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        resolve(xhr.response);
      }
    };

    xhr.open('GET', url);
    xhr.responseType = 'blob'; // convert type
    xhr.send();
  });
};
