import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Thumbnail, Left, Right, Body, CardItem, Text } from 'native-base';
import { RkButton } from 'react-native-ui-kitten';

export default class friendsList extends Component {
  render() {
    const { picture, name } = this.props.friend;
    return (
      <CardItem>
        <Left style={{ flex: 1, }}>
          <Thumbnail
            style={{ height: picture.data.height, width: picture.data.width }}
            source={{ uri: picture.data.url }}
          />
        </Left>
        <Body style={{ flex: 3 }}>
          <Text style={{ textAlign: 'left', color: 'black', fontSize: 14 }}>
            { name }
          </Text>
          <Text note> SCORE: 100,000 </Text>
        </Body>
        <Right style={{ flex: 1 }}>
          <RkButton
            style={{ backgroundColor: 'blue', width: 100 }}
            contentStyle={{ color: 'white', fontSize: 16 }}
          >
            INVITAR
          </RkButton>
        </Right>
      </CardItem>
    );
  }
}

friendsList.propTypes = {
  friend: PropTypes.object,
};
