import React from 'react';
import { View, ScrollView, Image, ImageBackground, TouchableOpacity, Alert, UIManager, LayoutAnimation } from 'react-native';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { Asset, AppLoading } from 'expo';
import PropTypes from 'prop-types';
import { TextField } from 'react-native-material-textfield';
import Modal from 'react-native-modal';
import { scale, scaleVertical, size } from '../../utils/scale';
import dont from '../../assets/icons/dont.png';
import challengeImage from '../../assets/icons/reto-3.png';
import bonusImage from '../../assets/icons/bonus-3.png';
import { consultSimpleBetDeerChallenge, consultBonusBetDeerChallenge, playSimpleBetDeerChallenge, playBonusBetDeerChallenge, formatNumber, getWalletPoints } from '../../Store/Services/Socket';


class ModalSimpleBetDeerChallenge extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalSimpleHitVisible: false,
      isModalSimpleOutVisible: false,

      isModalExtraHitVisible: false,
      isModalExtraOutVisible: false,


      isModalNotPartnerVisible: false,


      isModalInPlayVisible: false,

      game: false,
      pointsToWin: 0,
      pointsToBet: 0,
      bonusInfo: false,
      walletPoints: false
    };
  }
  componentDidMount() {
    this._cacheResourcesAsync();
    getWalletPoints()
      .then((wallet) => {
        // console.log('getWalletPoints', wallet);
        this.setState({ walletPoints: wallet.points });
      })
      .catch((err) => { console.log('error', err); });
  }

  componentWillUpdate(nextProps, nextState) {
    let animate = false;

    if (this.state.isReady !== nextState.isReady || this.state.walletPoints !== nextState.walletPoints) {
      animate = true;
    }

    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }

      LayoutAnimation.easeInEaseOut();
    }
  }


  onChanged(text) {
    let newText = '';
    const numbers = '0123456789';

    for (let i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        newText += text[i];
      } else {
        // your call back function
        console.log('please enter numbers only');
      }
    }
    this.setState({ pointsToBet: newText });
  }

  async _cacheResourcesAsync() {
    const images = [
      dont,
      challengeImage,
      bonusImage
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }


  _consultSimpleBetDeerChallenge(event) {
    const sendData = {
      playerID: this.props.player.id,
      points: this.state.pointsToBet,
      event
    };
    consultSimpleBetDeerChallenge(sendData)
      .then((result) => {
        const points = parseInt(result.gain, 0);
        console.log('result', result);
        console.log('points', points);
        this.setState({ pointsToWin: points });
      })
      .catch(() => {
        this.setState({
          consultSimpleBetDeerChallenge: []
        });
      });
  }
  _consultBonusBetDeerChallenge(event) {
    console.log('consultBonusBetDeerChallenge ', this.state.pointsToBet);
    const sendData = {
      playerID: this.props.player.id,
      points: this.state.pointsToBet,
      event: event
    };
    consultBonusBetDeerChallenge(sendData)
      .then((result) => {
        const points = parseInt(result.gain, 0);
        this.setState({ pointsToWin: points });
      })
      .catch(() => {
        this.setState({
          consultBonusBetDeerChallenge: []
        });
      });
  }


  _toggleModalHitSimple() {
    this.setState({ pointsToBet: 0 });
    this.setState({ pointsToWin: 0 });
    this.setState({ isModalSimpleHitVisible: !this.state.isModalSimpleHitVisible });
  }

  _toggleModalOutSimple() {
    this.setState({ pointsToBet: 0 });
    this.setState({ pointsToWin: 0 });
    this.setState({ isModalSimpleOutVisible: !this.state.isModalSimpleOutVisible });
  }


  _toggleModalOutExtra() {
    console.log('_toggleModalOutExtra');
    this.setState({ isModalExtraOutVisible: !this.state.isModalExtraOutVisible });
  }

  _toggleModalHitExtra() {
    console.log('_toggleModalHitExtra');
    this.setState({ isModalExtraHitVisible: !this.state.isModalExtraHitVisible });
  }

  _closeModalOutExtra() {
    this.setState({ isModalExtraOutVisible: !this.state.isModalExtraOutVisible });
    this.setState({ isModalInPlayVisible: !this.state.isModalInPlayVisible });

  }
  _closeModalHitExtra() {
    this.setState({ isModalExtraHitVisible: !this.state.isModalExtraHitVisible });
    this.setState({ isModalInPlayVisible: !this.state.isModalInPlayVisible });

  }
  _toggleModalIsOnPlay() {
    console.log('_toggleModalIsOnPlay');
    this.setState({ isModalInPlayVisible: !this.state.isModalInPlayVisible });
  }

  _toggleIsModalNotPartnerVisible() {
    // console.log('  _toggleIsModalNotPartnerVisible');
    this.setState({ isModalNotPartnerVisible: !this.state.isModalNotPartnerVisible });
  }




  _playSimpleDeerChallenge(event) {
    if (this.state.pointsToBet === 0) {
      Alert.alert(
        'Error',
        '¿Cuantos puntos quieres jugar?',
      );
      return;
    }
    const sendData = {
      playerID: this.props.player.id,
      points: this.state.pointsToBet,
      event,
    };

    if (this.props.profile.data.userType !== 6) {
      this._toggleIsModalNotPartnerVisible();
      return;
    }

    playSimpleBetDeerChallenge(sendData)
      .then((bonusInfo) => {
        const points = this.state.walletPoints - this.state.pointsToBet;
        this.setState({ walletPoints: points });
        this.setState({ bonusInfo: bonusInfo });
        if (event === 1) {
          console.log('bonusInfo this.state.bonusInfo hit', this.state.bonusInfo);
          console.log('bonusInfo this.state.bonusInfo hit', this.state.bonusInfo);
          console.log('bonusInfo this.state.bonusInfo hit', this.state.bonusInfo);
          console.log('bonusInfo this.state.bonusInfo hit', this.state.bonusInfo);

          this._toggleModalHitSimple();
          this._toggleModalHitExtra();
        } else {
          console.log('bonusInfo this.state.bonusInfo out', this.state.bonusInfo);
          console.log('bonusInfo this.state.bonusInfo out', this.state.bonusInfo);
          console.log('bonusInfo this.state.bonusInfo out', this.state.bonusInfo);
          console.log('bonusInfo this.state.bonusInfo out', this.state.bonusInfo);

          this._toggleModalOutSimple();
          this._toggleModalOutExtra();
        }
      })
      .catch(() => {
      });
  }

  _playExtraDeerChallenge(event) {
    if (this.state.pointsToBet === 0) {
      Alert.alert(
        'Error',
        '¿Cuantos puntos quieres jugar?',
      );
      return;
    }

    if (!this.state.bonusInfo) {
      Alert.alert(
        'Error',
        'Ocurrio algo inesperado',
      );
      return;
    }
    const sendData = {
      points: this.state.pointsToBet,
      id: this.state.bonusInfo.id
    };
    playBonusBetDeerChallenge(sendData)
      .then((bonusInfo) => {
        const points = this.state.walletPoints - this.state.pointsToBet;
        this.setState({ walletPoints: points });
        console.log('estas jugando bonus', bonusInfo);
        if (event === 1) {
          this._toggleModalHitExtra();
          this._toggleModalIsOnPlay();
        } else {
          this._toggleModalOutExtra();
          this._toggleModalIsOnPlay();
        }
      })
      .catch(() => {
      });
  }

  _renderModalSimpleOut() {
    return (
      <Modal isVisible={this.state.isModalSimpleOutVisible}>
        <View style={styles.modalTrasparentContent}>
          <View style={{ flex: 1, backgroundColor: '#002157' }}>
            <View style={{ flexDirection: 'row', height: size.width / 8 }}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleModalOutSimple(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground resizeMode="contain" style={styles.dontImageStyle} source={dont} />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5, backgroundColor: 'transparent' }} />
            </View>
            <ScrollView style={{ flex: 1 }}>
              <View style={{ flex: 1 }}>
                <View style={{ height: scaleVertical(80), alignItems: 'center' }}>
                  <Image style={{ flex: 1, resizeMode: 'center', alignSelf: 'center' }} source={challengeImage} />
                </View>
                <View style={{ flex: 1 }}>
                  <RkText style={{
                    fontFamily: 'TTPollsBoldItalic',
                    fontSize: scaleVertical(28),
                    textAlign: 'center',
                    color: '#fff'
                  }}>Reto Venados</RkText>
                  <RkText style={{
                    fontFamily: 'Roboto-Regular',
                    fontSize: scaleVertical(18),
                    textAlign: 'center',
                    color: '#fff'
                  }}>¿Cuántos puntos a que tu jugador le dan un out?</RkText>
                </View>
                <View style={{ flex: 0.75, flexDirection: 'row' }}>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <RkText style={{
                      fontFamily: 'Roboto-Light',
                      fontSize: scaleVertical(14),
                      textAlign: 'center',
                      color: '#fff'
                    }}>TIENES</RkText>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#6dcff6', height: scaleVertical(30) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Bold',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                        color: '#fff'
                      }}>{formatNumber(this.state.walletPoints)}</RkText>
                    </View>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <RkText style={{
                      fontFamily: 'Roboto-Light',
                      fontSize: scaleVertical(14),
                      textAlign: 'center',
                      color: '#fff'
                    }}>PUNTOS</RkText>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: 'row' }}>


                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }} >


                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                        color: '#fff'
                      }}>LÍNEA DE HIT</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#6dcff6', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{this.props.player.lineHit}</RkText>
                      </View>
                    </View>
                  </View>


                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                        color: '#fff'
                      }}>PUNTOS A GANAR</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#6dcff6', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{formatNumber(this.state.pointsToWin)}</RkText>
                      </View>
                    </View>
                  </View>



                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }} >


                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                        color: '#fff'
                      }}>LÍNEA DE OUT</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#6dcff6', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{this.props.player.lineOut}</RkText>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, paddingHorizontal: scaleVertical(15) }}>
                    <TextField
                      textColor='#fff'
                      tintColor='#fff'
                      baseColor='#fff'
                      onBlur={() => console.log('onBlur Ingresa puntos')}
                      onFocus={() => console.log('onFocus Ingresa puntos')}
                      label='Ingresa puntos'
                      keyboardType='numeric'
                      onChangeText={(text) => this.onChanged(text)}
                      value={this.state.pointsToBet}
                      error=''
                    />
                  </View>
                  <View style={{ flex: 1, alignItems: 'center', alignSelf: 'center' }}>
                    <RkButton onPress={() => { this._consultSimpleBetDeerChallenge(2); }} style={styles.botonCalcular}>
                      <RkText style={{ textAlign: 'center', color: '#fff', fontFamily: 'Roboto-Regular', fontSize: scaleVertical(12) }}>CALCULAR</RkText>
                    </RkButton>
                  </View>
                </View>
                <View style={{ flex: 1, paddingVertical: scale(25) }}>
                  <RkButton onPress={() => { this._playSimpleDeerChallenge(2); }} style={styles.boton}> JUGAR </RkButton>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }

  _renderModalExtraOut() {
    return (
      <Modal isVisible={this.state.isModalExtraOutVisible}>
        <View style={styles.modalTrasparentContent}>
          <View style={{ flex: 1, backgroundColor: '#002157' }}>
            <View style={{ flexDirection: 'row', height: size.width / 8 }}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._closeModalOutExtra(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground resizeMode="contain" style={styles.dontImageStyle} source={dont} />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5, backgroundColor: 'transparent' }} />
            </View>
            <ScrollView style={{ flex: 1 }}>
              <View style={{ flex: 1 }}>
                <View style={{ height: scaleVertical(80), alignItems: 'center' }}>
                  <Image style={{ flex: 1, resizeMode: 'center', alignSelf: 'center' }} source={bonusImage} />
                </View>
                <View style={{ flex: 1 }}>
                  <RkText style={{
                    fontFamily: 'TTPollsBoldItalic',
                    fontSize: scaleVertical(28),
                    textAlign: 'center',
                    color: '#fff'
                  }}>Reto Venados</RkText>
                  <RkText style={{
                    fontFamily: 'Roboto-Regular',
                    fontSize: scaleVertical(18),
                    textAlign: 'center',
                    color: '#fff'
                  }}>¿Cuántos puntos a que tu jugador será ponchado?</RkText>
                </View>
                <View style={{ flex: 0.75, flexDirection: 'row' }}>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <RkText style={{
                      fontFamily: 'Roboto-Light',
                      fontSize: scaleVertical(14),
                      textAlign: 'center',
                      color: '#fff'
                    }}>TIENES</RkText>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#6dcff6', height: scaleVertical(30) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Bold',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                        color: '#fff'
                      }}>{formatNumber(this.state.walletPoints)}</RkText>
                    </View>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <RkText style={{
                      fontFamily: 'Roboto-Light',
                      fontSize: scaleVertical(14),
                      textAlign: 'center',
                      color: '#fff'
                    }}>PUNTOS</RkText>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: 'row' }}>


                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                        color: '#fff'
                      }}>LÍNEA DE HIT</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#6dcff6', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{this.props.player.lineHit}</RkText>
                      </View>
                    </View>
                  </View>



                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                        color: '#fff'
                      }}>PUNTOS A GANAR</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#6dcff6', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{formatNumber(this.state.pointsToWin)}</RkText>
                      </View>
                    </View>
                  </View>


                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                        color: '#fff'
                      }}>LÍNEA DE OUT</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#6dcff6', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{this.props.player.lineOut}</RkText>
                      </View>
                    </View>
                  </View>


                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, paddingHorizontal: scaleVertical(15) }}>
                    <TextField
                      textColor='#fff'
                      tintColor='#fff'
                      baseColor='#fff'
                      onBlur={() => console.log('onBlur Ingresa puntos')}
                      onFocus={() => console.log('onFocus Ingresa puntos')}
                      label='Ingresa puntos'
                      keyboardType='numeric'
                      onChangeText={(text) => this.onChanged(text)}
                      value={this.state.pointsToBet}
                      error=''
                    />
                  </View>
                  <View style={{ flex: 1, alignItems: 'center', alignSelf: 'center' }}>
                    <RkButton onPress={() => { this._consultBonusBetDeerChallenge(2); }} style={styles.botonCalcular}>
                      <RkText style={{ textAlign: 'center', color: '#fff', fontFamily: 'Roboto-Regular', fontSize: scaleVertical(12) }}>CALCULAR</RkText>
                    </RkButton>
                  </View>
                </View>
                <View style={{ flex: 1, paddingVertical: scale(25) }}>
                  <RkButton onPress={() => { this._playExtraDeerChallenge(2); }} style={styles.boton}> JUGAR </RkButton>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }



  _renderModalIsInPlay() {
    return (
      <Modal isVisible={this.state.isModalInPlayVisible}>
        <View style={styles.modalTrasparentContent}>
          <View style={{ flex: 1, backgroundColor: '#002157' }}>
            <View style={{ flexDirection: 'row', height: size.width / 8 }}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleModalIsOnPlay(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground resizeMode="contain" style={styles.dontImageStyle} source={dont} />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5, backgroundColor: 'transparent' }} />
            </View>
            <ScrollView style={{ flex: 1 }}>
              <View style={{ flex: 1 }}>
                <View style={{ height: scaleVertical(80), alignItems: 'center' }}>
                  <Image style={{ flex: 1, resizeMode: 'center', alignSelf: 'center' }} source={bonusImage} />
                </View>
                <View style={{ flex: 1 }}>
                  <RkText style={{
                    fontFamily: 'TTPollsBoldItalic',
                    fontSize: scaleVertical(28),
                    textAlign: 'center',
                    color: '#fff'
                  }}>Jugando Reto venados</RkText>
                  <RkText style={{
                    fontFamily: 'TTPollsBoldItalic',
                    fontSize: scaleVertical(28),
                    textAlign: 'center',
                    color: '#fff'
                  }}>CON</RkText>

                  <RkText style={{
                    fontFamily: 'TTPollsBoldItalic',
                    fontSize: scaleVertical(28),
                    textAlign: 'center',
                    color: '#fff'
                  }}>{this.props.player.name}</RkText>

                </View>


                <View style={{ flex: 1, paddingVertical: scale(25) }}>
                  <RkButton onPress={() => { this._toggleModalIsOnPlay(); }} style={styles.boton}> ACEPTAR </RkButton>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }

  _rederModalSimpleHit() {
    return (
      <Modal isVisible={this.state.isModalSimpleHitVisible}>
        <View style={styles.modalTrasparentContent}>
          <View style={{ flex: 1, backgroundColor: '#6dcff6' }}>
            <View style={{ flexDirection: 'row', height: size.width / 8 }}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleModalHitSimple(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground resizeMode="contain" style={styles.dontImageStyle} source={dont} />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5, backgroundColor: 'transparent' }} />
            </View>
            <ScrollView style={{ flex: 1 }}>

              <View style={{ flex: 1 }}>
                <View style={{ height: scaleVertical(80), alignItems: 'center' }}>

                  <Image style={{ flex: 1, resizeMode: 'center', alignSelf: 'center' }} source={challengeImage} />
                </View>
                <View style={{ flex: 1 }}>
                  <RkText style={styles.deerText}>Reto Venados</RkText>
                  <RkText style={styles.deerSubText}>¿Cuántos puntos a que tu jugador anota hit?</RkText>
                </View>
                <View style={{ flex: 0.75, flexDirection: 'row' }}>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <RkText style={styles.haveText}>TIENES</RkText>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#2e3192', height: scaleVertical(30) }}>
                      <RkText style={styles.myPointsWhite}>{formatNumber(this.state.walletPoints)}</RkText>
                    </View>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <RkText style={{
                      fontFamily: 'Roboto-Light',
                      fontSize: scaleVertical(14),
                      textAlign: 'center'
                    }}>PUNTOS</RkText>
                  </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>


                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                      }}>LÍNEA DE HIT</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#2e3192', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{this.props.player.lineHit}</RkText>
                      </View>
                    </View>
                  </View>



                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                      }}>PUNTOS A GANAR</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#2e3192', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{formatNumber(this.state.pointsToWin)}</RkText>
                      </View>
                    </View>
                  </View>



                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                      }}>LÍNEA DE OUT</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#2e3192', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{this.props.player.lineOut}</RkText>
                      </View>
                    </View>
                  </View>

                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, paddingHorizontal: scaleVertical(15) }}>
                    <TextField
                      textColor='black'
                      tintColor='black'
                      baseColor='black'
                      onBlur={() => console.log('onBlur Ingresa puntos')}
                      onFocus={() => console.log('onFocus Ingresa puntos')}
                      label='Ingresa puntos'
                      keyboardType='numeric'
                      onChangeText={(text) => this.onChanged(text)}
                      value={this.state.pointsToBet}
                      error='' />
                  </View>
                  <View style={{ flex: 1, alignItems: 'center', alignSelf: 'center' }}>
                    <RkButton style={styles.botonCalcular} onPress={() => { this._consultSimpleBetDeerChallenge(1); }}>
                      <RkText style={{ textAlign: 'center', color: 'black', fontFamily: 'Roboto-Regular', fontSize: scaleVertical(12) }}>CALCULAR</RkText>
                    </RkButton>
                  </View>
                </View>
                <View style={{ flex: 1, paddingVertical: scale(25) }}>
                  <RkButton onPress={() => { this._playSimpleDeerChallenge(1); }} style={styles.boton}> JUGAR </RkButton>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
  _rederModalExtraHit() {
    return (
      <Modal isVisible={this.state.isModalExtraHitVisible}>
        <View style={styles.modalTrasparentContent}>
          <View style={{ flex: 1, backgroundColor: '#6dcff6' }}>
            <View style={{ flexDirection: 'row', height: size.width / 8 }}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._closeModalHitExtra(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground resizeMode="contain" style={styles.dontImageStyle} source={dont} />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5, backgroundColor: 'transparent' }} />
            </View>
            <ScrollView style={{ flex: 1 }}>

              <View style={{ flex: 1 }}>
                <View style={{ height: scaleVertical(80), alignItems: 'center' }}>

                  <Image style={{ flex: 1, resizeMode: 'center', alignSelf: 'center' }} source={bonusImage} />
                </View>
                <View style={{ flex: 1 }}>
                  <RkText style={styles.deerText}>Reto Venados</RkText>
                  <RkText style={styles.deerSubText}>¿Cuántos puntos a que tu jugador anota Homerun?</RkText>
                </View>
                <View style={{ flex: 0.75, flexDirection: 'row' }}>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <RkText style={styles.haveText}>TIENES</RkText>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#2e3192', height: scaleVertical(30) }}>
                      <RkText style={styles.myPointsWhite}>{formatNumber(this.state.walletPoints)}</RkText>
                    </View>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <RkText style={{
                      fontFamily: 'Roboto-Light',
                      fontSize: scaleVertical(14),
                      textAlign: 'center'
                    }}>PUNTOS</RkText>
                  </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                      }}>LÍNEA DE HIT</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#2e3192', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{this.props.player.lineHit}</RkText>
                      </View>
                    </View>
                  </View>


                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                      }}>PUNTOS A GANAR</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#2e3192', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{formatNumber(this.state.pointsToWin)}</RkText>
                      </View>
                    </View>
                  </View>

                  <View style={{ flex: 1, justifyContent: 'center', paddingVertical: scaleVertical(15) }}>
                    <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: scaleVertical(15) }}>
                      <RkText style={{
                        fontFamily: 'Roboto-Light',
                        fontSize: scaleVertical(14),
                        textAlign: 'center',
                      }}>LÍNEA DE OUT</RkText>
                      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#2e3192', height: scaleVertical(30) }}>
                        <RkText style={{
                          fontFamily: 'Roboto-Bold',
                          fontSize: scaleVertical(14),
                          textAlign: 'center',
                          color: '#fff'
                        }}>{this.props.player.lineOut}</RkText>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ flex: 1, paddingHorizontal: scaleVertical(15) }}>
                    <TextField
                      textColor='black'
                      tintColor='black'
                      baseColor='black'
                      onBlur={() => console.log('onBlur Ingresa puntos')}
                      onFocus={() => console.log('onFocus Ingresa puntos')}
                      label='Ingresa puntos'
                      keyboardType='numeric'
                      onChangeText={(text) => this.onChanged(text)}
                      value={this.state.pointsToBet}
                      error=''
                    />
                  </View>
                  <View style={{ flex: 1, alignItems: 'center', alignSelf: 'center' }}>
                    <RkButton style={styles.botonCalcular} onPress={() => { this._consultBonusBetDeerChallenge(1); }}>
                      <RkText style={{ textAlign: 'center', color: 'black', fontFamily: 'Roboto-Regular', fontSize: scaleVertical(12) }}>CALCULAR</RkText>
                    </RkButton>
                  </View>
                </View>
                <View style={{ flex: 1, paddingVertical: scale(25) }}>
                  <RkButton onPress={() => { this._playExtraDeerChallenge(1); }} style={styles.boton}> JUGAR </RkButton>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }



  _renderNotPartnerModal() {
    const modal = (
      <Modal isVisible={this.state.isModalNotPartnerVisible}>
        <View style={styles.modalTransparentContainer}>
          <View style={styles.modalBackground}>
            <View style={styles.modalHeader}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleIsModalNotPartnerVisible(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground
                    resizeMode="contain"
                    style={styles.modalCloseImage} source={dont}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5 }} />
            </View>
            <View style={styles.modalContent}>
              <View style={styles.modalDontImageContent}>
                <Image style={styles.modalDontImage} source={bigDont} />
              </View>
              <View
                style={{
                  paddingHorizontal: scaleVertical(15)
                }}
              >
                <RkText style={styles.modalHeaderText}>Aún no eres socio Venados</RkText>
                <RkText style={styles.modalInfoText}>Para poder jugar necesitas hacerte socio.</RkText>
              </View>
              <View style={{
                paddingHorizontal: scaleVertical(15),
                paddingVertical: scaleVertical(15),
              }}>
                <RkButton style={{
                  backgroundColor: '#eb0029',
                  alignSelf: 'center',
                  justifyContent: 'center',
                  height: size.width / 7,
                  width: size.width - scaleVertical(80),
                }} onPress={() => { this._toggleIsModalNotPartnerVisible(); }}>
                  <RkText style={{
                    fontSize: scaleVertical(18),
                    fontFamily: 'DecimaMono',
                    textAlign: 'center',
                    color: '#fff',
                    textAlignVertical: 'center'
                  }}>ACEPTAR</RkText>
                </RkButton>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );

    return modal;
  }
  render() {
    if (!this.state.isReady || !this.state.walletPoints) {
      return (
        <AppLoading
          onError={console.warn}
        />
      );
    }

    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1, paddingVertical: scale(2.5), flexDirection: 'row', justifyContent: 'space-around' }}>
          <RkButton
            onPress={() => { this._toggleModalHitSimple(); }} style={{
              paddingHorizontal: scale(2),
              backgroundColor: '#6dcff6',
              width: size.width / 5.5,
              height: scaleVertical(30)
            }}
          >
            <View>
              <RkText style={styles.buttonText}> HIT </RkText>
              <RkText style={styles.buttonText2}> {this.props.player.lineHit} </RkText>
            </View>
          </RkButton>
          <RkButton
            onPress={() => { this._toggleModalOutSimple(); }} style={{
              paddingVertical: scaleVertical(5),
              paddingHorizontal: scale(2),
              backgroundColor: '#002157',
              width: size.width / 5.5,
              height: scaleVertical(30)
            }}
          >
            <View>
              <RkText style={styles.buttonText}> OUT  </RkText>
              <RkText style={styles.buttonText2}> {this.props.player.lineOut}  </RkText>
            </View>
          </RkButton>
          {this._rederModalSimpleHit()}
          {this._renderModalSimpleOut()}
          {this._renderModalIsInPlay()}
          {this._renderModalExtraOut()}
          {this._rederModalExtraHit()}
          {this._renderNotPartnerModal()}
        </View>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff'
  },
  header: {
    fontSize: scaleVertical(32),
    color: '#eb0029',
    fontFamily: 'TTPollsBold'
  },
  headerSection: {
    paddingVertical: scaleVertical(15),
    paddingHorizontal: scaleVertical(10)
  },
  subHeaderSection: {
    paddingVertical: scaleVertical(15),
    paddingHorizontal: scaleVertical(10)
  },
  subHeader: {
    fontSize: scaleVertical(18),
    fontFamily: 'TTPollsBold'
  },
  container: {
    paddingHorizontal: scaleVertical(10),
    flex: 1,
    flexDirection: 'row',
  },
  tableHeaderTextBlack: {
    height: scaleVertical(32),
    paddingVertical: scaleVertical(6),
    color: '#fff',
    fontFamily: 'Roboto-Bold',
    textAlign: 'center',
    fontSize: scaleVertical(14),
    backgroundColor: '#000',
  },
  tableHeaderTextRed: {
    height: scaleVertical(32),
    paddingVertical: scaleVertical(6),
    color: '#fff',
    fontFamily: 'Roboto-Bold',
    textAlign: 'center',
    fontSize: scaleVertical(14),
    backgroundColor: '#eb0029',
  },
  tableContentTextRed: {
    height: scaleVertical(32),
    paddingVertical: scaleVertical(6),
    color: '#fff',
    fontFamily: 'Roboto-Bold',
    textAlign: 'center',
    fontSize: scaleVertical(14),
  },
  tableContentTextBlack: {
    height: scaleVertical(32),
    paddingVertical: scaleVertical(6),
    color: '#fff',
    fontFamily: 'Roboto-Regular',
    textAlign: 'center',
    fontSize: scaleVertical(14),
  },
  tableBorder: {
    borderColor: '#9a9a9a',
    borderBottomWidth: scaleVertical(2),
    height: scaleVertical(40),
    flex: 1,
    flexDirection: 'row',
  },
  tableContentDeer: {
    height: scaleVertical(32),
    backgroundColor: '#eaeaea',
  },
  tableContentDetails: {
    height: scaleVertical(32),
    backgroundColor: '#fff',
  },
  boton: {
    backgroundColor: '#ed0a27',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45),
    paddingVertical: scaleVertical(25)
  },
  botonCalcular: {
    borderRadius: scale(5),
    alignSelf: 'center',
    backgroundColor: 'transparent',
    borderWidth: scaleVertical(2),
    borderColor: '#ed0a27',
    paddingHorizontal: scaleVertical(15)
  },
  headerText: {
    height: scaleVertical(40),
    paddingVertical: scaleVertical(6),
    color: '#eb0029',
    fontFamily: 'Roboto-Bold',
    paddingLeft: scaleVertical(4),
    fontSize: scaleVertical(14),
  },
  positionText: {
    height: scaleVertical(40),
    paddingVertical: scaleVertical(6),
    color: '#000',
    fontFamily: 'Roboto-Regular',
    textAlign: 'center',
    fontSize: scaleVertical(14)
  },
  whiteContainer: {
    flex: 5,
    height: scaleVertical(40),
    backgroundColor: '#e5e5e5'
  },
  grayContainer: {
    borderColor: '#9a9a9a',
    borderBottomWidth: scaleVertical(2),
    height: scaleVertical(40),
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: scaleVertical(5),
  },
  hitText: {
    color: '#fff',
    fontFamily: 'Roboto-Bold',
    fontSize: scaleVertical(12),
    textAlign: 'center',
  },
  outButton: {
    paddingVertical: scaleVertical(5),
    backgroundColor: '#002157',
    width: size.width / 5,
    height: scaleVertical(30)
  },
  buttonText: {
    color: '#fff',
    fontFamily: 'Roboto-Bold',
    fontSize: scaleVertical(12),
    textAlign: 'center',
  },
  buttonText2: {
    color: '#fff',
    fontFamily: 'Roboto-Bold',
    fontSize: scaleVertical(10),
    textAlign: 'center',
  },
  goToPlayerLink: {
    flex: 1,
    justifyContent: 'center',
  },
  seeText: {
    fontFamily: 'Roboto-Bold',
    color: '#00aeef',
    fontSize: scaleVertical(12),
    textAlign: 'center',
    textVerticalAlign: 'center'
  },
  fullModalContainerLightBlue: {
    flex: 1,
    paddingHorizontal: scaleVertical(5),
    paddingVertical: scaleVertical(size.height / 12)
  },
  deerText: {
    fontFamily: 'TTPollsBoldItalic',
    fontSize: scaleVertical(28),
    textAlign: 'center'
  },
  deerSubText: {
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(18),
    textAlign: 'center'
  },
  haveText: {
    fontFamily: 'Roboto-Light',
    fontSize: scaleVertical(14),
    textAlign: 'center'
  },
  myPointsWhite: {
    fontFamily: 'Roboto-Bold',
    fontSize: scaleVertical(14),
    textAlign: 'center',
    color: '#fff'
  },


  modalTrasparentContent: {
    flex: 1,
    backgroundColor: 'transparent',
    paddingHorizontal: scaleVertical(5),
    paddingVertical: scaleVertical(20),
    alignContent: 'center'
  },
  dontImageStyle: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  },
  modalTransparentContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    paddingHorizontal: scaleVertical(5),
    paddingVertical: scaleVertical(20),
    alignContent: 'center'
  },
  modalBackground: {
    flex: 1,
    backgroundColor: '#ebebeb',
  },
  modalBackgroundQuiniela: {
    flex: 1,
    backgroundColor: '#394353',
  },
  modalHeader: {
    flex: 1,
    flexDirection: 'row'
  },
  modalCloseImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  },
  modalContent: {
    flex: 10,
    alignItems: 'center',
  },
  modalDontImageContent: {
    padding: scaleVertical(15),
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalDontImage: {
    height: size.width / 4,
    width: size.width / 4,
    alignSelf: 'center'
  },
  modalHeaderText: {
    fontSize: scaleVertical(25),
    fontFamily: 'TTPollsBoldItalic',
    textAlign: 'center'
  },
  modalInfoText: {
    fontSize: scaleVertical(20),
    fontFamily: 'Roboto-Regular',
    textAlign: 'center'
  },
}));


ModalSimpleBetDeerChallenge.propTypes = {
  player: PropTypes.object
};


const mapStateToProps = state => ({
  profile: state.profileData,
});


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalSimpleBetDeerChallenge);
