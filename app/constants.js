// export const API_URL = 'http://venados-api.quody.mx'; // production url
export const API_URL = 'http://venados-api-dev.quody.mx'; // test url

export const FACEBOOKSDK = {
  API_URL: 'https://graph.facebook.com',
  API_VERSION: 'v3.1',
  FIELDS: 'id,friends{email,name,id},picture.type(large)',
  POST_FIELD: 'application,admin_creator,from,icon,picture,link,created_time,is_published,message,scheduled_publish_time,shares',
  PERMISSIONS: [
    'public_profile',
    'email',
    'user_posts',
    'user_friends',
  ],
};

export const FACEBOOK_UI = {
  URL: 'https://www.facebook.com/dialog/share?',
  // APP_ID: '170851646934308', // production id
  APP_ID: '1050481638485930', // test id
  sharedLinks: {
    shared1: 'https://reactjs.org/',
  },
};
