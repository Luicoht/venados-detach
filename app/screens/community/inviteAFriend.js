import React from 'react';
import { View, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { TextField } from 'react-native-material-textfield';
import { Asset, AppLoading } from 'expo';
import Modal from 'react-native-modal';
import { ShareDialog } from 'react-native-fbsdk';
import { scale, scaleVertical, size } from '../../utils/scale';
import { authentication, baseDeDatos } from '../../Store/Services/Firebase';
import shutterstock from '../../assets/JPEG/shutterstock_212667757.jpg';
import dont from '../../assets/icons/dont.png';
import { getWalletPoints } from '../../Store/Services/Socket';


class InviteAFriend extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isReady: false,
      user: '',
      wallet: '',
      isModalInviteFriendsVisible: false,
    };
  }

  componentWillMount() {
    getWalletPoints().then((wallet) => {
      this.setState({ wallet });
    }).catch((err) => {
      console.log('error', err);
    });

    baseDeDatos.ref('/profiles').orderByChild('email').equalTo(authentication.currentUser.providerData[0].email)
      .once('value')
      .then(snapshot => {
        this.setState({ user: Object.values(snapshot.val())[0] });
      });
  }


  // ----------------------
  // ------ methods -------
  // ----------------------
  onInviteFriendsByFacebook() {
    const shareLinkContent = {
      contentType: 'link',
      contentUrl: 'https://play.google.com/store/apps/details?id=com.quody.venados',
    };

    ShareDialog.canShow(shareLinkContent).then((canShow) => {
      if (canShow) {
        ShareDialog.show(shareLinkContent);
      }
    });
  }

  async cacheResourcesAsync() {
    const images = [dont, shutterstock];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());

    return Promise.all(cacheImages);
  }

  toggleIsModalInviteFriendsVisible() {
    this.setState({ isModalInviteFriendsVisible: !this.state.isModalInviteFriendsVisible });
  }

  renderInviteFriendsModal() {
    const modal = (
      <Modal isVisible={this.state.isModalInviteFriendsVisible}>
        <View style={styles.trasnparentContent}>
          <View style={styles.modalContent}>
            <View style={{ flexDirection: 'row', height: size.width / 8 }}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this.toggleIsModalInviteFriendsVisible(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground resizeMode='contain' style={styles.closeImage} source={dont} />
                </TouchableOpacity>
              </View>
              <View style={styles.jumpHeaderModal} />
            </View>

            <ScrollView style={{ flex: 1 }}>
              <View style={styles.contentModal}>
                <View style={{ paddingVertical: scaleVertical(15) }}>
                  <TextField
                    style={styles.textField}
                    textColor='white'
                    tintColor='white'
                    baseColor='white'
                    label='Nombre de tu amigo'
                  />

                  <TextField
                    style={styles.textField}
                    textColor='white'
                    tintColor='white'
                    baseColor='white'
                    label='Número de célular de tu amigo'
                  />
                </View>

                <View style={styles.modalFooter}>
                  <RkButton style={styles.buttonSend}>
                    <RkText style={styles.textSend}>ENVIAR</RkText>
                  </RkButton>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>);
    return modal;
  }

  renderInviteFriendsView() {
    const score = (
      <View style={{ backgroundColor: 'black' }}>
        <View style={{ height: size.width }}>
          <ImageBackground
            resizeMode='cover'
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              justifyContent: 'center'
            }} source={shutterstock}
          />

          <View style={{ height: size.width, backgroundColor: 'rgba(0, 0, 0, 0.7)', paddingHorizontal: scaleVertical(15) }}>
            <View style={{ paddingTop: size.width / 5, }}>
              <RkText style={{ alignSelf: 'center', color: '#fff', textAlign: 'center', fontFamily: 'TTPollsBoldItalic', fontSize: scaleVertical(25) }}>¡Invita a tus amigos a ser socios Venados y gana puntos!</RkText>
            </View>

            <View style={{ paddingVertical: scaleVertical(15) }}>
              <View style={{ paddingVertical: scaleVertical(10) }}>
                <RkButton style={{ width: size.widthMargin, height: scaleVertical(50), backgroundColor: '#eb0029', justifyContent: 'center', alignSelf: 'center' }} onPress={() => { this.toggleIsModalInviteFriendsVisible(); }}>
                  <RkText style={{ color: '#fff', textAlign: 'center', textVerticalAlign: 'center' }}>INGRESAR DATOS </RkText>
                </RkButton>
              </View>

              <View style={{ paddingVertical: scaleVertical(5) }}>
                <RkButton style={{ width: size.widthMargin, height: scaleVertical(50), backgroundColor: '#3b5998', justifyContent: 'center', alignSelf: 'center' }} onPress={() => { this.onInviteFriendsByFacebook(); }}>
                  <RkText style={{ color: '#fff', textAlign: 'center', textVerticalAlign: 'center' }}>INVITAR VÍA FACEBOOK </RkText>
                </RkButton>
              </View>
            </View>
          </View>
        </View>
      </View>
    );

    return score;
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this.cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }

    return (
      <View style={styles.root}>
        {this.renderInviteFriendsView()}
        {this.renderInviteFriendsModal()}
      </View>
    );
  }
}


const styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
  },
  closeImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  },
  trasnparentContent: {
    flex: 1,
    backgroundColor: 'transparent',
    paddingHorizontal: scaleVertical(5),
    paddingVertical: scaleVertical(20),
    alignContent: 'center',
    justifyContent: 'center'
  },
  modalContent: {
    backgroundColor: '#394353',
    height: size.width * 1.25
  },
  jumpHeaderModal: {
    flex: 5,
    backgroundColor: 'transparent'
  },
  contentModal: {
    flex: 1,
    paddingHorizontal: scaleVertical(15)
  },
  textField: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: scale(20),
    fontAlign: 'center'
  },
  modalFooter: {
    flex: 1,
    paddingHorizontal: scaleVertical(15),
    paddingVertical: scaleVertical(20),
  },
  buttonSend: {
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#eb0029',
    width: size.widthMargin - scaleVertical(60)
  },
  textSend: {
    textAlign: 'center',
    textVerticalAlign: 'center',
    color: '#fff',
    fontFamily: 'DecimaMono-Bold'
  }
}));


InviteAFriend.navigationOptions = {
  title: 'Comunidad'.toUpperCase(),
  hidden: false
};


export default InviteAFriend;
