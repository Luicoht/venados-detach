import React from 'react';
import { View, ScrollView, Image, UIManager, LayoutAnimation } from 'react-native';
import { RkText, RkStyleSheet, RkTabView } from 'react-native-ui-kitten';
import { Asset, AppLoading } from 'expo';
import { DoubleBounce } from 'react-native-loader';
import { scale, scaleVertical, size } from '../utils/scale';
import { getHistoryQuiniela, formatNumber } from '../Store/Services/Socket';
import c1b from '../assets/cards/1B.png';
import c2b from '../assets/cards/2B.png';
import c3b from '../assets/cards/3B.png';
import cc from '../assets/cards/C.png';
import ccf from '../assets/cards/CF.png';
import clf from '../assets/cards/LF.png';
import crf from '../assets/cards/RF.png';
import cp from '../assets/cards/P.png';
import css from '../assets/cards/SS.png';

export class QuinielaScore extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      quinielas: false,
      game: this.props.game,
      dateArray: this.props.dateArray,
    };
  }

  componentDidMount() {
    this._cacheResourcesAsync();
    getHistoryQuiniela(this.state.game)
      .then((quinielas) => {
        this.setState({ quinielas });
      })
      .catch((err) => { console.log('error', err); });
  }


  componentWillUpdate(nextProps, nextState) {
    let animate = false;
    if (this.state.isReady !== nextState.isReady || this.state.quinielas !== nextState.quinielas || this.state.game !== nextState.game || this.state.dateArray !== nextState.dateArray) {
      animate = true;
    }
    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      LayoutAnimation.easeInEaseOut();
    }
  }

  async _cacheResourcesAsync() {
    const images = [
      c1b,
      c2b,
      c3b,
      cc,
      ccf,
      clf,
      crf,
      cp,
      css
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  _getPrefig(count) {
    switch (count) {
      case 1:
        return 'ra.';
      case 2:
        return 'da.';
      case 3:
        return 'ra.';
      default:
        return 'ta.';
    }
  }

  _getRequire(name) {
    switch (name) {
      case '1B':
        return c1b;
      case '2B':
        return c2b;
      case '3B':
        return c3b;
      case 'C':
        return cc;
      case 'CF':
        return ccf;
      case 'LF':
        return clf;
      case 'RF':
        return crf;
      case 'P':
        return cp;
      default:
        return css;
    }
  }

  _getTablesView() {
    const tabs = (
      this.state.quinielas.map((quiniela) => {
        const internal = quiniela.cards.map((cardArray, colorID) => {
          let bgColor;
          let pointsToWin;
          switch (colorID) {
            case 0:
              bgColor = '#f05a28';
              pointsToWin = 160;
              break;
            case 1:
              bgColor = '#009344';
              pointsToWin = 400;
              break;
            default:
              bgColor = '#26aae0';
              pointsToWin = 800;
              break;
          }
          const colorCards = cardArray.map((card) => {
            const prefig = card.quantity >= 2 ? 'tarjetas' : 'tarjeta';
            const src = this._getRequire(card.position);
            const content = (
              <View style={{ flex: 1 }}>
                <View style={{ width: size.widthMargin / 3, padding: scaleVertical(4), justifyContent: 'center' }}>
                  <View style={{ flex: 1 }}>
                    <View style={{ height: size.widthMargin / 2.5, justifyContent: 'center' }}>
                      <Image style={{ alignSelf: 'center', resizeMode: 'contain' }} source={src} />
                    </View>
                    <View style={{ flex: 1 }}>
                      <RkText style={{ color: bgColor, fontSize: scaleVertical(12), textAlign: 'center' }}> Pudes ganar {formatNumber(pointsToWin * card.quantity)}</RkText>


                      <RkText style={{ color: bgColor, fontSize: scaleVertical(12), textAlign: 'center' }}> {card.quantity} {prefig} </RkText>
                      <RkText style={{ fontSize: scaleVertical(12), textAlign: 'center', color: bgColor }}> {card.name} </RkText>
                    </View>
                  </View>
                </View>
              </View>
            );
            return content;
          });
          return colorCards;
        });

        const view = (
          <RkTabView.Tab title={`${quiniela.inning + this._getPrefig(quiniela.inning)} quiniela`} >
            <ScrollView horizontal={true}>
              {internal}
            </ScrollView>
            <View style={{ flex: 1, backgroundColor: '#fff', paddingVertical: scaleVertical(15) }}>
              <RkText style={{ fontSize: scaleVertical(20), textAlign: 'center', color: '#2e3192' }}> {quiniela.winnerCards} TARJETAS GANADORAS </RkText>
              <RkText style={{ fontSize: scaleVertical(20), textAlign: 'center', color: '#2e3192' }}> {quiniela.winnerPoints} PUNTOS GANADOS </RkText>
            </View>
          </RkTabView.Tab>
        );
        return view;
      })
    );
    const table = (
      <View style={{ flex: 1 }}>
        <RkTabView rkType='standing' maxVisibleTabs={1.7}>
          {tabs}
        </RkTabView>
      </View>
    );
    return table;
  }

  _noGame() {
    const noGame = (
      <View style={{ flex: 1, backgroundColor: '#e1e1e1' }}>
        <View style={{ paddingVertical: scaleVertical(30), justifyContent: 'center' }}>
          <View style={{ flex: 1 }}>
            <RkText style={{ fontSize: scaleVertical(20), textAlign: 'center', color: '#2e3192' }}> NO PARTICIPASTE EN QUINIELA</RkText>
          </View>
        </View>
      </View>
    );
    return noGame;
  }


  render() {
    if (!this.state.isReady || !this.state.quinielas) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 6} color='#eb0029' />
        </View>
      );
    }

    const tables = this.state.quinielas.length !== 0 ? this._getTablesView() : this._noGame();
    return (
      <View style={styles.root}>
        <View style={{ flex: 1, backgroundColor: '#e1e1e1' }}>
          {tables}
        </View>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
  title: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(20),
    padding: scaleVertical(15),
  },
  date: {
    fontFamily: 'Roboto-Light',
    fontSize: scaleVertical(12),
    textAlign: 'right',
    padding: scaleVertical(10)
  },
  content: {
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(15),
    padding: scaleVertical(15)
  },

  boton: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45),
  },
}));
