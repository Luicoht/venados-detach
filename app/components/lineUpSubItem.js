import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Image, View, Platform, TouchableOpacity, ImageBackground } from 'react-native';
import _ from 'lodash';
import { RkText, RkStyleSheet, RkButton } from 'react-native-ui-kitten';
import { Asset, AppLoading } from 'expo';
import Modal from 'react-native-modal';
import { scale, scaleVertical, size, AppbarHeight } from '../utils/scale';
import subNav from '../assets/icons/line_up-3.png';
import dont from '../assets/icons/dont.png';
import bigDont from '../assets/icons/dont-4.png';

class LineUpSubItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isModalNotHaveGameVisible: false,
      game: false
    };
  }
  componentDidMount() {}

  _goTo(route) {
    if (this.props.currentActiveGame && this.props.currentActiveGame.finished === 1) {
      this._toggleIsModalNotHaveGameVisible();
      return;
    }

    if (this.props.navigation.state.routeName !== route) {
      if (route === 'DashboardMenu') {
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'DashboardMenu' })
          ]
        });
        this.props.navigation.dispatch(resetAction);
      } else {
        this.props.navigation.navigate(route);
      }
    }
  }

  _toggleIsModalNotHaveGameVisible() {
    // console.log('_toggleIsModalNotHaveGameVisible');
    this.setState({ isModalNotHaveGameVisible: !this.state.isModalNotHaveGameVisible });
  }

  async _cacheResourcesAsync() {
    const images = [
      subNav,
      dont,
      bigDont
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    return Promise.all(cacheImages);
  }

  _renderNotHaveGameModal() {
    const modal = (
      <Modal isVisible={this.state.isModalNotHaveGameVisible}>
        <View style={styles.modalTransparentContainer}>
          <View style={styles.modalBackground}>
            <View style={styles.modalHeader}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleIsModalNotHaveGameVisible(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground
                    resizeMode="contain"
                    style={styles.modalCloseImage} source={dont}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5 }} />
            </View>
            <View style={styles.modalContent}>
              <View style={styles.modalDontImageContent}>
                <Image style={styles.modalDontImage} source={bigDont} />
              </View>
              <View
                style={{
                  paddingHorizontal: scaleVertical(15)
                }}
              >
                <RkText style={styles.modalHeaderText}>No hay juegos disponibles</RkText>
                <RkText style={styles.modalInfoText}>Muy pronto publicaremos el próximo juego.</RkText>
              </View>
              <View style={styles.modalButtonContent}>
                <RkButton onPress={() => { this._toggleIsModalNotHaveGameVisible(); }} style={styles.modalButton}>
                  <RkText style={styles.modalButtonText}>ACEPTAR</RkText>
                </RkButton>
              </View>
            </View>
          </View>
        </View>
      </Modal>);
    return modal;
  }

  render() {
    if (!this.state.isReady || !this.props.currentActiveGame) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }
    return (
      <TouchableOpacity activeOpacity={1} style={styles.subMenuItem} onPress={() => { this._goTo('LineUpMenu'); }}>
        {this._renderNotHaveGameModal()}
        <View style={styles.subMenuItem}>
          <View style={styles.test2}>
            <ImageBackground
              resizeMode='contain'
              style={styles.bgImage} source={subNav}
            />
          </View>
          <View >
            <RkText style={styles.subMenuText}>LINE UP</RkText>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

let styles = RkStyleSheet.create(() => ({


  test2: {
    // height: Platform.OS === 'ios' ? scaleVertical(45) : scaleVertical(50),
    height: Platform.OS === 'ios' ? AppbarHeight - scaleVertical(10) : AppbarHeight - scaleVertical(15)
  },

  subMenuItem: {
    width: size.width / 5,
    height: AppbarHeight + scaleVertical(10),
    left: scale(1),
  },
  subMenuText: {
    color: '#fff',
    fontSize: scaleVertical(8),
    textAlign: 'center',
    textAlignVertical: 'bottom'
  },
  bgImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  },
  modalTransparentContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    paddingHorizontal: scaleVertical(5),
    paddingVertical: scaleVertical(20),
    alignContent: 'center'
  },
  modalBackground: {
    flex: 1,
    backgroundColor: '#ebebeb',
  },
  modalBackgroundQuiniela: {
    flex: 1,
    backgroundColor: '#394353',
  },
  modalHeader: {
    flex: 1,
    flexDirection: 'row'
  },
  modalCloseImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  },
  modalContent: {
    flex: 10,
    alignItems: 'center',
  },
  modalDontImageContent: {
    padding: scaleVertical(15),
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalDontImage: {
    height: size.width / 4,
    width: size.width / 4,
    alignSelf: 'center'
  },
  modalQuinielaImageContent: {
    padding: scaleVertical(15),
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalHeaderText: {
    fontSize: scaleVertical(25),
    fontFamily: 'TTPollsBoldItalic',
    textAlign: 'center'
  },
  modalInfoText: {
    fontSize: scaleVertical(20),
    fontFamily: 'Roboto-Regular',
    textAlign: 'center'
  },
  modalButtonContent: {
    paddingHorizontal: scaleVertical(15),
    paddingVertical: scaleVertical(15),
  },
  modalButton: {
    backgroundColor: '#eb0029',
    alignSelf: 'center',
    justifyContent: 'center',
    height: size.width / 7,
    width: size.width - scaleVertical(80),
  },
  modalButtonText: {
    fontSize: scaleVertical(18),
    fontFamily: 'DecimaMono',
    textAlign: 'center',
    color: '#fff',
    textAlignVertical: 'center'
  },
}));


LineUpSubItem.propTypes = {
  currentActiveGame: PropTypes.object,
  navigation: PropTypes.object,
};

const mapStateToProps = state => ({
  currentActiveGame: state.reducerGame,
});


export default connect(mapStateToProps)(LineUpSubItem);
