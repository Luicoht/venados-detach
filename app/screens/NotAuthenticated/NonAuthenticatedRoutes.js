import { createStackNavigator } from 'react-navigation';
import SignIn from './SignIn';
import SignUp from './SignUp';
import Recover from './Recover';
import Partner from './Partner';
import SplashScreen from './../other/splash';

const NonAuthenticatedRoutes = createStackNavigator({
  Home: {
    screen: SignIn
  },
  SignUp: {
    screen: SignUp
  },
  Recover: {
    screen: Recover
  },
  Partner: {
    screen: Partner
  }
}, { headerMode: 'none' });
export { NonAuthenticatedRoutes };
