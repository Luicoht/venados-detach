import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { RkStyleSheet, RkText } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { scaleVertical } from '../../utils/scale';
import { ArticlesTabView } from './articlesTabView';

class Store extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      user: '',
      wallet: '',
    };
    const options = {
      title: 'TIENDA',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }

  componentWillMount() { }

  render() {
    return (
      <View style={styles.root}>
        <View style={{ paddingVertical: scaleVertical(10), paddingHorizontal: scaleVertical(15), justifyContent: 'space-around' }}>
          <RkText style={{ fontSize: scaleVertical(25), fontFamily: 'TTPollsBold', color: '#000' }} >Productos</RkText>
          <RkText style={{ fontSize: scaleVertical(18), fontFamily: 'DecimaMono', color: '#000' }} >Canjea tus puntos por productos exclusivos.</RkText>
        </View>
        <ArticlesTabView navigation={this.props.navigation} />
      </View>
    );
  }
}


let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff',
  },
}));


Store.navigationOptions = {
  title: 'Tienda'.toUpperCase(),
  hidden: false
};


Store.propTypes = {
  navigation: PropTypes.object,
};

const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Store);
