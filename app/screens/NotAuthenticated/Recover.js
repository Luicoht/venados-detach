import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { connect } from 'react-redux';
import { DoubleBounce } from 'react-native-loader';
import { RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { Asset, LinearGradient } from 'expo';
import { scaleVertical, size } from '../../utils/scale';
import { actionRegistration, actionRecover } from '../../Store/ACTIONS';
import back from '../../assets/icons/back.png';
import RecoverForm from './Forms/RecoverForm';

class Recover extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false
    };
  }
  componentDidMount() { this._cacheResourcesAsync(); }

  async _cacheResourcesAsync() {
    const images = [
      back
    ];

    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    });
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }
  signinDeUsuario = values => {
    this.props.recover(values);
  };
  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    return (
      <LinearGradient
        style={styles.container}
        colors={['#e4e7ed', '#eaebed', '#c2beb5']}
      >
        <ScrollView style={{ flex: 1 }}>
          <TouchableOpacity activeOpacity={1} style={styles.imageBack} onPress={() => { this.props.navigation.goBack(); }} >
            <ImageBackground style={{ flex: 1 }} source={back} />
          </TouchableOpacity>
          <View style={{ flex: 1, paddingTop: scaleVertical(30) }}>
            <View style={{ padding: scaleVertical(15) }}>
              <RkText style={styles.headerTxt}> Recupera tu contraseña </RkText>
            </View>
            <View style={{ padding: scaleVertical(15), paddingTop: scaleVertical(10) }}>
              <RkText style={styles.headerInfoTxt}> Ingresa tu correo electónico, nombre se usuario o número de celular. </RkText>
            </View>
            <RecoverForm recover={this.signinDeUsuario} />
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 16
  },
  imageBack: {
    left: 10,
    top: size.width / 10,
    height: size.width / 10,
    width: size.width / 10,
  },
  image: {
    marginVertical: 20,
    height: scaleVertical(60),
    resizeMode: 'contain'
  },
  headerTxt: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(22),
  },
  headerInfoTxt: {
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(18)
  },
}));


const mapStateToProps = state => ({
  numero: state.reducerPrueba
});

const mapDispatchToProps = dispatch => ({
  registro: values => {
    dispatch(actionRegistration(values));
  },
  recover: datos => {
    dispatch(actionRecover(datos));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Recover);
