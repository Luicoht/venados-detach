import React from 'react';
import PropTypes from 'prop-types';
import { DoubleBounce } from 'react-native-loader';
import { connect } from 'react-redux';
import { View, Platform, Image, ImageBackground, TouchableOpacity, UIManager, LayoutAnimation } from 'react-native';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { Asset, LinearGradient } from 'expo';
import { scale, scaleVertical, size } from '../../utils/scale';
import QuinielaModals from '../../components/quinielaSubItem';
import imgInterior from '../../assets/JPEG/Interior.jpg';
import imgPlay from '../../assets/icons/jugar-4.png';
import imgReto from '../../assets/icons/reto-3.png';
import imgMisJuegos from '../../assets/icons/mis-juegos-3.png';
import imgQuiniela from '../../assets/icons/quiniela-3.png';
import imgLineUp from '../../assets/icons/line_up-3.png';
import imgCalendario from '../../assets/icons/calendario-4.png';
import imgResultados from '../../assets/icons/resultados-3.png';

class Play extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      game: false
    };
    const options = {
      title: 'JUGAR',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }
  componentWillMount() { }
  componentDidMount() {
    this._cacheResourcesAsync();
  }
  componentWillUpdate(nextProps, nextState) {
    let animate = false;
    if (this.state.isReady !== nextState.isReady || this.state.game !== nextState.game) {
      animate = true;
    }
    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      LayoutAnimation.easeInEaseOut();
    }
  }

  async _cacheResourcesAsync() {
    const images = [
      imgInterior,
      imgMisJuegos,
      imgPlay,
      imgQuiniela,
      imgReto,
      imgLineUp,
      imgCalendario,
      imgResultados,
    ];
    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    });
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }


  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }

    return (
      <View style={styles.root}>
        <LinearGradient
          style={styles.root}
          colors={['#3d4959', '#33363c', '#2a3139', '#292c32']}
        >
          <View style={{ flex: 2 }}>
            <ImageBackground style={{ flex: 1 }} source={imgInterior} >
              <RkText style={styles.textBg} >Jugar</RkText>
            </ImageBackground>
            <Image
              style={styles.playIcon}
              source={imgPlay}
            />
          </View>
          <View style={{ flex: 2 }}>
            <View style={{ flex: 1 }}>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <QuinielaModals navigation={this.props.navigation} stilo={2} />
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('DeerChallengeMenu') }} activeOpacity={1} style={{ flex: 1 }}>
                  <View style={{ flex: 3, margin: scaleVertical(15) }}>
                    <ImageBackground resizeMode='contain'
                      style={styles.iconStyle} source={imgReto} />
                  </View>
                  <View style={{ flex: 2 }}>
                    <RkText style={styles.subMenuText}>RETO VENADOS</RkText>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('LineUpMenu') }} activeOpacity={1} style={{ flex: 1 }}>
                  <View style={{ flex: 3, margin: scaleVertical(15) }}>
                    <ImageBackground resizeMode='contain'
                      style={styles.iconStyle} source={imgLineUp} />
                  </View>
                  <View style={{ flex: 2 }}>
                    <RkText style={styles.subMenuText}>LINE UP</RkText>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('CalendarMenu') }} activeOpacity={1} style={{ flex: 1 }}>
                  <View style={{ flex: 3, margin: scaleVertical(15) }}>
                    <ImageBackground resizeMode='contain'
                      style={styles.iconStyle} source={imgCalendario} />
                  </View>
                  <View style={{ flex: 2 }}>
                    <RkText style={styles.subMenuText}>CALENDARIO</RkText>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('MyGamesMenu') }} activeOpacity={1} style={{ flex: 1 }}>
                  <View style={{ flex: 3, margin: scaleVertical(15) }}>
                    <ImageBackground resizeMode='contain'
                      style={styles.iconStyle} source={imgMisJuegos} />
                  </View>
                  <View style={{ flex: 2 }}>
                    <RkText style={styles.subMenuText}>MIS JUEGOS</RkText>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('StandingMenu') }} activeOpacity={1} style={{ flex: 1 }}>
                  <View style={{ flex: 3, margin: scaleVertical(15) }}>
                    <ImageBackground resizeMode='contain'
                      style={styles.iconStyle} source={imgResultados} />
                  </View>
                  <View style={{ flex: 2 }}>
                    <RkText style={styles.subMenuText}>RESULTADO DE JUEGOS</RkText>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{ flex: 0.5, justifyContent: 'center' }}>
            <RkButton onPress={() => { this.props.navigation.navigate('RulesMenu'); }} rkType='clear' >
              <RkText style={styles.link} >REGLAS</RkText>
            </RkButton>
          </View>
        </LinearGradient>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1
  },
  playContainer: {
    flex: 1
  },
  imageBg: {
    width: size.width,
    height: size.height / 3
  },
  textBg: {
    color: 'white',
    fontFamily: 'TTPollsBold',
    fontSize: scale(40),
    top: Platform.OS === 'ios' ? scaleVertical(155) : scaleVertical(120),
    left: scale(20)
  },
  playIcon: {
    width: scaleVertical(64),
    height: scaleVertical(64),
    alignSelf: 'center',
    top: Platform.OS === 'ios' ? scaleVertical(145) : scaleVertical(110),
    position: 'absolute'
  },
  subBar: {
    flexDirection: 'row',
    flex: 1
  },
  subMenuItems: {
    width: (size.width / 3) - scaleVertical(10),
    height: (size.height / 6) - scaleVertical(10),
    flexDirection: 'column',
    fontSize: 8,
  },
  subMenuIcon: {
    marginTop: 1,
    marginBottom: 1,
  },
  subMenuText: {
    color: '#fff',
    fontSize: scaleVertical(14),
    alignSelf: 'center'
  },
  link: {
    color: '#00aeef',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'DecimaMono'
  },
  iconStyle: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  }
}));


Play.propTypes = {
  currentActiveGame: PropTypes.object,
  navigation: PropTypes.object,
};


const mapStateToProps = state => ({
  usuario: state.reducerSesion,
  currentActiveGame: state.reducerGame,
});
const mapDispatchToProps = () => ({});


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Play);
