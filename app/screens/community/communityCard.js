
import React from 'react';
import { View } from 'react-native';
import { Asset } from 'expo';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { DoubleBounce } from 'react-native-loader';
import { scale, scaleVertical, size } from '../../utils/scale';
import { AsyncImage } from '../../components/AsyncImage';
import { formatNumber } from '../../Store/Services/Socket';
import primero from '../../assets/icons/primero.png';
import segundo from '../../assets/icons/segundo.png';
import tercero from '../../assets/icons/tercero.png';
import cuarto from '../../assets/icons/cuarto.png';
import { communityData } from './communityExample';

export class CommunityCard extends React.Component {
  static navigationOptions = {
    title: 'Comunidad'.toUpperCase(),
    hidden: false
  };

  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }

  componentWillMount() {}
  componentDidMount() { this._cacheResourcesAsync(); }

  async _cacheResourcesAsync() {
    const images = [
      primero,
      segundo,
      tercero,
      cuarto
    ];
    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    });
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  _getRequire(key) {
    switch (key) {
      case 0:
        return primero;
      case 1:
        return segundo;
      case 2:
        return tercero;
      default:
        return cuarto;
    }
  }

  _getTopFive() {
    const content = communityData.map((user, index) => {
      let bgColor = '#c8eeff';
      if (index === 0) bgColor = '#fecc5a';
      if (index === 1) bgColor = '#bfbfbf';
      if (index === 2) bgColor = '#9f7951';
      const partial = (
        <View style={styles.item}>
          <View style={{ flex: 0.75, justifyContent: 'center' }}>
            <AsyncImage
              style={styles.flag}
              source={this._getRequire(index)}
              placeholderColor='transparent'
            />
          </View>
          <View style={{ flex: 1 }}>
            <View style={[styles.placeCircle, { backgroundColor: bgColor }]}>
              <View style={styles.whiteCirlce}>
                <View style={styles.contentCircle}>
                  <AsyncImage
                    style={styles.userImage}
                    source={{ uri: user.image }}
                    placeholderColor='transparent'
                  />
                </View>
              </View>
            </View>
          </View>
          <View style={{ flex: 1.5, justifyContent: 'space-around' }}>
            <RkText style={styles.textName}> {user.name}</RkText>
            <RkText style={styles.textScore}> {formatNumber(user.score)}</RkText>
          </View>
        </View>
      );
      return partial;
    });
    const solve = (
      <View style={{ flex: 1, paddingHorizontal: scaleVertical(10) }}>
        {content}
      </View>
    );
    return solve;
  }


  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 6} color='#eb0029' />
        </View>
      );
    }
    return (
      <View style={styles.root}>
        <View style={{ flex: 1, paddingVertical: scaleVertical(15), paddingHorizontal: scaleVertical(10) }}>
          <View style={styles.communityContainer}>
            <RkText style={styles.cardHeaderText} > Comunidad </RkText>
          </View>
          <View style={styles.topContainer}>
            <View style={{ flex: 1 }}>
              {this._getTopFive()}
            </View>
            <View style={{ flex: 1, paddingVertical: scaleVertical(10), justifyContent: 'center' }}>
              <RkButton style={{ backgroundColor: 'transparent', alignSelf: 'center' }}>
                <RkText style={{ alignSelf: 'center', color: '#2e3192', fontFamily: 'DecimaMono-Bold', textDecorationLine: 'underline' }}> VER MÁS</RkText>
              </RkButton>
            </View>
          </View>
        </View>
      </View>
    );
  }
}


let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#ec0029',
    paddingVertical: scaleVertical(15)
  },
  communityContainer: {
    backgroundColor: '#c9efff',
    height: size.width / 4,
    borderTopRightRadius: scaleVertical(10),
    borderTopLeftRadius: scaleVertical(10)
  },
  topContainer: {
    backgroundColor: 'white',
    borderBottomRightRadius: scaleVertical(10),
    borderBottomLeftRadius: scaleVertical(15)
  },
  cardHeaderText: {
    color: '#303191',
    fontSize: scale(30),
    marginTop: scale(20),
    marginLeft: scale(15),
    fontFamily: 'TTPollsBold',
  },
  flag: {
    width: size.width / 7,
    height: size.width / 7,
    alignSelf: 'center'
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: scaleVertical(10),
    borderBottomWidth: scaleVertical(2),
    borderColor: '#dbdbdb'
  },
  placeCircle: {
    width: size.width / 5,
    height: size.width / 5,
    borderRadius: size.width / 10,
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  whiteCirlce: {
    width: size.width / 5.4,
    height: size.width / 5.4,
    borderRadius: size.width / 10.8,
    backgroundColor: '#fff',
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  contentCircle: {
    width: size.width / 5.8,
    height: size.width / 5.8,
    borderRadius: size.width / 11.6,
    backgroundColor: '#fff',
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  userImage: {
    width: size.width / 5,
    height: size.width / 5,
    alignSelf: 'center'
  },
  textName: {
    textAlign: 'center',
    fontFamily: 'Roboto-Bold'
  },
  textScore: {
    textAlign: 'center',
    fontFamily: 'Roboto-Regular',
    color: '#002157',
    fontSize: scaleVertical(20)
  }
}));
