import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, ScrollView, TouchableOpacity, ImageBackground } from 'react-native';
import { connect } from 'react-redux';
import { RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { Asset, LinearGradient } from 'expo';
import { DoubleBounce } from 'react-native-loader';
import { actionRegistration } from '../../Store/ACTIONS';
import { scaleVertical, size, scale } from '../../utils/scale';
import logoImage from '../../assets/images/logo.png';
import backIcon from '../../assets/icons/back.png';
import SignUpForm from './Forms/SignUpForm';


class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isReady: false
    };
  }

  componentDidMount() {
    this._cacheResourcesAsync();
  }

  async _cacheResourcesAsync() {
    const images = [logoImage, backIcon];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });

    return Promise.all(cacheImages);
  }


  registroDeUsuario = values => {
    this.props.registro(values);
  };


  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }

    return (
      <LinearGradient
        style={styles.container}
        colors={['#e4e7ed', '#eaebed', '#c2beb5']}
      >
        <ScrollView style={{ flex: 1 }}>

          <View style={{ flex: 1, paddingTop: scaleVertical(30) }}>

            <Image resizeMode="contain" style={styles.image} source={logoImage} />

            <View style={{ paddingRight: scaleVertical(30) }}>
              <RkText style={styles.headerTxt}> Creación de cuenta </RkText>
            </View>

            <View style={{ paddingRight: scaleVertical(45), paddingTop: scaleVertical(10) }}>
              <RkText style={styles.headerInfoTxt}> ¡Solo llena los siguientes campos y listo! </RkText>
            </View>
          </View>

          <SignUpForm registro={this.registroDeUsuario} />
        </ScrollView>
      </LinearGradient>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 16
  },
  imageBack: {
    left: 10,
    top: size.width / 10,
    height: size.width / 10,
    width: size.width / 10,
  },
  image: {
    marginVertical: 20,
    height: scaleVertical(60),
  },
  headerTxt: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(27)
  },
  headerInfoTxt: {
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(18)
  },
}));


SignUp.propTypes = {
  navigation: PropTypes.object,
  registro: PropTypes.func,
};


const mapStateToProps = state => ({
  numero: state.reducerPrueba
});


const mapDispatchToProps = dispatch => ({
  registro: values => {
    dispatch(actionRegistration(values));
  }
});


export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
