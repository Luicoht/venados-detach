import React from 'react';
import PropTypes from 'prop-types';
import { View, Platform, ImageBackground, TouchableOpacity, UIManager, LayoutAnimation } from 'react-native';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { Asset, LinearGradient } from 'expo';
import { connect } from 'react-redux';
import { DoubleBounce } from 'react-native-loader';
import { scale, scaleVertical, size } from '../../utils/scale';
import { getWalletPoints } from '../../Store/Services/Socket';
import imgPuntos from '../../assets/JPEG/puntos_1.jpg';
import imgMisJuegos from '../../assets/icons/mis-juegos-3.png';
import imgJugar from '../../assets/icons/jugar-4.png';
import imgCheck from '../../assets/icons/check-4.png';
import imgBoleto from '../../assets/icons/boleto-1.png';
import imgBag from '../../assets/icons/bag-3.png';
import imgPuntos2 from '../../assets/icons/puntos-4.png';
import imgSocio from '../../assets/icons/socio-2.png';
import imgFriend from '../../assets/icons/friend-4.png';

class Points extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      wallet: false
    };
    const options = {
      title: 'PUNTOS',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }
  componentWillMount() { }
  componentDidMount() {
    this._cacheResourcesAsync();
    getWalletPoints()
      .then((wallet) => {
        this.setState({ wallet });
      })
      .catch((err) => { console.log('error', err); });
  }

  componentWillUpdate(nextProps, nextState) {
    let animate = false;
    if (this.state.isReady !== nextState.isReady || this.state.wallet !== nextState.wallet) {
      animate = true;
    }
    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      LayoutAnimation.easeInEaseOut();
    }
  }

  async _cacheResourcesAsync() {
    const images = [
      imgPuntos,
      imgMisJuegos,
      imgJugar,
      imgCheck,
      imgBoleto,
      imgFriend,
      imgBag,
      imgPuntos2,
      imgSocio,
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  render() {
    if (!this.state.isReady || !this.state.wallet) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    const liveGame = this.props.currentActiveGame && this.props.currentActiveGame.finished === 0 ? (
      <View style={{ justifyContent: 'center', padding: scale(30) }}>
        <View style={{ backgroundColor: '#eb0029', borderRadius: scale(10), bottom: scale(35), height: size.width / 4, width: size.width / 4, justifyContent: 'center' }}>
          <RkText rkType="h5  TTPollsRegular " style={{ color: 'white', textAlign: 'center' }} >{this.state.wallet.points}</RkText>
        </View>
      </View>
    ) :
      (<View />);
    return (
      <View style={styles.root}>
        <LinearGradient
          style={styles.root}
          colors={['#3d4959', '#33363c', '#2a3139', '#292c32']}
        >
          <View style={{ flex: 2 }}>
            <ImageBackground style={{ flex: 1 }} source={imgPuntos} >
              <View style={{ flex: 1 }} />
              <View style={{ flexDirection: 'row', flex: 1 }}>
                <View style={{ flex: 1 }}>
                  <RkText style={styles.textBg} >Puntos</RkText>
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  {liveGame}
                </View>
              </View>
              <View style={{ flex: 1 }} />
            </ImageBackground>
          </View>
          <View style={{ flex: 2 }}>
            <View style={{ flex: 1 }}>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <TouchableOpacity activeOpacity={1} onPress={() => { this.props.navigation.navigate('PlayMenu'); }} style={{ flex: 1 }}>
                  <View style={{ flex: 3, margin: scaleVertical(15) }}>
                    <ImageBackground
                      resizeMode='contain'
                      style={styles.iconImage} source={imgJugar}
                    />
                  </View>
                  <View style={{ flex: 2 }}>
                    <RkText style={styles.subMenuText}>JUGAR</RkText>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={1} onPress={() => { this.props.navigation.navigate('CheckInMenu'); }} style={{ flex: 1 }}>
                  <View style={{ flex: 3, margin: scaleVertical(15) }}>
                    <ImageBackground
                      resizeMode='contain'
                      style={styles.iconImage} source={imgCheck}
                    />
                  </View>
                  <View style={{ flex: 2 }}>
                    <RkText style={styles.subMenuText}>CHECK-IN</RkText>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={1} onPress={() => { this.props.navigation.navigate('ComingSoonMenu'); }} style={{ flex: 1 }}>
                  <View style={{ flex: 3, margin: scaleVertical(15) }}>
                    <ImageBackground
                      resizeMode='contain'
                      style={styles.iconImage} source={imgFriend}
                    />
                  </View>
                  <View style={{ flex: 2 }}>
                    <RkText style={styles.subMenuText}>INVITAR AMIGOS</RkText>
                  </View>
                </TouchableOpacity>
                {/* <TouchableOpacity activeOpacity={1} onPress={() => { this.props.navigation.navigate('RegisterTicketMenu'); }} style={{ flex: 1 }}>
                  <View style={{ flex: 3, margin: scaleVertical(15) }}>
                    <ImageBackground resizeMode='contain'
                      style={styles.iconImage} source={imgBoleto} />
                  </View>
                  <View style={{ flex: 2 }}>
                    <RkText style={styles.subMenuText}>REGISTRAR BOLETO</RkText>
                  </View>
                </TouchableOpacity> */}
              </View>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <TouchableOpacity activeOpacity={1} onPress={() => { this.props.navigation.navigate('ComingSoonMenu'); }} style={{ flex: 1 }}>
                  <View style={{ flex: 3, margin: scaleVertical(15) }}>
                    <ImageBackground
                      resizeMode='contain'
                      style={styles.iconImage} source={imgBag}
                    />
                  </View>
                  <View style={{ flex: 2 }}>
                    <RkText style={styles.subMenuText}>CANJEAR PUNTOS</RkText>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={1} onPress={() => { this.props.navigation.navigate('HistoryMenu'); }} style={{ flex: 1 }}>
                  <View style={{ flex: 3, margin: scaleVertical(15) }}>
                    <ImageBackground
                      resizeMode='contain'
                      style={styles.iconImage} source={imgPuntos2}
                    />
                  </View>
                  <View style={{ flex: 2 }}>
                    <RkText style={styles.subMenuText}>HISTORIAL DE PUNTOS</RkText>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={1} onPress={() => { this.props.navigation.navigate('ComingSoonMenu'); }} style={{ flex: 1 }}>
                  <View style={{ flex: 3, margin: scaleVertical(15) }}>
                    <ImageBackground
                      resizeMode='contain'
                      style={styles.iconImage} source={imgSocio}
                    />
                  </View>
                  <View style={{ flex: 2 }}>
                    <RkText style={styles.subMenuText}>HACERTE SOCIO</RkText>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{ flex: 0.5, justifyContent: 'center' }}>
            <RkButton style={styles.link} rkType='clear' >
              <RkText style={styles.link} onPress={() => { this.props.navigation.navigate('AboutPoints'); }}>¿CÓMO GANAR MÁS PUNTOS?</RkText>
            </RkButton>
          </View>
        </LinearGradient>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1
  },
  playContainer: {
    flex: 1
    // width: size.width,
    // height: size.height / 3
  },
  imageBg: {
    width: size.width,
    height: size.height / 3
  },
  textBg: {
    color: 'white',
    fontFamily: 'TTPollsBold',
    fontSize: scale(40),
    left: scale(20)
  },
  playIcon: {
    width: scaleVertical(64),
    height: scaleVertical(64),
    alignSelf: 'center',
    top: Platform.OS === 'ios' ? scaleVertical(145) : scaleVertical(110),
    position: 'absolute'
  },
  subBar: {
    flexDirection: 'row',
    flex: 1
  },
  subMenuItems: {
    width: (size.width / 3) - scaleVertical(10),
    height: (size.height / 6) - scaleVertical(10),
    flexDirection: 'column',
    fontSize: 8,
  },
  subMenuIcon: {
    marginTop: 1,
    marginBottom: 1,
  },
  subMenuText: {
    color: '#fff',
    fontSize: scaleVertical(14),
    alignSelf: 'center'
  },
  link: {
    color: '#00aeef',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'DecimaMono'
  },
  iconImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  }
}));



Points.propTypes = {
  currentActiveGame: PropTypes.object,
  navigation: PropTypes.object,
};


const mapStateToProps = state => ({
  currentActiveGame: state.reducerGame,
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Points);
