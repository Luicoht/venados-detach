import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, TouchableOpacity, SafeAreaView, Image } from 'react-native';
import { Icon } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { RkButton, RkText } from 'react-native-ui-kitten';
import { takeSnapshotAsync } from 'expo';
import { Text } from '../../../components/Text';
import venadosRojosIMG from '../../../assets/images/VenadosRojo.png';
import CodigoQRIMG from '../../../assets/images/codigo-qr.jpeg';

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(102, 189, 240, 1)',
    paddingLeft: 5,
    paddingRight: 5
  },
  title: {
    color: 'black',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10
  },
  text: {
    color: 'black',
    textAlign: 'center'
  },
};


class BuyInStoreModal extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.getWin();
    }, 5000);
  }
  onShareFacebook() {
    this.getImagen().then((result) => {
      if (result) {
        console.log('Result Image: ', result);
      } else {
        console.log(' No fue posible obtener la imagen ');
      }
    });
  }

  async getImagen() {
    const result = await takeSnapshotAsync(this.imageContainer, {
      result: 'file',
      height: 300,
      width: 200,
      quality: 1,
      format: 'png',
    });

    return result;
  }

  getWin() {
    return (
      <Col>
        <Text style={styles.text}> Ganaste {this.props.data.win} puntos</Text>
        <Text style={styles.title}>en Quiniela Venados</Text>
      </Col>
    );
  }

  render() {
    const { visible, onClose } = this.props;

    return (
      <Modal
        animationType='fade'
        visible={visible}
        transparent
        onRequestClose={() => {}}
        supportedOrientations={['portrait', 'landscape']}
      >
        <SafeAreaView style={styles.container}>
          <TouchableOpacity onPress={onClose} style={{ padding: 20, marginLeft: -280 }}>
            <Icon type='FontAwesome' name='times' color='white' style={{ width: 30, height: 30, color: 'red' }} />
          </TouchableOpacity>
          <Grid ref={this.imageContainer} style={{ marginTop: -90 }}>
            <Col>
              <Row style={{ height: 60, paddingTop: 0 }}>
                <Col style={{ height: 60, alignItems: 'center' }}>
                  <TouchableOpacity onPress={onClose} style={{ padding: 20 }}>
                    <Image
                      style={{ width: 200, height: 150, resizeMode: 'contain' }}
                      source={venadosRojosIMG}
                    />
                  </TouchableOpacity>
                </Col>
              </Row>
              <Row style={{ height: 60, marginTop: 60 }}>
                <Col style={{ height: 60, alignItems: 'center' }}>
                  <TouchableOpacity onPress={onClose} style={{ padding: 20 }}>
                    <Image
                      style={{ width: 150, height: 150, resizeMode: 'contain' }}
                      source={CodigoQRIMG}
                    />
                  </TouchableOpacity>
                </Col>
              </Row>
              <Row style={{ height: 60, marginTop: 130 }}>
                <Col style={{ height: 60 }}>
                  <Text style={styles.title}>Presenta este</Text>
                  <Text style={styles.title}>código en la Tienda</Text>
                  <Text style={styles.title}>Venados mas $200</Text>
                  <Text style={styles.text}>para canjear tu producto</Text>
                </Col>
              </Row>
              <Row style={{ height: 60, marginTop: 100 }}>
                <Col>
                  <RkButton rkType='buttonEmailLogin' style={{ alignSelf: 'center' }} onPress={this.onShareFacebook.bind(this)} >
                    <RkText rkType='h4  DecimaMonoBold' style={{ color: '#fff' }}> GUARDAR IMAGEN</RkText>
                  </RkButton>
                </Col>
              </Row>

              <Row style={{ height: 60 }}>
                <Col>
                  <RkButton
                    rkType="buttonPartnerLogin"
                    onPress={onClose}
                  >
                    <RkText rkType="h4  DecimaMonoBold" style={{ color: '#fff' }}> ENVIAR </RkText>
                  </RkButton>
                </Col>
              </Row>
            </Col>
          </Grid>
        </SafeAreaView>
      </Modal>
    );
  }
}


BuyInStoreModal.propTypes = {
  visible: PropTypes.bool,
  onClose: PropTypes.func,
  data: PropTypes.object,
};


export default BuyInStoreModal;
