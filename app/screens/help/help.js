import React from 'react';
import { View, Image, ScrollView } from 'react-native';
import { Asset } from 'expo';
import { RkStyleSheet, RkText } from 'react-native-ui-kitten';
import * as Animatable from 'react-native-animatable';
import { DoubleBounce } from 'react-native-loader';
import { connect } from 'react-redux';
import Accordion from 'react-native-collapsible/Accordion';
import { scaleVertical, size } from '../../utils/scale';
import up from '../../assets/icons/up.png';
import down from '../../assets/icons/down.png';

const SECTIONS = [
  {
    title: '¿Cómo puedo canjear puntos?',
    content: 'Los puntos se canjean por descuentos para adquirir productos exclusivos en nuestra tienda Venados.\n\nPuedes preseleccionar tus productos desde la App, o ir directamente a la tienda.\n\n\tPasos a seguir:\n\nIngresa a la sección de productos y selecciona el producto que quieres canjear.\n\nUna parte del producto lo adquieres con puntos y otra con dinero.\n\nMuestra en la tienda el código alfanumérico que te aparecerá ¡Listo!'
  },
  {
    title: '¿Los puntos tienen vigencia?',
    content: 'No, los puntos no tienen vigencia, puedes acumularlos y canjearlos cuando tú quieras.'
  },
  {
    title: '¿Cómo cambiar de número?',
    content: 'Solo ve a tu perfil y selecciona que quieres cambiar de número, se te enviará un mensaje de confirmación a tu celular para poder hacer dicho cambio.'
  },
  {
    title: '¿Puedo comprar puntos?',
    content: 'Los puntos no pueden comprarse, solo pueden generarse mediante las actividades designadas, las cuales podrás encontrarlas en la sección de puntos. IR'
  },
];


class Help extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false
    };
    const options = {
      title: 'AYUDA',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }
  componentDidMount() { this._cacheResourcesAsync(); }

  async _cacheResourcesAsync() {
    const images = [
      up,
      down,
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  _renderHeader(section, index, isActive) {
    const icon = isActive ? up : down;
    return (
      <Animatable.View
        duration={300}
        transition='backgroundColor'
        style={{ paddingHorizontal: scaleVertical(15), paddingVertical: scaleVertical(5), backgroundColor: 'white' }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 4 }}>
            <RkText style={styles.question}>{section.title}</RkText>

          </View>
          <View style={{ flex: 1 }}>

            <Image
              style={styles.icon}
              source={icon}
            />
          </View>
        </View>
        <View style={styles.linea} />
      </Animatable.View>
    );
  }

  _renderContent(section, i, isActive) {
    return (
      <Animatable.View
        duration={300}
        transition='backgroundColor'
        style={{ paddingHorizontal: scaleVertical(15), paddingVertical: scaleVertical(5), backgroundColor: 'white' }}
      >
        <Animatable.Text
          duration={200}
          easing='ease-out'
          animation={isActive ? 'zoomIn' : false}
          style={styles.answer}
        >
          {section.content}
        </Animatable.Text>
      </Animatable.View>
    );
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }

    return (
      <View style={styles.root}>
        <ScrollView style={styles.root}>
          <View style={{ flex: 1 }}>
            <RkText style={[styles.title, { color: 'black' }]}>Ayuda</RkText>
            <RkText style={[styles.title, { color: '#eb0029' }]}>Preguntas frecuentes</RkText>
          </View>
          <Accordion
            style={styles.root}
            sections={SECTIONS}
            renderHeader={this._renderHeader}
            renderContent={this._renderContent}
          />
          <View style={{ flex: 1 }}>
            <RkText style={[styles.titleTwo, { color: 'black' }]}>¿No encuentras lo que buscas?</RkText>
            <RkText style={[styles.subTitle, { color: 'black', paddingBottom: scaleVertical(25) }]}>Envia un correo a dudasapp@venados.com o llámanos por teléfono de lunes a domingo de 11:00 a 19:00 hrs. +(52)669-80-1010</RkText>
          </View>
        </ScrollView>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    backgroundColor: '#fff',
    flex: 1
  },
  icon: {
    width: scaleVertical(25),
    height: scaleVertical(25),
    alignSelf: 'center'
  },
  linea: {
    backgroundColor: '#DCDCDC',
    height: 2
  },
  title: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(25),
    paddingVertical: scaleVertical(15),
    paddingHorizontal: scaleVertical(15)
  },
  titleTwo: {
    fontFamily: 'Roboto-Bold',
    fontSize: scaleVertical(20),
    paddingVertical: scaleVertical(15),
    paddingHorizontal: scaleVertical(15)
  },
  subTitle: {
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(15),
    paddingHorizontal: scaleVertical(15)
  },
  question: {
    fontFamily: 'Roboto-Bold',
    fontSize: scaleVertical(18),
  },
  answer: {
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(15),
    color: '#58595b'
  }
}));

const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Help);
