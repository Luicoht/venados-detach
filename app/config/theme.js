import { scale } from '../utils/scale';

const Colors = {

};

const Fonts = {
  light: 'DecimaMono-LightItalic',
  regular: 'DecimaMono-Light',
  bold: 'DecimaMono-Bold',
  logo: 'Roboto-BoldItalic',
  DecimaMono: 'DecimaMono',
  DecimaMonoBold: 'DecimaMono-Bold',
  DecimaMonoBoldItalic: 'DecimaMono-BoldItalic',
  RobotoBlack: 'Roboto-Black',
  RobotoBlackItalic: 'Roboto-BlackItalic',
  RobotoBold: 'Roboto-Bold',
  RobotoBoldItalic: 'Roboto-BoldItalic',
  RobotoLight: 'Roboto-Light',
  RobotoRegular: 'Roboto-Regular',
  TTPollsBlack: 'TTPollsBlack',
  TTPollsBlackItalic: 'TTPollsBlackItalic',
  TTPollsBold: 'TTPollsBold',
  TTPollsBoldItalic: 'TTPollsBoldItalic',
  TTPollsRegular: 'TTPollsRegular',
};

const FontBaseValue = scale(18);

export const KittenTheme = {
  name: 'light',
  colors: {
    accent: Colors.accent,
    primary: Colors.primary,
    disabled: Colors.disabled,
    twitter: Colors.twitter,
    google: Colors.google,
    facebook: Colors.facebook,
    brand: Colors.accent,
    control: {
      background: Colors.background
    },
    dashboard: {
      stars: Colors.starsStat,
      tweets: Colors.tweetsStat,
      likes: Colors.likesStat,
    },
  },
  fonts: {
    sizes: {
      h0: scale(32),
      h1: scale(26),
      h2: scale(24),
      h3: scale(20),
      h4: scale(18),
      h5: scale(16),
      h6: scale(15),
      p1: scale(16),
      p2: scale(15),
      p3: scale(15),
      p4: scale(13),
      s1: scale(15),
      s2: scale(13),
      s3: scale(13),
      s4: scale(12),
      s5: scale(12),
      s6: scale(13),
      s7: scale(10),
      base: FontBaseValue,
      small: FontBaseValue * 0.8,
      medium: FontBaseValue,
      large: FontBaseValue * 1.2,
      xlarge: FontBaseValue / 0.75,
      xxlarge: FontBaseValue * 1.6,
      xxxlarge: FontBaseValue * 2.45,
    },
    lineHeights: {
      medium: 18,
      big: 24
    },
    family: {
      regular: Fonts.regular,
      light: Fonts.light,
      bold: Fonts.bold,
      logo: Fonts.logo,
      DecimaMono: Fonts.DecimaMono,
      DecimaMonoBold: Fonts.DecimaMonoBold,
      DecimaMonoBoldItalic: Fonts.DecimaMonoBoldItalic,
      RobotoBlack: Fonts.RobotoBlack,
      RobotoBlackItalic: Fonts.RobotoBlackItalic,
      RobotoBold: Fonts.RobotoBold,
      RobotoBoldItalic: Fonts.RobotoBoldItalic,
      RobotoLight: Fonts.RobotoLight,
      RobotoRegular: Fonts.RobotoRegular,
      TTPollsBlack: Fonts.TTPollsBlack,
      TTPollsBlackItalic: Fonts.TTPollsBlackItalic,
      TTPollsBold: Fonts.TTPollsBold,
      TTPollsBoldItalic: Fonts.TTPollsBoldItalic,
      TTPollsRegular: Fonts.TTPollsRegular,
    }
  }
};
