import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, StyleSheet, Dimensions, Alert } from 'react-native';
import { connect } from 'react-redux';
import { Icon } from 'native-base';
import { RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { updatePassword, updateProfile, getProfile, tryLinkTogoogle, tryLinkToFacebook } from '../../Store/Services/Socket';
import notifications from '../../utils/notifications';
import { updateProfileData, linkToGoogle, linkToFacebook, actionLinkEmailToGoogle, actionLinkEmailToFacebook } from '../../Store/ACTIONS';
import { scaleVertical } from '../../utils/scale';
import ChangesModal from './Modals/ChangesModal';
import ImagePicker from '../../components/imagePicker';
import SpinnerModal from '../../components/SpinnerModal';


const WINDOWS_HEIGHT = Dimensions.get('window').height;


class Settings extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      screenData: {},
      pictureProfileModal: false,
      celphoneModal: false,
      emailModal: false,
      birthdayModal: false,
      passwordModal: false,
      notificationsEnabled: true,
      locationEnabled: true,
      googleEnabled: true,
      facebookEnabled: true,
      user: null,
      userId: '',
      modalVisible: false,
    };
    console.log('props', this.props.usuario.providerData);
  }


  // ----------------------------
  // -------- methods -----------
  // ----------------------------
  onMainLayout() {
    const width = Dimensions.get('window').width;
    const height = Dimensions.get('window').height;
    const orientation = width < height ? 'portrait' : 'landscape';
    const screenData = { width, height, orientation };

    this.setState({ screenData });
  }

  async onImageUpload(image) {
    try {
      this.setState({ modalVisible: true });

      const params = {
        id: this.props.profileData.data.id,
        avatarURL: image.imageUri,
      };

      await updateProfile(params);
      const updatedProfileData = await getProfile();
      this.props.updateProfileData(updatedProfileData);
      this.setState({ modalVisible: false });

      this.props.navigation.goBack();
    } catch (error) {
      this.setState({ modalVisible: false });
      notifications.show('Ocurrio un error al actualizar imagen de perfil');
    }
  }

  onLinkToGoogle() {
    tryLinkTogoogle()
      .then(usuario => {
        // console.log('si jalo', usuario);
        Alert.alert(`Hola ${usuario.additionalUserInfo.profile.first_name}`);
      })
      .catch(error => {
        console.log('error en onLinkToGoogle', error.error);
        Alert.alert('Ocurrio un error al ligar la cuenta',
          'Es posible que exista un usuario registrado con esta cuenta');
      });
  }


  onLinkToFacebook() {
    tryLinkToFacebook()
      .then(usuario => {
        // console.log('si jalo', usuario);
        Alert.alert(`Hola ${usuario.additionalUserInfo.profile.first_name}`);
      })
      .catch(error => {
        // console.log('error en onLinkToFacebook', error);
        console.log('error en onLinkToFacebook', error.error);
        Alert.alert('Ocurrio un error al ligar la cuenta, es posible que exista un usuario ya registrado con esa cuenta');
      });
  }

  onSwitchChangeNotifications() {
    this.setState({ notificationsEnabled: !this.state.notificationsEnabled });
  }

  onSwitchChangeLocation() {
    this.setState({ locationEnabled: !this.state.locationEnabled });
  }

  onSwitchChangeFacebook() {
    this.setState({ facebookEnabled: !this.state.facebookEnabled });
  }

  onSwitchChangeGoogle() {
    this.setState({ googleEnabled: !this.state.googleEnabled });
  }

  async saveCelphone(value) {
    this.setState({ modalVisible: true });

    try {
      const params = {
        id: this.props.profileData.data.id,
        phone: value,
      };

      await updateProfile(params);
      const updatedProfileData = await getProfile();
      this.props.updateProfileData(updatedProfileData);
      this.setState({ modalVisible: false });

      this.props.navigation.goBack();
    } catch (error) {
      this.setState({ modalVisible: false });
      notifications.show('Ocurrio un error al actualizar numero de teléfono');
    }
  }

  saveEmail(value) {
    console.log('Guardado saveEmail', value);
  }

  async saveBirthday(value) {
    this.setState({ modalVisible: true });
    const date = new Date(value);
    try {
      const params = {
        id: this.props.profileData.data.id,
        birthday: `${date.getDay()}/${date.getMonth()}/${date.getFullYear()}`,
      };
      await updateProfile(params);
      const updatedProfileData = await getProfile();
      this.props.updateProfileData(updatedProfileData);
      this.setState({ modalVisible: false });
      this.props.navigation.goBack();
    } catch (error) {
      this.setState({ modalVisible: false });
      notifications.show('Ocurrio un error al actualizar fecha de nacimiento');
    }
  }

  updatePassword(value) {
    updatePassword(value).then((response) => {
      Alert.alert(response);
    }).catch(() => {
      Alert.alert('Falla al cambiar contraseña. Por seguridad, cierre sesión y vuelva a realizar la operación');
    });

    this.setState({ passwordModal: false });
  }


  // -----------------------------
  // ------ render methods -------
  // -----------------------------
  renderChangePasswordOption() {
    const isUserLinked = this.props.usuario.providerData || [];
    const foundProvider = isUserLinked.find(prv => prv.providerId === 'facebook.com' || prv.providerId === 'google.com');

    if (!foundProvider) {
      return (
        <View style={styles.row}>
          <Icon style={styles.icon} name="md-key" />

          <TouchableOpacity activeOpacity={1} style={styles.rowButton} onPress={() => this.setState({ passwordModal: true })}>
            <RkText style={styles.option}>Cambiar contraseña</RkText>
          </TouchableOpacity>

          <ChangesModal
            title='Contraseña'
            placeholder='Ingresa la nueva contraseña'
            visible={this.state.passwordModal}
            onClose={() => this.setState({ passwordModal: false })}
            onSave={this.updatePassword.bind(this)}
          />
        </View>
      );
    }

    return null;
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.section}>
          <View style={[styles.row, styles.heading]}>
            <RkText style={styles.titleTxt}>Configuración de la aplicación</RkText>
          </View>

          <View style={styles.row}>
            <Icon style={styles.icon} name="md-contact" />

            <ImagePicker
              onImageUpload={this.onImageUpload.bind(this)}
              name='userImage'
            >
              <RkText style={{ ...styles.option, marginVertical: 10 }}>
                Cambiar foto de perfil
              </RkText>
            </ImagePicker>
          </View>

          <View style={styles.row}>
            <Icon style={styles.icon} name="md-call" />

            <TouchableOpacity activeOpacity={1} style={styles.rowButton} onPress={() => this.setState({ celphoneModal: true })}>
              <RkText style={styles.option}>Cambiar celular</RkText>
            </TouchableOpacity>

            <ChangesModal
              cellphone
              title='Celular'
              placeholder='Ingresa el nuevo celular'
              visible={this.state.celphoneModal}
              onClose={() => this.setState({ celphoneModal: false })}
              onSave={this.saveCelphone.bind(this)}
            />
          </View>

          <View style={styles.row}>
            <Icon style={styles.icon} name="md-calendar" />

            <TouchableOpacity activeOpacity={1} style={styles.rowButton} onPress={() => this.setState({ birthdayModal: true })}>
              <RkText style={styles.option}>Cambiar fecha de nacimiento</RkText>
            </TouchableOpacity>

            <ChangesModal
              datepicker
              title='Fecha de nacimiento'
              placeholder='Ingresa la fecha de nacimiento'
              visible={this.state.birthdayModal}
              onClose={() => this.setState({ birthdayModal: false })}
              onSave={this.saveBirthday.bind(this)}
            />
          </View>


          {/* <View style={styles.row}>
            <Icon style={styles.icon} name="md-calendar" />

            <TouchableOpacity activeOpacity={1} style={styles.rowButton} onPress={() => { console.log('link to google'); this.onLinkToGoogle(); }}>
              <RkText style={styles.option}>LINK TO GOOGLE</RkText>
            </TouchableOpacity>
          </View>
          <View style={styles.row}>
            <Icon style={styles.icon} name="md-calendar" />
            <TouchableOpacity activeOpacity={1} style={styles.rowButton} onPress={() => { console.log('link to google'); this.onLinkToFacebook(); }}>
              <RkText style={styles.option}>LINK TO FACEBOOK</RkText>
            </TouchableOpacity>
          </View> */}



          {this.renderChangePasswordOption()}

          <SpinnerModal
            visible={this.state.modalVisible}
            label="Actualizando datos"
          />
        </View>
      </View>
    );
  }
}


const styles = RkStyleSheet.create(theme => ({
  container: {
    height: WINDOWS_HEIGHT,
    backgroundColor: 'rgb(245,245,245)'
  },
  header: {
    paddingVertical: 25,
  },
  section: {
    backgroundColor: 'white',
    marginVertical: 25,
    elevation: 1
  },
  heading: {
    paddingBottom: 12.5
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingHorizontal: 17.5,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.border.base,
    alignItems: 'center'
  },
  rowButton: {
    flex: 1,
    paddingVertical: scaleVertical(10)
  },
  switch: {
    marginVertical: 14
  },
  titleTxt: {
    marginTop: 10,
    fontSize: scaleVertical(25),
    fontFamily: 'TTPollsBold'
  },
  option: {
    fontSize: scaleVertical(15),
    fontFamily: 'Roboto-Bold',
    color: 'rgb(70,70,70)'
  },
  leftItem: {
    flex: 3
  },
  rigthItem: {
    flex: 1
  },
  itemsRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    height: 26,
    width: 26,
    marginRight: 5,
    color: 'rgb(70,70,70)'
  }
}));


Settings.propTypes = {
  usuario: PropTypes.object,
  navigation: PropTypes.object,
  profileData: PropTypes.object,
  updateProfileData: PropTypes.func,
  linkToGoogle: PropTypes.func,
  linkToFacebook: PropTypes.func,
  actionLinkEmailToGoogle: PropTypes.func,
  actionLinkEmailToFacebook: PropTypes.func,
};


Settings.navigationOptions = {
  title: 'Configuración'.toUpperCase(),
  hidden: false
};


const mapStateToProps = state => ({
  prop: state.prop,
  tokenFirebase: state.reducerSesion.tokenFirebase,
  usuario: state.reducerSesion,
  profileData: state.profileData
});

export default connect(mapStateToProps, { updateProfileData, linkToGoogle, linkToFacebook, actionLinkEmailToGoogle, actionLinkEmailToFacebook })(Settings);
