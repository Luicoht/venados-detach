import React from 'react';
import { StyleSheet, View, Platform, TouchableOpacity, ImageBackground, UIManager, LayoutAnimation } from 'react-native';
import _ from 'lodash';
import { RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { Asset, AppLoading } from 'expo';
import { scale, scaleVertical, size, AppbarHeight, StatusbarHeight, } from '../utils/scale';
import estadisticas from '../assets/icons/estadisticas.png';


export class StatsSubItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }

  componentWillUpdate(nextProps, nextState) {
    let animate = false;
    if (this.state.isReady !== nextState.isReady) {
      animate = true;
    }
    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      LayoutAnimation.easeInEaseOut();
    }
  }

  _goTo(route) {
    if (this.props.navigation.state.routeName !== route) {
      if (route == 'DashboardMenu') {
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'DashboardMenu' })
          ]
        });
        this.props.navigation.dispatch(resetAction)
      } else {
        this.props.navigation.navigate(route);
      }
    }
  }


  async _cacheResourcesAsync() {
    const images = [
      estadisticas,
    ];
    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    });
    return Promise.all(cacheImages);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }
    return (
      <TouchableOpacity activeOpacity={1} style={styles.subMenuItem} onPress={() => { this._goTo('StadisticsMenu'); }}>
        <View style={styles.subMenuItem}>
          <View style={styles.test2}>
            <ImageBackground
              resizeMode='contain'
              style={styles.bgImage} source={estadisticas}
            />
          </View>
          <View >
            <RkText style={styles.subMenuText}>ESTADISTICAS</RkText>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  layout: {
    backgroundColor: '#fff',
    height: scaleVertical(60),
    paddingTop: Platform.OS === 'ios' ? StatusbarHeight : 0,
    paddingBottom: Platform.OS === 'ios' ? scaleVertical(10) : 0
  },
  layoutSubNav: {
    backgroundColor: '#000',
  },
  containerSubNav: {
    flexDirection: 'row',
    height: AppbarHeight
  },
  containerSubNavHidden: {
    flexDirection: 'row',
    height: 0,
  },
  subNav: {
    flexDirection: 'row',
  },
  subNavHidden: {
    backgroundColor: 'rgba(0,0,0,0.85)',
  },
  container: {
    flexDirection: 'row',
    height: AppbarHeight,
  },
  left: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    justifyContent: 'center'
  },
  right: {
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
  },
  title: {
    ...StyleSheet.absoluteFillObject,
    flexDirection: 'row',
  },
  menu: {
    width: scaleVertical(35),
    height: scaleVertical(35),
  },
  header: {
    paddingTop: 25,
  },
  row: {
    flexDirection: 'row'
  },
  info: {
    flexDirection: 'row',
  },
  section: {
    flex: 1,
    alignItems: 'center'
  },
  center: {
    alignSelf: 'center',
  },
  menuIcon: {
    position: 'relative',
    top: Platform.OS === 'ios' ? scaleVertical(2) : scaleVertical(2),
    left: Platform.OS === 'ios' ? scaleVertical(10) : scaleVertical(10),
    height: (size.width / 6) - scaleVertical(35),
    width: (size.width / 6) - scaleVertical(35),
  },
  test2: {
    // height: Platform.OS === 'ios' ? scaleVertical(45) : scaleVertical(50),
    height: Platform.OS === 'ios' ? AppbarHeight - scaleVertical(10) : AppbarHeight - scaleVertical(15)
  },
  test2Hidden: {
    // height: Platform.OS === 'ios' ? scaleVertical(45) : scaleVertical(50),
    height: AppbarHeight - scaleVertical(15),
    alignItems: 'center',
  },
  test: {
    height: Platform.OS === 'ios' ? scaleVertical(10) : scaleVertical(12),
    height: AppbarHeight
  },
  subMenuItem: {
    width: size.width / 5,
    height: AppbarHeight + scaleVertical(10),
    left: scale(1),
  },
  subMenuItemHidden: {
    width: size.width / 5,
    fontSize: scale(5),
    left: scale(1),
    marginTop: scaleVertical(10),
  },
  subMenuText: {
    color: '#fff',
    fontSize: scaleVertical(8),
    textAlign: 'center',
    textAlignVertical: 'bottom'
  },
  bgImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  },
  bgImageHidden: {
    position: 'absolute',
    paddingLeft: scaleVertical(10),
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: Platform.OS === 'ios' ? AppbarHeight - scaleVertical(5) : AppbarHeight - scaleVertical(10),
    width: Platform.OS === 'ios' ? AppbarHeight - scaleVertical(5) : AppbarHeight - scaleVertical(10),
    alignSelf: 'center'
  },
  modalTransparentContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    paddingHorizontal: scaleVertical(5),
    paddingVertical: scaleVertical(20),
    alignContent: 'center'
  },
  modalBackground: {
    flex: 1,
    backgroundColor: '#ebebeb',
  },
  modalBackgroundQuiniela: {
    flex: 1,
    backgroundColor: '#394353',
  },
  modalHeader: {
    flex: 1,
    flexDirection: 'row'
  },
  modalCloseImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  },
  modalContent: {
    flex: 10,
    alignItems: 'center',
  },
  modalDontImageContent: {
    padding: scaleVertical(15),
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalDontImage: {
    height: size.width / 4,
    width: size.width / 4,
    alignSelf: 'center'
  },
  modalQuinielaImageContent: {
    padding: scaleVertical(15),
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalQuinielaImage: {
    height: size.width / 5,
    width: size.width / 5,
    alignSelf: 'center'
  },
  modalHeaderText: {
    fontSize: scaleVertical(25),
    fontFamily: 'TTPollsBoldItalic',
    textAlign: 'center'
  },
  modalInfoText: {
    fontSize: scaleVertical(20),
    fontFamily: 'Roboto-Regular',
    textAlign: 'center'
  },
  modalHeaderTextQuiniela: {
    color: '#fff',
    fontSize: scaleVertical(25),
    fontFamily: 'TTPollsBoldItalic',
    textAlign: 'center'
  },
  modalInfoTextQuiniela: {
    color: '#fff',
    fontSize: scaleVertical(15),
    paddingVertical: scaleVertical(10),
    fontFamily: 'Roboto-Regular',
    textAlign: 'center'
  },
  modalSubInfoTextQuiniela: {
    color: '#fff',
    fontSize: scaleVertical(15),
    paddingVertical: scaleVertical(10),
    fontFamily: 'Roboto-Light',
    textAlign: 'center'
  },
  modalSubInfoColorTextQuiniela: {
    color: '#fff',
    fontSize: scaleVertical(18),
    paddingVertical: scaleVertical(5),
    fontFamily: 'Roboto-Bold',
    textAlign: 'center',
  },
  modalButtonContent: {
    paddingHorizontal: scaleVertical(15),
    paddingVertical: scaleVertical(15),
  },
  modalButton: {
    backgroundColor: '#eb0029',
    alignSelf: 'center',
    justifyContent: 'center',
    height: size.width / 7,
    width: size.width - scaleVertical(80),
  },
  modalButtonText: {
    fontSize: scaleVertical(18),
    fontFamily: 'DecimaMono',
    textAlign: 'center',
    color: '#fff',
    textAlignVertical: 'center'
  },
  count: {
    fontSize: scaleVertical(12),
    fontFamily: 'Roboto-Bold',
    textAlign: 'center'
  }
}));
