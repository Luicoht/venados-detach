import React from 'react';
import _ from 'lodash';
import { createStackNavigator } from 'react-navigation';
import { withRkTheme } from 'react-native-ui-kitten';
import { NavBar } from '../../components/index';
import transition from './transitions';
import { MainRoutes, MenuRoutes } from './routes';

const main = {};
const flatRoutes = {};
MenuRoutes.map((route, index) => {
  const wrapToRoute = route => {
    return {
      screen: withRkTheme(route.screen),
      title: route.title
    };
  };

  flatRoutes[route.id] = wrapToRoute(route);
  main[route.id] = wrapToRoute(route);
  for (const child of route.children) {
    flatRoutes[child.id] = wrapToRoute(child);
  }
});

const ThemedNavigationBar = withRkTheme(NavBar);

const DrawerRoutes = Object.keys(main).reduce((routes, name) => {
  const stack_name = name;
  routes[stack_name] = {
    name: stack_name,
    screen: createStackNavigator(flatRoutes, {
      initialRouteName: name,
      headerMode: 'screen',
      cardStyle: { backgroundColor: 'transparent' },
      transitionConfig: transition,
      navigationOptions: ({ navigation, screenProps }) => ({
        gesturesEnabled: false,
        header: headerProps => {
          return (<
            ThemedNavigationBar navigation={navigation}
            headerProps={headerProps}
          />
          );
        }
      })
    })
  };
  return routes;
}, {});

export const AppRoutes = DrawerRoutes;

export const PlayRoutes = _.find(MainRoutes, { id: 'PlayMenu' }).children;
// export const OtherRoutes = _.find(MainRoutes, { id: 'OtherMenu' }).children;
export const PointsRoutes = _.find(MainRoutes, { id: 'PointsMenu' }).children;
export const CalendarRoutes = _.find(MainRoutes, { id: 'CalendarMenu' }).children;

export const StandingRoutes = _.find(MainRoutes, { id: 'StandingMenu' }).children;
export const CommunityRoutes = _.find(MainRoutes, { id: 'CommunityMenu' }).children;
export const NewsRoutes = _.find(MainRoutes, { id: 'NewsMenu' }).children;
export const CheckInRoutes = _.find(MainRoutes, { id: 'CheckInMenu' }).children;
export const ProfileRoutes = _.find(MainRoutes, { id: 'ProfileMenu' }).children;
// export const DashboardRoutes = _.find(MainRoutes, { id: 'DashboardMenu' }).children;
