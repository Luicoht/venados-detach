
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Dimensions, TouchableOpacity, ImageBackground, Alert } from 'react-native';
import { connect } from 'react-redux';
import { DoubleBounce } from 'react-native-loader';
import {
  RkText,
  RkStyleSheet
} from 'react-native-ui-kitten';
import { Asset } from 'expo';
import { scaleVertical, size } from '../../utils/scale';
import { joinDeerSponsor, updateProfileData } from '../../Store/ACTIONS';
import { registerLikeDeerMember, confirmRegisterLikeDeerMember, getProfile } from '../../Store/Services/Socket';
import { getItem } from '../../utils/localdatabase'; // eslint-disable-line
import PartnerForm from './Forms/PartnerForm';
import bgSocio from '../../assets/JPEG/backgroundSocio.png';
import VenadosRojo from '../../assets/images/VenadosRojo.png';
import notifications from '../../utils/notifications';
import SpinnerModal from '../../components/SpinnerModal';

class Partner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      phone: '',
      memberNumber: '',
      profile: '',
      changeForm: false,
      partnerData: {},
      partnerData2: false,
      modalVisible: false,
    };
    const options = {
      title: 'SOCIO',
      hidden: true
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() {
    this._cacheResourcesAsync();
  }

  getDeerExponsor(data) {
    if (!this.state.changeForm) {
      const params = {
        memberCode: data.memberCode,
        phone: data.phone,
        userID: this.props.profile.data.id,
      };
      registerLikeDeerMember(params).then(res => {
        if (res === 'ok') {
          console.log('its ok perrrorr');
          this.setState({ changeForm: true });
          this.setState({ partnerData2: params });
        } else {
          console.log('its error no its ok');
          Alert.alert(
            '',
            'Ya está registrado como socio venado" el proceso termina debido a que ya está registrado',
            [
              { text: 'OK', onPress: () => this.props.navigation.navigate('PlayMenu') },
            ]
          );
        }
        return null;
      })
        .catch(err => {
          Alert.alert(
            '',
            err.message,
          );
        });
      return null;
    }
    if (this.state.partnerData2) {
      const params = {
        memberCode: this.state.partnerData2.memberCode,
        phone: this.state.partnerData2.phone,
        smsCode: data.smsCode,
        userID: this.props.profile.data.id,
      };

      confirmRegisterLikeDeerMember(params)
        .then((res) => {
          if (res === 'ok') {
          console.log('confirmRegisterLikeDeerMember then', res);

          this.setState({ modalVisible: true });
          getProfile()
            .then(updatedProfileData => {
              this.props.updateProfileData(updatedProfileData);
              this.setState({ modalVisible: false });
              this.props.navigation.navigate('ProfileMenu');
            })
            .catch(() => {
              console.log('confirmRegisterLikeDeerMember cathc');
              this.setState({ modalVisible: false });
            });
          }
        })
        .catch(err => console.log('Confirmación err:', err));
    }
  }


  async _cacheResourcesAsync() {
    const images = [
      bgSocio,
      VenadosRojo,
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }


  async updateProfile() {
    try {
      this.setState({ modalVisible: true });
      const updatedProfileData = await getProfile();
      this.props.updateProfileData(updatedProfileData);
      this.setState({ modalVisible: false });
      notifications.show('Hola Venado');
      this.props.navigation.goBack();
    } catch (error) {
      this.setState({ modalVisible: false });
      notifications.show('Ocurrio un error al actualizar el perfil');
    }
  }
  signinDeUsuario(values) {
    const sendData = values;
    this.getDeerExponsor(sendData);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color="#eb0029" />
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <ImageBackground
            imageStyle={{ resizeMode: 'cover' }}
            style={styles.containerImage}
            source={bgSocio} // eslint-disable-line
          >
            <View style={{ flex: 0.1 }} />
            <View style={{ flex: 2 }}>
              <Image
                style={styles.image}
                source={VenadosRojo} // eslint-disable-line
              />
              <RkText style={{ ...styles.textBg, fontSize: 23, }} >Agregar cuenta Socio Venados</RkText>
              <RkText style={{ ...styles.textBg, fontSize: 16, marginTop: 15, marginBottom: -20 }} >¡Solo llena los siquientes campos y listo!</RkText>
              <PartnerForm
                changeForm={this.state.changeForm}
                deerSponsor={this.signinDeUsuario.bind(this)}
              />
            </View>
          </ImageBackground>
        </View>
        <SpinnerModal
          visible={this.state.modalVisible}
          label="Actualizando datos"
        />
      </View>
    );
  }
}

Partner.propTypes = {
  navigation: PropTypes.object,
};

Partner.navigationOptions = {
  title: 'Socio',
  hidden: true
};


let styles = RkStyleSheet.create(() => ({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 16
  },
  textBg: {
    paddingLeft: 25,
    width: Dimensions.get('window').width / 1.5,
    color: 'white',
    textAlign: 'left',
  },
  containerImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  imageBack: {
    left: 10,
    top: size.width / 10,
    height: size.width / 10,
    width: size.width / 10,
  },
  image: {
    marginVertical: 20,
    height: scaleVertical(60),
    marginLeft: -(Dimensions.get('window').width - 50),
    resizeMode: 'contain',
    alignItems: 'flex-start'
  },
  headerTxt: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(22),

  },
  headerInfoTxt: {
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(18)
  },
}));

const mapStateToProps = state => ({
  prop: state.prop,
  usuarioToken: state.reducerSesion,
  profile: state.profileData,
});

export default connect(
  mapStateToProps,
  { joinDeerSponsor, updateProfileData }
)(Partner);
