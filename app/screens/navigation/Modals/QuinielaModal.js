import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, TouchableOpacity, SafeAreaView } from 'react-native';
import { Icon } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { RkButton, RkText } from 'react-native-ui-kitten';
import { Text } from '../../../components/Text';
import NumbersQuiniela from './NumbersQuiniela';

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,1)',
    paddingLeft: 5,
    paddingRight: 5
  },
  title: {
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10
  },
  text: {
    color: 'white',
    textAlign: 'center'
  },
};


class QuinielaModal extends Component {
  componentDidMount() {
    // setTimeout(() => {
    //   this.getWin();
    // }, 5000);
  }
  onShareFacebook() {

  }

  getWin() {
    return (
      <Col>
        <Text style={styles.text}> Ganaste {this.props.data.win} puntos</Text>
        <Text style={styles.title}>en Quiniela Venados</Text>
      </Col>
    );
  }

  render() {
    const { visible, onClose, data } = this.props;
    const numbers = data.win.toString();
    const number1 = parseInt(numbers[0], 32);
    const number2 = parseInt(numbers[1], 32);
    const number3 = parseInt(numbers[2], 32);
    const number4 = parseInt(numbers[3], 32);

    return (
      <Modal
        animationType='fade'
        visible={visible}
        transparent
        onRequestClose={() => {}}
        supportedOrientations={['portrait', 'landscape']}
      >
        <SafeAreaView style={styles.container}>
          <TouchableOpacity onPress={onClose} style={{ padding: 20, marginLeft: -280 }}>
            <Icon type='FontAwesome' name='times' color='white' style={{ width: 30, height: 30, color: 'red' }} />
          </TouchableOpacity>
          <Grid style={{ marginTop: 200 }}>
            <Col>
              <Row style={{ height: 60 }}>
                <Col style={{ height: 60 }}>
                  <Text style={styles.title}>¡Bien jugado Venado!</Text>
                  <Text style={styles.text}>Tu { data.position } anotó una carrera</Text>
                </Col>
              </Row>
              <Row style={{ height: 60, textAlign: 'center' }}>
                <Col />
                <Col />
                <Col>
                  <NumbersQuiniela numberSelected={number1} />
                </Col>
                <Col>
                  <NumbersQuiniela numberSelected={number2} />
                </Col>
                <Col>
                  <NumbersQuiniela numberSelected={number3} />
                </Col>
                <Col>
                  <NumbersQuiniela numberSelected={number4} />
                </Col>
                <Col />
                <Col />
              </Row>
              <Row style={{ height: 60 }}>
                {this.getWin()}
              </Row>
              <Row style={{ height: 60 }}>
                <Col>
                  <RkButton
                    rkType="buttonGoogleLogin"
                    onPress={this.onShareFacebook.bind(this)}
                  >
                    <RkText rkType="h4  DecimaMonoBold" style={{ color: '#fff' }}> COMPARTIR </RkText>
                  </RkButton>
                </Col>
              </Row>

              <Row style={{ height: 60 }}>
                <Col>
                  <RkButton
                    rkType="buttonPartnerLogin"
                    onPress={onClose}
                  >
                    <RkText rkType="h4  DecimaMonoBold" style={{ color: '#fff' }}> ACEPTAR </RkText>
                  </RkButton>
                </Col>
              </Row>
            </Col>
          </Grid>
        </SafeAreaView>
      </Modal>
    );
  }
}


QuinielaModal.propTypes = {
  visible: PropTypes.bool,
  title: PropTypes.string,
  message: PropTypes.string,
  onClose: PropTypes.func,
  data: PropTypes.object,
};


export default QuinielaModal;
