import React, { Component } from 'react';
import { View, Image, ScrollView } from 'react-native';
import { RkButton, RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { Asset, LinearGradient } from 'expo';
import { connect } from 'react-redux';
import { DoubleBounce } from 'react-native-loader';
import { scaleVertical, size } from '../../utils/scale';
import logo from '../../assets/images/logo.png';
import EdsonGarciaPerfil from '../../assets/players/Edson_Garcia_Perfil.png';
import CarlosGastelumPerfil from '../../assets/players/Carlos_Gastelum_Perfil.png';
import AntonyGiansantiPerfil from '../../assets/players/Antony_Giansanti_Perfil.png';
import KennyWilsonPerfil from '../../assets/players/Kenny_Wilson_Perfil.png';
import AlexLiddiPerfil from '../../assets/players/Alex_Liddi_Perfil.png';
import GilbertoGalavizPerfil from '../../assets/players/Gilberto_Galaviz_Perfil.png';
// import EdgarMuñozPerfil from '../../assets/players/Edgar_Muñoz_Perfil.png';
import BrianHernandezPerfil from '../../assets/players/Brian_Hernandez_Perfil.png';
import Latimore from '../../assets/players/Latimore.jpg';
import JesusFabelaPerfil from '../../assets/players/Jesus_Fabela_Perfil.png';
import JoseLunaPerfil from '../../assets/players/Jose_Luna_Perfil.png';
import asaelsanchez from '../../assets/players/asael_sanchez.jpg';
import SebastianVallePerfil from '../../assets/players/Sebastian_Valle_Perfil.png';
import ShermanJhonsonPerfil from '../../assets/players/Sherman_Jhonson_Perfil.png';
import SamarLeyvaPerfil from '../../assets/players/Samar_Leyva_Perfil.png';
import QuincyLatimore from '../../assets/players/Quincy_Latimore_Perfil.png';
import RicardoValenzuela from '../../assets/players/Ricardo_Valenzuela_Perfil.png';
import defaultpic from '../../assets/players/default.jpg';
import Table from '../../components/table';
import { players } from './playersData';


class Player extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      playerGet: this.props.navigation.state.params.player,
      player: false,
      playerImage: false,
    };
    const options = {
      title: 'JUGADOR',
      hidden: true
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() {
    this._cacheResourcesAsync();
    for (let i = 0; i < players.length; i++) {
      const route = players[i];
      if (this.state.playerGet.id === route.id) {
        this.setState({ player: route });
        this.setState({ playerImage: this.getImage(route) });
      }
    }
  }

  getImage(route) {
    switch (route.image) {
      case 'Edson_Garcia_Perfil.png':
        return EdsonGarciaPerfil;
      case 'Carlos_Gastelum_Perfil.png':
        return CarlosGastelumPerfil;
      case 'Antony_Giansanti_Perfil.png':
        return AntonyGiansantiPerfil;
      case 'Kenny_Wilson_Perfil.png':
        return KennyWilsonPerfil;
      case 'Alex_Liddi_Perfil.png':
        return AlexLiddiPerfil;
      case 'Gilberto_Galaviz_Perfil.png':
        return GilbertoGalavizPerfil;
      case 'Edgar_Muñoz_Perfil.png':
        return GilbertoGalavizPerfil;
      case 'Brian_Hernandez_Perfil.png':
        return BrianHernandezPerfil;
      case 'Latimore.jpg':
        return Latimore;
      case 'Jesus_Fabela_Perfil.png':
        return JesusFabelaPerfil;
      case 'Jose_Luna_Perfil.png':
        return JoseLunaPerfil;
      case 'asael_sanchez.jpg':
        return asaelsanchez;
      case 'Sebastian_Valle_Perfil.png':
        return SebastianVallePerfil;
      case 'Sherman_Jhonson_Perfil.png':
        return ShermanJhonsonPerfil;
      case 'Samar_Leyva_Perfil.png':
        return SamarLeyvaPerfil;
      case 'Ricardo_Valenzuela_Perfil.png':
        return RicardoValenzuela;
      case 'Quincy_Latimore_Perfil.png':
        return QuincyLatimore;
      default:
        return defaultpic;
    }
  }

  async _cacheResourcesAsync() {
    const images = [
      logo
    ];

    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }


  tablePitcher() {
    if (this.state.player.statsPiching.length === 0) return <View />;
    return (
      <Table
        header={['AÑO', 'W', 'L', 'ERA', 'SV', 'IP', 'H', 'R', 'ER', 'BB', 'WHIP']}
        content={this.state.player.statsPiching}
        sizes={[2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]}
      />);
  }


  tableOutfield() {
    if (this.state.player.startsOutfield.length === 0) return <View />;
    return (
      <Table
        header={['AÑO', 'AB', 'HR', 'RBI', 'SB', 'H', 'R', 'DOUBLE', 'TRIPLE', 'AVG', 'BB', 'CS', 'SW', 'HBP']}
        content={this.state.player.startsOutfield}
        sizes={[2, 2, 1, 2, 1, 1, 1, 3, 3, 2, 2, 1, 1, 2]}
      />);
  }


  tableInfield() {
    if (this.state.player.statsInfield.length === 0) return <View />;
    return (
      <Table
        header={['AÑO', 'AB', 'HR', 'RBI', 'SB', 'H', 'R', 'DOUBLE', 'TRIPLE', 'AVG', 'BB', 'CS', 'SW', 'HBP']}
        content={this.state.player.statsInfield}
        sizes={[2, 2, 1, 2, 1, 1, 1, 3, 3, 2, 2, 1, 1, 2]}
      />);
  }


  tableCatcher() {
    if (this.state.player.statsBatting.length === 0) return <View />;
    return (
      <Table
        header={['AÑO', 'AB', 'HR', 'RBI', 'SB', 'H', 'R', 'DOUBLE', 'TRIPLE', 'AVG', 'BB', 'CS', 'SW', 'HBP']}
        content={this.state.player.statsBatting}
        sizes={[2, 2, 1, 2, 1, 1, 1, 3, 3, 2, 2, 1, 1, 2]}
      />);
  }

  render() {
    if (!this.state.isReady || !this.state.playerGet
      || !this.state.player
      || !this.state.playerImage) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    return (
      <LinearGradient
        style={styles.screen}
        colors={['#1f2b45', '#202945']}
      >
        <ScrollView style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: scaleVertical(20) }}>
              <Image
                style={styles.image}
                source={logo}
              />
            </View>
            <View style={styles.containerLinea} ><View style={styles.linea} /></View>

            <View style={styles.avatarContainer}>
              <Image
                style={styles.avatar}
                source={this.state.playerImage}
              />
            </View>
            <View style={styles.containerLinea} ><View style={styles.linea} /></View>
            <View style={{ flex: 1, padding: scaleVertical(20), flexDirection: 'row', paddingHorizontal: scaleVertical(10) }}>
              <View style={styles.playerNameContainer}>
                <RkText style={styles.playerName}>{this.state.player.name} </RkText>
                <RkText style={styles.playerName}>{this.state.player.lastName} </RkText>
                <RkText style={styles.playerPST}>{this.state.player.possiton} </RkText>
              </View>
              <View style={{ flex: 1, alignContent: 'center', paddingHorizontal: scaleVertical(40), justifyContent: 'center' }}>
                <RkButton style={styles.buttonDeer} onPress={() => { this.props.navigation.goBack(); }}>
                  <RkText style={styles.buttonDeerTXT}>JUGAR RETO VENADOS</RkText>
                </RkButton>
              </View>
            </View>
            <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: scaleVertical(10) }}>
              <View style={styles.playerNameContainer}>
                <View style={{ flex: 1 }}>
                  <RkText style={styles.playerPST}>NACIMIENTO: </RkText>
                  <RkText style={styles.playerName}>{this.state.player.placeOfBirth} </RkText>
                </View>
                <View style={{ flex: 1 }}>
                  <RkText style={styles.playerPST}>NACIONALIDAD: </RkText>
                  <RkText style={styles.playerName}>{this.state.player.nationality} </RkText>
                </View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <RkText style={styles.playerPST}>EDAD: </RkText>
                  <RkText style={styles.playerName}>{this.state.player.age} </RkText>
                </View>
              </View>
              <View style={styles.playerNameContainer}>
                <View style={{ flex: 1 }}>
                  <RkText style={styles.playerPST}>LANZA: </RkText>
                  <RkText style={styles.playerName}>{this.state.player.throw} </RkText>
                </View>
                <View style={{ flex: 1 }}>
                  <RkText style={styles.playerPST}>BATEA: </RkText>
                  <RkText style={styles.playerName}>{this.state.player.batting} </RkText>
                </View>
              </View>
            </View>
            <View style={styles.containerLinea} ><View style={styles.linea} /></View>
            <View style={{ paddingVertical: scaleVertical(20) }} >
              {this.tablePitcher()}
              {this.tableOutfield()}
              {this.tableInfield()}
              {this.tableCatcher()}
            </View>

            <View style={{ paddingVertical: scaleVertical(20) }} >
              <RkButton style={styles.buttonStyle} onPress={() => { this.props.navigation.navigate('StadisticsMenu'); }}>
                <RkText style={styles.buttonTextStyle}>ESTADÍSTICAS</RkText>
              </RkButton>
            </View>
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  screen: {
    flex: 1,
    backgroundColor: '#2e3192'
  },
  image: {
    marginVertical: scaleVertical(15),
    width: scaleVertical(size.width / 2),
    resizeMode: 'contain'
  },
  playerName: {
    color: '#fff',
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(15)
  },
  avatar: {
    height: size.widthMargin / 2,
    width: size.widthMargin / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  playerNameContainer: {
    flex: 2,
    justifyContent: 'center',
    paddingHorizontal: scaleVertical(20)
  },
  playerPST: {
    color: '#fff',
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(12)
  },
  buttonDeer: {
    borderRadius: scaleVertical(10),
    alingSelf: 'center',
    alignItems: 'center',
    height: size.widthMargin / 4.2,
    width: size.widthMargin / 4,
    backgroundColor: '#eb0029'
  },
  buttonDeerTXT: {
    color: '#fff',
    fontFamily: 'DecimaMono-Bold',
    fontSize: scaleVertical(12),
    textAlign: 'center'
  },
  containerLinea: {
    paddingHorizontal: scaleVertical(20)
  },
  linea: {
    height: scaleVertical(1.2),
    width: size.width - scaleVertical(40),
    backgroundColor: '#fff',
  },

  buttonStyle: {
    width: size.width - scaleVertical(55),
    alignSelf: 'center',
    height: size.width / 7,
    backgroundColor: '#eb0029',
    justifyContent: 'center',
  },
  buttonTextStyle: {
    fontSize: scaleVertical(18),
    color: '#fff',
    textVerticalAlign: 'center',
  }
}));


const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Player);
