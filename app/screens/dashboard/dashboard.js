import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, Image, TouchableOpacity, ImageBackground, UIManager, LayoutAnimation } from 'react-native';
import { RkStyleSheet, RkText } from 'react-native-ui-kitten';
import { Asset, Notifications } from 'expo';
import { DoubleBounce } from 'react-native-loader';
import { scale, size, } from '../../utils/scale';
import { getWalletPoints, formatNumber, getProfile, socket } from '../../Store/Services/Socket';
import InteriorImage from '../../assets/JPEG/Interior.jpg';
import puntos1Image from '../../assets/JPEG/puntos_1.jpg';
import PerfilImage from '../../assets/JPEG/Perfil.jpg';
import image423 from '../../assets/JPEG/423.jpg';
import shutterstock476504869Image from '../../assets/JPEG/shutterstock_476504869.jpg';
import noticias3Image from '../../assets/icons/noticias-3.png';
import check4Image from '../../assets/icons/check-4.png';
import calendario1Image from '../../assets/icons/calendario-1.png';
import resultados3Image from '../../assets/icons/resultados-3.png';
import jugar4Image from '../../assets/icons/jugar-4.png';
import { actionEstablishCurrentGame, actionRefreshWallet } from '../../Store/ACTIONS';
import { getItem } from './../../utils/localdatabase';


class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      wallet: false,
      notification: {},
    };
    const options = {
      title: 'INICIO',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() {
    this._getCurrentProfile();
    this._notificationSubscription = Notifications.addListener(this._handleNotification.bind(this));
    this._cacheResourcesAsync();

    getWalletPoints().then((wallet) => {
      this.setState({ wallet });
    }).catch((err) => {
      console.log(err);
    });

    socket.on('startGame', (game) => {
      this.props.establishCurrentGame(game);
    });

    socket.on('endGame', (game) => {
      this.props.establishCurrentGame(game);
    });

    socket.on('changeInning', (game) => {
      this.props.establishCurrentGame(game);
    });
  }


  componentWillUpdate(nextProps, nextState) {
    let animate = false;
    if (this.state.isReady !== nextState.isReady) {
      animate = true;
    }
    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      LayoutAnimation.easeInEaseOut();
    }
  }

  async onUpdateWallet() {
    try {
      const wallet = await getWalletPoints();
      this.props.refreshWallet(wallet);
    } catch (error) {
      // notifications.show('Ocurrio un error al actualizar imagen de perfil');
    }
  }


  setAction(notification) {
    // console.log('setAction', notification);
    switch (notification.action) {
      case 'quinielaWin':
        this.props.navigation.navigate('QuinielaWinnerMenu', { notification });
        break;
      case 'deerChallengeWin':
        this.props.navigation.navigate('DeerChallengeWinnerMenu', { notification });
        break;
      default:
        break;
    }
  }

  getValuesFromDb() {
    getItem('userID').then((result) => {
      this.setState({ userID: result });
    }).catch(err => console.log(err));
  }


  async _getCurrentProfile() {
    try {
      const currentProfile = await getProfile();
      this.props.setCurrentProfile(currentProfile);
    } catch (error) {
      this.setState({ modalVisible: false });
    }
  }


  _handleNotification(notification) {
    if (notification.data && notification.data.action) {
      this.setAction(notification.data);
    }
  }

  _getImageProfile() {
    // console.log('on _getImageProfile');
    const { profile, usuario } = this.props;
    if (profile.data && profile.data.facebookID) {
      const photoUrl = `https://graph.facebook.com/${profile.data.facebookID}/picture?height=500`;
      return photoUrl;
    }
    if (profile.data && profile.data.avatarURL) {
      return profile.data.avatarURL;
    }
    if (usuario && usuario.providerData && usuario.providerData[0] && usuario.providerData[0].photoURL) {
      return usuario.providerData[0].photoURL;
    }
    return 'https://d500.epimg.net/cincodias/imagenes/2016/07/04/lifestyle/1467646262_522853_1467646344_noticia_normal.jpg';
  }

  async _cacheResourcesAsync() {
    const images = [
      InteriorImage,
      puntos1Image,
      PerfilImage,
      image423,
      shutterstock476504869Image,
      noticias3Image,
      check4Image,
      calendario1Image,
      resultados3Image,
      jugar4Image,
    ];

    const cacheImages = images.map((image) => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });

    return Promise.all(cacheImages);
  }

  renderPoints() {
    return (
      <View style={{ paddingHorizontal: scale(40) }}>
        <View style={{ backgroundColor: 'rgba(0,0,0,0.1)', borderRadius: size.width / 7, bottom: scale(35), height: size.width / 3.5, width: size.width / 3.5, justifyContent: 'center' }}>
          <View style={{ alignSelf: 'center', backgroundColor: 'rgba(235,0,41,0.9)', borderRadius: size.width / 8, height: size.width / 4, width: size.width / 4, justifyContent: 'center', elevation: 5 }}>
            <RkText rkType='h5  TTPollsRegular ' style={{ color: 'white', textAlign: 'center' }} > {formatNumber(`${this.state.wallet.points}`)} </RkText>
            <RkText rkType='s3  RobotoLight ' style={{ color: 'white', textAlign: 'center' }} > PUNTOS </RkText>
          </View>
        </View>
      </View>
    );
  }

  renderGameActive() {
    if (this.props.currentActiveGame && this.props.currentActiveGame.finished !== 1) {
      return (
        <View style={{ paddingHorizontal: scale(20) }}>
          <View style={{ backgroundColor: '#eb0029', borderRadius: scale(10), bottom: scale(35), height: size.width / 4, width: size.width / 4, justifyContent: 'center' }}>
            <RkText rkType='h5  TTPollsRegular ' style={{ color: 'white', textAlign: 'center' }} >JUEGO EN VIVO</RkText>
          </View>
        </View>
      );
    }

    return null;
  }

  render() {
    if (!this.state.isReady || !this.state.wallet || !this.props.profile || !this.props.profile.data) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }


    return (
      <View style={styles.root}>
        <View style={{ flex: 1 }}>
          <TouchableOpacity activeOpacity={1} style={{ flex: 1 }} onPress={() => { this.props.navigation.navigate('PlayMenu'); }} >
            <View style={styles.root}>
              <ImageBackground style={{ flex: 1, flexDirection: 'column-reverse' }} source={InteriorImage} >
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ paddingLeft: scale(20), paddingBottom: scale(30) }}>
                    <RkText rkType='h1  TTPollsBold xxxlarge' style={{ color: 'white' }}>Jugar</RkText>
                  </View>

                  <View style={{ paddingLeft: scale(20), paddingBottom: scale(30) }}>
                    <Image
                      style={{ width: scale(48), height: scale(48), alignSelf: 'center' }}
                      source={jugar4Image}
                    />
                  </View>

                  {this.renderGameActive()}
                </View>
              </ImageBackground>
            </View>
          </TouchableOpacity>
        </View>

        <View style={{ flex: 1 }}>
          <TouchableOpacity activeOpacity={1} style={{ flex: 1 }} onPress={() => { this.props.navigation.navigate('PointsMenu'); }}>
            <View style={{ flex: 1 }}>
              <ImageBackground style={{ flex: 1, flexDirection: 'column-reverse' }} source={puntos1Image} >
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ paddingLeft: scale(20), paddingBottom: scale(30) }}>
                    <RkText rkType='h1  TTPollsBold xxxlarge' style={{ color: 'white' }}>Puntos</RkText>
                  </View>

                  {this.renderPoints()}
                </View>
              </ImageBackground>
            </View>
          </TouchableOpacity>
        </View>

        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 30 }}>
            <TouchableOpacity activeOpacity={1} style={{ flex: 1 }} onPress={() => { this.props.navigation.navigate('ProfileMenu'); }}>
              <View style={{ flex: 1 }}>
                <ImageBackground style={{ flex: 1 }} source={{ uri: this._getImageProfile() }}>
                  <RkText rkType='h3  TTPollsBold ' style={{ color: 'white', textAlign: 'center', paddingVertical: scale(10) }}>Perfil</RkText>
                </ImageBackground>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{ flex: 50 }}>
            <View style={{ flex: 1 }}>
              <TouchableOpacity activeOpacity={1} style={{ flex: 1 }} onPress={() => { this.props.navigation.navigate('CommunityMenu'); }}>
                <View style={{ flex: 1 }}>
                  <ImageBackground style={{ flex: 1 }} source={image423} >
                    <RkText rkType='h3  TTPollsBold ' style={{ color: 'white', textAlign: 'center', paddingVertical: scale(10) }}>Comunidad</RkText>
                  </ImageBackground>
                </View>
              </TouchableOpacity>
            </View>

            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ backgroundColor: '#000', flex: 1 }}>
                <TouchableOpacity activeOpacity={1} style={{ flex: 1, justifyContent: 'space-around', paddingVertical: scale(10) }} onPress={() => { this.props.navigation.navigate('NewsMenu'); }}>
                  <View style={{ flex: 1 }}>
                    <Image
                      style={{ width: scale(32), height: scale(32), alignSelf: 'center' }}
                      source={noticias3Image}
                    />
                  </View>

                  <View style={{ flex: 1 }}>
                    <RkText rkType='h6  TTPollsBold ' style={{ color: 'white', textAlign: 'center' }} >Noticias</RkText>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={{ backgroundColor: '#c8eeff', flex: 1 }}>
                <TouchableOpacity activeOpacity={1} style={{ flex: 1, justifyContent: 'space-around', paddingVertical: scale(10) }} onPress={() => { this.props.navigation.navigate('CheckInMenu'); }}>
                  <View style={{ flex: 1 }}>
                    <Image
                      style={{ width: scale(32), height: scale(32), alignSelf: 'center' }}
                      source={check4Image}
                    />
                  </View>

                  <View style={{ flex: 1 }}>
                    <RkText rkType='h6  TTPollsBold ' style={{ color: '#eb0029', textAlign: 'center' }} >Check-in</RkText>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={{ flex: 26.25 }}>
            <View style={{ backgroundColor: '#eb0029', flex: 1 }}>
              <TouchableOpacity activeOpacity={1} style={{ flex: 1, justifyContent: 'space-around', paddingVertical: scale(10) }} onPress={() => { this.props.navigation.navigate('CalendarMenu'); }}>
                <View style={{ flex: 1 }}>
                  <Image
                    style={{ width: scale(32), height: scale(32), alignSelf: 'center' }}
                    source={calendario1Image}
                  />
                </View>
                <View style={{ flex: 1 }}>
                  <RkText rkType='s1  TTPollsBold ' style={{ textAlign: 'center', color: '#fff' }} >Calendario</RkText>
                </View>
              </TouchableOpacity>
            </View>

            <View style={{ flex: 1 }}>
              <TouchableOpacity activeOpacity={1} style={{ flex: 1 }} onPress={() => { this.props.navigation.navigate('StandingMenu'); }}>
                <View style={{ flex: 1 }}>
                  <ImageBackground style={{ flex: 1, justifyContent: 'space-around', paddingVertical: scale(10) }} source={shutterstock476504869Image} >
                    <View style={{ flex: 1 }}>
                      <Image
                        style={{ width: scale(32), height: scale(32), alignSelf: 'center' }}
                        source={resultados3Image}
                      />
                    </View>

                    <View style={{ flex: 1 }}>
                      <RkText rkType='h6  TTPollsBold ' style={{ textAlign: 'center', color: '#fff' }} >Standing</RkText>
                    </View>
                  </ImageBackground>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}


let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#000'
  },
}));


Dashboard.propTypes = {
  establishCurrentGame: PropTypes.func,
  refreshWallet: PropTypes.func,
  navigation: PropTypes.object,
  usuario: PropTypes.object,
  currentActiveGame: PropTypes.object,
  profile: PropTypes.object,
};


const mapStateToProps = state => ({
  usuario: state.reducerSesion,
  currentActiveGame: state.reducerGame,
  profile: state.profileData,
  currentWallet: state.reducerWallet
});

const mapDispatchToProps = dispatch => ({
  establishCurrentGame: values => {
    console.log('setear juego activo nuevo');
    dispatch(actionEstablishCurrentGame(values));
  },
  refreshWallet: () => {
    getWalletPoints().then((wallet) => {
      dispatch(actionRefreshWallet(wallet));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
