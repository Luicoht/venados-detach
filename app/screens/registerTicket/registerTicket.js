import React from 'react';
import { View, Image } from 'react-native';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { LinearGradient, Asset } from 'expo';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';
import { DoubleBounce } from 'react-native-loader';
import { scale, scaleVertical, size } from '../../utils/scale';
import boleto from '../../assets/icons/boleto-1.png';

class RegisterTicket extends React.Component {
  static navigationOptions = {
    title: 'Registrar'.toUpperCase(),
    hidden: false
  };

  constructor(props) {
    super(props);
    this.state = {
      isReady: false
    };
    const options = {
      title: 'REGISTRAR',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }
  componentWillMount() { }
  componentDidMount() { this._cacheResourcesAsync(); }
  async _cacheResourcesAsync() {
    const images = [
      boleto,

    ];
    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    });
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }

    return (
      <View style={styles.root}>
        <LinearGradient style={styles.root} colors={['#3d4959', '#33363c', '#2a3139', '#292c32']} >
          <View style={{ flex: 3, flexDirection: 'row' }}>
            <View style={{ flex: 1 }} />
            <Image style={styles.image} source={boleto} />
            <View style={{ flex: 1 }} />
          </View>
          <View style={{ flex: 5, paddingHorizontal: scaleVertical(45) }}>
            <RkText style={styles.header}>Registrar boleto</RkText>
            <RkText style={styles.info}>Registra tu boleto de entrada al estadio y gana 100 puntos.</RkText>
            <TextField
              style={styles.input}
              textColor='white'
              tintColor='white'
              baseColor='white'
              label='Ingresa código'
            />
            <RkButton style={styles.botonLogin} > CONFIRMAR </RkButton>
          </View>
        </LinearGradient>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1
  },
  header: {
    color: 'white',
    fontFamily: 'TTPollsBold',
    fontSize: scale(30),
    textAlign: 'center'
  },
  info: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: scale(20),
    textAlign: 'center'
  },
  input: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: scale(20),
    fontAlign: 'center'
  },
  image: {
    flex: 2,
    resizeMode: 'contain'
  },
  botonLogin: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45)
  },
}));

const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterTicket);
