import { Platform, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

//Guideline sizes are based on standard ~5' screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = size => width / guidelineBaseWidth * size;
const scaleVertical = size => height / guidelineBaseHeight * size;
const scaleModerate = (size, factor = 0.5) => size + (scale(size) - size) * factor;

const size = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
  widthMargin: Dimensions.get('window').width - scaleVertical(30),
  onePercent: (Dimensions.get('window').width - scaleVertical(20)) / 100
};
const AppbarHeight = scaleVertical(50);
const AppJumpBarHeight = Platform.OS === 'ios' ? scaleVertical(15) : 0;
const StatusbarHeight = Platform.OS === 'ios' ? scaleVertical(25) : 0;


export { scale, scaleVertical, scaleModerate, size, AppbarHeight, AppJumpBarHeight, StatusbarHeight };
