import React from 'react';
import { TouchableOpacity, View, ScrollView } from 'react-native';
import { RkText, RkButton } from 'react-native-ui-kitten';
import { Asset } from 'expo';
import { connect } from 'react-redux';
import { DoubleBounce } from 'react-native-loader';
import { scaleVertical, size } from '../../utils/scale';
import { AsyncImage } from '../../components/AsyncImage';
import { randomBG } from '../../utils/colorUtils';
import aguilas from '../../assets/images/aguilas.png';
import caneros from '../../assets/images/caneros.png';
import charros from '../../assets/images/charros.png';
import mayos from '../../assets/images/mayos.png';
import naranjeros from '../../assets/images/naranjeros.png';
import tomateros from '../../assets/images/tomateros.png';
import venados from '../../assets/images/venados.png';
import yaquis from '../../assets/images/yaquis.png';
// import { data } from './exampleResult';


class Results extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      info: {},
      isReady: false
    };
  }

  componentDidMount() { this._cacheResourcesAsync(); }

  async _cacheResourcesAsync() {
    const images = [
      aguilas,
      caneros,
      charros,
      mayos,
      naranjeros,
      tomateros,
      venados,
      yaquis,
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  _getRequire(teamName) {
    switch (teamName) {
      case 'Venados':
        return venados;
      case 'Aguilas':
        return aguilas;
      case 'Naranjeros':
        return naranjeros;
      case 'Yaquis':
        return yaquis;
      case 'Mayos':
        return mayos;
      case 'Cañeros':
        return caneros;
      case 'Tomateros':
        return tomateros;
      default:
        return charros;
    }
  }


  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    const content = this.state.info.map((route) => {
      return (
        <TouchableOpacity underlayColor={'rgba(0,0,0,1)'} activeOpacity={1} >
          <View style={{ paddingHorizontal: scaleVertical(15), paddingVertical: scaleVertical(10) }}>
            <View style={{ backgroundColor: '#f2f2f2' }}>
              <View style={{ backgroundColor: randomBG() }}>
                <View style={{ backgroundColor: '#c8eeff', paddingVertical: scaleVertical(10), alignContent: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                  <RkText style={{ color: '#002157', textAlign: 'center', fontSize: scaleVertical(15) }}> {route.day} </RkText>
                  <RkText style={{ color: '#002157', textAlign: 'center', fontSize: scaleVertical(15) }}> {route.time} </RkText>
                </View>
              </View>
              <View style={{ paddingVertical: scaleVertical(10), flexDirection: 'row' }}>
                <View style={{ flex: 5, flexDirection: 'row', justifyContent: 'center' }}>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <AsyncImage
                      style={{
                        height: scaleVertical(40),
                        width: scaleVertical(40),
                        alignSelf: 'center'
                      }}
                      source={{ uri: route.homeImage }}
                      placeholderColor='transparent' />
                    <RkText style={{ textAlign: 'center', fontSize: scaleVertical(12), fontFamily: 'Roboto-Regular' }}> {route.home} </RkText>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', paddingVertical: scaleVertical(20) }}>
                    <RkText style={{ textVerticalAlign: 'center', fontFamily: 'Roboto-Black', color: '#eb0029', textAlign: 'center', fontSize: scaleVertical(20) }}> {route.homePoints} </RkText>
                    <RkText style={{ textVerticalAlign: 'center', fontFamily: 'Roboto-Light', textAlign: 'center', fontSize: scaleVertical(20) }}> VS </RkText>
                    <RkText style={{ textVerticalAlign: 'center', fontFamily: 'Roboto-Black', color: '#002157', textAlign: 'center', fontSize: scaleVertical(20) }}> {route.awayPoints} </RkText>
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <AsyncImage
                      style={{
                        height: scaleVertical(40),
                        width: scaleVertical(40),
                        alignSelf: 'center'
                      }}
                      source={{ uri: route.awayImage }}
                      placeholderColor='transparent' />
                    <RkText style={{ textAlign: 'center', fontSize: scaleVertical(12), fontFamily: 'Roboto-Regular' }}> {route.away} </RkText>
                  </View>
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <RkButton style={{ alignSelf: 'center', backgroundColor: 'transparent', height: scaleVertical(30), width: scaleVertical(40) }}>
                    <RkText style={{ color: '#2e3192', fontSize: scaleVertical(11) }}> VER </RkText>
                  </RkButton>
                </View>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      );
    });
    return (
      <ScrollView style={{ flex: 1 }}>
        <RkText> {this.props.passedData} </RkText>
        {content}
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Results);
