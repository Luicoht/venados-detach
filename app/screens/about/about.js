import React from 'react';
import {
  View, Platform, Image, ImageBackground,
  ScrollView
} from 'react-native';
import { RkStyleSheet, RkText, UIManager, LayoutAnimation } from 'react-native-ui-kitten';
import { Asset } from 'expo';
import { DoubleBounce } from 'react-native-loader';
import { connect } from 'react-redux';
import { scale, scaleVertical, size } from '../../utils/scale';
import background from '../../assets/JPEG/DUe9U7SUQAAT4Kj.jpg';
import VenadosRojo from '../../assets/images/VenadosRojo.png';
import logo from '../../assets/images/logo.png';


class About extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false
    };
    const options = {
      title: 'VENADOS',
      hidden: true
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() { this._cacheResourcesAsync(); }

  componentWillUpdate(nextProps, nextState) {
    let animate = false;
    if (this.state.isReady !== nextState.isReady) {
      animate = true;
    }
    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      LayoutAnimation.easeInEaseOut();
    }
  }

  async _cacheResourcesAsync() {
    const images = [
      background,
      VenadosRojo,
      logo
    ];
    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    });
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    return (
      <View style={styles.root}>
        <ScrollView style={styles.root}>
          <View style={{ flex: 4 }}>
            <View style={{ marginTop: scaleVertical(20), }}>
              <Image
                style={styles.image}
                source={logo}
              />
            </View>
            <RkText style={styles.headerRed}>Los Venados de Mazatlán</RkText>
            <RkText style={styles.header}>Son un equipo de béisbol profesional integrante de la liga Mexicana delPacífico (LMP), con sede en la ciudad de Mazatlán, Sinaloa, México.</RkText>
          </View>
          <View style={{ flex: 2 }}>
            <ImageBackground style={{
              width: size.width,
              height: size.height / 3.5,
            }} source={background} >
            </ImageBackground>
          </View>
          <View style={{ flex: 4 }}>
            <RkText style={styles.info}>Cuenta con 9 campeonatos en su historia en la liga y 2 campeonatos en Serie del Caribe. El equipo juega en el Estadio Teodoro Mariscal.</RkText>
            <RkText style={styles.info}>Los Venados de Mazatlán  participaron en la vieja Liga de la Costa del Pacífico, quienes consiguieron 5 campeonatos en 13 temporadas.</RkText>
          </View>
          <View style={styles.jump} />
        </ScrollView>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(theme => ({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
  playContainer: {
    flex: 1
    // width: size.width,
    // height: size.height / 3
  },
  imageBg: {
    width: size.width,
    height: size.height / 3
  },
  textBg: {
    color: 'white',
    fontFamily: 'TTPollsBold',
    fontSize: scale(40),
    top: Platform.OS === 'ios' ? scaleVertical(155) : scaleVertical(120),
    left: scale(20)
  },
  headerRed: {
    color: '#eb0029',
    fontFamily: 'TTPollsBold',
    fontSize: scale(20),
    paddingHorizontal: scaleVertical(15),
    paddingTop: scaleVertical(15)
  },
  header: {

    fontFamily: 'TTPollsBold',
    fontSize: scale(20),
    paddingHorizontal: scaleVertical(15),
    paddingBottom: scaleVertical(15)

  },
  info: {
    fontFamily: 'Roboto-Regular',
    fontSize: scale(20),
    paddingHorizontal: scaleVertical(15),
    paddingVertical: scaleVertical(15)
  },
  image: {

    height: scaleVertical(60),
    resizeMode: 'contain'
  },

  boton: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45),
  },
  jump: {
    height: scaleVertical(25)
  }
}));

const mapStateToProps = state => ({ prop: state.prop });
const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(About);
