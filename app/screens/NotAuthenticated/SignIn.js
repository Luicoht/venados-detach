import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, ScrollView, BackHandler } from 'react-native';
import { connect } from 'react-redux';
import { RkButton, RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { Asset, LinearGradient } from 'expo';
import { DoubleBounce } from 'react-native-loader';
import logoImage from '../../assets/images/logo.png';
import SignInForm from './Forms/SignInForm';
import { actionSignIn, actionLoginGoogle, actionLoginFacebook } from '../../Store/ACTIONS';
import { scale, scaleVertical, size } from '../../utils/scale';


class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false
    };
  }

  componentDidMount() {
    this._cacheResourcesAsync();
    this.initBackHandler();
  }

  async _cacheResourcesAsync() {
    const images = [logoImage];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });

    return Promise.all(cacheImages);
  }

  signinDeUsuario(values) {
    this.props.login(values);
  }

  initBackHandler() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.navigation.goBack();
    });
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color="#eb0029" />
        </View>
      );
    }

    return (
      <LinearGradient style={styles.screen} colors={['#e4e7ed', '#eaebed', '#c2beb5']}>
        <ScrollView style={{ flex: 1 }}>
          <View style={styles.header}>
            <Image style={styles.image} source={logoImage} />
            <RkText rkType="h0 xxlarge TTPollsBlack">Iniciar sesión</RkText>
          </View>
          <View style={{ flex: 1, paddingHorizontal: scaleVertical(45), paddingVertical: scaleVertical(15) }}>
            <SignInForm login={this.signinDeUsuario.bind(this)} />
          </View>
          <View style={{ flex: 1 }} >
            <View style={{ paddingHorizontal: scale(10) }} >
              <RkText
                style={styles.link}
                onPress={() => { this.props.navigation.navigate('Recover'); }}
              > ¿OLVIDASTE TU CONTRASEÑA? </RkText>
            </View>

            {/* <View style={styles.loginButtonContainer} >
              <RkButton
                rkType="buttonPartnerLogin"
                onPress={() => { this.props.navigation.navigate('Partner'); }}
              >
                <RkText rkType="h4  DecimaMonoBold" style={{ color: '#000' }}> SOY SOCIO VENADO </RkText>
              </RkButton>
            </View> */}

            <View style={styles.loginButtonContainer} >
              <RkButton
                rkType="buttonGoogleLogin"
                onPress={() => { this.props.loginGoogle(); }}
              >
                <RkText rkType="h4  DecimaMonoBold" style={{ color: '#fff' }}> ENTRAR CON GOOGLE</RkText>
              </RkButton>
            </View>

            <View style={styles.loginButtonContainer} >
              <RkButton
                rkType="buttonFacebookLogin"
                onPress={() => { this.props.loginFacebook(); }}
              >
                <RkText rkType="h4  DecimaMonoBold" style={{ color: '#fff' }}> ENTRAR CON FACEBOOK</RkText>
              </RkButton>
            </View>
          </View>

          <View style={{ paddingBottom: scaleVertical(25), justifyContent: 'space-around', paddingLeft: scaleVertical(10), paddingRight: scaleVertical(10), paddingTop: scaleVertical(15) }} >
            <RkText
              style={styles.link}
              onPress={() => { this.props.navigation.navigate('SignUp'); }}
            > ¿NO TIENES CUENTA? REGÍSTRATE </RkText>
          </View>
        </ScrollView>
      </LinearGradient>
    );
  }
}


let styles = RkStyleSheet.create(() => ({
  screen: {
    flex: 1,
  },
  link: {
    color: '#2700fd',
    fontSize: scaleVertical(15),
    textAlign: 'center'
  },

  image: {
    paddingVertical: 20,
    height: scaleVertical(70),
    resizeMode: 'contain'
  },
  header: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: scaleVertical(20),
  },

  loginButtonContainer: {
    paddingHorizontal: scaleVertical(45),
    paddingTop: scaleVertical(15)
  }
}));


SignIn.signinDeUsuario = {};


SignIn.propTypes = {
  login: PropTypes.func,
  loginGoogle: PropTypes.func,
  navigation: PropTypes.object,
  loginFacebook: PropTypes.func,
};


const mapStateToProps = state => ({
  prop: state.prop
});


const mapDispatchToProps = (dispatch) => ({
  login: datos => {
    dispatch(actionSignIn(datos));
  },
  loginGoogle: () => {
    dispatch(actionLoginGoogle());
  },
  loginFacebook: () => {
    dispatch(actionLoginFacebook());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
