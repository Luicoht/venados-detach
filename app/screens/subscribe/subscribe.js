import React from 'react';
import { View, Image, ImageBackground } from 'react-native';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { Asset } from 'expo';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';
import { DoubleBounce } from 'react-native-loader';
import { scale, scaleVertical, size } from '../../utils/scale';
import home from '../../assets/JPEG/home-plate-black.jpeg';
import VenadosRojo from '../../assets/images/VenadosRojo.png';

class Subcribe extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false
    };
    const options = {
      title: 'SUSCRIBIRSE',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }
  componentDidMount() { this._cacheResourcesAsync(); }
  async _cacheResourcesAsync() {
    const images = [
      home,
      VenadosRojo,
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }
  registro = values => {
    this.props.registro(values);
  };
  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    return (
      <View style={styles.root}>
        <View style={{ flex: 2 }}>
          <ImageBackground style={{ flex: 1 }} source={home} >
            <RkText style={styles.textBg} >Agregar cuenta socio Venados</RkText>
          </ImageBackground>
          <Image
            style={styles.IconBG}
            source={VenadosRojo}
          />
        </View>
        <View style={{ flex: 3, padding: scaleVertical(20) }}>
          <RkText style={styles.textInfo} >!Solo llena los siguientes campos y listo¡</RkText>
          <TextField
            textColor='white'
            tintColor='white'
            baseColor='white'
            label='Numero de socio (8 dígitos)'
            error=''
          />
          <TextField
            textColor='white'
            tintColor='white'
            baseColor='white'
            error=''
            label='Contraseña'
          />
          <RkButton style={styles.boton} > AGREGAR </RkButton>
        </View>
      </View>
    );
  }
}


let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1
  },
  playContainer: {
    flex: 1
    // width: size.width,
    // height: size.height / 3
  },
  imageBg: {
    width: size.width,
    height: size.height / 3
  },
  label: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
  },
  textInfo: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: scale(20),
    paddingRight: scaleVertical(25)
    // top:  size.height/ 5,
    // left: scaleVertical(20)
  },
  textBg: {
    color: 'white',
    fontFamily: 'TTPollsBold',
    fontSize: scale(30),
    top: size.height / 5,
    left: scaleVertical(20)
  },
  IconBG: {
    // width: scaleVertical(64),
    height: scaleVertical(64),
    alignSelf: 'center',
    resizeMode: 'contain',
    // padding: scaleVertical(5),
    top: size.height / 24,
    position: 'absolute'
  },
  subBar: {
    flexDirection: 'row',
    flex: 1
  },
  subMenuItems: {
    width: (size.width / 3) - scaleVertical(10),
    height: (size.height / 6) - scaleVertical(10),
    flexDirection: 'column',
    fontSize: 8,
  },
  subMenuIcon: {
    marginTop: 1,
    marginBottom: 1,
  },
  subMenuText: {
    color: '#fff',
    fontSize: scaleVertical(14),
    alignSelf: 'center'
  },
  boton: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45)
  },
}));
const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Subcribe);
