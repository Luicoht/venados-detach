import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import { Icon } from 'native-base';
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu';
import { ImagePicker, Permissions } from 'expo';
import notifications from '../utils/notifications';
import { uploadImage } from '../Store/Services/Firebase';
import SpinnerModal from './SpinnerModal';

const GRAY = 'rgb(120,120,120)';


class SelectPicker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasCameraPermission: null,
      hasCameraRollPermission: null,
      uploadingImage: false,
    };
  }

  componentDidMount() {
    this.requestCameraPermission();
  }

  async onCameraPress() {
    const { hasCameraPermission, hasCameraRollPermission } = this.state;

    const launchCamera = () => {
      const options = {
        base64: true,
        allowsEditing: true,
        aspect: this.props.aspectRatio || [4, 3],
      };

      ImagePicker.launchCameraAsync(options).then((result) => {
        if (!result.cancelled) {
          this.uploadImage(result);
        }
      }).catch(() => {
        notifications.show('No fue posible acceder a la camara');
      });
    };

    if (hasCameraPermission && hasCameraRollPermission) {
      launchCamera();
    } else {
      const permissionsGranted = await this.requestCameraPermission();

      if (permissionsGranted) {
        launchCamera();
      } else {
        notifications.show('Se requiere permiso a la camara para acceder a esta funcionalidad');
      }
    }
  }

  async onImageFromDevicePress() {
    const { hasCameraRollPermission } = this.state;

    const launchImageLibrary = () => {
      const options = {
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        base64: true,
        allowsEditing: true,
        aspect: this.props.aspectRatio || [4, 3],
      };

      ImagePicker.launchImageLibraryAsync(options).then((result) => {
        if (!result.cancelled) {
          this.uploadImage(result);
        }
      }).catch(() => {
        notifications.show('No fue posible acceder a sus documentos');
      });
    };

    if (hasCameraRollPermission) {
      launchImageLibrary();
    } else {
      const permissionsGranted = await this.requestCameraPermission();

      if (permissionsGranted) {
        launchImageLibrary();
      } else {
        notifications.show('Se requiere permiso a sus documentos para acceder a esta funcionalidad');
      }
    }
  }

  async requestCameraPermission() {
    const cameraResponse = await Permissions.askAsync(Permissions.CAMERA);
    const cameraGranted = cameraResponse.status === 'granted';

    if (cameraGranted) {
      this.setState({ hasCameraPermission: cameraGranted });
    } else {
      return false;
    }

    const cameraRollresponse = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    const cameraRollGranted = cameraRollresponse.status === 'granted';

    if (cameraRollGranted) {
      this.setState({ hasCameraRollPermission: cameraRollGranted });
      return true;
    }

    return false;
  }

  urlToBlob(url) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest(); // eslint-disable-line
      xhr.onerror = reject;

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          resolve(xhr.response);
        }
      };

      xhr.open('GET', url);
      xhr.responseType = 'blob'; // convert type
      xhr.send();
    });
  }

  async uploadImage(result) {
    if (this.props.onImageUpload) {
      this.setState({ uploadingImage: true });
      const blob = await this.urlToBlob(result.uri);

      uploadImage(blob, this.props.name, (image) => {
        this.setState({ uploadingImage: false });

        this.props.onImageUpload({
          imageUri: image,
          image: result.uri,
          // imageBase64: result.base64,
        });
      });
    }

    if (this.props.onImageSelected) {
      this.props.onImageSelected(result);
    }
  }

  render() {
    return (
      <View style={this.props.style}>
        <Menu rendererProps={{ preferredPlacement: 'bottom' }}>
          <MenuTrigger>
            { this.props.children }
          </MenuTrigger>

          <MenuOptions style={{ padding: 5 }}>
            <MenuOption onSelect={this.onImageFromDevicePress.bind(this)}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon name="md-folder" style={{ color: GRAY, marginRight: 5 }} />

                <Text style={{ color: GRAY }} numberOfLines={2}>
                  Imagen desde mis documentos
                </Text>
              </View>
            </MenuOption>

            <MenuOption onSelect={this.onCameraPress.bind(this)}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon name="md-camera" style={{ color: GRAY, marginRight: 5 }} />
                <Text style={{ color: GRAY }}>Tomar fotografia</Text>
              </View>
            </MenuOption>
          </MenuOptions>
        </Menu>

        <SpinnerModal
          visible={this.state.uploadingImage}
          label="Subiendo imagen"
        />
      </View>
    );
  }
}


SelectPicker.propTypes = {
  children: PropTypes.any,
  style: PropTypes.object,
  aspectRatio: PropTypes.array,
  onImageUpload: PropTypes.func,
  onImageSelected: PropTypes.func,
  name: PropTypes.string,
};


export default SelectPicker;
