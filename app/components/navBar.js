import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, View, Platform, TouchableOpacity, ImageBackground, UIManager, LayoutAnimation } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import _ from 'lodash';
import { RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { Asset, AppLoading } from 'expo';
import { scale, scaleVertical, size, AppbarHeight, AppJumpBarHeight } from '../utils/scale';
import { getCurrentActiveGame, socket } from '../Store/Services/Socket';
import burger from '../assets/icons/burger.png';
import dont from '../assets/icons/dont.png';
import back from '../assets/icons/back.png';
import logo from '../assets/images/logo.png';
import enVivo from '../assets/icons/en_vivo.png';
import bag from '../assets/icons/bag-1.png';
import notification from '../assets/icons/notificacion-2.png';
import { MyGamesSubItem, StatsSubItem, DeerChallengeSubItem } from './subItems';
import LineUpSubItem from './lineUpSubItem';
import QuinielaModals from './quinielaSubItem';
// porno porno porno


export class NavBar extends React.Component {
  constructor(props) {
    super(props);
    const subMenu = (
      <View style={styles.subNav}>
        <MyGamesSubItem navigation={this.props.navigation} />
        <StatsSubItem navigation={this.props.navigation} />
        <DeerChallengeSubItem navigation={this.props.navigation} />
        <QuinielaModals navigation={this.props.navigation} />
        <LineUpSubItem navigation={this.props.navigation} />
      </View>
    );
    this.state = {
      isReady: false,
      game: false,
      submenu: subMenu
    };
  }

  componentDidMount() {
    socket.on('startGame', () => {
      this._getGame();
    });

    socket.on('endGame', () => {
      this._getGame();
    });

    socket.on('changeInning', () => {
      this._getGame();
    });
    this._getGame();
  }




  componentWillUpdate(nextProps, nextState) {
    let animate = false;

    if (this.state.isReady !== nextState.isReady || this.state.game !== nextState.game) {
      animate = true;
    }

    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }

      LayoutAnimation.easeInEaseOut();
    }
  }


  _getGame() {
    getCurrentActiveGame()
      .then((game) => {
        this.setState({ game });
        if (!game) {
          this.setState({ game: { active: 0, finished: 1, inning: 0 } });
        }
      })
      .catch((err) => {
        this.setState({ game: { active: 0, finished: 1, inning: 0 } });
      });
  }



  _goTo(route) {
    if (this.props.navigation.state.routeName !== route) {
      if (route === 'DashboardMenu') {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'DashboardMenu' })],
        });
        this.props.navigation.dispatch(resetAction);
      } else {
        this.props.navigation.navigate(route);
      }
    }
  }

  async _cacheResourcesAsync() {
    const images = [
      burger,
      dont,
      back,
      logo,
      enVivo,
      bag,
      notification
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    return Promise.all(cacheImages);
  }

  _renderSubMenu(options) {
    if (!options.hidden) {
      const renderLeftContent = () => {
        return (this.state.submenu);
      };
      return renderLeftContent();
    }
  }

  _menuNotHidden(options) {
    const renderLeftContent = () => {
      const index = this.props.navigation.dangerouslyGetParent().state.index;
      if (index > 0) {
        return (
          <TouchableOpacity activeOpacity={1} style={styles.menuDrawerButton} onPress={() => {
            this.props.navigation.goBack();
          }}
          >
            <ImageBackground resizeMode='contain' style={styles.bgImage} source={back} />
          </TouchableOpacity>
        );
      }
      return (
        <TouchableOpacity activeOpacity={1} style={styles.menuDrawerButton} onPress={() => {
          this.props.navigation.toggleDrawer();
        }}
        >
          <ImageBackground resizeMode='contain' style={styles.bgImage} source={burger} />
        </TouchableOpacity>
      );
    };

    const liveIcon = (this.state.game && this.state.game.finished !== 1) ?
      (<View style={styles.liveImageContainer} >
        <ImageBackground resizeMode='contain' style={styles.imageBackground} source={enVivo} />
      </View>) : <View />;
    const menu = (
      <View style={styles.title} >
        <View style={{ flex: 1 }} >
          {renderLeftContent()}
        </View>
        <TouchableOpacity onPress={() => { this._goTo('DashboardMenu'); }} activeOpacity={1} style={{ flex: 2.5 }} >
          <ImageBackground resizeMode='contain' style={styles.bgImage} source={logo} />
        </TouchableOpacity>
        <View style={styles.containerTitleAndLiveImage} >
          <View style={styles.titleContainer} >
            <RkText style={styles.titleText}>{options.title}</RkText>
          </View>
          {liveIcon}
        </View>
        <View style={styles.extrasContainer} >
          <View style={styles.extrasItem} >
            <TouchableOpacity onPress={() => { this._goTo('StoreMenu'); }} activeOpacity={1} style={{ flex: 2.5 }} >
              <ImageBackground resizeMode='contain' style={styles.bgImage} source={bag} />
            </TouchableOpacity>
          </View>
          {/* <View style={styles.extrasItem} >
            <ImageBackground resizeMode='contain' style={styles.imageBackground} source={notification} />
          </View> */}
        </View>
      </View>
    );
    return menu;
  }

  _menuHidden() {
    const renderLeftContent = () => {
      const index = this.props.navigation.dangerouslyGetParent().state.index;
      if (index > 0) {
        return (
          <View style={{ backgroundColor: '#000', height: AppbarHeight, flexDirection: 'row' }}>
            <View style={{ flex: 1, paddingHorizontal: scaleVertical(5), paddingVertical: scaleVertical(10) }} >
              <TouchableOpacity activeOpacity={1} style={{
                flex: 1,
              }} onPress={() => {
                this.props.navigation.goBack();
              }}
              >
                <ImageBackground resizeMode='contain' style={styles.dontImage} source={dont} />
              </TouchableOpacity>
            </View>
            <View style={{ flex: 7 }} />
          </View>
        );
      }
      return (
        <View style={{ backgroundColor: '#000', height: AppbarHeight, flexDirection: 'row' }}>
          <View style={{ flex: 1, paddingHorizontal: scaleVertical(5), paddingVertical: scaleVertical(10) }} >
            <TouchableOpacity activeOpacity={1} style={{
              flex: 1,
            }} onPress={() => {
              this.props.navigation.navigate('DrawerOpen');
            }}
            >
              <ImageBackground
                resizeMode='contain'
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  bottom: 0,
                  right: 0,
                  alignSelf: 'center'
                }} source={burger}>
              </ImageBackground>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 7 }} />
        </View>
      );
    };
    return renderLeftContent();
  }

  _renderMenu(options) {
    if (!options.hidden) {
      return this._menuNotHidden(options);
    }
    return this._menuHidden();
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }
    let options;
    if (this.props.navigation.state.params && this.props.navigation.state.params.options) {
      options = this.props.navigation.state.params.options;
    } else {
      options = {
        title: '',
        hidden: false
      };
    }

    //   <View style={options.hidden ? styles.subNavHidden : styles.layout}>
    //   <View style={options.hidden ? styles.subNavHidden : styles.container}>
    //     {this._renderMenu(options)}
    //   </View>
    // </View>
    return (
      <View>

        <View>
          <View style={{
            backgroundColor: options.hidden ? '#000' : '#fff',
            height: Platform.OS === 'ios' ? AppJumpBarHeight : 0,
          }} />
          <View style={{
            height: AppbarHeight,
            backgroundColor: '#fff',
          }}>
            {this._renderMenu(options)}
          </View>
        </View>


        <View style={styles.layoutSubNav}>
          <View style={options.hidden ? styles.containerSubNavHidden : styles.containerSubNav}>
            {this._renderSubMenu(options)}
          </View>
        </View>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(theme => ({
  layout: {
    height: AppbarHeight,
    // paddingTop: Platform.OS === 'ios' ? StatusbarHeight : 0,
    // paddingBottom: Platform.OS === 'ios' ? scaleVertical(10) : 0
  },


  container: {
    paddingTop: Platform.OS === 'ios' ? AppJumpBarHeight : 0,
    backgroundColor: 'cyan',
    height: AppbarHeight,
  },



  layoutSubNav: {
    backgroundColor: '#000',
  },
  containerSubNav: {
    flexDirection: 'row',
    height: AppbarHeight
  },
  containerSubNavHidden: {
    flexDirection: 'row',
    height: 0,
  },



  subNav: {
    flexDirection: 'row',
  },
  subNavHidden: {
    height: AppbarHeight,
    paddingTop: Platform.OS === 'ios' ? AppJumpBarHeight : 0,
    backgroundColor: '#eaeaea'
    // backgroundColor: 'rgba(0,0,0,0.85)',
  },





  title: {
    ...StyleSheet.absoluteFillObject,
    flexDirection: 'row',
  },
  info: {
    flexDirection: 'row',
  },
  test2: {
    height: Platform.OS === 'ios' ? AppbarHeight - scaleVertical(10) : AppbarHeight - scaleVertical(15)
  },
  test2Hidden: {
    height: AppbarHeight - scaleVertical(15),
    alignItems: 'center',
  },
  subMenuItemHidden: {
    width: size.width / 5,
    fontSize: scale(5),
    left: scale(1),
    marginTop: scaleVertical(10),
  },
  bgImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  },
  bgImageHidden: {
    position: 'absolute',
    paddingLeft: scaleVertical(10),
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: Platform.OS === 'ios' ? AppbarHeight - scaleVertical(5) : AppbarHeight - scaleVertical(10),
    width: Platform.OS === 'ios' ? AppbarHeight - scaleVertical(5) : AppbarHeight - scaleVertical(10),
    alignSelf: 'center'
  },
  menuDrawerButton: {
    flex: 1,
  },
  containerTitleAndLiveImage: {
    flex: 4.5,
    alignContent: 'space-around',
    flexDirection: 'row'
  },
  titleContainer: {
    flex: 2,
    alignItems: 'center',
    alignSelf: 'center'
  },
  titleText: {
    alignItems: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: scale(12),
    textAlignVertical: 'center'
  },
  imageBackground: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  liveImageContainer: {
    flex: 0.75,
    margin: 0,
    padding: 0
  },
  extrasContainer: {
    flex: 2,
    flexDirection: 'row'
  },
  extrasItem: {
    flex: 1,
    margin: scale(5)
  },

  imageMenuButton: {

    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    alignSelf: 'center'
  },
  dontImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    alignSelf: 'center'
  }
}));
