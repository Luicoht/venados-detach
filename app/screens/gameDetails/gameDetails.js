import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { View, ScrollView, Dimensions } from 'react-native';
import { RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { DoubleBounce } from 'react-native-loader';
import { connect } from 'react-redux';
import HTML from 'react-native-render-html';
import { scale, scaleVertical, size } from '../../utils/scale';
import { Scoreboard } from '../../components/scoreboard';

// import { tableHeaderPitcher, tablePitcher, tableHeaderBatter, tableBatter } from './detailsData';

const months = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre'
];
class GameDetails extends React.Component {
  constructor(props) {
    super(props);
    const marking = this.props.navigation.state.params.marking;
    this.state = {
      marking: marking,
      gameDetails: false,
      dateArray: marking.date.split('-'),
      boxscore: false
    };
    const options = {
      title: 'DETALLES',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }


  componentDidMount() {
    // https://api.lmp.mx/medios-v1.2/boxscore/{game_id}
    // https://api.lmp.mx/medios-v1.2/gameday/{game_id}
    const config = {
      method: 'get',
      url: `https://api.lmp.mx/medios-v1.2/scoreboard/${this.state.marking.year}/${this.state.marking.month}/${this.state.marking.day}`,
    };
    axios(config).then((response) => {
      const serverResponse = response.data.response;
      for (let i = 0; i < serverResponse.length; i++) {
        const game = serverResponse[i];
        if (game.team_home === this.state.marking.abbreviation || game.team_away === this.state.marking.abbreviation) {
          this.setState({ gameDetails: game });
          this.getBoxscore();
        }
      }
    }).catch((error) => {
      this.setState({ isReady: true });
    });
  }


  getBoxscore() {
    const config = {
      method: 'get',
      url: `https://api.lmp.mx/medios-v1.2/boxscore/${this.state.gameDetails.id}`,
    };

    axios(config).then((response) => {
      let serverResponse;
      if (response.data.length !== 0) { serverResponse = response.data.response[0]; } else { serverResponse = 'NoGame'; }
      this.setState({ boxscore: serverResponse });
    }).catch((error) => {
      console.log('error', error);
      this.setState({ isReady: true });
    });
  }

  _getPitcherContentTable(type) {
    const tablePitcher = type === 1 ? this.state.boxscore.pitching.home : this.state.boxscore.pitching.away;

    const row = tablePitcher.map((player, index) => {
      const bg = index % 2 === 0 ? '#c8eeff' : '#e6e7e9';
      const col = (
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: bg }}>
          <View style={{ flex: 3, justifyContent: 'center' }}>
            <RkText style={{ color: '#eb0029', fontSize: scaleVertical(12), textAlign: 'center' }}>{player.name.split(' (')[0]}</RkText>
          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.bb}</RkText>
          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.er}</RkText>
          </View>
          <View style={{ flex: 2, justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.era}</RkText>
          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.h}</RkText>
          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.hr}</RkText>
          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.ip}</RkText>
          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.r}</RkText>
          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.so}</RkText>
          </View>
        </View>
      );
      return col;
    });


    const content = (
      <View style={{ height: size.AppbarHeight / 1.3, flex: 1, backgroundColor: '#e6e7e9', justifyContent: 'center' }} >
        {row}
      </View>
    );
    return content;
  }

  _getPitcherHeaderTable(type) {
    const team = type === 1 ? this.state.boxscore.homeName : this.state.boxscore.awayName;
    const text =
      (<View style={{ flex: 1, justifyContent: 'center' }}>
        <RkText style={{ color: '#002157', fontSize: scaleVertical(20), textAlign: 'left' }}>Estadísticas picheo {team}</RkText>
      </View>);
    const row = (
      <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
        <View style={{ flex: 3, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>Nombre</RkText>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>BB</RkText>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>ER</RkText>
        </View>
        <View style={{ flex: 2, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>ERA</RkText>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>H</RkText>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>HR</RkText>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>IP</RkText>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>R</RkText>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>SO</RkText>
        </View>
      </View>
    );

    const header = (

      <View style={{ flex: 1 }}>
        {text}
        <View style={{ height: size.AppbarHeight / 2, flex: 1, backgroundColor: '#000', flexDirection: 'row', justifyContent: 'center', padding: scaleVertical(5) }}>
          {row}
        </View>
      </View>
    );
    return header;
  }


  _getPitcherTableAway() {
    const header = this._getPitcherHeaderTable(2);
    const content = this._getPitcherContentTable(2);
    const solve = (
      <View style={{ flex: 1 }}>
        {header}
        {content}
      </View>
    );
    return solve;
  }

  _getPitcherTableHome() {
    const header = this._getPitcherHeaderTable(1);
    const content = this._getPitcherContentTable(1);
    const solve = (
      <View style={{ flex: 1 }}>
        {header}
        {content}
      </View>
    );
    return solve;
  }


  _getBattingContentTable(type) {
    const tablePitcher = type === 1 ? this.state.boxscore.batting.home : this.state.boxscore.batting.away;

    const row = tablePitcher.map((player, index) => {
      const bg = index % 2 === 0 ? '#c8eeff' : '#dadada';
      const data = (
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: bg }}>

          <View style={{ width: scale(80), justifyContent: 'center' }}>
            <RkText style={{ color: '#eb0029', fontSize: scaleVertical(12), textAlign: 'center' }}>{player.name}</RkText>
          </View>
          <View style={{ width: scale(30), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.a}</RkText>
          </View>
          <View style={{ width: scale(30), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.ab}</RkText>
          </View>
          <View style={{ width: scale(35), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.avg}</RkText>
          </View>
          <View style={{ width: scale(30), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.bb}</RkText>
          </View>
          <View style={{ width: scale(30), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.h}</RkText>
          </View>
          <View style={{ width: scale(30), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.h2b}</RkText>
          </View>
          <View style={{ width: scale(30), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.h3b}</RkText>
          </View>
          <View style={{ width: scale(30), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.hr}</RkText>
          </View>
          <View style={{ width: scale(35), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.obp}</RkText>
          </View>
          <View style={{ width: scale(30), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.po}</RkText>
          </View>
          <View style={{ width: scale(30), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.r}</RkText>
          </View>
          <View style={{ width: scale(30), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.rbi}</RkText>
          </View>
          <View style={{ width: scale(35), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.slg}</RkText>
          </View>
          <View style={{ width: scale(30), justifyContent: 'center' }}>
            <RkText style={{ color: '#000', fontSize: scaleVertical(15), textAlign: 'center' }}>{player.so}</RkText>
          </View>

        </View>
      );
      return data;
    });


    const content = (
      <View style={{ height: size.AppbarHeight / 1.3, flex: 1, backgroundColor: '#e6e7e9', justifyContent: 'center' }} >
        {row}
      </View>
    );
    return content;
  }

  _getBattingHeaderTable(type) {
    const team = type === 1 ? this.state.boxscore.homeName : this.state.boxscore.awayName;
    const text =
      (<View style={{ flex: 1, justifyContent: 'center' }}>
        <RkText style={{ color: '#002157', fontSize: scaleVertical(20), textAlign: 'left' }}>Estadísticas bateo {team}</RkText>
      </View>);
    const row = (
      <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
        <View style={{ width: scale(80), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>Nombre</RkText>
        </View>
        <View style={{ width: scale(30), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>A</RkText>
        </View>
        <View style={{ width: scale(30), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>AB</RkText>
        </View>
        <View style={{ width: scale(35), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>AVG</RkText>
        </View>
        <View style={{ width: scale(30), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>BB</RkText>
        </View>
        <View style={{ width: scale(30), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>H</RkText>
        </View>
        <View style={{ width: scale(30), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>H2B</RkText>
        </View>
        <View style={{ width: scale(30), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>H3B</RkText>
        </View>
        <View style={{ width: scale(30), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>HR</RkText>
        </View>
        <View style={{ width: scale(35), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>OBP</RkText>
        </View>
        <View style={{ width: scale(30), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>PO</RkText>
        </View>
        <View style={{ width: scale(30), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>R</RkText>
        </View>
        <View style={{ width: scale(30), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>RBI</RkText>
        </View>
        <View style={{ width: scale(35), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>SLG</RkText>
        </View>
        <View style={{ width: scale(30), justifyContent: 'center' }}>
          <RkText style={{ color: '#fff', fontSize: scaleVertical(15), textAlign: 'center' }}>SO</RkText>
        </View>
      </View>
    );

    const header = (

      <View style={{ flex: 1 }}>
        {text}
        <View style={{ height: size.AppbarHeight / 2, flex: 1, backgroundColor: '#000', justifyContent: 'center', padding: scaleVertical(5) }}>
          {row}
        </View>
      </View>
    );
    return header;
  }


  _getBattingTableAway() {
    const header = this._getBattingHeaderTable(2);
    const content = this._getBattingContentTable(2);
    const solve = (
      <View style={{ flex: 1 }}>
        <ScrollView
          horizontal={true}
          style={{ flex: 1 }}
        >
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
              {header}
            </View>
            <View style={{ flex: 1 }}>
              {content}
            </View>
          </View>
        </ScrollView>
      </View>
    );
    return solve;
  }

  _getBattingTableHome() {
    const header = this._getBattingHeaderTable(1);
    const content = this._getBattingContentTable(1);
    const solve = (
      <View style={{ flex: 1 }}>
        <ScrollView
          horizontal={true}
          style={{ flex: 1 }}
        >
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
              {header}
            </View>
            <View style={{ flex: 1 }}>
              {content}
            </View>
          </View>
        </ScrollView>
      </View>
    );
    return solve;
  }


  render() {
    if (!this.state.marking || !this.state.gameDetails || !this.state.boxscore) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    // console.log(this.state.gameDetails);
    return (
      <View style={styles.root}>
        <ScrollView style={{ flex: 1 }}>
          <RkText style={styles.textResultados} >Resultados</RkText>
          <RkText style={styles.textDate} >{this.state.marking.day} de {months[this.state.marking.month - 1]} de {this.state.marking.year}</RkText>
          <RkText style={styles.textDate} >{this.state.gameDetails.away_time} {this.state.gameDetails.time_ampm}</RkText>
          <Scoreboard game={this.state.marking} dateArray={this.state.dateArray} />


          <View style={{ flex: 1, paddingHorizontal: scaleVertical(15), paddingVertical: scaleVertical(10) }}>
            {this._getPitcherTableHome()}
          </View>
          <View style={{ flex: 1, paddingHorizontal: scaleVertical(15), paddingVertical: scaleVertical(10) }}>
            {this._getBattingTableHome()}
          </View>


          <View style={{ flex: 1, paddingHorizontal: scaleVertical(15), paddingVertical: scaleVertical(10) }}>
            {this._getPitcherTableAway()}
          </View>
          <View style={{ flex: 1, paddingHorizontal: scaleVertical(15), paddingVertical: scaleVertical(10) }}>
            {this._getBattingTableAway()}
          </View>

          <View style={{ flex: 1, backgroundColor: '#fafafa', paddingHorizontal: scaleVertical(20), paddingVertical: scaleVertical(10) }}>
            <HTML html={this.state.boxscore.gameInfo.replace(/; /g, '</br>')} />
          </View>

        </ScrollView>
      </View>
    );
  }
}


let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
  title: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(20),
    padding: scaleVertical(15),
  },
  content: {
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(15),
    padding: scaleVertical(15)
  },
  boton: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45),
  },
  textResultados: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(25),
    paddingTop: scaleVertical(15),
    paddingHorizontal: scaleVertical(15),
  },
  textDate: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(25),
    paddingTop: scaleVertical(5),
    paddingBotton: scaleVertical(10),
    paddingHorizontal: scaleVertical(15),
  }
}));




GameDetails.propTypes = {

};


const mapStateToProps = () => ({});


const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GameDetails);
