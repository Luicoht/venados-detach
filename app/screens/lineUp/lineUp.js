import React from 'react';
import { View, ScrollView, Image, UIManager, LayoutAnimation } from 'react-native';
import { RkStyleSheet, RkText, RkButton, TouchableOpacity } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { AppLoading } from 'expo';
import { DoubleBounce } from 'react-native-loader';
import PropTypes from 'prop-types';
import bigDont from '../../assets/icons/dont-4.png';
import { scaleVertical, size } from '../../utils/scale';
import { getPlayers, getWalletPoints, getProfile } from '../../Store/Services/Socket';
import ModalSimpleBetDeerChallenge from './modalSimpleBetDeerChallenge';

class LineUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalSimpleHitVisible: false,
      isModalSimpleOutVisible: false,
      players: false,
      wallet: false,
      profile: false,
      game: false
    };
    const options = {
      title: 'LINE UP',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }
  componentDidMount() {
    getWalletPoints()
      .then((wallet) => {
        this.setState({ wallet });
      })
      .catch(() => {
        this.setState({
          wallet: {}
        });
      });
    getPlayers()
      .then((players) => {
        this.setState({ players });
      })
      .catch(() => {
        this.setState({
          players: []
        });
      });


    getProfile()
      .then((profile) => {
        this.setState({ profile });
        console.log('tu perfil es ', profile);
      })
      .catch(() => {
        console.log('Error en get profile lineup');
      });
  }


  componentWillUpdate(nextProps, nextState) {
    let animate = false;
    if (this.state.players !== nextState.players || this.state.wallet !== nextState.wallet || this.props.currentActiveGame !== nextState.game) {
      animate = true;
    }
    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      LayoutAnimation.easeInEaseOut();
    }
  }
  _getTableHeader() {
    const header = (
      <View style={styles.containerHeaderTable}>
        <View style={{ flex: 0.5, justifyContent: 'center' }}>
          <RkText style={styles.tableHeaderText} />
        </View>
        <View style={{ flex: 3, justifyContent: 'center' }}>
          <RkText style={styles.tableHeaderText}>Jugador</RkText>
        </View>
        <View style={{ flex: 5, backgroundColor: '#eb0029', justifyContent: 'center' }}>
          <RkText style={styles.tableHeaderText}>Reto Venados</RkText>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkText style={styles.tableHeaderText} />
        </View>
      </View>
    );
    return header;
  }

  _getTableContent() {
    const content = this.state.players.map((route, count) => {
      return (
        <View key={route.id} style={styles.rowContainer}>
          <View style={{ flex: 0.5, justifyContent: 'center' }}>
            <RkText style={styles.headerTextBlack}>{count + 1}</RkText>
          </View>
          <View style={{ flex: 3, justifyContent: 'center' }}>
            <RkText style={styles.headerTextRed}>{route.name}</RkText>
          </View>
          <View style={{ flex: 5, backgroundColor: '#e5e5e5', justifyContent: 'center' }}>
            <ModalSimpleBetDeerChallenge player={route} game={this.props.currentActiveGame || { active: 0, finished: 1, inning: 0 }} wallet={this.state.wallet} />
          </View>
          <View style={{ flex: 1, justifyContent: 'center', alignItems:'center' }}>

            <RkButton style={{
              backgroundColor: 'transparent',
              width: scaleVertical(50),
              alignSelf: 'center',
              justifyContent: 'center',
            }} onPress={() => { this.props.navigation.navigate('PlayerMenu', { player: route }); }}
            >
              <RkText style={styles.headerTextBlack2}>VER</RkText>
            </RkButton>
          </View>
        </View>
      );
    });
    return content;
  }


  _getGame() {
    const game = (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <ScrollView
          style={styles.root}
        >
          <View style={styles.headerSection} >
            <RkText style={styles.header}>Line up</RkText>
          </View>

          <View style={styles.subHeaderSection}>
            <RkText style={styles.subHeader}>Selecciona a tu jugador para participar en el Reto Venados, elige Hit u Out.</RkText>
          </View>


          {this._getTableHeader()}
          {this._getTableContent()}


        </ScrollView>
      </View>
    );
    return game;
  }

  _notInGame() {
    const notGame = (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1, alignItems: 'center', paddingVertical: scaleVertical(40) }}>
          <View style={styles.modalDontImageContent}>
            <Image style={{
              padding: scaleVertical(15),
              alignItems: 'center',
              justifyContent: 'center',
              height: size.width / 3,
              width: size.width / 3
            }} source={bigDont} />
          </View>
          <View
            style={{
              paddingHorizontal: scaleVertical(15)
            }}
          >
            <RkText style={{
              fontSize: scaleVertical(25),
              fontFamily: 'TTPollsBoldItalic',
              textAlign: 'center'
            }}>No hay juegos disponibles</RkText>
            <RkText style={{
              fontSize: scaleVertical(20),
              fontFamily: 'Roboto-Regular',
              textAlign: 'center'
            }}>Muy pronto publicaremos el próximo juego.</RkText>
          </View>
          <View style={{
            paddingHorizontal: scaleVertical(15),
            paddingVertical: scaleVertical(15)
          }}>
            <RkButton style={{
              backgroundColor: '#eb0029',
              alignSelf: 'center',
              justifyContent: 'center',
              height: size.width / 7,
              width: size.width - scaleVertical(80),
            }} onPress={() => { this.props.navigation.navigate('DashboardMenu'); }}>
              <RkText style={{
                fontSize: scaleVertical(18),
                fontFamily: 'DecimaMono',
                textAlign: 'center',
                color: '#fff',
                textAlignVertical: 'center'
              }}>ACEPTAR</RkText>
            </RkButton>
          </View>
        </View>
      </View>
    );
    return notGame;
  }

  _notPartner() {
    return (
      <View style={{ alignItems: 'center', paddingTop: scaleVertical(35) }}>
        <View style={{
          padding: scaleVertical(15),
          alignItems: 'center',
          justifyContent: 'center'
        }}>
          <Image style={{
            height: size.width / 4,
            width: size.width / 4,
            alignSelf: 'center'
          }} source={bigDont} />
        </View>
        <View
          style={{
            paddingHorizontal: scaleVertical(15)
          }}
        >
          <RkText style={{
            fontSize: scaleVertical(25),
            fontFamily: 'TTPollsBoldItalic',
            textAlign: 'center'
          }}>Aún no eres socio Venados</RkText>
          <RkText style={{
            fontSize: scaleVertical(20),
            fontFamily: 'Roboto-Regular',
            textAlign: 'center'
          }}>Para poder jugar necesitas hacerte socio.</RkText>
        </View>
        <View style={{
          paddingHorizontal: scaleVertical(15),
          paddingVertical: scaleVertical(15),
        }}>
          <RkButton style={{
            backgroundColor: '#eb0029',
            alignSelf: 'center',
            justifyContent: 'center',
            height: size.width / 7,
            width: size.width - scaleVertical(80),
          }} onPress={() => { this.props.navigation.navigate('Partner'); }}>
            <RkText style={{
              fontSize: scaleVertical(18),
              fontFamily: 'DecimaMono',
              textAlign: 'center',
              color: '#fff',
              textAlignVertical: 'center'
            }}>QUIERO SER SOCIO</RkText>
          </RkButton>
        </View>
      </View>);
  }
  render() {
    if (!this.state.players || !this.state.wallet || !this.props.currentActiveGame || !this.state.profile || !this.state.profile) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    // if (this.state.profile.userType !== 6) return this._notPartner();
    const solve = this.props.currentActiveGame && this.props.currentActiveGame.finished !== 1 ? this._getGame() : this._notInGame();
    return solve;
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff'
  },
  header: {
    fontSize: scaleVertical(32),
    color: '#eb0029',
    fontFamily: 'TTPollsBold'
  },
  headerSection: {
    paddingVertical: scaleVertical(15),
    paddingHorizontal: scaleVertical(10)
  },
  subHeaderSection: {
    paddingVertical: scaleVertical(15),
    paddingHorizontal: scaleVertical(10)
  },
  subHeader: {
    fontSize: scaleVertical(18),
    fontFamily: 'TTPollsBold'
  },


  rowContainer: {
    borderColor: '#9a9a9a',
    borderBottomWidth: scaleVertical(2),
    paddingHorizontal: scaleVertical(10),
    height: scaleVertical(40),
    flex: 1,
    flexDirection: 'row',
  },


  containerHeaderTable: {
    flex: 1,
    flexDirection: 'row',
    height: scaleVertical(40),
    paddingHorizontal: scaleVertical(10),
    backgroundColor: '#000'
  },


  tableHeaderText: {
    color: '#fff',
    fontFamily: 'Roboto-Bold',
    textAlign: 'center',
    fontSize: scaleVertical(14),
  },

  tableBorder: {

    height: scaleVertical(40),
    flex: 1,
    flexDirection: 'row',
  },
  headerTextRed: {
    color: '#eb0029',
    fontFamily: 'Roboto-Bold',
    textAlign: 'center',
    fontSize: scaleVertical(14),
  },
  headerTextBlack: {
    color: '#000',
    fontFamily: 'Roboto-Bold',
    textAlign: 'center',
    fontSize: scaleVertical(14),
  },
  headerTextBlack2: {
    color: '#000',
    fontFamily: 'Roboto-Bold',
    textAlign: 'center',
    fontSize: scaleVertical(9),
  },


}));


LineUp.propTypes = {
  navigation: PropTypes.object,
  currentActiveGame: PropTypes.object,
};


const mapStateToProps = state => ({
  prop: state.prop,
  usuario: state.reducerSesion,
  currentActiveGame: state.reducerGame
});


export default connect(mapStateToProps)(LineUp);
