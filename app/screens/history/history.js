import React from 'react';
import { DoubleBounce } from 'react-native-loader';
import { View, ScrollView } from 'react-native';
import { RkStyleSheet, RkText } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { scaleVertical, size, scale } from '../../utils/scale';
import { historyRecords } from '../../Store/Services/Socket';


class MyHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      records: false
    };
    const options = {
      title: 'HISTORIAL',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() {
    historyRecords()
      .then((records) => {
        this.setState({ records });
        if (!records) this.setState({ records: [] });
      })
      .catch(() => {
        this.setState({ records: [] });
      });
  }

  _getTable() {
    const tableHeader = (
      <View style={[{ flex: 1, flexDirection: 'row' }, styles.head]}>
        <View style={{ flex: 1 }}>
          <RkText style={styles.textHeaderTable}>Fecha</RkText>
        </View>
        <View style={{ flex: 3 }}>
          <RkText style={styles.textHeaderTable}>Concepto</RkText>
        </View>
        <View style={{ flex: 1 }}>
          <RkText style={styles.textHeaderTable}>Puntos</RkText>
        </View>
      </View>
    );

    const solve = (
      <View style={{ flex: 1 }}>
        {tableHeader}
        {this._getContent()}
      </View>
    );
    return solve;
  }

  _getContent() {
    const content = this.state.records.map((record, count) => {
      const bg = count % 2 === 0 ? '#fff' : '#c8eeff';
      const colorText = record.points < 0 ? '#eb0029' : '#000';
      const row = (
        <View style={{ flex: 1, flexDirection: 'row', backgroundColor: bg }}>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <RkText style={{ fontSize: scale(10), color: 'black', textAlign: 'center', fontFamily: 'Roboto-Bold' }}>{record.createdAt.substring(0, 10)}</RkText>
          </View>
          <View style={{ flex: 3, justifyContent: 'center' }}>
            <RkText style={{ fontSize: scale(14), color: 'black', textAlign: 'center', fontFamily: 'Roboto-Bold' }}>{record.name}</RkText>
          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <RkText style={{ fontSize: scale(16), color: colorText, textAlign: 'center', fontFamily: 'Roboto-Bold' }}>{record.points}</RkText>
          </View>
        </View>
      );
      return row;
    });
    return content;
  }


  render() {
    if (!this.state.records) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color="#eb0029" />
        </View>
      );
    }

    return (
      <View style={styles.root} >
        <ScrollView style={styles.root} >
          <View style={styles.header} >
            <RkText style={{
              fontFamily: 'TTPollsBold',
              fontSize: scaleVertical(25),
              paddingHorizontal: scaleVertical(15),
              paddingVertical: scaleVertical(15)
            }}> Historial de cuenta </RkText>
          </View>

          <View style={{ flex: 7 }} >
            {this._getTable()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    backgroundColor: '#fff',
    flex: 1
  },
  header: {
    flex: 1,
    justifyContent: 'center'
  },
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: { height: 40, backgroundColor: '#000', color: 'white' },
  textHeaderTable: { fontSize: scale(16), margin: 6, color: 'white', textAlign: 'center', fontFamily: 'Roboto-Bold' },
  text: { margin: 6, textAlign: 'center', fontFamily: 'Roboto-Regular' },
  row: { flexDirection: 'row', backgroundColor: 'white' },
  btnText: { textAlign: 'center', color: '#fff' }
}));
const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyHistory);
