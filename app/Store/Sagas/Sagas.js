import { takeEvery, call, put } from 'redux-saga/effects';
import { AsyncStorage, Alert } from 'react-native';
import * as Firebase from 'firebase';
import { Google, Facebook } from 'expo';
import { registerLikeDeerMember, authInDevice } from '../Services/Socket';
import * as walletController from '../Services/Wallet';
import { authentication } from '../Services/Firebase';
import * as authController from './../Services/Auth';
import { FACEBOOKSDK, FACEBOOK_UI } from '../../constants';
import CONSTANTS from '../CONSTANTS';
/*
const registroEnFirebase = (values) =>
  authentication.createUserWithEmailAndPassword(values.email, values.password)
    .then(success => success);
*/

const { PERMISSIONS } = FACEBOOKSDK;
const { APP_ID } = FACEBOOK_UI;


function sagaTest() {
  try {
    console.log('on sagaTest ');
  } catch (error) {
    console.log(error);
  }
}


function sagaPartner(datos) {
  try {
    console.log('sagaPartner', datos);
    registerLikeDeerMember(datos.datos);
  } catch (error) {
    console.log(error);
  }
}

function sagaRecover(datos) {
  try {
    authentication.sendPasswordResetEmail(datos.datos.correo).then((solve) => {
      console.log('datos.datos.correo ', solve);
    }).catch((error) => {
      console.log('ocurrio un error', error);
      // An error happened.
    });
  } catch (error) {
    console.log(error);
  }
}


function* sagaRegistro(values) {
  try {
    console.log('on sagaRegistro ');
    // authController.register(values.datos);
    const registro = yield call(authController.register, values.datos);
    console.log('TERMINO ', registro);

    // const registro = yield call(registroEnFirebase, values.datos);
    // walletController.loginEmail(values.datos, registro);
  } catch (error) {
    displayAlert(error.code);
    console.log(error);
  }
}

const loginEnFirebase = ({ correo, password }) =>
  authentication.signInWithEmailAndPassword(correo, password)
    .then(success => {
      console.log('Login firebase el usuario es', success.user.providerData);
      console.log('Login firebase el token del usuario es', success.user.stsTokenManager);
      // authInDevice();
      return success;
    });


function displayAlert(errorCode) {
  console.log('errorCode', errorCode);
  switch (errorCode) {
    case 'auth/user-not-found':
      Alert.alert(
        'No se encontro usuario'
      );
      break;
    case 'auth/wrong-password':
      Alert.alert(
        'Contraseña incorrecta'
      );
      break;
    case 'auth/email-already-in-use':
      Alert.alert(
        'El email ya se encuentra en uso'
      );
      break;
    case 'auth/network-request-failed':
      Alert.alert(
        'Demasiado tiempo de espera, conexión interrumpida o host inalcanzable'
      );
      break;
    case 'auth/user-token-expired':
      Alert.alert(
        'Inicie sesión de nuevo'
      );
      break;
    default:
      Alert.alert(
        'Ups!..'
      );
      break;
  }
}

function* sagaLogin(values) {
  console.log('SIGNIN ', values);
  try {
    const resultado = yield call(loginEnFirebase, values.datos);
    return resultado;
  } catch (error) {
    displayAlert(error.code);
    return error;
  }
}


function* sagaLoginPartner(values) {
  console.log('sagaLoginPartner ', values);
  try {
    const resultado = yield call(loginEnFirebase, values.datos);
    return resultado;
  } catch (error) {
    console.log(error);
    return error;
  }
}


const resultGoogle = () => (
  Google.logInAsync({
    androidClientId: '383435945674-mulbiqr2qt2442buf7dpoevkqoql9evn.apps.googleusercontent.com',
    iosClientId: '383435945674-tancgilrgl5u6ieojcsgecf4onp2p3r5.apps.googleusercontent.com',
    // iosClientId: 'com.googleusercontent.apps.383435945674-tancgilrgl5u6ieojcsgecf4onp2p3r5',
    scopes: ['profile', 'email'],
    // androidStandaloneAppClientId: '383435945674-877to7mhvig9mc340ihejagmfidoc9hf.apps.googleusercontent.com'
  })
);


const resultFacebook = () => (
  Facebook.logInWithReadPermissionsAsync(APP_ID, {
    permissions: PERMISSIONS,
  })
);

function* sagaLinkFacebook() {
  try {
    console.log('sagaLinkFacebook! try');
    const { type, token } = yield call(resultFacebook);
    console.log('result');

    if (type === 'success') {
      const provider = Firebase.auth.FacebookAuthProvider;
      const credential = provider.credential(token);
      Firebase.auth().currentUser.linkAndRetrieveDataWithCredential(credential);
      // authInDevice();
    }
    return { cancelled: true };
  } catch (e) {
    console.log('error sagaLinkFacebook ', e);
  }
}


// function* sagaLinkEmailToGoogle() {
//   try {
//     console.log('try sagaLinkEmailToGoogle');
//     const result = yield call(resultGoogle);
//     if (result.type === 'success') {
//       const provider = Firebase.auth.GoogleAuthProvider;
//       const credential = provider.credential(null, result.accessToken);
//       const credentialEmail = Firebase.auth.EmailAuthProvider.credential('hurbogt522o@gmail.com', '123456789');
//       Firebase.auth().signInAndRetrieveDataWithCredential(credential).then((user) => {
//         Firebase.auth().currentUser.linkAndRetrieveDataWithCredential(credentialEmail).then(function (usercred) {
//           var user = usercred.user;
//           console.log("Account linking success", user);
          // authInDevice();
//         }, function (error) {
//           console.log("Account linking error", error);
//         })
//       });
//       // Firebase.auth().signInAndRetrieveDataWithCredential(credential).then(() => {
//       //   console.log('ahora estoy logeado con google');
//       //   Firebase.auth().currentUser.linkAndRetrieveDataWithCredential(credentialEmail).then((usercred) => {
//       //     console.log('estoy intentando linkear linkAndRetrieveDataWithCredential ');
//       //     const user = usercred.user;
//       //     console.log('Account linking success', user);
      //     authInDevice();
//       //   }, (error) => {
//       //     console.log('ERROR linkear linkAndRetrieveDataWithCredential ');

//       //     console.log('Account linking error', error);
//       //   });
//       // });
//     }
//     return { cancelled: true };
//   } catch (e) {
//     console.log('error google ', e);
//   }
// }


// function* sagaLinkEmailToFacebook() {
//   try {
//     console.log('try sagaLinkEmailToFacebook');
//     const { type, token } = yield call(resultFacebook);
//     if (type === 'success') {
//       const credential = Firebase.auth.FacebookAuthProvider.credential(token);
//       // Firebase.auth().signInAndRetrieveDataWithCredential(credential).then(user => {
//       //   authInDevice();
//       //   return user;
//       // }).catch((error) => {
//       //   console.error('Error in login FacebookAuthProvider: ', error);
//       // });

//       const credentialEmail = Firebase.auth.EmailAuthProvider.credential('hurbogt522o@gmail.com', '123456789');
//       Firebase.auth().signInAndRetrieveDataWithCredential(credential).then(() => {
//         Firebase.auth().currentUser.linkAndRetrieveDataWithCredential(credentialEmail).then((usercred) => {
//           const user = usercred.user;
//           console.log('Account linking success', user);
          // authInDevice();
//         }, (error) => {
//           console.log('Account linking error', error);
//         });
//       });
//     }
//   } catch (err) { console.log('error fb:', err); }
// }

// export function* tryLinkTogoogle() {
//   console.log('tryLinkTogoogle');
//   const result = yield call(resultGoogle);
//   return new Promise(((resolve, reject) => {
//     console.log('sagaLinkGoogle! try');
//     console.log('result');
//     const prevUser = Firebase.auth().currentUser;
//     if (result.type === 'success') {
//       const provider = Firebase.auth.GoogleAuthProvider;
//       const credential = provider.credential(null, result.accessToken);
//       prevUser.linkAndRetrieveDataWithCredential(credential).then(user => {
        // authInDevice();
//         resolve(user);
//       })
//         .catch(error => {
//           reject(error);
//         });
//     }
//     console.log('cancelled', result.accessToken);
//   }));
// }


// function* sagaLinkGoogle() {
//   try {
//     console.log('sagaLinkGoogle! try');
//     const result = yield call(resultGoogle);
//     console.log('result');
//     const prevUser = Firebase.auth().currentUser;
//     if (result.type === 'success') {
//       const provider = Firebase.auth.GoogleAuthProvider;
//       const credential = provider.credential(null, result.accessToken);
//       prevUser.linkAndRetrieveDataWithCredential(credential);
      // authInDevice();
//     }
//     console.log('cancelled', result.accessToken);
//     return { cancelled: true };
//   } catch (e) {
//     console.log('error sagaLinkGoogle ', e);
//   }
// }

function* sagaLoginFacebook() {
  try {
    const { type, token } = yield call(resultFacebook);
    if (type === 'success') {
      yield AsyncStorage.setItem(CONSTANTS.SAVE_FBTK, token);
      yield put({ type: CONSTANTS.SAVE_FBTK, payload: token });
      const credential = Firebase.auth.FacebookAuthProvider.credential(token);
      Firebase.auth().signInAndRetrieveDataWithCredential(credential).then(user => {
        // authInDevice();
        // walletController.findProfileByUser(user);
        return user;
      }).catch((error) => {
        console.error('Error in login FacebookAuthProvider: ', error);
      });
    }
  } catch (err) { console.log('error fb:', err); }
}


function* sagaLoginGoogle() {
  try {
    console.log('sagaLoginGoogle! try');
    const result = yield call(resultGoogle);
    console.log('result');

    if (result.type === 'success') {
      const provider = Firebase.auth.GoogleAuthProvider;
      const credential = provider.credential(null, result.accessToken);
      Firebase.auth().signInAndRetrieveDataWithCredential(credential).then((user) => {
        // authInDevice();
        // walletController.findProfileByUser(user);
        return user;
      }).catch((error) => {
        console.error('Error in login: ', error);
      });
    } else {
      console.log('cancelled', result.accessToken);
      return { cancelled: true };
    }
  } catch (e) {
    console.log('error google ', e);
  }
}

export default function* funcionPrimaria() {
  yield takeEvery(CONSTANTS.TEST, sagaTest);
  yield takeEvery(CONSTANTS.RECOVER, sagaRecover);
  yield takeEvery(CONSTANTS.PARTNER, sagaPartner);
  yield takeEvery(CONSTANTS.REGISTRO, sagaRegistro);
  yield takeEvery(CONSTANTS.SIGNIN, sagaLogin);
  yield takeEvery(CONSTANTS.LOGINPARTNER, sagaLoginPartner);
  yield takeEvery(CONSTANTS.LOGINGOOGLE, sagaLoginGoogle);
  // yield takeEvery(CONSTANTS.LINKFACEBOOK, sagaLinkFacebook);
  // yield takeEvery(CONSTANTS.LINKGOOGLE, tryLinkTogoogle);
  // yield takeEvery(CONSTANTS.LINKGOOGLE, sagaLinkGoogle);
  // yield takeEvery(CONSTANTS.LINKEMAILTOGOOGLE, sagaLinkEmailToGoogle);
  // yield takeEvery(CONSTANTS.LINKEMAILTOFACEBOOK, sagaLinkEmailToFacebook);
  yield takeEvery(CONSTANTS.LOGINFACEBOOK, sagaLoginFacebook);
  console.log('Desde nuestra función generadora');
}
