export const storeData = [
    {
        category: {
            name: 'Hombre',
            id: 1
        },
        articles: [
            {
                name: 'Juego Pants Y Sudadera John Leopard Gorra De Regalo',
                description: 'John Leopard 80% algodón/20% poliester muy suave detalle bordado ajustados/ pantalon entubado',
                price: 799.00,
                points: 1400,
                photoURL: 'https://johnleopard.com.mx/img/p/3/2/32-large_default.jpg',
                id: 1
            },
            {
                name: 'Track suit / sudadera y pants vino',
                description: 'John Leopard 80% algodón/20% poliester muy suave detalle bordado ajustados/ pantalon entubado',
                price: 799.00,
                points: 1600,
                photoURL: 'https://johnleopard.com.mx/img/p/5/6/56-large_default.jpg',
                id: 2
            },
            {
                name: 'Playera verde agua cuello V profundo',
                description: '90% algodón/10% elastano muy suave cuello V profundo muscle fit (ajusre en brazos)',
                price: 249.00,
                points: 800,
                photoURL: 'https://johnleopard.com.mx/img/p/8/7/87-large_default.jpg',
                id: 3
            },
            {
                name: 'Camisa negra cuello mao manga larga',
                description: '90% algodón/10% elastano muy suave strech (se ajusta al cuerpo) slim fit (ajusre en brazos)',
                price: 549.00,
                points: 1100,
                photoURL: 'https://johnleopard.com.mx/img/p/1/5/3/153-large_default.jpg',
                id: 4
            },
            {
                name: 'Hoodie ancla all black',
                description: '80% algodón/20% poliester muy suave detalle bordado en relieve bolsa de canguro oculta relased fit (corte holgado)',
                price: 399.00,
                points: 900,
                photoURL: 'https://johnleopard.com.mx/img/p/1/5/3/153-large_default.jpg',
                id: 5
            },
        ]
    }
    , {
        category: {
            name: 'Mujer',
            id: 2
        },
        articles: [
            {
                name: 'Juego pants y sudadera John Leopard gorra de regalo',
                description: 'Chaqueta outdoor con parte delantera y trasera en tejido ligeramente acolchado. Modelo con capucha forrada, cremallera delante, bolsillos al bies con cremallera y mangas raglán largas con abertura para el pulgar. Tejido termoaislante ligeramente cepillado en las mangas y los laterales. Parcialmente forrada.',
                price: 599.00,
                points: 800,
                photoURL: 'https://lp.hm.com/hmprod?set=source%5B%2Fenvironment%2F2018%2FH00_0000_b11c7e88d8b42a15b51df33b345520ca5241d7f0.jpg%5D%2Cmedia_type%5BLOOKBOOK%5D%2Ctshirt_size%5BXL%5D%2Cquality%5BH%5D%2Csr_x%5B-327%5D%2Csr_y%5B0%5D%2Csr_height%5B3496%5D%2Csr_width%5B2990%5D%2Chmver%5B1%5D&call=url%5Bfile%3A%2Fstudio2%2Fv1%2Fproduct.chain%5D',
                id: 6
            },
            {
                name: 'Medias sport',
                description: 'Mallas de deporte en tejido funcional de secado rápido con secciones de ventilación en malla, cintura elástica con hilos brillantes en la trama y bolsillo para las llaves de malla oculto en la pretina.',
                price: 499.00,
                points: 800,
                photoURL: 'https://lp.hm.com/hmprod?set=source%5B%2Fenvironment%2F2018%2FH00_0000_b712947a4b1e5dc78d1beb98be39b4bdb603b9d4.jpg%5D%2Cmedia_type%5BLOOKBOOK%5D%2Ctshirt_size%5BL%5D%2Cquality%5BH%5D%2Csr_x%5B-327%5D%2Csr_y%5B0%5D%2Csr_height%5B3496%5D%2Csr_width%5B2990%5D%2Chmver%5B1%5D&call=url%5Bfile%3A%2Fstudio2%2Fv1%2Fproduct.chain%5D',
                id: 7
            },
            {
                name: 'Camiseta de correr con capucha',
                description: 'Camiseta ajustada de correr en tejido funcional de secado rápido con capucha con borde elástico y cremallera arriba. Modelo con mangas raglán largas, bolsillo pequeño con cremallera detrás, bajo ligeramente redondeado y espalda algo más larga. Detalles reflectantes. Interior cepillado suave.',
                price: 499,
                points: 800,
                photoURL: 'https://lp.hm.com/hmprod?set=source%5B%2Fenvironment%2F2018%2FH00_0000_7e9635b5028eb054f9f198b6afa37efd00c88e75.jpg%5D%2Cmedia_type%5BLOOKBOOK%5D%2Ctshirt_size%5BXL%5D%2Cquality%5BH%5D%2Csr_x%5B-580%5D%2Csr_y%5B0%5D%2Csr_height%5B15699%5D%2Csr_width%5B13426%5D%2Chmver%5B1%5D&call=url%5Bfile%3A%2Fstudio2%2Fv1%2Fproduct.chain%5D',
                id: 8
            },
            {
                name: 'Sujetador sport Low support',
                description: 'Sujetador de deporte en tejido funcional de secado rápido. Modelo con copas ligeramente acolchadas con relleno extraíble, tirantes dobles ajustables y espalda olímpica. Sin costuras. Completamente forrado. Sujeción ligera.',
                price: 299,
                points: 400,
                photoURL: 'https://lp.hm.com/hmprod?set=source%5B%2Fenvironment%2F2018%2FH00_0000_08edc68ea2b1e89387444521f9b412d3ffd56c3f.jpg%5D%2Cmedia_type%5BLOOKBOOK%5D%2Ctshirt_size%5BL%5D%2Cquality%5BH%5D%2Csr_x%5B-327%5D%2Csr_y%5B0%5D%2Csr_height%5B3496%5D%2Csr_width%5B2990%5D%2Chmver%5B1%5D&call=url%5Bfile%3A%2Fstudio2%2Fv1%2Fproduct.chain%5D',
                id: 9
            },
        ]
    }

    , {
        category: {
            name: 'Infantil',
            id: 3
        },
        articles: [
            {
                name: 'Track top J zoo SST',
                description: 'Esta chamarra le imprime un toque divertido y femenino a una icónica prenda infaltable en todo clóset. Está cubierta con un llamativo estampado tropical que visto desde la distancia parece ser flores, pero de cerca se convierte en un divertido diseño de animales. Está confeccionada en tejido tricot que es suave al tacto y ofrece un look deportivo. Las 3 Franjas en las mangas le imprimen el auténtico sello adidas.',
                price: 999.00,
                points: 1200,
                photoURL: 'https://www.adidas.mx/dis/dw/image/v2/aaqx_prd/on/demandware.static/-/Sites-adidas-products/default/dwdb39e41f/zoom/D98902_01_laydown.jpg?sh=840&strip=false&sw=840',
                id: 10
            },
            {
                name: 'Jersey FC Bayern München UCL',
                description: 'Talento inigualable y versatilidad increíble son algunas de las cualidades con las que el Bayern lanza una descarga de ataques de primera categoría. Este jersey de fútbol para niños es una versión del que usan los jugadores del FC Bayern Múnchen cuando juegan dentro del área. Presenta un diseño fabricado en tejido entrelazado reciclado con tecnología transpirable climacool® y paneles de malla en las mangas para darte ventilación en los lugares donde más lo necesitas. Luce el escudo del club en el pecho para que todos sepan que no hay mejor lugar en el mundo que el estadio Allianz Arena.',
                price: 1299.00,
                points: 1400,
                photoURL: 'https://www.adidas.mx/dis/dw/image/v2/aaqx_prd/on/demandware.static/-/Sites-adidas-products/default/dw70daca08/zoom/AZ4667_21_model.jpg?sh=840&strip=false&sw=840',
                id: 11
            },
        ]
    }
];


