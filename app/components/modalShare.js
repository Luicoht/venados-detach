import React, { Component } from 'react';
import {
  Modal,
  View,
  WebView,
  TouchableOpacity,
  Text,
  Dimensions,
  StyleSheet,
} from 'react-native';
import { Icon } from 'native-base';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    padding: 10,
    flexDirection: 'row',
  },
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignSelf: 'flex-start',
    flexGrow: 0,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 3,
    marginRight: 10,
  },
  buttonText: {
    color: '#fff',
  },
});

const title = 'https://www.facebook.com/dialog/return/close?error_code=4201&error_message=User+canceled+the+Dialog+flow#_=_';

class ModalShare extends Component {
  onNavigationStateChange(webViewState) {
    if (webViewState.title === title) {
      this.props.onCloseModal();
    }
  }

  render() {
    console.log('DIM ->', Dimensions.get('window'));
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.props.modalVisible}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                <TouchableOpacity onPress={this.props.onCloseModal} style={{ paddingLeft: 20, paddingTop: 8 }}>
                  <Icon name='close' style={{ width: 30, height: 30 }} resizeMode="contain" />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                <Text
                  numberOfLines={1}
                  style={{ marginLeft: -125, marginTop: 12 }}
                >
                  {this.props.uri}
                </Text>
              </View>
            </View>

            <WebView
              source={{ uri: this.props.uri }}
              javaScriptEnabled
              domStorageEnabled
              startInLoadingState
              ignoreSslError
              onNavigationStateChange={this.onNavigationStateChange.bind(this)}
              renderError={() => console.log('Error render', { message: 'Render failed' })}
              style={{
                flex: 1,
                marginTop: -(Dimensions.get('window').height / 2.5),
                height: Dimensions.get('window').height,
                width: Dimensions.get('window').width,
              }}
            />
          </View>
        </View>
      </Modal>
    );
  }
}

ModalShare.propTypes = {
  uri: PropTypes.string,
  onCloseModal: PropTypes.func,
  modalVisible: PropTypes.bool,
};

export default ModalShare;
