import React, { Component } from 'react';
import {
  TouchableOpacity,
  Text,
  View
} from 'react-native';
import PropTypes from 'prop-types';

import styleConstructor from './style';
import { shouldUpdate } from '../../../component-updater';
import { size, scaleVertical } from './../../../../../utils/scale';
import { AsyncImage } from './../../../../AsyncImage';

const imageDone = require('./../../../../../assets/icons/done-1-87.png');

import { NavigationActions } from 'react-navigation';




class Day extends Component {
  static propTypes = {
    // TODO: disabled props should be removed
    state: PropTypes.oneOf(['selected', 'disabled', 'today', '']),

    // Specify theme properties to override specific styles for calendar parts. Default = {}
    theme: PropTypes.object,
    marking: PropTypes.any,
    onPress: PropTypes.func,
    date: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.style = styleConstructor(props.theme);
    this.onDayPress = this.onDayPress.bind(this);
    this.onDayLongPress = this.onDayLongPress.bind(this);
  }

  onDayPress() {
    this.props.onPress(this.props.date);
  }

  onDayPressGame(marking) {
    // cambiame cuando tenga datos reales porno
    const navigateAction = NavigationActions.navigate({
      routeName: 'GameDetailsMenu',
      params: { marking: marking.customData },
    });
    this.props.navigation.dispatch(navigateAction);
  }

  onDayLongPress() {
    this.props.onLongPress(this.props.date);
  }

  shouldComponentUpdate(nextProps) {
    return shouldUpdate(this.props, nextProps, ['state', 'children', 'marking', 'onPress', 'onLongPress']);
  }

  render() {
    let containerStyle = [this.style.base];
    let textStyle = [this.style.text];

    let marking = this.props.marking || {};
    if (marking && marking.constructor === Array && marking.length) {
      marking = {
        marking: true
      };
    }
    const isDisabled = typeof marking.disabled !== 'undefined' ? marking.disabled : this.props.state === 'disabled';

    if (marking.selected) {
      containerStyle.push(this.style.selected);
    } else if (isDisabled) {
      textStyle.push(this.style.disabledText);
    } else if (this.props.state === 'today') {
      textStyle.push(this.style.todayText);
    }

    if (marking.customStyles && typeof marking.customStyles === 'object') {
      const styles = marking.customStyles;
      if (styles.container) {
        if (styles.container.borderRadius === undefined) {
          styles.container.borderRadius = 16;
        }
        containerStyle.push(styles.container);
      }
      if (styles.text) {
        textStyle.push(styles.text);
      }
    }
    let contentStyle = {};

    if (marking && marking.customData) {
      contentStyle = marking.customData.isLocal ?
        ({
          backgroundColor: '#eb0029',
          width: size.width / 7,
          height: size.width / 7,
        }) :
        ({
          backgroundColor: '#c8eeff',
          width: size.width / 7,
          height: size.width / 7,
        });
      let textColor = marking.customData.isLocal ? '#fff' : '#000';
      let headerText = { fontSize: scaleVertical(14), fontFamily: 'Roboto-Black' };
      let infoText = { fontSize: scaleVertical(10), fontFamily: 'Roboto-Light' };

      let image = marking.customData.iWent ? (
        <AsyncImage
          style={{
            height: size.width / 23,
            width: size.width / 23,
            alignSelf: 'center',
            padding: scaleVertical(2)
            // position: 'absolute'
          }}
          source={imageDone}
          placeholderColor='transparent' />
      ) : <View />;
      return (

        < TouchableOpacity
          style={contentStyle}
          onPress={() => this.onDayPressGame(marking)
          }
          onLongPress={this.onDayLongPress}
          activeOpacity={marking.activeOpacity}
          disabled={marking.disableTouchEvent}
        >

          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1 }}>
              <Text allowFontScaling={false} style={[infoText, { color: textColor, paddingLeft: scaleVertical(2), }]}>{String(this.props.children)}</Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              {image}
            </View>

          </View>

          <Text allowFontScaling={false} style={[headerText, { color: textColor, paddingLeft: scaleVertical(2), }]}>{marking.customData.abbreviation}</Text>
          <Text allowFontScaling={false} style={[infoText, { color: textColor, paddingLeft: scaleVertical(2), }]}>{marking.customData.place}</Text>
        </TouchableOpacity >
      )
    }



    return (
      <TouchableOpacity
        style={{
          width: size.width / 7,
          height: size.width / 7,
          backgroundColor: '#fff',
        }}
        onPress={this.onDayPress}
        onLongPress={this.onDayLongPress}
        activeOpacity={marking.activeOpacity}
        disabled={marking.disableTouchEvent}
      >
        <Text allowFontScaling={false} style={{ fontSize: scaleVertical(10), fontFamily: 'Roboto-Light', paddingLeft: scaleVertical(2) }}>{String(this.props.children)}</Text>
      </TouchableOpacity >
    );
  }
}


// customData: {
//   isLocal: game.isLocal,
//   place: game.isLocal ? 'CASA' : 'VISITA',
//   iWent: i % 2 == 0 ? true : false,
//   name: game.name,
//   abbreviation: this.getAbbreviation(game.name)
// }
// AGREGA INFO
export default Day;
