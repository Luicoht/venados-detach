import CONSTANTS from './CONSTANTS';


// firebase
export const setProfileData = usuario => ({
  type: CONSTANTS.PROFILE_DATA,
  usuario,
});

export const saveFacebookToken = token => ({
  type: CONSTANTS.SAVE_FBTK,
  payload: token,
});

export const actionRegistration = values => ({
  type: CONSTANTS.REGISTRO,
  datos: values,
});

export const actionRefreshWallet = (wallet) => ({
  type: CONSTANTS.ACTIOMN_REFRESH_WALLET,
  payload: wallet,
});


export const actionEstablishProfile = (data) => ({
  type: CONSTANTS.ACTION_ESTABLISH_PROFILE,
  payload: data
});


// api venados
export const updateProfileData = (data) => ({
  type: CONSTANTS.SET_PROFILE_DATA,
  payload: data
});

export const linkToGoogle = () => ({
  type: CONSTANTS.LINKGOOGLE,
});

export const linkToFacebook = () => ({
  type: CONSTANTS.LINKFACEBOOK,
});

export const actionPartner = values => ({
  type: CONSTANTS.PARTNER,
  datos: values,
});

export const actionTest = datos => ({
  type: CONSTANTS.TEST,
  datos,
});
export const actionSignIn = datos => ({
  type: CONSTANTS.SIGNIN,
  datos,
});

export const actionLoginPartner = datos => ({
  type: CONSTANTS.LOGINPARTNER,
  datos,
});

export const actionRecover = datos => ({
  type: CONSTANTS.RECOVER,
  datos,
});

export const actionLoginGoogle = () => ({
  type: CONSTANTS.LOGINGOOGLE,
});
export const actionLinkEmailToGoogle = () => ({
  type: CONSTANTS.LINKEMAILTOGOOGLE,
});
export const actionLinkEmailToFacebook = () => ({
  type: CONSTANTS.LINKEMAILTOFACEBOOK,
});

export const actionLoginFacebook = () => ({
  type: CONSTANTS.LOGINFACEBOOK,
});

export const actionEstablishSession = usuario => ({
  type: CONSTANTS.ESTABLERCER_SESION,
  usuario,
});
export const actionEstablishCurrentGame = game => ({
  type: CONSTANTS.ESTABLERCER_JUEGO_ACTUAL,
  game,
});

export const actionCloseSession = () => ({
  type: CONSTANTS.CERRAR_SESION,
});


export const actionEstablishActiveGame = game => ({
  type: CONSTANTS.ESTABLISHACTIVEGAME,
  game,
});
