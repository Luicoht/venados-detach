import React from 'react';
import { View } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { RkButton, RkText, RkStyleSheet } from 'react-native-ui-kitten';
import { TextField } from 'react-native-material-textfield';
import { scale, scaleVertical, size } from '../../../utils/scale';


const validate = values => {
  const errors = {};
  if (!values.correo) {
    errors.correo = 'Requerido';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.correo)) {
    errors.correo = 'Correo invalido';
  }
  return errors;
};


const fieldNombre = props => (
  <View style={{ paddingLeft: scaleVertical(45), paddingRight: scaleVertical(45) }}>
    <TextField
      textColor='white'
      tintColor='white'
      baseColor='white'
      style={styles.titleTextStyle}
      placeholder={props.ph}
      onChangeText={props.input.onChange}
      value={props.input.value}
      keyboardType={props.input.name === 'correo' ? 'email-address' : 'default'}
      autoCapitalize='none'
      secureTextEntry={
        !!(
          props.input.name === 'password' || props.input.name === 'confirmacion'
        )
      }
      onBlur={props.input.onBlur}
      label={props.ph}
    />
    {props.meta.touched &&
      props.meta.error && (
        <RkText style={styles.errors}> {props.meta.error} </RkText>
      )}
  </View>
);


const RecoverForm = props => (
  <View>
    <Field name='correo' component={fieldNombre} ph='Correo o nombre de usuario' />
    <RkButton style={styles.boton} onPress={props.handleSubmit(props.recover)}> RECUPERAR </RkButton>
  </View>
);

let styles = RkStyleSheet.create(() => ({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  customImput: {
    textAlign: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.1)'
  },
  boton: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45)
  },
  errors: {
    color: '#ed0a27'
  },
  titleTextStyle: {
    color: 'black',
    textAlign: 'center',
    fontFamily: 'DecimaMono',
  }
}));

export default reduxForm({ form: 'RecoverForm', validate })(RecoverForm);
