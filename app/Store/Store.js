import { createStore, combineReducers, applyMiddleware } from 'redux';
import { reducer as form } from 'redux-form';
import createSagaMiddleware from 'redux-saga';
import funcionPrimaria from './Sagas/Sagas';
import CONSTANTS from './CONSTANTS';

const initialStateSesion = {
  facebookToken: '',
  tokenFirebase: '',
};

const initialStateGame = {
  deerSponsor: null,
};


const reducerSesion = (state = initialStateSesion, action) => {
  switch (action.type) {
    case CONSTANTS.ESTABLERCER_SESION:
      return action.usuario;
    case CONSTANTS.CERRAR_SESION:
      return null;
    case CONSTANTS.SAVE_FBTK:
      return {
        ...state,
        facebookToken: action.payload,
      };
    default:
      return state;
  }
};

const reducerGame = (state = initialStateGame, action) => {
  switch (action.type) {
    case CONSTANTS.ESTABLERCER_JUEGO_ACTUAL:
      return action.game;
    case CONSTANTS.ESTABLISH_ACTIVE_GAME:
      return action.game;
    case CONSTANTS.JOIN_DEER_SPONSOR:
      return { ...state, deerSponsor: action.payload };
    default:
      return state;
  }
};

const profileData = (state = {}, action) => {
  switch (action.type) {
    case CONSTANTS.PROFILE_DATA:
      return action.usuario;

    case CONSTANTS.ACTION_ESTABLISH_PROFILE:
      return { ...state, data: { ...action.payload } };
    case CONSTANTS.SET_PROFILE_DATA:
      return { ...state, data: { ...action.payload } };
    default:
      return state;
  }
};

const reducerWallet = (state = {}, action) => {
  switch (action.type) {
    case CONSTANTS.ACTION_REFRESH_WALLET:
      return { ...state, data: { ...action.payload } };
    default:
      return state;
  }
};

const sagaMiddleware = createSagaMiddleware();

const reducers = combineReducers({
  reducerSesion,
  reducerWallet,
  profileData,
  reducerGame,
  form,
});

const store = createStore(reducers, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(funcionPrimaria);

export default store;
