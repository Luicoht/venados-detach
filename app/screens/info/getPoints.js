import React from 'react';
import { ScrollView, View, Image, ImageBackground, TouchableOpacity } from 'react-native';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { Asset } from 'expo';
import { DoubleBounce } from 'react-native-loader';
import { connect } from 'react-redux';
import { scale, scaleVertical, size } from '../../utils/scale';
import shutterstock from '../../assets/JPEG/shutterstock-bat.jpeg';
import VenadosRojo from '../../assets/images/VenadosRojo.png';


class GetPoints extends React.Component {
  constructor(props) {
    super(props);
    const Content = [
      {
        about: 'Por ser Socio Venados',
        score: '20,000'
      },
      {
        about: 'Descargar la aplicación',
        score: '1,000'
      },
      {
        about: 'Registrar tu boleto de entrada al estadio',
        score: '200'
      },
      {
        about: 'Hacer Check-in con foto publicada',
        score: '300'
      },
      {
        about: 'Entrar antes de la 1r entrada',
        score: '300'
      },
      {
        about: 'Permanecer en el estadio hasta la última entrada',
        score: '300'
      },
      {
        about: 'Permanecer en el estadio hasta el final del juego en extra innings',
        score: '500'
      },
      {
        about: 'Bono de asistencia a partir del 3er juego consecutivo',
        score: '500'
      },
    ];

    this.state = {
      isReady: false,
      content: Content
    };
    const options = {
      title: 'GANAR PUNTOS',
      hidden: true
    };
    this.props.navigation.setParams({ options });
  }

  componentDidMount() { this._cacheResourcesAsync(); }

  async _cacheResourcesAsync() {
    const images = [
      shutterstock,
      VenadosRojo,
    ];
    const cacheImages = images.map(image => {
      return Asset.fromModule(image).downloadAsync();
    });
    this.setState({ isReady: true })
    return Promise.all(cacheImages);
  }


  registro = values => {
    this.props.registro(values);
  };
  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    const content = this.state.content.map((route) => {
      return (
        <TouchableOpacity activeOpacity={1} style={styles.tableContent}>
          <View style={styles.tableHeaderContent}>
            <View style={styles.item}>
              <View style={styles.leftContent}>
                <RkText style={styles.textLeftContent} > {route.about}</RkText>
              </View>
              <View style={styles.rightContent}>
                <RkText style={styles.textRightContent} > {route.score}</RkText>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      );
    });


    return (
      <View style={styles.root}>
        <ImageBackground style={{ flex: 1 }} source={shutterstock} >
          <ScrollView style={{ flex: 1 }}>
            <View style={styles.table}>
              <Image
                style={styles.IconBG}
                source={VenadosRojo}
              />
              <RkText style={styles.textBg} >Gana más puntos</RkText>
              <RkText style={styles.textInfo} >!Solo llena los siguientes campos y listo¡</RkText>
              <View style={styles.tableHeader}>
                <View style={styles.leftHeader}>
                  <RkText style={[styles.textHeader, styles.textLeft]}> CONCEPTO</RkText>
                </View>
                <View style={styles.rightHeader}>
                  <RkText style={[styles.textHeader, styles.textRight]}> PUNTOS</RkText>
                </View>
              </View>
              {content}
              <RkButton style={styles.boton} > PUNTOS </RkButton>
              <View style={{ height: scaleVertical(25) }} />
            </View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1
  },
  playContainer: {
    flex: 1
    // width: size.width,
    // height: size.height / 3
  },
  imageBg: {
    width: size.width,
    height: size.height / 3
  },
  label: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
  },
  textInfo: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: scale(20),
    paddingVertical: scaleVertical(10),
    paddingHorizontal: scaleVertical(20),
  },
  textBg: {
    color: 'white',
    fontFamily: 'TTPollsBold',
    fontSize: scale(30),
    paddingVertical: scaleVertical(10),
    paddingHorizontal: scaleVertical(20),
  },
  IconBG: {
    // width: scaleVertical(64),
    height: scaleVertical(50),
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  subMenuItems: {
    width: (size.width / 3) - scaleVertical(10),
    height: (size.height / 6) - scaleVertical(10),
    flexDirection: 'column',
    fontSize: 8,
  },
  subMenuIcon: {
    marginTop: 1,
    marginBottom: 1,
  },
  subMenuText: {
    color: '#fff',
    fontSize: scaleVertical(14),
    alignSelf: 'center'
  },
  boton: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45),
  },
  table: {
    backgroundColor: 'rgba(0,0,0,0.85)',
  },
  tableHeader: {
    marginLeft: scale(10),
    marginRight: scale(10),
    marginTop: scale(20),
    flexDirection: 'row',
  },
  tableContent: {
    marginLeft: scale(10),
    marginRight: scale(10),
    marginTop: scale(20),
  },
  leftHeader: {
    flex: 3,
    padding: scale(5),
  },
  rightHeader: {
    flex: 1,
    padding: scale(5),
  },
  textHeader: {
    color: '#eb0029',
    fontSize: scale(15),
  },
  textLeft: {
    textAlign: 'left',
    marginLeft: scale(5),
  },
  textRight: {
    textAlign: 'right',
    color: '#eb0029',
  },
  tableHeaderContent: {
    marginLeft: scale(10),
    marginRight: scale(10),
    flexDirection: 'row',
    alignSelf: 'center'
  },
  item: {
    flex: 1,
    borderBottomWidth: scale(1),
    flexDirection: 'row',
  },
  leftContent: {
    flex: 3,
  },
  rightContent: {
    flex: 1.5,
  },
  textLeftContent: {
    fontSize: scale(16),
    textAlign: 'left',
    color: 'white',
    fontFamily: 'Roboto-Regular',
  },
  textRightContent: {
    fontSize: scale(18),
    textAlign: 'right',
    color: '#eb0029',
    fontFamily: 'Roboto-Regular',
  },
}));


const mapStateToProps = state => ({ prop: state.prop });

const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GetPoints);
