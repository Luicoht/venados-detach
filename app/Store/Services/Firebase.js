import * as firebase from 'firebase';

/*
// production project
const config = {
  apiKey: 'AIzaSyDqBFF5_H5xvmHdbWbmzCANRDyJ82Bag4E',
  authDomain: 'venados-a40e3.firebaseapp.com',
  databaseURL: 'https://venados-a40e3.firebaseio.com',
  projectId: 'venados-a40e3',
  storageBucket: 'venados-a40e3.appspot.com',
  messagingSenderId: '383435945674'
};
*/

// test project
const config = {
  apiKey: 'AIzaSyAdHeNa5oNrttfycnfspDkPdN8_Vt11x4c',
  authDomain: 'venados-dev.firebaseapp.com',
  databaseURL: 'https://venados-dev.firebaseio.com',
  projectId: 'venados-dev',
  storageBucket: 'venados-dev.appspot.com',
  messagingSenderId: '1051316078695'
};


firebase.initializeApp(config);
const storage = firebase.storage();

export const authentication = firebase.auth();
export const baseDeDatos = firebase.database();


export const uploadImage = async (file, name, imageCb) => {
  try {
    const user = authentication.currentUser;
    const storageRef = storage.ref();
    const mountainsRef = storageRef.child(`${user.uid}/${name}.png`);
    await mountainsRef.put(file);

    const image = await storageRef.child(`${user.uid}/${name}.png`).getDownloadURL();
    console.log('user, image :', user, image);
    imageCb(image);
  } catch (err) {
    console.log('ERR', err);
  }
};
