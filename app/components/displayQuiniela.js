import React from 'react';
import { RkStyleSheet, RkText } from 'react-native-ui-kitten';
import { AppLoading } from 'expo';
import { View, ScrollView, Image } from 'react-native';
import { scaleVertical, size, scale } from '../utils/scale';
import c1b from '../assets/cards/1B.png';
import c2b from '../assets/cards/2B.png';
import c3b from '../assets/cards/3B.png';
import cc from '../assets/cards/C.png';
import ccf from '../assets/cards/CF.png';
import clf from '../assets/cards/LF.png';
import crf from '../assets/cards/RF.png';
import cp from '../assets/cards/P.png';
import css from '../assets/cards/SS.png';

export class DisplayQuiniela extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      cards: this.props.cards
    };
  }

  _getContent() {
    const contentCards = this.state.cards.map((cardsArray, colorID) => {
      let bgColor;
      let pointsToWin;

      switch (colorID) {
        case 0:
          bgColor = '#f05a28';
          pointsToWin = 160;
          break;
        case 1:
          bgColor = '#009344';
          pointsToWin = 400;
          break;
        default:
          bgColor = '#26aae0';
          pointsToWin = 800;
          break;
      }

      const Text = cardsArray.length !== 0 ? (<RkText style={{ color: bgColor, paddingTop: scale(15), fontFamily: 'TTPollsBoldItalic', fontSize: scale(15), textAlign: 'center' }} >Podrás ganar  {pointsToWin} puntos por cada carta ganadora.</RkText>) : (<View />);

      const colorCard = cardsArray.map((card) => {
        const src = this._getRequire(card.position);
        const cardsInfo = (
          <View style={{ width: size.widthMargin / 3.4, padding: scaleVertical(4), justifyContent: 'center' }}>
            <View style={{ flex: 1 }}>
              <View style={{ height: size.widthMargin / 2.5, justifyContent: 'center' }}>
                <Image style={{ alignSelf: 'center', resizeMode: 'contain' }} source={src} />
              </View>
              <View style={{ flex: 1 }}>
                <RkText style={{ fontSize: scaleVertical(12), textAlign: 'center', color: bgColor }}> {card.name || 'falta dato'} </RkText>
                <RkText style={{ fontSize: scaleVertical(12), textAlign: 'center', color: bgColor }}> Adquiriste {card.count || 'falta dato'} </RkText>
              </View>
            </View>
          </View>
        );
        return cardsInfo;
      });
      const contentColor = (
        <View style={{ flex: 1 }}>
          {Text}
          <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
            {colorCard}
          </View>
        </View>
      );
      return contentColor;
    });


    const content = (
      <View style={{ flex: 1, flexWrap: 'wrap' }}>
        {contentCards}
      </View>
    );
    return content;
  }


  _getRequire(name) {
    switch (name) {
      case '1B':
        return c1b;
      case '2B':
        return c2b;
      case '3B':
        return c3b;
      case 'C':
        return cc;
      case 'CF':
        return ccf;
      case 'LF':
        return clf;
      case 'RF':
        return crf;
      case 'P':
        return cp;
      default:
        return css;
    }
  }


  render() {
    if (!this.state.cards) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }
    return (
      <View style={styles.root} >
        <ScrollView style={styles.root} >
          <View style={{ paddingVertical: scaleVertical(15), alignSelf: 'center' }} >
            {this._getContent()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(theme => ({
  root: {
    flex: 1
  },


}));
