import React from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView } from 'react-native';
import { RkStyleSheet, RkText, RkButton, RkModalImg } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { scaleVertical, size } from '../../utils/scale';
import { AsyncImage } from '../../components/AsyncImage';
import { getProfile, getWalletPoints } from '../../Store/Services/Socket';
import BuyInStoreModal from './Modals/BuyInStore';
import { storeData } from './storeExample';

class ArticleDetails extends React.Component {
  static navigationOptions = {
    title: 'Tienda'.toUpperCase(),
    hidden: false
  };

  constructor(props) {
    super(props);
    this.state = {
      article: storeData[0].articles[0],
      similary1: storeData[0].articles[1],
      similary2: storeData[0].articles[2],
      buyInStoreModal: false,
      wallet: false,
      profile: null,
    };
  }

  checkPoints() {
    const { usuarioToken: { tokenFirebase } } = this.props;

    const data = {
      token: tokenFirebase,
    };


    getProfile(data)
      .then((profile) => {
        this.setState({ profile });
      })
      .catch((err) => { console.log('error', err); });


    getWalletPoints()
      .then((wallet) => {
        this.setState({ wallet });
      })
      .catch((err) => { console.log('error', err); });

    // this.setState({ buyInStoreModal: true });
  }

  render() {
    const { id, name, photoURL, points, price, description } = this.state.article;

    return (
      <View style={styles.root}>
        <ScrollView style={{ flex: 1 }} >
          <View style={{ height: size.width * 1.25, padding: scaleVertical(15) }}>
            <RkText style={{ fontSize: scaleVertical(20), fontFamily: 'Roboto-Bold', textAlign: 'center' }}> {name} </RkText>
            <RkModalImg
              style={{
                height: size.widthMargin,
                width: size.widthMargin,
                alignSelf: 'center',
                paddingVertical: scaleVertical(15)
              }}
              source={{ uri: photoURL }}
              placeholderColor='transparent'
            />
          </View>
          <View style={{ flex: 1, paddingHorizontal: scaleVertical(18), paddingVertical: scaleVertical(15) }}>
            <RkButton
              style={{ paddingVertical: scaleVertical(5), height: scaleVertical(35), width: size.widthMargin - scaleVertical(30), alignSelf: 'center', backgroundColor: '#eb0029' }}
              onPress={this.checkPoints.bind(this)}
            >
              <RkText style={{ color: '#fff' }}> CANJEAR</RkText>
            </RkButton>
            <View style={{ height: scaleVertical(10) }} />
            <RkButton style={{ paddingVertical: scaleVertical(5), height: scaleVertical(35), width: size.widthMargin - scaleVertical(30), alignSelf: 'center', backgroundColor: 'TRANSPARENT', borderColor: '#eb0029', borderWidth: scaleVertical(2) }}>
              <RkText style={{ color: '#000' }}> FAVORITOS</RkText>
            </RkButton>
            <View style={{ height: scaleVertical(10) }} />
            <RkButton style={{ height: scaleVertical(35), width: size.widthMargin - scaleVertical(30), alignSelf: 'center', backgroundColor: '#3b5998' }}>
              <RkText style={{ paddingVertical: scaleVertical(5), color: '#fff' }}> COMPARTIR</RkText>
            </RkButton>
          </View>
          <View style={{ flex: 1, paddingHorizontal: scaleVertical(15), paddingVertical: scaleVertical(15) }}>
            <View style={{ flex: 1 }}>
              <RkText style={{ fontSize: scaleVertical(20), fontFamily: 'Roboto-Light', color: '#000', textAlign: 'center' }}> Obtenla con </RkText>
            </View>
            <View style={{ flex: 1 }}>
              <RkText style={{ fontSize: scaleVertical(18), fontFamily: 'Roboto-Bold', color: '#eb0029', textAlign: 'center', textDecorationLine: 'underline' }}> {points} puntos </RkText>
            </View>
            <View style={{ flex: 1 }}>
              <RkText style={{ fontSize: scaleVertical(15), fontFamily: 'Roboto-Bold', color: '#002157', textAlign: 'center', textDecorationLine: 'underline' }}> ${price} </RkText>
            </View>
          </View>
          <View style={{ flex: 1, paddingHorizontal: scaleVertical(15), paddingVertical: scaleVertical(15) }}>
            <RkText style={{ fontSize: scaleVertical(20), fontFamily: 'Roboto-Bold', color: '#000', textAlign: 'left' }}> Descripción del producto </RkText>
            <RkText style={{ paddingHorizontal: scaleVertical(15), paddingVertical: scaleVertical(10), fontSize: scaleVertical(15), fontFamily: 'Roboto-Bold', color: '#002157', textAlign: 'auto' }}> ${description} </RkText>
          </View>
          <View style={{ flex: 1, paddingTop: scaleVertical(15), paddingBottom: scaleVertical(60), backgroundColor: '#363636' }}>
            <RkText style={{ fontSize: scaleVertical(20), fontFamily: 'Roboto-Bold', color: '#fff', textAlign: 'center', fontSize: scaleVertical(20), fontFamily: 'TTPollsBold' }}> Productos similares </RkText>
            <View style={{ height: scaleVertical(15) }} />
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <AsyncImage
                  style={{
                    height: size.width / 3.5,
                    width: size.width / 3.5,
                    alignSelf: 'center',
                    paddingBottom: scaleVertical(10)
                  }}
                  source={{ uri: this.state.similary1.photoURL }}
                  placeholderColor='transparent'
                />
                <View style={{ height: scaleVertical(10) }} />
                <RkText style={{ padinHorizontal: scaleVertical(15), color: '#fff', fontFamily: 'Roboto-Light', fontSize: scaleVertical(16), textAlign: 'center' }}>{this.state.similary1.name}</RkText>
              </View>
              <View style={{ flex: 1 }}>
                <AsyncImage
                  style={{
                    height: size.width / 3.5,
                    width: size.width / 3.5,
                    alignSelf: 'center',
                    paddingBottom: scaleVertical(10)
                  }}
                  source={{ uri: this.state.similary2.photoURL }}
                  placeholderColor='transparent'
                />
                <View style={{ height: scaleVertical(10) }} />
                <RkText style={{ padinHorizontal: scaleVertical(15), color: '#fff', fontFamily: 'Roboto-Light', fontSize: scaleVertical(16), textAlign: 'center' }}>{this.state.similary2.name}</RkText>
              </View>
            </View>
            <View style={{ flex: 1, paddingVertical: scaleVertical(15), justifyContent: 'center' }}>
              <View style={{ height: scaleVertical(30) }} />
              <RkButton style={{ height: scaleVertical(35), width: size.widthMargin - scaleVertical(30), alignSelf: 'center', backgroundColor: '#eb0029' }}>
                <RkText style={{ color: '#fff' }}> FAVORITOS</RkText>
              </RkButton>
            </View>
          </View>
        </ScrollView>

        <BuyInStoreModal
          visible={this.state.buyInStoreModal}
          onClose={() => this.setState({ buyInStoreModal: false })}
          data={{ articleID: id }}
        />
      </View>
    );
  }
}


let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff',
    apdding: scaleVertical(15)
  },
}));

ArticleDetails.propTypes = {
  usuarioToken: PropTypes.object,
};

const mapStateToProps = state => {
  console.log('tokenFirebase: ', state.reducerSesion.tokenFirebase);
  return {
    prop: state.prop,
    usuarioToken: state.reducerSesion,
  };
};


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ArticleDetails);
