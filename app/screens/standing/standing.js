import React from 'react';
import PropTypes from 'prop-types';
import { RkStyleSheet, RkText, RkTheme } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { scaleVertical } from '../../utils/scale';
import Standings from './standings';

class Standing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      roots: ['STANDING', 'RESULTADOS'],
      root: 0,
      timestamp: '',
      selectDate: 2017
    };
    const options = {
      title: 'STANDING',
      hidden: true
    };
    this.props.navigation.setParams({ options });
  }

  _renderHeader() {
    if (this.state.root === 0) {
      return (
        <RkText style={styles.headerTxt}>{this.state.roots[this.state.root]}</RkText>
      );
    }
    return (
      <View>
        <RkText style={styles.headerTxt} >{this.state.roots[this.state.root]}</RkText>
      </View>
    );
  }

  render() {
    RkTheme.setType('RkTabView', 'example', {
      backgroundColor: '#e5e5e5',
      borderColor: '#e5e5e5',
      color: '#000'
    });

    RkTheme.setType('RkTabView', 'exampleSelected', {
      backgroundColor: '#fff',
      borderColor: '#fff',
      color: '#000'
    });
    {/* <RkTabView.Tab title={'RESULTADOS'} >
            <Results passedData={this.state.selectDate} ></Results>
          </RkTabView.Tab> */}

    return (
      <View style={styles.root} >
        <View style={styles.header} >
          {this._renderHeader()}
        </View>
        <Standings />
        {/* <RkTabView onTabChanged={(index) => { this.setState({ root: index }); }} rkType='example'>
          <RkTabView.Tab title={'STANDING'}>
            <Standings />
          </RkTabView.Tab>
        </RkTabView> */}
      </View>
    );
  }
}


let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    backgroundColor: '#c8edff',
    height: scaleVertical(60),
    // alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: scaleVertical(15),
  },
  headerTxt: {
    fontSize: scaleVertical(20),
    fontFamily: 'TTPollsBold',
    textAlign: 'left',
  },
  container: {
    backgroundColor: '#fff',
    flex: 1
  },
  tableContent: {
    paddingVertical: scaleVertical(20),
    paddingHorizontal: scaleVertical(10),
  },
  head: { height: 40, backgroundColor: '#e5e5e5' },
  text: { margin: 6 },
  row: { flexDirection: 'row', backgroundColor: '#FFF1C1' },
  btn: { width: 58, height: 18, backgroundColor: '#78B7BB', borderRadius: 2 },
  btnText: { textAlign: 'center', color: '#fff' }
}));

Standing.navigationOptions = {
  title: 'Standing'.toUpperCase(),
  hidden: true
};
Standing.propTypes = {
  currentActiveGame: PropTypes.object
};
const mapStateToProps = state => ({
  currentActiveGame: state.reducerGame,
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Standing);
