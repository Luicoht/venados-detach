
import { Alert } from 'react-native';
import axios from 'axios';
import { API_URL } from '../../constants';
// import { API_URL } from '../../constants';
import { authentication } from './Firebase';
import { authInDevice } from './../Services/Socket';

function displayAlert(errorCode) {
  console.log('errorCode', errorCode);
  switch (errorCode) {
    case 'auth/user-not-found':
      Alert.alert(
        'No se encontro usuario'
      );
      break;
    case 'auth/wrong-password':
      Alert.alert(
        'Contraseña incorrecta'
      );
      break;
    case 'auth/email-already-in-use':
      Alert.alert(
        'El email ya se encuentra en uso'
      );
      break;
    case 'auth/network-request-failed':
      Alert.alert(
        'Demasiado tiempo de espera, conexión interrumpida o host inalcanzable'
      );
      break;
    case 'auth/user-token-expired':
      Alert.alert(
        'Inicie sesión de nuevo'
      );
      break;
    default:
      Alert.alert(
        'Ups!..'
      );
      break;
  }
}


export const register = values => {
  console.log('register values', values);
  const data = {
    email: values.email,
    password: values.password
  };
  const config = {
    method: 'post',
    url: `${API_URL}/auth/register`,
    data,
  };

  axios(config).then((response) => {
    const serverResponse = response.data;
    authentication.createUserWithEmailAndPassword(values.email, values.password)
      .then((user) => {
        // authInDevice();
      })
      .catch((err) => {
        displayAlert(err.code);
      });
    return serverResponse;
  }).catch((error) => {
    console.log('error: ', error);
  });
};
