export const data = [




    {
        player: "ALEJANDRO SOTO",
        position: "PITCHER",
        event: 2,
        pointsToWin: 100,
        pointsToWinInBonus: 200,
        bonus: false,
        bonusPoints: 220,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/1/Foto/vista/alejandro-soto-copia-1578-1634.jpg"
    },
    {
        player: "EVAN MARSHALL",
        position: "PITCHER",
        event: 2,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: false,
        bonusPoints: 220,
        isWinner: true,
        image: "http://www.venadosdemazatlan.com/files/Jugador/6/Foto/vista/evan-marshall-copia-1583-1639.jpg"
    },

    {
        player: "MICHAEL LIVELY",
        position: "PITCHER",
        event: 2,
        pointsToWin: 600,
        pointsToWinInBonus: 600,
        bonus: false,
        bonusPoints: 220,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/10/Foto/vista/mitch-lively-copia-1586-1642.jpg"
    },

    {
        player: "ARTURO BARRADAS",
        position: "PITCHER",
        event: 2,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: false,
        bonusPoints: 0,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/7/Foto/vista/arturo-barradas-copia-1579-1645.jpg"
    },

    {
        player: "WALTER SILVA",
        position: "PITCHER",
        event: 2,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: false,
        bonusPoints: 0,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/16/Foto/vista/walter-silva-copia-1592-1649.jpg"
    },

    {
        player: "BRIAN HERNANDEZ",
        position: "INFIELDER",
        event: 1,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: false,
        bonusPoints: 0,
        isWinner: true,
        image: "http://www.venadosdemazatlan.com/files/Jugador/3/Foto/vista/brian-hernandez-copia-1580-1636.jpg"
    },

    {
        player: "ALFONSO SANCHEZ",
        position: "PITCHER",
        event: 2,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: false,
        bonusPoints: 0,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/11/Foto/vista/alfonso-sanchez-copia-1587-1643.jpg"
    },

    {
        player: "ROBERTO YAHIR VALENZUELA",
        position: "INFIELDER",
        event: 2,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: false,
        bonusPoints: 220,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/15/Foto/vista/roberto-valenzuela-copia-1591-1647.jpg"
    },

    {
        player: "CARLOS MUÑOZ",
        position: "INFIELDER",
        event: 1,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: false,
        bonusPoints: 0,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/4/Foto/vista/carlos-munoz-copia-1581-1637.jpg"
    },

    {
        player: "EREMIAS PINEDA",
        position: "OUTFILDER",
        event: 1,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: false,
        bonusPoints: 220,
        isWinner: true,
        image: "http://www.venadosdemazatlan.com/files/Jugador/8/Foto/vista/jeremias-pineda-copia-1584-1640.jpg"
    },

    {
        player: "JOSÉ JUAN AGUILAR",
        position: "OUTFILDER",
        event: 2,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: false,
        bonusPoints: 220,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/9/Foto/vista/jose-aguilar-copia-1585-1641.jpg"
    },

    {
        player: "GILBERTO GALAVIZ",
        position: "CATCHER",
        event: 1,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: false,
        bonusPoints: 0,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/12/Foto/vista/gilberto-galaviz-copia-1588-1644.jpg"
    },

    {
        player: "NORMAN SALVADOR ELENES",
        position: "PITCHER",
        event: 1,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: false,
        bonusPoints: 0,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/13/Foto/vista/norman-elenes-copia-1589-1646.jpg"
    },

    {
        player: "ESTEBAN QUIROZ",
        position: "INFIELDER",
        event: 2,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: false,
        bonusPoints: 0,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/5/Foto/vista/esteban-quiroz-copia-1582-1638.jpg"
    },



    {
        player: "ALEJANDRO SOTO",
        position: "PITCHER",
        event: 2,
        pointsToWin: 100,
        pointsToWinInBonus: 200,
        bonus: true,
        bonusPoints: 220,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/1/Foto/vista/alejandro-soto-copia-1578-1634.jpg"
    },
    {
        player: "EVAN MARSHALL",
        position: "PITCHER",
        event: 2,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: true,
        bonusPoints: 350,
        isWinner: true,
        image: "http://www.venadosdemazatlan.com/files/Jugador/6/Foto/vista/evan-marshall-copia-1583-1639.jpg"
    },



    {
        player: "ARTURO BARRADAS",
        position: "PITCHER",
        event: 2,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: true,
        bonusPoints: 0,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/7/Foto/vista/arturo-barradas-copia-1579-1645.jpg"
    },



    {
        player: "BRIAN HERNANDEZ",
        position: "INFIELDER",
        event: 1,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: true,
        bonusPoints: 600,
        isWinner: true,
        image: "http://www.venadosdemazatlan.com/files/Jugador/3/Foto/vista/brian-hernandez-copia-1580-1636.jpg"
    },



    {
        player: "ROBERTO YAHIR VALENZUELA",
        position: "INFIELDER",
        event: 2,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: true,
        bonusPoints: 220,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/15/Foto/vista/roberto-valenzuela-copia-1591-1647.jpg"
    },



    {
        player: "EREMIAS PINEDA",
        position: "OUTFILDER",
        event: 1,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: true,
        bonusPoints: 440,
        isWinner: true,
        image: "http://www.venadosdemazatlan.com/files/Jugador/8/Foto/vista/jeremias-pineda-copia-1584-1640.jpg"
    },



    {
        player: "GILBERTO GALAVIZ",
        position: "CATCHER",
        event: 1,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: true,
        bonusPoints: 0,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/12/Foto/vista/gilberto-galaviz-copia-1588-1644.jpg"
    },



    {
        player: "ESTEBAN QUIROZ",
        position: "INFIELDER",
        event: 2,
        pointsToWin: 400,
        pointsToWinInBonus: 400,
        bonus: true,
        bonusPoints: 0,
        isWinner: false,
        image: "http://www.venadosdemazatlan.com/files/Jugador/5/Foto/vista/esteban-quiroz-copia-1582-1638.jpg"
    },







];



