import React from 'react';
import { UIManager, LayoutAnimation, StyleSheet, View, Platform, Image, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { RkText, RkStyleSheet, RkButton } from 'react-native-ui-kitten';
import { Asset, AppLoading } from 'expo';
import Modal from 'react-native-modal';
import { buyCards, getCards, getWalletPoints, formatNumber } from '../Store/Services/Socket';
import { scale, scaleVertical, size, AppbarHeight, StatusbarHeight } from '../utils/scale';
import dont from '../assets/icons/dont.png';
import bigDont from '../assets/icons/dont-4.png';
import quiniela from '../assets/icons/quiniela-3.png';
import { DisplayQuiniela } from './displayQuiniela';


class QuinielaModals extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isModalNotPartnerVisible: false,
      isModalNotHaveGameVisible: false,
      isModalNeedCheckInVisible: false,
      isModalAddQuinielaVisible: false,
      isModalNotHavePointsVisible: false,
      isModalNotHaveSelectedCardsVisible: false,
      isModalPurchasedCardsVisible: false,
      currentInGameCards: [],
      countOrange: 0,
      countGreen: 0,
      countBlue: 0,
      totalCost: 0,
      totalCount: 0,
      cards: [],
      wallet: ''
    };
  }

  componentDidMount() {
    this._cacheResourcesAsync();
    getCards()
      .then((cards) => {
        this.setState({ cards });
      })
      .catch((err) => { console.log('error', err); });


    getWalletPoints()
      .then((wallet) => {
        // console.log('getWalletPoints', wallet);
        this.setState({ wallet });
      })
      .catch((err) => { console.log('error', err); });
  }


  componentWillUpdate(nextProps, nextState) {
    let animate = false;
    if (this.state.isReady !== nextState.isReady || this.state.cards !== nextState.cards || this.state.wallet !== nextState.wallet) {
      animate = true;
    }
    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      LayoutAnimation.easeInEaseOut();
    }
  }


  _cleanData() {
    this.setState({ countOrange: 0 });
    this.setState({ countGreen: 0 });
    this.setState({ countBlue: 0 });
    this.setState({ totalCost: 0 });
    // this.setState({ totalCount: 0 });
  }
  _quinielaToErrorPoints() {
    // console.log('_quinielaToErrorPoints porno');
    this._toggleIsModalAddQuinielaVisible();
    this._toggleIsModalNotHavePointsVisible();
  }

  _errorPointsToQuiniela() {
    // console.log('_errorPointsToQuiniela porno');
    this._toggleIsModalNotHavePointsVisible();
    this._toggleIsModalAddQuinielaVisible();
  }

  _quinielaToErrorCards() {
    // console.log('_quinielaToErrorCards porno');
    this._toggleIsModalAddQuinielaVisible();
    this._toggleIsModalNotHaveSelectedCardsVisible();
  }

  _buyCardsToCardsInGame() {
    this._toggleIsModalAddQuinielaVisible();
    this._toggleIsModalCardsInGameVisible();
  }
  _errorCardsToQuiniela() {
    // console.log('_errorCardsToQuiniela porno');
    this._toggleIsModalNotHaveSelectedCardsVisible();
    this._toggleIsModalAddQuinielaVisible();
  }


  _buyCardsToPurchased() {
    this._cleanData();
    this._toggleIsModalAddQuinielaVisible();
    this._toggleIsModalPurchasedCardsVisible();
  }

  _playGame() {
    if (this.state.countOrange === 0 && this.state.countGreen === 0 && this.state.countBlue === 0) {
      this._quinielaToErrorCards();
      return;
    }
    const totalCost =
      (this.state.cards[0].price * this.state.countOrange) +
      (this.state.cards[1].price * this.state.countGreen) +
      (this.state.cards[2].price * this.state.countBlue);

    const totalCount = this.state.countOrange + this.state.countGreen + this.state.countBlue;

    this.setState({ totalCount });
    this.setState({ totalCost });

    if (totalCost > this.state.wallet.points) {
      this._quinielaToErrorPoints();
      return;
    }

    const cards = [];
    if (this.state.countOrange !== 0) { cards.push({ id: 1, num: this.state.countOrange }); }
    if (this.state.countGreen !== 0) { cards.push({ id: 2, num: this.state.countGreen }); }
    if (this.state.countBlue !== 0) { cards.push({ id: 3, num: this.state.countBlue }); }
    if (cards.length === 0) {
      this._buyCardsToPurchased();
      return;
    }
    this._buyCards(cards);
  }


  _buyCards(cards) {
    buyCards(cards)
      .then((currentInGameCards) => {
        this.setState({ currentInGameCards });
        this._buyCardsToPurchased();
        // this._toggleIsModalCardsInGameVisible();
      })
      .catch((err) => {
        console.log('err al comprar cartas', err);
      });
  }

  _addTo(count) {
    switch (count) {
      case 1:
        this.setState({ countOrange: this.state.countOrange + 1 });
        break;
      case 2:
        this.setState({ countGreen: this.state.countGreen + 1 });
        break;

      default:
        this.setState({ countBlue: this.state.countBlue + 1 });
        break;
    }
  }
  _removeTo(count) {
    switch (count) {
      case 1:
        if (this.state.countOrange - 1 >= 0) { this.setState({ countOrange: this.state.countOrange - 1 }); }
        break;
      case 2:
        if (this.state.countGreen - 1 >= 0) { this.setState({ countGreen: this.state.countGreen - 1 }); }
        break;
      default:
        if (this.state.countBlue - 1 >= 0) { this.setState({ countBlue: this.state.countBlue - 1 }); }
        break;
    }
  }


  _goTo(route) {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: route })]
    });
    this.props.navigation.dispatch(resetAction);
  }

  _toggleQuiniela() {
    this._cleanData();

    // cambiame porno
    const haveCheckIn = true;
    const isParner = true;
    // const isParner = this.props.profile.data.userType === 6;

    if (this.props.currentActiveGame.finished === 1) {
      this._toggleIsModalNotHaveGameVisible();
      return;
    }

    if (!haveCheckIn) {
      this._toggleIsModalNeedCheckInVisible();
      return;
    }

    if (!isParner) {
      this._toggleIsModalNotPartnerVisible();
      return;
    }

    if (isParner && this.props.currentActiveGame.finished !== 1 && haveCheckIn) {
      this._toggleIsModalAddQuinielaVisible();
      return;
    }



    // this.setState({ isModalOneVisible: !this.state.isModalOneVisible })
  }


  _toggleIsModalNotPartnerVisible() {
    // console.log('  _toggleIsModalNotPartnerVisible');
    this.setState({ isModalNotPartnerVisible: !this.state.isModalNotPartnerVisible });
  }


  _toggleIsModalNotHaveGameVisible() {
    // console.log('_toggleIsModalNotHaveGameVisible');
    this.setState({ isModalNotHaveGameVisible: !this.state.isModalNotHaveGameVisible });
  }

  _toggleIsModalNotHavePointsVisible() {
    // console.log('_toggleIsModalNotHavePointsVisible');
    this.setState({ isModalNotHavePointsVisible: !this.state.isModalNotHavePointsVisible });
  }

  _toggleIsModalNeedCheckInVisible() {
    // console.log('_toggleIsModalNeedCheckInVisible');
    this.setState({ isModalNeedCheckInVisible: !this.state.isModalNeedCheckInVisible });
  }

  _toggleIsModalAddQuinielaVisible() {
    // console.log('_toggleIsModalAddQuinielaVisible');
    this.setState({ isModalAddQuinielaVisible: !this.state.isModalAddQuinielaVisible });
  }

  _toggleIsModalCardsInGameVisible() {
    // console.log('_toggleIsModalCardsInGameVisible');
    this.setState({ isModalCardsInGameVisible: !this.state.isModalCardsInGameVisible });
  }

  _toggleIsModalNotHaveSelectedCardsVisible() {
    // console.log('_toggleIsModalNotHaveSelectedCardsVisible');
    this.setState({ isModalNotHaveSelectedCardsVisible: !this.state.isModalNotHaveSelectedCardsVisible });
  }
  _toggleIsModalPurchasedCardsVisible() {
    // console.log('_toggleIsModalPurchasedCardsVisible');
    this.setState({ isModalPurchasedCardsVisible: !this.state.isModalPurchasedCardsVisible });
  }

  _renderNotPartnerModal() {
    const modal = (
      <Modal isVisible={this.state.isModalNotPartnerVisible}>
        <View style={styles.modalTransparentContainer}>
          <View style={styles.modalBackground}>
            <View style={styles.modalHeader}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleIsModalNotPartnerVisible(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground
                    resizeMode="contain"
                    style={styles.modalCloseImage} source={dont}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5 }} />
            </View>
            <View style={styles.modalContent}>
              <View style={styles.modalDontImageContent}>
                <Image style={styles.modalDontImage} source={bigDont} />
              </View>
              <View
                style={{
                  paddingHorizontal: scaleVertical(15)
                }}
              >
                <RkText style={styles.modalHeaderText}>Aún no eres socio Venados</RkText>
                <RkText style={styles.modalInfoText}>Para poder jugar necesitas hacerte socio.</RkText>
              </View>
              <View style={styles.modalButtonContent}>
                <RkButton style={styles.modalButton} onPress={() => { this._toggleIsModalNotPartnerVisible(); this._goTo('Partner'); }}>
                  <RkText style={styles.modalButtonText}>QUIERO SER SOCIO</RkText>
                </RkButton>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );

    return modal;
  }


  _renderNotHaveGameModal() {
    const modal = (
      <Modal isVisible={this.state.isModalNotHaveGameVisible}>
        <View style={styles.modalTransparentContainer}>
          <View style={styles.modalBackground}>
            <View style={styles.modalHeader}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleIsModalNotHaveGameVisible(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground
                    resizeMode="contain"
                    style={styles.modalCloseImage} source={dont}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5 }} />
            </View>
            <View style={styles.modalContent}>
              <View style={styles.modalDontImageContent}>
                <Image style={styles.modalDontImage} source={bigDont} />
              </View>
              <View
                style={{
                  paddingHorizontal: scaleVertical(15)
                }}
              >
                <RkText style={styles.modalHeaderText}>No hay juegos disponibles</RkText>
                <RkText style={styles.modalInfoText}>Muy pronto publicaremos el próximo juego.</RkText>
              </View>
              <View style={styles.modalButtonContent}>
                <RkButton style={styles.modalButton} onPress={() => { this._toggleIsModalNotHaveGameVisible(); }} >
                  <RkText style={styles.modalButtonText}>ACEPTAR</RkText>
                </RkButton>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );

    return modal;
  }

  _renderNotHavePointsModal() {
    const modal = (
      <Modal isVisible={this.state.isModalNotHavePointsVisible}>
        <View style={styles.modalTransparentContainer}>
          <View style={styles.modalBackground}>
            <View style={styles.modalHeader}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleIsModalNotHavePointsVisible(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground
                    resizeMode="contain"
                    style={styles.modalCloseImage} source={dont}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5 }} />
            </View>
            <View style={styles.modalContent}>
              <View style={styles.modalDontImageContent}>
                <Image style={styles.modalDontImage} source={bigDont} />
              </View>
              <View
                style={{
                  paddingHorizontal: scaleVertical(15)
                }}
              >
                <RkText style={styles.modalHeaderText}>Puntos insuficientes</RkText>
                <RkText style={styles.modalInfoText}> Lo sentimos, parece que no tienes puntos suficientes para jugar.</RkText>
                <RkText style={[styles.modalHeaderText, { paddingVertical: scaleVertical(15), color: '#eb0029' }]}> Te faltan {formatNumber(this.state.totalCost - this.state.wallet.points)} puntos.</RkText>
              </View>
              <View style={styles.modalButtonContent}>
                <RkButton style={styles.modalButton} onPress={() => { this._errorPointsToQuiniela(); }}>
                  <RkText style={styles.modalButtonText}>ACEPTAR</RkText>
                </RkButton>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );

    return modal;
  }

  _renderNeedCheckInModal() {
    const modal = (
      <Modal isVisible={this.state.isModalNeedCheckInVisible}>
        <View style={styles.modalTransparentContainer}>
          <View style={styles.modalBackground}>
            <View style={styles.modalHeader}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleIsModalNeedCheckInVisible(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground
                    resizeMode="contain"
                    style={styles.modalCloseImage} source={dont}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5 }} />
            </View>
            <View style={styles.modalContent}>
              <View style={styles.modalDontImageContent}>
                <Image style={styles.modalDontImage} source={bigDont} />
              </View>
              <View
                style={{
                  paddingHorizontal: scaleVertical(15)
                }}
              >
                <RkText style={styles.modalHeaderText}>Aún no haz hecho check-in</RkText>
                <RkText style={styles.modalInfoText}>Es necesario que registres tu visita para comenzar a jugar.</RkText>
              </View>
              <View style={styles.modalButtonContent}>
                <RkButton style={styles.modalButton} onPress={() => { this._goTo('CheckInMenu'); this._toggleIsModalNeedCheckInVisible(); }}>
                  <RkText style={styles.modalButtonText}>HACER CHECK-IN</RkText>
                </RkButton>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );

    return modal;
  }

  _renderAddQuinielaModal(cardsView) {
    const modal = (
      <Modal isVisible={this.state.isModalAddQuinielaVisible}>
        <View style={styles.modalTransparentContainer}>
          <View style={styles.modalBackgroundQuiniela}>
            <View style={{ flexDirection: 'row', height: size.width / 8 }}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleIsModalAddQuinielaVisible(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground resizeMode="contain" style={styles.modalCloseImage} source={dont} />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5, backgroundColor: 'transparent' }} />
            </View>
            <ScrollView style={{ flex: 1 }}>
              <View style={{ flex: 1 }}>
                <View style={styles.modalContent}>
                  <View style={styles.modalQuinielaImageContent}>
                    <Image style={styles.modalQuinielaImage} source={quiniela} />
                  </View>
                  <View style={{ paddingHorizontal: scaleVertical(15) }}>
                    <RkText style={styles.modalHeaderTextQuiniela}>Quiniela entrada {this.props.currentActiveGame.inning + 1}</RkText>
                    <RkText style={styles.modalInfoTextQuiniela}>Selecciona el tipo y número de tarjetas a jugar.</RkText>
                  </View>

                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ paddingHorizontal: scaleVertical(5) }}>
                      <RkText style={styles.modalSubInfoTextQuiniela}>Tienes</RkText>
                    </View>
                    <View style={{ paddingHorizontal: scaleVertical(5), backgroundColor: '#6a7786', justifyContent: 'center' }}>
                      <RkText style={styles.modalSubInfoColorTextQuiniela}>{formatNumber(this.state.wallet.points || 0)}</RkText>
                    </View>
                    <View style={{ paddingHorizontal: scaleVertical(5) }}>
                      <RkText style={styles.modalSubInfoTextQuiniela}>puntos</RkText>
                    </View>
                  </View>
                  {cardsView}
                  <View
                    style={{
                      paddingHorizontal: scaleVertical(15)
                    }}
                  >
                    <RkText style={styles.modalInfoTextQuiniela}>En la quiniela las posiciones se asignan aleatoriamente.</RkText>
                  </View>
                  <View
                    style={{
                      paddingHorizontal: scaleVertical(15),
                      paddingBottom: scaleVertical(20)
                    }}
                  >
                    <RkButton onPress={() => this._playGame()} style={{ backgroundColor: '#eb0029', justifyContent: 'center', alignSelf: 'center' }}>
                      <RkText
                        style={{
                          color: '#fff',
                          fontSize: scaleVertical(15),
                          fontFamily: 'Roboto-Regular',
                          textAlign: 'center'
                        }}
                      >JUGAR</RkText>
                    </RkButton>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );

    return modal;
  }

  _renderCardsInGameModal() {
    const modal = (
      <Modal isVisible={this.state.isModalCardsInGameVisible}>
        <View style={styles.modalTransparentContainer}>
          <View style={styles.modalBackgroundQuiniela}>
            <View style={{ flexDirection: 'row', height: size.width / 8 }}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleIsModalCardsInGameVisible(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground resizeMode="contain" style={styles.modalCloseImage} source={dont} />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5, backgroundColor: 'transparent' }} />
            </View>
            <ScrollView style={{ flex: 1 }}>
              <View style={{ flex: 1 }}>
                <View style={styles.modalContent}>
                  <View style={styles.modalQuinielaImageContent}>
                    <Image style={styles.modalQuinielaImage} source={quiniela} />
                  </View>
                  <RkText>CARTAS EN JUEGO</RkText>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );

    return modal;
  }

  _renderNotHaveSelectedCardsModal() {
    const modal = (
      <Modal isVisible={this.state.isModalNotHaveSelectedCardsVisible}>
        <View style={styles.modalTransparentContainer}>
          <View style={styles.modalBackground}>
            <View style={styles.modalHeader}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleIsModalNotHaveSelectedCardsVisible(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground
                    resizeMode="contain"
                    style={styles.modalCloseImage} source={dont}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5 }} />
            </View>
            <View style={styles.modalContent}>
              <View style={styles.modalDontImageContent}>
                <Image style={styles.modalDontImage} source={bigDont} />
              </View>
              <View
                style={{
                  paddingHorizontal: scaleVertical(15)
                }}
              >
                <RkText style={styles.modalHeaderText}>Aún no seleccionas tarjetas</RkText>
                <RkText style={styles.modalInfoText}>Es selecciona almenos un tipo de tarjeta.</RkText>
              </View>
              <View style={styles.modalButtonContent}>
                <RkButton onPress={() => { this._errorCardsToQuiniela(); }} style={styles.modalButton}>
                  <RkText style={styles.modalButtonText}>JUGAR</RkText>
                </RkButton>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );

    return modal;
  }


  _renderPurchasedCardsModal() {
    const modal = (
      <Modal isVisible={this.state.isModalPurchasedCardsVisible}>
        <View style={styles.modalTransparentContainer}>
          <View style={styles.modalBackground}>
            <View style={styles.modalHeader}>
              <View style={{ flex: 1, padding: scaleVertical(5) }}>
                <TouchableOpacity onPress={() => { this._toggleIsModalPurchasedCardsVisible(); }} activeOpacity={1} style={{ flex: 1 }} >
                  <ImageBackground
                    resizeMode="contain"
                    style={styles.modalCloseImage} source={dont}
                  />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 5 }} />
            </View>
            <View style={styles.modalContent}>

              <View style={{ flex: 1 }}>

                <View style={{ flex: 1 }}>
                  <RkText style={{ paddingTop: scale(15), fontFamily: 'TTPollsBoldItalic', fontSize: scale(25), textAlign: 'center' }} >¡Mucha suerte!</RkText>
                  <RkText style={{ fontFamily: 'Roboto-Regular', fontSize: scale(15), textAlign: 'center' }} >Haz adquirido {this.state.totalCount}  para jugar la entrada {this.props.currentActiveGame.inning + 1}</RkText>
                </View>
                <View style={{ flex: 2 }}>
                  <DisplayQuiniela cards={this.state.currentInGameCards} />
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <RkButton
                    style={{
                      alignSelf: 'center',
                      backgroundColor: '#eb0029',
                      width: size.width - scale(100),
                      height: scale(45),
                      paddingVertical: scale(15)
                    }}
                    onPress={() => { this._toggleIsModalPurchasedCardsVisible(); }}
                  >
                    <RkText rkType="h4  DecimaMonoBold" style={{ color: '#fff' }}> ACEPTAR  </RkText>
                  </RkButton>
                </View>
              </View>

            </View>
          </View>
        </View>
      </Modal>
    );

    return modal;
  }


  async _cacheResourcesAsync() {
    const images = [
      dont,
      bigDont,
      quiniela
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }


  _getCardsForm() {
    const solve = (
      this.state.cards.map((route, index) => {
        let count;
        let color;
        switch (route.id) {
          case 1: {
            count = <RkText style={styles.count}>{this.state.countOrange}</RkText>;
            color = '#f05a28';
            break;
          }
          case 2: {
            count = <RkText style={styles.count}>{this.state.countGreen}</RkText>;
            color = '#009344';
            break;
          }

          default: {
            count = <RkText style={styles.count}>{this.state.countBlue}</RkText>;
            color = '#26aae0';
            break;
          }
        }


        return (
          <View key={index} style={{ flexDirection: 'row', paddingVertical: scaleVertical(10), paddingHorizontal: scaleVertical(5) }}>
            <View style={{ flex: 1, paddingHorizontal: scaleVertical(5), justifyContent: 'center' }}>
              <RkText
                style={{
                  color: '#fff',
                  fontSize: scaleVertical(12),
                  fontFamily: 'Roboto-Regular',
                  textAlign: 'center'
                }}
              >Con {route.price} puntos </RkText>
            </View>
            <View style={{ flex: 1, paddingHorizontal: scaleVertical(5), margin: scaleVertical(3), justifyContent: 'center', backgroundColor: color, borderColor: '#fff', borderWidth: scaleVertical(2), borderRadius: scaleVertical(5) }}>
              <RkText
                style={{
                  color: '#fff',
                  fontSize: scaleVertical(12),
                  fontFamily: 'Roboto-Regular',
                  textAlign: 'center'
                }}
              >GANA</RkText>
              <RkText
                style={{
                  color: '#fff',
                  fontSize: scaleVertical(12),
                  fontFamily: 'Roboto-Bold',
                  textAlign: 'center'
                }}
              >{route.reward}</RkText>
            </View>
            <View style={{ flex: 1, paddingHorizontal: scaleVertical(5), margin: scaleVertical(3), backgroundColor: '#fff', justifyContent: 'center' }}>
              {count}
            </View>
            <View style={{ flex: 0.75, paddingHorizontal: scaleVertical(5), justifyContent: 'center' }}>
              <RkButton style={{ backgroundColor: 'transparent', height: size.width / 10, width: size.width / 7 }} onPress={() => this._addTo(route.id)}>
                <RkText
                  style={{
                    color: '#eb0029',
                    fontSize: scaleVertical(25),
                    fontFamily: 'Roboto-Regular',
                    textAlign: 'center'
                  }}
                >+</RkText>
              </RkButton>
            </View>
            <View style={{ flex: 0.75, paddingHorizontal: scaleVertical(5), justifyContent: 'center' }}>
              <RkButton onPress={() => this._removeTo(route.id)} style={{ backgroundColor: 'transparent', height: size.width / 10, width: size.width / 7 }}>
                <RkText
                  style={{
                    color: '#eb0029',
                    fontSize: scaleVertical(25),
                    fontFamily: 'Roboto-Regular',
                    textAlign: 'center'
                  }}
                >-</RkText>
              </RkButton>
            </View>
          </View>
        );
      })


    );
    return solve;
  }


  render() {
    // console.log('Quiniela subItem this.props.currentActiveGame', this.props.currentActiveGame);
    if (!this.state.isReady) {
      return (
        <AppLoading
          onError={console.warn}
        />
      );
    }
    const cardsView = this._getCardsForm();
    if (this.props.stilo && this.props.stilo === 3) {
      return (
        <View style={{ padding: scaleVertical(5) }}>
          <RkButton onPress={() => { this._toggleQuiniela(); }} style={{ width: size.widthMargin * 0.8, backgroundColor: '#eb0029', alignSelf: 'center' }}>
            <RkText style={{ color: '#fff' }}>JUGAR QUINIELA</RkText>
          </RkButton>
          {this._renderNotPartnerModal()}
          {this._renderCardsInGameModal()}
          {this._renderNotHaveGameModal()}
          {this._renderNotHavePointsModal()}
          {this._renderAddQuinielaModal(cardsView)}
          {this._renderNeedCheckInModal()}
          {this._renderNotHaveSelectedCardsModal()}
          {this._renderPurchasedCardsModal()}
        </View>
      );
    }
    if (this.props.stilo && this.props.stilo === 2) {
      return (
        <TouchableOpacity activeOpacity={1} style={{ flex: 1 }} onPress={() => { this._toggleQuiniela(); }}>
          <View style={{ flex: 3, margin: scaleVertical(15) }}>
            <ImageBackground
              resizeMode="contain"
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
              }} source={quiniela}
            />
          </View>
          <View style={{ flex: 2 }}>
            <RkText
              style={{
                color: '#fff',
                fontSize: scaleVertical(14),
                alignSelf: 'center'
              }}
            >QUINIELA</RkText>
          </View>

          {this._renderNotPartnerModal()}

          {this._renderCardsInGameModal()}
          {this._renderNotHaveGameModal()}
          {this._renderNotHavePointsModal()}
          {this._renderAddQuinielaModal(cardsView)}
          {this._renderNeedCheckInModal()}
          {this._renderNotHaveSelectedCardsModal()}
          {this._renderPurchasedCardsModal()}
        </TouchableOpacity>

      );
    }

    return (
      <TouchableOpacity activeOpacity={1} style={styles.subMenuItem} onPress={() => { this._toggleQuiniela(); }}>
        <View style={styles.subMenuItem}>
          <View style={styles.test2}>
            <ImageBackground resizeMode="contain" style={styles.bgImage} source={quiniela} />
          </View>
          <View >
            <RkText style={styles.subMenuText}>QUINIELA</RkText>
          </View>
        </View>

        {this._renderNotPartnerModal()}

        {this._renderNotHaveGameModal()}
        {this._renderNotHavePointsModal()}
        {this._renderAddQuinielaModal(cardsView)}
        {this._renderNeedCheckInModal()}
        {this._renderNotHaveSelectedCardsModal()}
        {this._renderPurchasedCardsModal()}
      </TouchableOpacity>
    );
  }
}


let styles = RkStyleSheet.create(() => ({
  layout: {
    backgroundColor: '#fff',
    height: scaleVertical(60),
    paddingTop: Platform.OS === 'ios' ? StatusbarHeight : 0,
    paddingBottom: Platform.OS === 'ios' ? scaleVertical(10) : 0
  },
  layoutSubNav: {
    backgroundColor: '#000',
  },
  containerSubNav: {
    flexDirection: 'row',
    height: AppbarHeight
  },
  containerSubNavHidden: {
    flexDirection: 'row',
    height: 0,
  },
  subNav: {
    flexDirection: 'row',
  },
  subNavHidden: {
    backgroundColor: 'rgba(0,0,0,0.85)',
  },
  container: {
    flexDirection: 'row',
    height: AppbarHeight,
  },
  left: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    justifyContent: 'center'
  },
  right: {
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
  },
  title: {
    ...StyleSheet.absoluteFillObject,
    flexDirection: 'row',
  },
  menu: {
    width: scaleVertical(35),
    height: scaleVertical(35),
  },
  header: {
    paddingTop: 25,
  },
  row: {
    flexDirection: 'row'
  },
  info: {
    flexDirection: 'row',
  },
  section: {
    flex: 1,
    alignItems: 'center'
  },
  center: {
    alignSelf: 'center',
  },
  menuIcon: {
    position: 'relative',
    top: Platform.OS === 'ios' ? scaleVertical(2) : scaleVertical(2),
    left: Platform.OS === 'ios' ? scaleVertical(10) : scaleVertical(10),
    height: (size.width / 6) - scaleVertical(35),
    width: (size.width / 6) - scaleVertical(35),
  },
  test2: {
    // height: Platform.OS === "ios" ? scaleVertical(45) : scaleVertical(50),
    height: Platform.OS === 'ios' ? AppbarHeight - scaleVertical(10) : AppbarHeight - scaleVertical(15)
  },
  test2Hidden: {
    // height: Platform.OS === "ios" ? scaleVertical(45) : scaleVertical(50),
    height: AppbarHeight - scaleVertical(15),
    alignItems: 'center',
  },
  test: {
    height: Platform.OS === 'ios' ? scaleVertical(10) : scaleVertical(12),
  },
  subMenuItem: {
    width: size.width / 5,
    height: AppbarHeight + scaleVertical(10),
    left: scale(1),
  },
  subMenuItemHidden: {
    width: size.width / 5,
    fontSize: scale(5),
    left: scale(1),
    marginTop: scaleVertical(10),
  },
  subMenuText: {
    color: '#fff',
    fontSize: scaleVertical(8),
    textAlign: 'center',
    textAlignVertical: 'bottom'
  },
  bgImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  },
  bgImageHidden: {
    position: 'absolute',
    paddingLeft: scaleVertical(10),
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    height: Platform.OS === 'ios' ? AppbarHeight - scaleVertical(5) : AppbarHeight - scaleVertical(10),
    width: Platform.OS === 'ios' ? AppbarHeight - scaleVertical(5) : AppbarHeight - scaleVertical(10),
    alignSelf: 'center'
  },
  modalTransparentContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    paddingHorizontal: scaleVertical(5),
    paddingVertical: scaleVertical(20),
    alignContent: 'center'
  },
  modalBackground: {
    flex: 1,
    backgroundColor: '#ebebeb',
  },
  modalBackgroundQuiniela: {
    flex: 1,
    backgroundColor: '#394353',
  },
  modalHeader: {
    flex: 1,
    flexDirection: 'row'
  },
  modalCloseImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  },
  modalContent: {
    flex: 10,
    alignItems: 'center',
  },
  modalDontImageContent: {
    padding: scaleVertical(15),
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalDontImage: {
    height: size.width / 4,
    width: size.width / 4,
    alignSelf: 'center'
  },
  modalQuinielaImageContent: {
    padding: scaleVertical(15),
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalQuinielaImage: {
    height: size.width / 5,
    width: size.width / 5,
    alignSelf: 'center'
  },
  modalHeaderText: {
    fontSize: scaleVertical(25),
    fontFamily: 'TTPollsBoldItalic',
    textAlign: 'center'
  },
  modalInfoText: {
    fontSize: scaleVertical(20),
    fontFamily: 'Roboto-Regular',
    textAlign: 'center'
  },
  modalHeaderTextQuiniela: {
    color: '#fff',
    fontSize: scaleVertical(25),
    fontFamily: 'TTPollsBoldItalic',
    textAlign: 'center'
  },
  modalInfoTextQuiniela: {
    color: '#fff',
    fontSize: scaleVertical(15),
    paddingVertical: scaleVertical(10),
    fontFamily: 'Roboto-Regular',
    textAlign: 'center'
  },
  modalSubInfoTextQuiniela: {
    color: '#fff',
    fontSize: scaleVertical(15),
    paddingVertical: scaleVertical(10),
    fontFamily: 'Roboto-Light',
    textAlign: 'center'
  },
  modalSubInfoColorTextQuiniela: {
    color: '#fff',
    fontSize: scaleVertical(18),
    paddingVertical: scaleVertical(5),
    fontFamily: 'Roboto-Bold',
    textAlign: 'center',
  },
  modalButtonContent: {
    paddingHorizontal: scaleVertical(15),
    paddingVertical: scaleVertical(15),
  },
  modalButton: {
    backgroundColor: '#eb0029',
    alignSelf: 'center',
    justifyContent: 'center',
    height: size.width / 7,
    width: size.width - scaleVertical(80),
  },
  modalButtonText: {
    fontSize: scaleVertical(18),
    fontFamily: 'DecimaMono',
    textAlign: 'center',
    color: '#fff',
    textAlignVertical: 'center'
  },
  count: {
    fontSize: scaleVertical(12),
    fontFamily: 'Roboto-Bold',
    textAlign: 'center'
  }
}));

QuinielaModals.propTypes = {
  currentActiveGame: PropTypes.object,
  stilo: PropTypes.object,
  navigation: PropTypes.object,
};


const mapStateToProps = state => ({
  profile: state.profileData,
  currentActiveGame: state.reducerGame

});

export default connect(mapStateToProps)(QuinielaModals);
