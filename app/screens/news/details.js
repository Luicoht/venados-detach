import React from 'react';
import { View, ImageBackground, ScrollView, UIManager, LayoutAnimation } from 'react-native';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { scale, scaleVertical, size } from '../../utils/scale';


class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.navigation.state.params.new,
      isReady: false,
    };
    const options = {
      title: 'DETALLES',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }


  componentDidMount() { }

  componentWillUpdate(nextProps, nextState) {
    let animate = false;

    if (this.state.isReady !== nextState.isReady) {
      animate = true;
    }

    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }

      LayoutAnimation.easeInEaseOut();
    }
  }

  render() {
    return (
      <View style={styles.root}>
        <ScrollView
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'space-between'
          }}
        >
          <RkText style={styles.title} >{this.state.data.title}</RkText>
          <View style={{ flex: 2 }}>
            <ImageBackground style={{ flex: 1 }} source={{ uri: this.state.data.image }} />
          </View>
          <RkText style={styles.date} >{this.state.data.date}</RkText>
          <View style={{ flex: 3 }}>
            <RkText style={styles.content}>{this.state.data.content}</RkText>
            <RkButton style={styles.boton} > COMPARTIR </RkButton>
            <View style={{ height: scaleVertical(25) }} />
          </View>
        </ScrollView>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
  title: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(20),
    padding: scaleVertical(15),
  },
  date: {
    fontFamily: 'Roboto-Light',
    fontSize: scaleVertical(12),
    textAlign: 'right',
    padding: scaleVertical(10)
  },
  content: {
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(15),
    padding: scaleVertical(15)
  },
  boton: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45),
  },
}));


const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Details);
