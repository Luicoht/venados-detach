import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet } from 'react-native';
import { Button } from 'native-base';
import { FACEBOOK_UI } from '../constants';
import { encodeHTMLURL } from '../utils/facebook';

const { URL, APP_ID, sharedLinks: { shared1 } } = FACEBOOK_UI;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

/*
  https://www.facebook.com/dialog/share?app_id=966242223397117&display=popup&href=https%3A%2F%2Fdocs.expo.io%2Fversions%2Flatest%2F
*/

const image = 'https://image.ibb.co/nAFDr9/36443430-1048267648659021-8108797108368179200-n.jpg';


class sharedButton extends Component {
  onClick() {
    const encodeUri = encodeHTMLURL(shared1);

    const imageUri = encodeHTMLURL(image);

    const share = `${URL}app_id=${APP_ID}&channel_url=${encodeUri}&image=${imageUri}&locale=es_MX&mobile_iframe=true&href=${encodeUri}&description=${encodeHTMLURL('esta buena la salsa')}`;
    console.log('SHARE URI: --> ', share);

    this.props.sharedLink(share);
  }

  button() {
    return (
      <Button
        style={styles.paragraph}
        onPress={this.onClick.bind(this)}
      >
        <Text>Compartir</Text>
      </Button>
    );
  }

  render() {
    return (
      <View>
        {this.button()}
      </View>
    );
  }
}

sharedButton.propTypes = {
  sharedLink: PropTypes.func,
};

export default sharedButton;
