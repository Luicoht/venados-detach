import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as NativeBase from 'native-base';


class Text extends Component {
  render() {
    return (
      <NativeBase.Text
        style={{ ...this.props.style }}
        numberOfLines={this.props.numberOfLines}
      >
        { this.props.children }
      </NativeBase.Text>
    );
  }
}


Text.propTypes = {
  style: PropTypes.object,
  numberOfLines: PropTypes.number,
  children: PropTypes.any
};


export { Text };
