import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Field, reduxForm } from 'redux-form';

import { TextField } from 'react-native-material-textfield';
import { RkText, RkButton, RkPicker } from '../../../../node_modules/react-native-ui-kitten';
import { scale, scaleVertical, size } from './../../../utils/scale';


const fieldNombre = props => (
  <View style={styles.texInput}>
    <TextField
      textColor='#000'
      tintColor='#000'
      baseColor='#000'
      labelTextStyle={{ fontSize: scale(18), fontFamily: 'Roboto-Regular' }}
      titleTextStyle={{ fontSize: scale(18), fontFamily: 'Roboto-Regular' }}
      affixTextStyle={{ fontSize: scale(18), fontFamily: 'Roboto-Regular' }}
      errorColor='#eb0029'
      placeholder={props.ph}
      onChangeText={props.input.onChange}
      value={props.input.value}
      keyboardType={props.input.name === 'correo' ? 'email-address' : 'default'}
      autoCapitalize='none'
      secureTextEntry={!!(props.input.name === 'password' || props.input.name === 'confirmation')}
      onBlur={props.input.onBlur}
      label={props.ph}
      error={props.meta.touched &&
        props.meta.error && props.meta.error ? props.meta.error : ''}
    />
  </View>
);
const validate = (values, props) => {
  const errors = {};

  // if (!values.name) {
  //   errors.name = 'Requerido';
  // } else if (values.name.length < 4) {
  //   errors.name = 'Deben ser al menos 4 caracteres';
  // } else if (values.name.length > 30) {
  //   errors.name = 'Debe ser menor de 30 caracteres';
  // }


  // if (!values.username) {
  //   errors.username = 'Requerido';
  // } else if (values.username.length < 4) {
  //   errors.username = 'Deben ser al menos 4 caracteres';
  // } else if (values.username.length > 30) {
  //   errors.username = 'Debe ser menor de 30 caracteres';
  // }

  if (!values.email) {
    errors.email = 'Requerido';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'email invalido';
  }


  // if (!values.phone) {
  //   errors.phone = 'Requerido';
  // } else if (!/^[0-9+]{10,14}$/i.test(values.phone)) {
  //   errors.phone = 'phone invalido';
  // }

  if (!values.password) {
    errors.password = 'Requerido';
  } else if (values.password.length < 8) {
    errors.password = 'Deben ser al menos 8 caracteres';
  } else if (values.password.length > 18) {
    errors.password = 'Debe ser menor de 18 caracteres';
  } else if (values.password !== values.confirmation && values.confirmation === '') {
    errors.password = 'Debe confirmar su contraseña';
  }

  if (!values.confirmation) {
    errors.confirmation = 'Requerido';
  } else if (values.password !== values.confirmation) {
    errors.confirmation = 'No coincide con la contraseña';
  }

  return errors;
};

const SignUpForm = props => (
  <View style={styles.container} >


    <Field name='email' component={fieldNombre} ph='Correo electrónico' />
    {/* <Field name='username' component={fieldNombre} ph='Nombre de usuario' /> */}
    {/* <Field name='phone' component={fieldNombre} ph='Telefono' /> */}
    {/* <Field name='name' component={fieldNombre} ph='Nombre completo' /> */}
    {/* <Field name='firstName' component={fieldNombre} ph='Apellido paterno' /> */}
    {/* <Field name='secondName' component={fieldNombre} ph='Apellido materno' /> */}


    <Field name='password' component={fieldNombre} ph='Contraseña' />
    <Field name='confirmation' component={fieldNombre} ph='Contraseña' />

    <RkButton
      style={styles.botonLogin}
      onPress={props.handleSubmit(props.registro)}
    > CONTINUAR </RkButton>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1

  },
  texInput: {
    marginBottom: 16
  },
  linea: {
    backgroundColor: '#DCDCDC',
    height: 2
  },
  errors: {
    color: '#ed0a27'
  },
  botonLogin: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginBottom: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45)

  },
});

export default reduxForm({
  form: 'SignUpForm',
  validate
})(SignUpForm);
