import React from 'react';
import { View, ScrollView } from 'react-native';
import { RkStyleSheet, RkText, RkButton, RkTabView } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { scale, scaleVertical, size } from '../../utils/scale';
import { PitchingTop } from './pitchingTop';
import { PitchingTeam } from './pitchingTeam';
import { BattingTop } from './battingTop';
import { BattingTeam } from './battingTeam';

class Stadistics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

      battingLeaders: false,
      pitchingLeaders: false,
      batting: false,
      pitching: false,
      glosary: [
        [['AB', 'Al bate'], ['AVG', 'Promedio de bateo']],
        [['BB', 'Base por bolas'], ['CS', 'Out en intento de robo']],
        [['G', 'Juegos lanzados'], ['H', 'Hits permitidos']],
        [['H2B', 'Dobles'], ['H3B', 'Triples']],
        [['HBP', 'Golpeado por un pitcheo'], ['HR', 'Jonrones cuadrangulares']],
        [['IBB', 'Base por bolas internacional'], ['OPS', 'Porcentaje en base más slugging']],
        [['POS', 'Posición'], ['R', 'Carreras permitidas']],
        [['SAC', 'Sacrificios'], ['SB', 'Bases robadas']],
        [['SF', 'Elevados de sacrificio'], ['SLG', 'Porcentaje de slugging']],
        [['SO', 'Ponches'], ['', '']],
      ]
    };
    const options = {
      title: 'ESTADÍSTICAS',
      hidden: false
    };
    this.props.navigation.setParams({ options });
  }

  componentWillMount() { }
  componentDidMount() { }

  _getGlossarySection() {
    const glosary = this.state.glosary.map((route) => {
      const solve = route.map((item) => {
        const section = item.map((itemSection, indexSectionItem) => {
          let styleText;
          let style;
          if (indexSectionItem === 0) {
            style = { flex: 2 };
            styleText = {
              color: '#eb0029',
              fontFamily: 'Roboto-Bold',
              fontSize: scaleVertical(13)
            };
          } else {
            style = { flex: 5 };
            styleText = {
              fontFamily: 'Roboto-Regular',
              fontSize: scaleVertical(12),
              paddingLeft: scaleVertical(2)
            };
          }
          return (
            <View style={style}>
              <RkText style={styleText}>{itemSection}</RkText>
            </View>
          );
        });
        return (
          <View style={{ flexDirection: 'row', flex: 1 }}>
            {section}
          </View>
        );
      });
      const row = (
        <View>
          <View style={{ flexDirection: 'row', paddingVertical: scaleVertical(3) }}>
            {solve}
          </View>
        </View>);
      return row;
    });

    return (
      <View style={styles.glossarySection}>
        <View style={styles.jump} />
        <View style={styles.headerTable}>
          <View style={{ justifyContent: 'center', flex: 1 }}>
            <RkText style={{ fontFamily: 'Roboto-Bold', fontSize: scaleVertical(15), textAlign: 'center', color: '#fff' }}>GLOSARIO</RkText>
          </View>
        </View>
        <View style={{ paddingHorizontal: scaleVertical(15) }}>
          {glosary}
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.root}>
        <ScrollView style={styles.root}>
          <View style={{ padding: scale(15) }}>
            <RkText style={styles.headerText}>Estadísticas de Venados</RkText>
            <RkTabView rkType='standing'>
              <RkTabView.Tab title={'PITCHEO'}>
                <PitchingTeam />
              </RkTabView.Tab>
              <RkTabView.Tab title={'BATEO'}>
                <ScrollView horizontal={true}>
                  <BattingTeam />
                </ScrollView>
              </RkTabView.Tab>
            </RkTabView>
          </View>
          <View style={{ padding: scale(15) }}>
            <RkText style={styles.headerText}>Líderes</RkText>
            <RkTabView rkType='standing'>
              <RkTabView.Tab title={'PITCHEO'}>
                <PitchingTop />
              </RkTabView.Tab>
              <RkTabView.Tab title={'BATEO'}>
                <ScrollView horizontal={true}>
                  <BattingTop />
                </ScrollView>
              </RkTabView.Tab>
            </RkTabView>
          </View>
          {this._getGlossarySection()}
          <RkButton onPress={() => { this.props.navigation.navigate('CalendarMenu'); }} style={styles.boton}> CALENDARIO </RkButton>
          <View style={styles.jump} />
        </ScrollView>
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff'
  },
  headerSection: {
    flex: 1,
    paddingHorizontal: scaleVertical(15),
    paddingVertical: scaleVertical(25),
  },
  headerText: {
    fontFamily: 'TTPollsBold',
    fontSize: scaleVertical(32)
  },
  jump: {
    backgroundColor: '#fff',
    height: scaleVertical(35),
  },
  headerTable: {
    backgroundColor: '#000',
    fontFamily: 'Roboto-Bold',
    height: scaleVertical(35),
    flexDirection: 'row'
    // paddingTop: scaleVertical(40)
  },
  glossarySection: {
    flex: 1,
    padingHorinzontal: scaleVertical(15)
  },
  boton: {
    textAlign: 'center',
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45),
  },
}));

const mapStateToProps = state => ({ prop: state.prop });


const mapDispatchToProps = dispatch => ({

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Stadistics);
