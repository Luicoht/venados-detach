import React from 'react';
import axios from 'axios';
import { View, ScrollView } from 'react-native';
import { RkStyleSheet, RkText } from 'react-native-ui-kitten';
import { DoubleBounce } from 'react-native-loader';
import { scale, scaleVertical, size } from '../../utils/scale';
import { AsyncImage } from '../../components/AsyncImage';

export class BattingTop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: false,
    };
  }

  componentWillMount() { }
  componentDidMount() {
    const config = {
      method: 'get',
      url: 'https://www.lmp.mx/general/batting/1/10/avg/DESC/',
    };
    axios(config).then((response) => {
      const serverResponse = response.data.response.data;
      this.setState({ data: serverResponse });
    }).catch((error) => {
      this.setState({ data: [] });
    });
  }

  _getDataSection() {
    const PitchingLeadersSection = this.state.data.map((player, index) => {
      const bgColor = index % 2 == 0 ? '#c8eeff' : '#fff';
      const row = (
        <View style={{ flexDirection: 'row', flex: 1, backgroundColor: bgColor, paddingVertical: scale(5) }}>
          <View style={{ flexDirection: 'row', paddingHorizontal: scale(3), borderColor: '#fff', width: scale(150) }}>

            <View style={{ flex: 1, paddingHorizontal: scale(3) }}>
              <AsyncImage
                style={{
                  height: scale(45),
                  width: scale(45),
                  alignSelf: 'center',
                }}
                source={{ uri: player.img }}
                placeholderColor='transparent'
              />
            </View>
            <View style={{ flex: 4, justifyContent: 'center', paddingHorizontal: scale(3) }}>
              <RkText style={[styles.tableHeaderTwoText, { textAlign: 'center' }]}>{player.name}</RkText>
            </View>
          </View>
          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(80) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.team}</RkText>
          </View>
          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.g}</RkText>
          </View>
          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.ab}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.r}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.h}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.h2b}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.h3b}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.hr}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.rbi}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.bb}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.so}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.sb}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.cs}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.obp}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.slg}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.avg}</RkText>
          </View>

          <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
            <RkText style={styles.tableHeaderTwoText}>{player.ops}</RkText>
          </View>
        </View>
      );
      return row;
    });

    const content = (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={{ flex: 1 }}>
          {this._getHeaderTable()}
        </View>
        <View style={{ flex: 1 }}>
          {PitchingLeadersSection}
        </View>
      </View>
    );
    const solve = (
      <ScrollView horizontal={true} style={{ flex: 1 }}>
        {content}
      </ScrollView>
    );
    return solve;
  }


  _getHeaderTable() {
    return (
      <View style={styles.headerTable}>
        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(150) }}>
          <RkText style={styles.tableHeaderTwo}>JUGADOR</RkText>
        </View>
        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(80) }}>
          <RkText style={styles.tableHeaderTwo}>EQUIPO</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>G</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>AB</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>R</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>H</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>2B</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>3B</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>HR</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>RBI</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>BB</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>SO</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>SB</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>CS</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>OBP</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>SLG</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>AVG</RkText>
        </View>

        <View style={{ paddingHorizontal: scale(3), borderColor: '#fff', justifyContent: 'center', width: scale(60) }}>
          <RkText style={styles.tableHeaderTwo}>OPS</RkText>
        </View>
      </View>
    );
  }


  render() {
    if (!this.state.data) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }
    return (
      <View style={styles.root}>
        {this._getDataSection()}
      </View>
    );
  }
}

let styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff'
  },
  headerTable: {
    backgroundColor: '#000',
    fontFamily: 'Roboto-Bold',
    height: scaleVertical(35),
    flexDirection: 'row'
    // paddingTop: scaleVertical(40)
  },
  tableHeaderTwo: {
    fontFamily: 'Roboto-Bold',
    fontSize: scaleVertical(15),
    color: '#fff'
  },
  tableHeaderTwoText: {
    fontFamily: 'Roboto-Bold',
    fontSize: scaleVertical(15),
    color: '#000'
  },
}));
