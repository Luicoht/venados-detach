import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Dimensions, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import { Card } from 'native-base';
import friendList from './friendListDommie.json';
import backIcon from './../../assets/icons/dont.png';
import FriendList from './friendsList';

export default class InviteFriends extends Component {
  constructor(props) {
    super(props);

    this.state = {
      friendList: [],
    };
  }

  componentDidMount() {
    this.setState({ friendList: friendList.data });
  }

  renderFriends({ item }) {
    return (
      <FriendList
        friend={item}
      />
    );
  }

  renderHeaderContent() {
    return (
      <View style={{ flex: 0.8, flexDirection: 'row', paddingLeft: 5, top: 5, paddingRight: 10, left: 10 }}>
        <View style={{ flex: 1, alignContent: 'flex-start', zIndex: 1 }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.goBack();
            }}
          >
            <Image
              style={{ height: 30, width: 30 }}
              source={backIcon}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={{ backgroundColor: '#c9ddff', height: Dimensions.get('window').width + 150 }}>
        <View style={{ flex: 0.8, flexDirection: 'column' }}>
          { this.renderHeaderContent() }
          <View style={{ flex: 7, alignContent: 'flex-start' }}>
            <Text style={{ color: 'black', fontSize: 22, marginLeft: 20, width: Dimensions.get('window').width / 1.5, marginBottom: 15 }}>
            !Invita a tus amigos a vivir la experiencia de ser Socio Venados, y ganen puntos jutos
            </Text>
            <Card style={{ backgroundColor: 'white', width: Dimensions.get('window').width / 1.1, marginLeft: 18, top: 12 }}>
              <FlatList
                data={this.state.friendList || []}
                renderItem={this.renderFriends}
              />
            </Card>
          </View>
        </View>
      </View>
    );
  }
}

InviteFriends.propTypes = {
  navigation: PropTypes.object,
};
