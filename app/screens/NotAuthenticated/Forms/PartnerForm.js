// import React from 'react';
// import PropTypes from 'prop-types';
// import { View } from 'react-native';
// import { Field, reduxForm } from 'redux-form';
// import { RkButton, RkText } from 'react-native-ui-kitten';
// import { TextField } from 'react-native-material-textfield';

// import { scale } from './../../../utils/scale';

// const validate = values => {
//   const errors = {};
//   if (!values.code) {
//     errors.code = 'Requerido';
//   } else if (values.code.length <= 7) {
//     errors.code = 'Deben ser 8 caracteres';
//   } else if (values.code.length > 8) {
//     errors.password = 'Debe ser 8 caracteres';
//   }
//   if (!values.phone) {
//     errors.phone = 'Requerido';
//   } else if (values.phone.length < 10) {
//     errors.phone = 'Deben ser al menos 10 caracteres';
//   } else if (values.phone.length > 10) {
//     errors.password = 'Debe ser menor de 11 caracteres';
//   }
//   return errors;
// };

// const fieldNombre = props => (
//   <View style={{ }}>
//     <TextField
//       textColor='#fff'
//       tintColor='#fff'
//       baseColor='#fff'
//       labelTextStyle={{ fontSize: scale(18), fontFamily: 'Roboto-Regular' }}
//       titleTextStyle={{ fontSize: scale(18), fontFamily: 'Roboto-Regular' }}
//       affixTextStyle={{ fontSize: scale(18), fontFamily: 'Roboto-Regular' }}
//       errorColor='#eb0029'
//       placeholder={props.ph}
//       onChangeText={props.input.onChange}
//       value={props.input.value}
//       keyboardType={props.input.name === 'code' ? 'code' : 'phone'}
//       autoCapitalize='none'
//       onBlur={props.input.onBlur}
//       label={props.ph}
//       error={props.meta.touched &&
//         props.meta.error && props.meta.error ? props.meta.error : ''}
//     />
//   </View>
// );

// fieldNombre.propTypes = {
//   ph: PropTypes.string,
//   input: PropTypes.object,
//   meta: PropTypes.object,
// };

// const PartnerForm = props => (
//   <View>
//     <Field name='code' component={fieldNombre} ph='Número de socio (8 dígitos)' />
//     <Field secureTextEntry name='phone' component={fieldNombre} ph='Número de celular' />
//     <View style={{}}>
//       <RkButton rkType='buttonEmailLogin' style={{ alignSelf: 'center' }} onPress={props.handleSubmit(props.partner)} >
//         <RkText rkType='h4  DecimaMonoBold' style={{ color: '#fff' }}> CONFIRMAR </RkText>
//       </RkButton>
//     </View>
//   </View>
// );

// PartnerForm.propTypes = {
//   handleSubmit: PropTypes.func,
//   login: PropTypes.object,
// };

// export default reduxForm({ form: 'PartnerForm', validate })(PartnerForm);
import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { RkButton, RkText } from 'react-native-ui-kitten';
import { TextField } from 'react-native-material-textfield';

import { scale, scaleVertical } from './../../../utils/scale';

const validate = values => {
  const errors = {};
  if (!values.memberCode) {
    errors.memberCode = 'Requerido';
  } else if (values.memberCode.length !== 8) {
    errors.memberCode = 'Deben ser 8 caracteres';
  }

  if (!values.phone) {
    errors.phone = 'Requerido';
  } else if (values.phone.length !== 10) {
    errors.phone = 'Deben ser numero a 10 caracteres';
  }
  return errors;
};

const fieldNombre = props => (
  <View style={{ paddingHorizontal: scaleVertical(32) }}>
    <TextField
      textColor="#fff"
      tintColor="#b7b7b7"
      baseColor="#b7b7b7"
      placeholderTextColor="#ffffff"
      labelTextStyle={{ fontSize: scale(18), fontFamily: 'Roboto-Regular' }}
      titleTextStyle={{ fontSize: scale(18), fontFamily: 'Roboto-Regular' }}
      affixTextStyle={{ fontSize: scale(18), fontFamily: 'Roboto-Regular' }}
      errorColor='#eb0029'
      placeholder={props.ph}
      onChangeText={props.input.onChange}
      value={props.input.value}
      keyboardType={'numeric'}
      autoCapitalize='none'
      onBlur={props.input.onBlur}
      // label={props.ph}
      error={props.meta.touched &&
        props.meta.error && props.meta.error ? props.meta.error : ''}
    />
  </View>
);

fieldNombre.propTypes = {
  ph: PropTypes.string,
  input: PropTypes.object,
  meta: PropTypes.object,
};

const PartnerForm = (props) => {
  const SignInPartner = (
    <View>
      <Field name='memberCode' component={fieldNombre} ph='Número de socio (8 dígitos)' />
      <Field name='phone' component={fieldNombre} ph='Número de celular' />
      <View style={{ paddingVertical: scale(45) }}>
        <RkButton rkType='buttonEmailLogin' style={{ alignSelf: 'center', marginTop: -25 }} onPress={props.handleSubmit(props.deerSponsor)} >
          <RkText rkType='h4  DecimaMonoBold' style={{ color: '#fff' }}> CONFIRMACIÓN </RkText>
        </RkButton>
      </View>
    </View>
  );

  const ConfirmPartner = (
    <View>
      <Field name='smsCode' component={fieldNombre} ph='Código SMS' />
      <View style={{ paddingVertical: scale(45) }}>
        <RkButton rkType='buttonEmailLogin' style={{ alignSelf: 'center', marginTop: -25 }} onPress={props.handleSubmit(props.deerSponsor)} >
          <RkText rkType='h4  DecimaMonoBold' style={{ color: '#fff' }}> CONFIRMACIÓN SMS </RkText>
        </RkButton>
      </View>
    </View>
  );

  return (
    <View>
      { props.changeForm ? ConfirmPartner : SignInPartner }
    </View>
  );
};

PartnerForm.propTypes = {
  handleSubmit: PropTypes.func,
  deerSponsor: PropTypes.func,
  changeForm: PropTypes.bool,
};

export default reduxForm({ form: 'PartnerForm', validate })(PartnerForm);
