import React from "react";
import { View } from "react-native";
import {
  RkStyleSheet
} from "react-native-ui-kitten";

export class Loading extends React.Component {
  static navigationOptions = {
    title: "Loading".toUpperCase(),
    hidden: false
  };

  constructor(props) {
    super(props);
    this.state = {
      year: 2017,
      month: 8
    };
  }

  render() {
    return (
      <View style={styles.screen}>

      </View>
    );
  }
}

let styles = RkStyleSheet.create(theme => ({
  screen: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: theme.colors.screen.base
  }
}));
