import _ from 'lodash';
import Dashboard from '../../screens/dashboard/dashboard';
import Rules from '../../screens/rules/rules';
import RegisterTicket from '../../screens/registerTicket/registerTicket';
import GetPoints from '../../screens/info/getPoints';
import LineUp from '../../screens/lineUp/lineUp';
import Player from '../../screens/player/player';
import DeerChallenge from '../../screens/deerChallenge/deerChallenge';
import MyGames from '../../screens/myGames/myGames';
import Play from '../../screens/play/play';
import GameDetails from '../../screens/gameDetails/gameDetails';
import DetailsOfPreviousGames from '../../screens/myGames/detailsOfPreviousGames';
import Stadistics from '../../screens/statistics/statistics';
import About from '../../screens/about/about';
import Points from '../../screens/points/points';
import AboutPoints from '../../screens/points/aboutPoints';
import CalendarView from '../../screens/calendar/calendar';
import Standing from '../../screens/standing/standing';
import Community from '../../screens/community/community';
import MyFriends from '../../screens/community/myFriends';
import News from '../../screens/news/news';
import Details from '../../screens/news/details';
import CheckIn from '../../screens/checkIn/checkIn';
import Profile from '../../screens/profile/profile';
import Settings from '../../screens/other/settings';
import ComingSoon from '../../screens/other/comingSoon';
import MyHistory from '../../screens/history/history';
import BeAMember from '../../screens/beAMember/beAMember';
import InviteAFriend from '../../screens/inviteAFriend/inviteAFriend';
import Help from '../../screens/help/help';
import Subcribe from '../../screens/subscribe/subscribe';
import Store from '../../screens/store/store';
import ArticleDetails from '../../screens/store/articleDetails';
import QuinielaWinner from '../../screens/winnerScreens/quinielaWinner';
import DeerChallengeWinner from '../../screens/winnerScreens/DeerChallengeWinner';
import Partner from '../../screens/NotAuthenticated/Partner';
import Photo from '../../screens/photos';
import FriendsList from '../../screens/friendList';

export const MainRoutes = [
  {
    id: 'PlayMenu',
    title: 'Jugar',
    screen: Play,
    children: [
      {
        id: 'RulesMenu',
        title: 'Reglas',
        screen: Rules,
        children: []
      },
      {
        id: 'QuinielaWinnerMenu',
        title: 'Ganaste',
        screen: QuinielaWinner,
        children: []
      },
      {
        id: 'DeerChallengeWinnerMenu',
        title: 'Ganaste',
        screen: DeerChallengeWinner,
        children: []
      },
      {
        id: 'RegisterTicketMenu',
        title: 'Registrar boleto',
        screen: RegisterTicket,
        children: []
      },
      {
        id: 'GetPointsMenu',
        title: 'Ganar puntos',
        screen: GetPoints,
        children: []
      },
      {
        id: 'LineUpMenu',
        title: 'Ganar puntos',
        screen: LineUp,
        children: []
      },
      {
        id: 'PlayerMenu',
        title: 'Jugador',
        screen: Player,
        children: []
      },
      {
        id: 'DeerChallengeMenu',
        title: 'Reto Venados',
        screen: DeerChallenge,
        children: []
      },
      {
        id: 'MyGamesMenu',
        title: 'Ganar puntos',
        screen: MyGames,
        children: []
      },
      {
        id: 'GameDetailsMenu',
        title: 'Detalles de juego',
        screen: GameDetails,
        children: []
      },
      {
        id: 'detailsOfPreviousGamesMenu',
        title: 'Detalles',
        screen: DetailsOfPreviousGames,
        children: []
      },
      {
        id: 'DashboardMenu',
        title: 'Inicio',
        screen: Dashboard,
        children: []
      },
      {
        id: 'StadisticsMenu',
        title: 'Play',
        screen: Stadistics,
        children: []
      },
      {
        id: 'AboutMenu',
        title: 'Venados',
        screen: About,
        children: []
      }
    ]
  },
  {
    id: 'PointsMenu',
    title: 'Puntos',
    screen: Points,
    children: [
      {
        id: 'AboutPoints',
        title: 'Acerca de puntos',
        screen: AboutPoints,
        children: []
      }
    ]
  },
  {
    id: 'CalendarMenu',
    title: 'Calendario',
    screen: CalendarView,
    children: []
  },
  {
    id: 'StandingMenu',
    title: 'Standing',
    screen: Standing,
    children: []
  },
  {
    id: 'CommunityMenu',
    title: 'Comunidad',
    // screen: ComingSoon,
    screen: Community,
    children: [
      {
        id: 'FriendsMenu',
        title: 'Amigos',
        screen: MyFriends,
        children: []
      },
    ]
  },
  {
    id: 'NewsMenu',
    title: 'Noticias',
    screen: News,
    children: [
      {
        id: 'DetailsMenu',
        title: 'Detalles',
        screen: Details,
        children: []
      }
    ]
  },
  {
    id: 'CheckInMenu',
    title: 'CheckIn',
    screen: CheckIn,
    children: []
  },
  {
    id: 'Partner',
    title: 'Socio Venados',
    screen: Partner,
    children: []
  },
  // {
  //   id: 'friendsComunnity',
  //   title: 'Invitación a amigos',
  //   screen: FriendsList,
  //   children: []
  // },
  // {
  //   id: 'Photos',
  //   title: 'Hacer Foto',
  //   screen: Photo,
  //   children: []
  // },
  {
    id: 'ProfileMenu',
    title: 'Perfil',
    screen: Profile,
    children: [
      {
        id: 'ComingSoonMenu',
        title: 'Próximamente',
        screen: ComingSoon,
        children: []
      },
      {
        id: 'OtherMenu',
        title: 'Configuraciones',
        screen: Settings,
        children: []
      },
      {
        id: 'HistoryMenu',
        title: 'Historial',
        screen: MyHistory,
        children: []
      },
      {
        id: 'BeAMemberMenu',
        title: 'Hacerte socio',
        screen: BeAMember,
        children: []
      },
      // {
      //   id: 'InviteAFriendMenu',
      //   title: 'Invitar',
      //   screen: InviteAFriend,
      //   children: []
      // },
      {
        id: 'HelpMenu',
        title: 'Ayuda',
        screen: Help,
        children: []
      },
      {
        id: 'SubcribeMenu',
        title: 'Suscribirse',
        screen: Subcribe,
        children: []
      }
    ]
  },
  {
    id: 'StoreMenu',
    title: 'Tienda',
    screen: ComingSoon,
    // screen: Store,
    children: [
      {
        id: 'ArticleDetailsMenu',
        title: 'Tienda',
        screen: ArticleDetails,
        children: []
      }
    ]
  },
  {
    id: 'LogOutMenu',
    title: 'Cerrar sessión',
    screen: '',
    children: []
  },
];

const menuRoutes = _.cloneDeep(MainRoutes);
menuRoutes.unshift(
  {
    id: 'DashboardMenu',
    title: 'Inicio',
    screen: Dashboard,
    children: []
  }
);
export const MenuRoutes = menuRoutes;
