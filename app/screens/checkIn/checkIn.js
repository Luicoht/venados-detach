import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, UIManager, LayoutAnimation, AsyncStorage } from 'react-native';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { DoubleBounce } from 'react-native-loader';
import { ShareDialog } from 'react-native-fbsdk';
import { Asset, LinearGradient, Permissions, Location } from 'expo';
import { connect } from 'react-redux';
import { scale, scaleVertical, size } from '../../utils/scale';
import check from '../../assets/icons/check-4.png';
import { findCheckinPost } from '../../utils/facebook';
import CONSTANTS from '../../Store/CONSTANTS';
import ImagePicker from '../../components/imagePicker';
import { checkIn } from '../../Store/Services/Socket';

class CheckIn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isReady: false,
      userLocation: null,
      errorMessage: null,
      token: null,
    };

    const options = {
      title: 'CHECK-IN',
      hidden: false
    };

    this.props.navigation.setParams({ options });
  }

  componentDidMount() {
    this.cacheResourcesAsync();
    this.getUserPosition();

    this.getToken(token => {
      this.setState({ token });
    });
  }

  componentWillUpdate(nextProps, nextState) {
    let animate = false;

    if (this.state.isReady !== nextState.isReady) {
      animate = true;
    }

    if (animate) {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      LayoutAnimation.easeInEaseOut();
    }
  }


  // -----------------------
  // ------ methods --------
  // -----------------------
  onCheckInPressed() {
    this.startListeningShare();

    const shareLinkContent = {
      contentType: 'link',
      contentUrl: 'http://i69.servimg.com/u/f69/12/99/63/34/venado10.jpg',
      // photos: [{ imageUrl: 'http://i69.servimg.com/u/f69/12/99/63/34/venado10.jpg' }],
      // quote: 'Comparte tu ubicacion en el estadio y gana puntos',
      contentTitle: 'venados app',
      // placeId: '245893465454475',
      // contentUrl: 'https://play.google.com/store/apps/details?id=deezer.android.app',
    };

    ShareDialog.canShow(shareLinkContent).then(async (canShow) => {
      if (canShow) {
        await ShareDialog.show(shareLinkContent);
      }
    });
  }

  onCheckInWithPhotoPressed({ imageUri }) {
    this.startListeningShare();

    const shareLinkContent = {
      contentType: 'link',
      contentUrl: imageUri,
      // quote: 'Comparte tu ubicacion en el estadio y gana puntos',
      contentTitle: 'venados app',
    };

    ShareDialog.canShow(shareLinkContent).then(async (canShow) => {
      if (canShow) {
        await ShareDialog.show(shareLinkContent);
      }
    });
  }

  async getToken(cb) {
    try {
      const value = await AsyncStorage.getItem(CONSTANTS.SAVE_FBTK);
      if (value !== null) {
        // We have data!!
        cb(value);
      }
    } catch (error) {
      cb('');
    }
  }

  async getUserPosition() {
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    const options = { enableHighAccuracy: true };

    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    Location.getCurrentPositionAsync(options).then((location) => {
      if (location.coords.accuracy < 70) {
        const userLocation = {
          latitude: location.coords.latitude,
          longitude: location.coords.longitude,
          accuracy: location.coords.accuracy
        };

        this.setState({ userLocation });
      }
    });
  }

  getDistance(firstLocation, secondLocation) {
    const ONE_DEGREE_IN_KILOMETERS = 111.325;
    const width = Math.abs(firstLocation.longitude - secondLocation.longitude);
    const height = Math.abs(firstLocation.latitude - secondLocation.latitude);
    const rawDistance = Math.hypot(width, height);
    const kilometers = rawDistance * ONE_DEGREE_IN_KILOMETERS;

    return parseFloat(kilometers.toFixed(3));
  }

  startListeningShare() {
    const currentDate = new Date();
    this.openingDate = new Date(currentDate.getTime() + (currentDate.getTimezoneOffset() * 60 * 1000));
    this.sharePostFound = false;
    const limitInMinutes = 1000 * 60 * 10; // ten minutes

    this.intervalID = setInterval(async () => {
      const params = {
        facebookToken: this.state.token,
        openingDate: this.openingDate,
      };

      findCheckinPost(params, (error, postToVerify) => {
        clearInterval(this.intervalID);
        if (!error) {
          this.sharePostFound = true;
          const validPlaceIDs = ['663558853656423', '201477687019242', '1266312313411066', '157168374303021']; // ids del estadio de los venados
          const foundPlaceID = validPlaceIDs.find((validID) => validID === postToVerify.place.id);
          clearInterval(this.intervalID);
          if (foundPlaceID) {
            const selectedPlaceLocation = {
              latitude: postToVerify.place.location.latitude,
              longitude: postToVerify.place.location.longitude
            };

            const distanceFromStadio = this.getDistance(selectedPlaceLocation, this.state.userLocation);

            if (distanceFromStadio < 0.2) {
              console.log('dar puntos a usuario por checking');
              checkIn({
                withPhoto: false,
              });
            } else {
              console.log('No se encuentra fisicamente en el estadio');
            }
          } else {
            console.log('Checking con ubicacion incorrecta');
          }
        }
      });
    }, 5000);

    setTimeout(() => {
      if (!this.sharePostFound) {
        clearInterval(this.intervalID);
      }
    }, limitInMinutes);
  }

  async cacheResourcesAsync() {
    const images = [check];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });

    return Promise.all(cacheImages);
  }


  // ------------------------------
  // ------- render methods --------
  // ------------------------------
  render() {
    if (!this.state.isReady) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }

    return (
      <View style={styles.root}>
        <LinearGradient style={styles.root} colors={['#3d4959', '#33363c', '#2a3139', '#292c32']}>
          <View style={styles.content}>
            <View style={{ flex: 2, justifyContent: 'center' }}>
              <Image style={styles.image} source={check} />
            </View>

            <View style={{ flex: 3, paddingHorizontal: scaleVertical(45) }}>
              <RkText style={styles.header}>¡Ganemos juntos!</RkText>
              <RkText style={styles.info}>Para poder jugar es importante que estés presente en nuestro estadio.</RkText>

              <RkButton style={styles.botonLogin} onPress={this.onCheckInPressed.bind(this)}>
                HACER CHECK-IN
              </RkButton>

              <ImagePicker
                onImageUpload={this.onCheckInWithPhotoPressed.bind(this)}
                name='checkin'
              >
                <View style={[styles.botonLogin, { justifyContent: 'center', fontFamily: 'TTPollsBold', }]}>
                  <RkText style={[styles.header, { fontSize: 18 }]}>
                    CHECK-IN CON FOTO
                  </RkText>
                </View>
              </ImagePicker>
            </View>
          </View>
        </LinearGradient>
      </View>
    );
  }
}


const styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
  },
  header: {
    color: 'white',
    fontFamily: 'TTPollsBold',
    fontSize: scale(30),
    textAlign: 'center'
  },
  info: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: scale(20),
    textAlign: 'center'
  },
  input: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: scale(20),
    fontAlign: 'center'
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
    alignSelf: 'center'
  },
  botonLogin: {
    backgroundColor: '#ed0a27',
    marginTop: scaleVertical(30),
    borderRadius: scale(5),
    alignSelf: 'center',
    width: size.width - scaleVertical(90),
    height: scaleVertical(45),
  },
  content: {
    flex: 1,
    justifyContent: 'space-around',
    paddingVertical: scale(20)
  }
}));


CheckIn.propTypes = {
  navigation: PropTypes.object
};


const mapStateToProps = state => ({
  prop: state.prop
});


export default connect(mapStateToProps)(CheckIn);

