import React from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { RkStyleSheet, RkText, RkButton } from 'react-native-ui-kitten';
import { scale, scaleVertical, size } from '../../utils/scale';
import { authentication, baseDeDatos } from '../../Store/Services/Firebase';
import { getWalletPoints } from '../../Store/Services/Socket';
import { ScorePlayer } from './scorePlayer';
import InviteAFriend from './inviteAFriend';
import { FriendsCard } from './friendsCard';
import { FeaturedPlayer } from './featuredPlayer';
import { ProfileImage } from './profileImage';


class MyFriends extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isReady: false,
      user: '',
      wallet: '',
    };
  }

  componentWillMount() {
    getWalletPoints()
      .then((wallet) => {
        this.setState({ wallet });
      })
      .catch((err) => { console.log('error', err); });

    baseDeDatos.ref('/profiles').orderByChild('email').equalTo(authentication.currentUser.providerData[0].email).once('value')
      .then(snapshot => {
        this.setState({ user: Object.values(snapshot.val())[0] });
      });
  }

  _getButtonsView() {
    const buttonsView = (
      <View style={styles.btnRow}>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkButton rkType='clear' style={styles.btnFriends}>
            <RkText style={{ color: '#002157', textVerticalAlign: 'center' }}>AMIGOS</RkText>
          </RkButton>
        </View>

        <View style={{ flex: 1, justifyContent: 'center' }}>
          <RkButton rkType='clear' onPress={() => { this.props.navigation.navigate('CommunityMenu'); }} style={styles.btnCommunity}>
            <RkText style={{ color: '#fff', textVerticalAlign: 'center' }}>COMUNIDAD</RkText>
          </RkButton>
        </View>
      </View>
    );

    return buttonsView;
  }

  _getProfileScoreView() {
    const score = (
      <View style={styles.infoContent}>
        <RkText style={styles.userName}> {this.state.user.displayName} </RkText>
        <View style={styles.divider} />
        <RkText style={styles.userScore}>SCORE:  {this.state.wallet.points} </RkText>
        <View style={styles.divider} />
      </View>
    );
    return score;
  }

  render() {
    return (
      <ScrollView style={styles.root}>
        { this._getButtonsView() }
        <ProfileImage />
        <ScorePlayer complete color={'#00a3e3'} />
        <FriendsCard />
        <InviteAFriend />
        <FeaturedPlayer backgroundColor={'#00a3e3'} />
      </ScrollView>
    );
  }
}


const styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#00a3e3',
  },
  btnRow: {
    flexDirection: 'row',
    paddingTop: scaleVertical(15),
    paddingBottom: scaleVertical(20),
  },
  userImage: {
    paddingBottom: scaleVertical(30),
  },
  btnFriends: {
    backgroundColor: '#c8eeff',
    width: (size.width / 2) - scale(45),
    fontSize: scale(40),
    height: scaleVertical(40),
    alignSelf: 'center',
    marginLeft: scale(15),
    justifyContent: 'center'
  },
  btnCommunity: {
    backgroundColor: '#eb0029',
    width: (size.width / 2) - scale(45),
    fontSize: scale(40),
    height: scaleVertical(40),
    alignSelf: 'center',
    marginRight: scale(15),
    justifyContent: 'center'
  },
  imageBackground: {
    width: size.width / 2.3,
    height: size.width / 2.3,
    alignSelf: 'center'
  },
  whiteCircle: {
    width: size.width / 2.3,
    height: size.width / 2.3,
    borderRadius: size.width / 4.6,
    alignSelf: 'center',
    overflow: 'hidden',
  },
  redCircle: {
    width: size.width / 2.1,
    height: size.width / 2.1,
    borderRadius: size.width / 4.2,
    backgroundColor: '#00a3e3',
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  imageCircle: {
    width: size.width / 2,
    height: size.width / 2,
    borderRadius: size.width / 4,
    backgroundColor: 'white',
    alignSelf: 'center',
    overflow: 'hidden',
    justifyContent: 'center'
  },
  infoContent: {
    marginLeft: scale(15),
    marginRight: scale(15),
  },
  divider: {
    backgroundColor: 'white',
    width: size.width - scale(30),
    height: scale(2),
  },
  dividerBlack: {
    backgroundColor: '#d9d9d9',
    marginLeft: scale(15),
    marginRight: scale(15),
    height: scale(2),
  },
  userName: {
    color: '#fff',
    alignSelf: 'center',
    fontFamily: 'TTPollsBold',
    fontSize: scale(30),
    textAlign: 'center'
  },
  userScore: {
    color: '#fff',
    alignSelf: 'center',
    fontFamily: 'Roboto-Bold',
    fontSize: scale(15),
    marginTop: scale(15),
    marginBottom: scale(15),
  },
  cardHeaderText: {
    color: '#303191',
    fontSize: scale(30),
    marginTop: scale(20),
    marginLeft: scale(15),
    fontFamily: 'TTPollsBold',
  },
  closeImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    margin: scale(4)
  },
  trasnparentContent: {
    flex: 1,
    backgroundColor: 'transparent',
    paddingHorizontal: scaleVertical(5),
    paddingVertical: scaleVertical(20),
    alignContent: 'center',
    justifyContent: 'center'
  },
  modalContent: {
    backgroundColor: '#394353',
    height: size.width * 1.25
  },
  jumpHeaderModal: {
    flex: 5,
    backgroundColor: 'transparent'
  },
  contentModal: {
    flex: 1,
    paddingHorizontal: scaleVertical(15)
  },
  textField: {
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontSize: scale(20),
    fontAlign: 'center'
  },
  modalFooter: {
    flex: 1,
    paddingHorizontal: scaleVertical(15),
    paddingVertical: scaleVertical(20),
  },
  buttonSend: {
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: '#eb0029',
    width: size.widthMargin - scaleVertical(60)
  },
  textSend: {
    textAlign: 'center',
    textVerticalAlign: 'center',
    color: '#fff',
    fontFamily: 'DecimaMono-Bold'
  }
}));


MyFriends.navigationOptions = {
  title: 'Comunidad'.toUpperCase(),
  hidden: false
};


MyFriends.propTypes = {
  navigation: PropTypes.object,
};


const mapStateToProps = state => ({
  prop: state.prop
});


export default connect(mapStateToProps)(MyFriends);
