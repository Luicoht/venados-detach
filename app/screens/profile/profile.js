import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, ImageBackground, TouchableOpacity, ScrollView, Image } from 'react-native';
import { RkStyleSheet, RkText } from 'react-native-ui-kitten';
import { Asset } from 'expo';
import { DoubleBounce } from 'react-native-loader';
import { connect } from 'react-redux';
import formatter from '../../utils/formatter';
import { scale, scaleVertical, size } from '../../utils/scale';
// import FacebookModal from '../../components/modalShare';
// import FacebookShare from '../../components/sharedButton';
import { getFriends } from '../../utils/facebook';
import { authentication, baseDeDatos } from '../../Store/Services/Firebase';
import configuracion4PNG from '../../assets/icons/configuracion-4.png';
import socio3PNG from '../../assets/icons/socio-3.png';
import historial4PNG from '../../assets/icons/historial-4.png';
import friend3PNG from '../../assets/icons/friend-3.png';
import favorito1PNG from '../../assets/icons/favorito1-01.png';
import dont4PNG from '../../assets/icons/dont-4.png';
import addPNG from '../../assets/icons/add.png';
import bonusPNG from '../../assets/icons/bonus-3.png';


class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      uri: '',
      picture: '',
      modalVisible: false,
      isReady: false,
      isUserReady: false,
      user: '',
      links: this._getLinks(),
    };
    const options = {
      title: 'PERFIL',
      hidden: false
    };
    this.props.navigation.setParams({ options });
    console.log(this.props.profile);
  }

  componentDidMount() {
    this._cacheResourcesAsync();
    const providers = authentication.currentUser.providerData;

    baseDeDatos.ref('/profiles').orderByChild('email').equalTo(`${providers ? providers[0].email : ''}`).once('value')
      .then(snapshot => {
        const solve = Object.values(snapshot.val())[0];

        this.setState({ user: solve });
        this.setState({ isUserReady: true });
      });

    if (this.props.usuario.token) {
      getFriends(this.props.usuario.token).then((friends) => {
        const { picture } = friends;
        if (picture) {
          fetch(picture.data.url).then(response => { // eslint-disable-line
            response.blob().then(blob => {
              const reader = new FileReader(); // eslint-disable-line
              reader.readAsDataURL(blob);
              reader.onloadend = () => {
                this.setState({ picture: reader.result });
              };
            });
          });
        }
      }).catch(err => console.log(err));
    }
  }


  getImageURL() {
    const { picture, user } = this.state;
    const { profile } = this.props;

    if (profile.data && profile.data.avatarURL) {
      return profile.data.avatarURL;
    }

    return picture || user.photoURL;
  }

  _getImageProfile() {
    const { user } = this.state;
    const img = 'https://d500.epimg.net/cincodias/imagenes/2016/07/04/lifestyle/1467646262_522853_1467646344_noticia_normal.jpg';
    return (user && user.photoURL) ? user.photoURL : img;
  }

  _getLinks() {
    const { profile } = this.props;
    if (profile && profile.data && profile.data.userType === 6) {
      return [{
        name: 'Configuración de la app',
        icon: configuracion4PNG,
        link: 'OtherMenu'
      },
      {
        name: 'Historial de cuenta',
        icon: historial4PNG,
        link: 'HistoryMenu'
      },
      {
        name: 'Invitar a un amigo',
        icon: friend3PNG,
        link: 'ComingSoonMenu'
        // link: 'InviteAFriendMenu'
      },
      {
        name: 'Ayuda',
        icon: favorito1PNG,
        link: 'HelpMenu'
      },
      {
        name: 'Cerrar sesión',
        icon: dont4PNG,
        link: 'sigOut'
      }];
    }
    return [{
      name: 'Configuración de la app',
      icon: configuracion4PNG,
      link: 'OtherMenu'
    },
    {
      name: '¿Como hacerte socio?',
      icon: bonusPNG,
      // link: 'ComingSoonMenu'
      link: 'BeAMemberMenu'
    },
    {
      name: 'Hacerme socio',
      icon: socio3PNG,
      // link: 'ComingSoonMenu'
      link: 'Partner'
    },
    {
      name: 'Invitar a un amigo',
      icon: friend3PNG,
      link: 'ComingSoonMenu'
      // link: 'InviteAFriendMenu'
    },
    {
      name: 'Ayuda',
      icon: favorito1PNG,
      link: 'HelpMenu'
    },
    {
      name: 'Cerrar sesión',
      icon: dont4PNG,
      link: 'sigOut'
    }];
  }

  async _cacheResourcesAsync() {
    const images = [
      addPNG,
      configuracion4PNG,
      socio3PNG,
      historial4PNG,
      friend3PNG,
      favorito1PNG,
      dont4PNG,
    ];
    const cacheImages = images.map(image => Asset.fromModule(image).downloadAsync());
    this.setState({ isReady: true });
    return Promise.all(cacheImages);
  }

  _goTo(route) {
    if (route.link === 'sigOut') {
      authentication.signOut();
    }

    this.props.navigation.navigate(route.link);
  }

  _getProfileSecction() {
    const { profile } = this.props;
    if (profile.data) {
      if (profile.data.userType === 6) {
        return (<ImageBackground style={styles.bgImage} source={{ uri: this.getImageURL() }}>
          <View style={styles.profileSectionName}>
            <View style={styles.div}>
              <RkText style={styles.textName}>
                {profile.data.name}
              </RkText>
              <View style={styles.transparentInfo}>
                <RkText style={styles.textStatus}>SOCIO VENADOS {profile.data.deerMemberCode}</RkText>
              </View>
            </View>
          </View>
          <RkText style={styles.textBg} >Mi perfil </RkText>
        </ImageBackground>);
      }
      return (<ImageBackground style={styles.bgImage} source={{ uri: this.getImageURL() }}>
        <View style={styles.profileSectionNameRed}>
          <View style={styles.div}>
            <RkText style={styles.textName}>
              {profile.data.name}
            </RkText>

            <View style={styles.transparentInfo}>
              <RkText style={styles.textStatus}>SER SOCIO VENADOS</RkText>
            </View>
          </View>
        </View>
        <RkText style={styles.textBg} >Mi perfil </RkText>
      </ImageBackground>);
    }

    return null;
  }

  _getProfileInfo() {
    const { profile } = this.props;

    if (profile.data) {
      return (
        <View style={styles.profileInfo}>
          <View style={styles.items}>
            <View style={styles.item}>
              <View style={styles.itemLeft}>
                <RkText style={styles.itemLeftTXT}>Teléfono</RkText>
              </View>
              <View style={styles.itemRight}>
                <RkText style={styles.itemRightTXT}>
                  {formatter.applyMask('(xxx) xxx xxxx', profile.data.phone) || ' -  -  -  -  -'}
                </RkText>
              </View>
            </View>

            <View style={styles.item}>
              <View style={styles.itemLeft}>
                <RkText style={styles.itemLeftTXT}>Correo</RkText>
              </View>
              <View style={styles.itemRight}>
                <RkText style={styles.itemRightTXT}>
                  {profile.data.email}
                </RkText>
              </View>
            </View>

            <View style={styles.item}>
              <View style={styles.itemLeft}>
                <RkText style={styles.itemLeftTXT}>Cumpleaños</RkText>
              </View>
              <View style={styles.itemRight}>
                <RkText style={styles.itemRightTXT}>
                  {profile.data.birthday || '--/--/--'}
                </RkText>
              </View>
            </View>
          </View>
        </View>
      );
    }

    return null;
  }

  _getProfilePreferencesSecction() {
    const menu = this.state.links.map((route, index) => (
      <TouchableOpacity key={index} style={styles.panelMenu} activeOpacity={0} onPress={() => this._goTo(route)} >
        <View>
          <View style={styles.linea} />
          <View style={styles.itemsRow}>
            <View style={styles.leftItem}>
              <RkText style={styles.routeName}>
                {route.name}
              </RkText>
            </View>
            <View style={styles.rigthItem}>
              <Image style={styles.iconList} source={route.icon} />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    ));

    return menu;
  }

  render() {
    const ProfileSecction = this._getProfileSecction();
    const ProfileInfoSecction = this._getProfileInfo();
    const ProfilePreferencesSecction = this._getProfilePreferencesSecction();

    if (!this.state.isReady || !this.props.profile) {
      return (
        <View style={{ flex: 1, backgroundColor: '#363636', justifyContent: 'center', alignItems: 'center' }}>
          <DoubleBounce size={size.width / 3} color='#eb0029' />
        </View>
      );
    }

    return (
      <ScrollView style={styles.root}>
        <View style={styles.root}>
          {ProfileSecction}
          {ProfileInfoSecction}
          {ProfilePreferencesSecction}
          {/* <FacebookShare
            sharedLink={(uri) => {
              if (uri) {
                this.setState({ uri, modalVisible: true });
              }
            }}
          /> */}
        </View>
        {/*         <FacebookModal
          uri={this.state.uri}
          modalVisible={this.state.modalVisible}
          onCloseModal={() => this.setState({ modalVisible: false })}
        /> */}
      </ScrollView>
    );
  }
}


const styles = RkStyleSheet.create(() => ({
  root: {
    flex: 1,
    backgroundColor: '#fff'
  },
  textName: {
    color: 'white',
    fontFamily: 'TTPollsBold',
    fontSize: scale(25),
  },
  textStatus: {
    color: 'white',
    fontFamily: 'Roboto-Bold',
    fontSize: scale(20),
  },
  textBg: {
    color: 'white',
    fontFamily: 'TTPollsBold',
    fontSize: scale(30),
    top: size.height / 3.2,
    left: scale(20)
  },
  bgImage: {
    height: size.height / 2,
    width: size.width,
    flexDirection: 'column-reverse',
  },
  div: {
    flex: 1,
    justifyContent: 'space-around',
    left: scale(20)
  },
  profileSectionName: {
    height: size.height / 9,
    backgroundColor: 'rgba(0,0,0,0.6)',
    paddingTop: scale(10),
    paddingBottom: 10
  },

  profileSectionNameRed: {
    height: size.height / 9,
    backgroundColor: 'rgba(235,0,41,0.6)',
    paddingTop: scale(10),
    paddingBottom: 10
  },



  profileInfo: {
    backgroundColor: 'white',
    paddingVertical: 10,
    paddingLeft: 5
  },
  iconAdd: {
    width: scaleVertical(26),
    height: scaleVertical(26),
    left: size.width / 2.8
  },
  iconList: {
    width: scaleVertical(26),
    height: scaleVertical(26),
    alignSelf: 'center'
  },
  transparentInfo: {
    flexDirection: 'row',
    alignContent: 'space-around',
  },
  items: {
    flex: 1
  },
  item: {
    flexDirection: 'row',
    paddingTop: scale(5),
    paddingBottom: scale(5),
  },
  itemLeft: {
    flex: 2,
  },
  itemLeftTXT: {
    color: '#eb0029',
    fontFamily: 'Roboto-Bold',
    fontSize: scaleVertical(16)
  },
  itemRightTXT: {
    color: '#363636',
    fontFamily: 'Roboto-Regular',
    fontSize: scaleVertical(13)
  },
  itemRight: {
    flex: 3,
    paddingLeft: scale(10),
  },
  infoContent: {
    flex: 1,
    marginTop: scale(25),
    marginBottom: scale(20),
    marginLeft: scale(10),
    marginRight: scale(10),
  },
  linea: {
    backgroundColor: '#b7b7b7',
    height: scaleVertical(1),
  },
  itemsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: scaleVertical(5),
    paddingTop: scaleVertical(5),
  },
  routeName: {
    fontFamily: 'Roboto-Regular'
  },
  leftItem: {
    flex: 3
  },
  rigthItem: {
    flex: 1
  },
  panelMenu: {
    paddingHorizontal: scaleVertical(10),
    backgroundColor: 'white'
  }
}));



Profile.propTypes = {
  navigation: PropTypes.object,
  usuario: PropTypes.object,
  profile: PropTypes.object,
};


const mapStateToProps = state => ({
  prop: state.prop,
  usuario: state.reducerSesion,
  profile: state.profileData
});


export default connect(mapStateToProps)(Profile);
