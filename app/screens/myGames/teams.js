
export const teams = [
    {
        name: 'Mayos',
        place: 'Mexicali',
        logo: 'https://cdn.lmp.mx/indexlmp/100x100-mayos.png',
    },
    {
        name: 'Aguilas',
        place: 'Mexicali',
        logo: 'https://cdn.lmp.mx/indexlmp/100x100-aguilas.png',
    },
    {
        name: 'Naranjeros',
        place: 'XXX',
        logo: 'https://cdn.lmp.mx/indexlmp/100x100-naranjeros.png',
    },
    {
        name: 'Yaquis',
        place: 'XXX',
        logo: 'https://cdn.lmp.mx/indexlmp/100x100-yaquis.png',
    },
    {
        name: 'Cañeros',
        place: 'Los Mochis',
        logo: 'https://cdn.lmp.mx/indexlmp/100x100-caneros.png',
    },
    {
        name: 'Tomateros',
        place: 'Culiacán',
        logo: 'https://cdn.lmp.mx/indexlmp/100x100-tomateros.png',
    },
    {
        name: 'Charros',
        place: 'XXX',
        logo: 'https://cdn.lmp.mx/indexlmp/100x100-charros.png',
    },
    {
        name: 'Venados',
        place: 'Mazatlán',
        logo: 'https://cdn.lmp.mx/indexlmp/100x100-venados.png',
    }
];
