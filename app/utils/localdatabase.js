import { AsyncStorage } from 'react-native';
import _ from 'lodash';


export const setItem = async (key, value) => {
  try {
    return await AsyncStorage.setItem(key, JSON.stringify(value));
  } catch (error) {
    // console.error('AsyncStorage#setItem error: ' + error.message);
  }
};


export const getItem = async (key) => {
  // console.log('OBTENER LLAVE: ', key);
  return await AsyncStorage.getItem(key)
    .then((result) => {
      let resultData = result;
      if (result) {
        try {
          resultData = JSON.parse(result);
        } catch (e) {
          // console.error('AsyncStorage#getItem error deserializing JSON for key: ' + key, e.message);
        }
      }
      return resultData;
    });
};

export const removeItem = async (key) => {
  return await AsyncStorage.removeItem(key);
};
